# ChipScope 96
 
This is a very nice 96-channel logic analyzer based on the Xilinx ChipScope-Technique and am Spartan3 XC3S400-TQFP144 FPGA.

![Prototype rev 2.1](https://gitlab.com/MHeinrichs/chipscope/-/blob/master/Bilder/Board.jpg)

It is the most weirdest FPGA-Design: 97 inputs and NO output. You have to tell the compiler not to optimize everything ;).

How does it work? It has a clockinput and six 16bit levelshifters to adapt to old 5V-logic. The FPGA simply samples the input. Internally a ChipScope logic-analyzer samples this data at a frequency of more than 100MHz and stores 1024 samples in the internal blockram. The ChipScope application does the rest and samples the data via JTAG.
JTAG? Do I need a JTAG-adapter for this? NO! I installed a FTDI232R-USB2Serial converter on the board and there is a very nice programm called mlab, adopted from https://github.com/tmbinc/xvcd.
You simply start the small exec and it prompts the parameters to enter in the chipscope- "Open PlugIn" dialoge of the Xilinx Analyzer-app. The best thing: It works over ethernet! So you can start your chipscope and teh mlab-server somewhere, open a port in your LAN / VPN and a remote computer can read the scope! E.G. your much more talented girlfriend, who knows better how to look at that thing! 



**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!


## What is in this repository?
This repository consists of several directories:
* PCB: The board, schematic, several Adapters for debugging Commodore Amigas and bill of material (BOM) are in this directory. 
* Logic: Here the code, project files and pin-assignments for the FPGA is stored.
* Pics: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. 

## How to programm the board?
The FPGA must be programmed via Xilinx Analyzer. The Bit-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/index.php?threads/83268/). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

