<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="3" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="5" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="7" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="tdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="bdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SUPPLY1">
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-1.27" size="1.016" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND" uservalue="yes">
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="micro-mc68000">
<description>&lt;b&gt;Motorola MC68000 Processors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL64">
<description>&lt;b&gt;Dual In Line&lt;/b&gt;</description>
<wire x1="-40.64" y1="-1.27" x2="-40.64" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="1.27" x2="-40.64" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="40.64" y1="-10.414" x2="40.64" y2="10.414" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="-10.414" x2="40.64" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="10.414" x2="-40.64" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-40.64" y1="10.414" x2="40.64" y2="10.414" width="0.1524" layer="21"/>
<pad name="1" x="-39.37" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-36.83" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-34.29" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-31.75" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-29.21" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-26.67" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-24.13" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-21.59" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-19.05" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-16.51" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="-13.97" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="-11.43" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-8.89" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-6.35" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-3.81" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-1.27" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="1.27" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="3.81" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="6.35" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="32" x="39.37" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="33" x="39.37" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="46" x="6.35" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="47" x="3.81" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="48" x="1.27" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="49" x="-1.27" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="50" x="-3.81" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="51" x="-6.35" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="52" x="-8.89" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="53" x="-11.43" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="54" x="-13.97" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="55" x="-16.51" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="56" x="-19.05" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="57" x="-21.59" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="58" x="-24.13" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="59" x="-26.67" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="60" x="-29.21" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="61" x="-31.75" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="62" x="-34.29" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="63" x="-36.83" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="64" x="-39.37" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="8.89" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="11.43" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="13.97" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="16.51" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="19.05" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="25" x="21.59" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="26" x="24.13" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="27" x="26.67" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="28" x="29.21" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="29" x="31.75" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="30" x="34.29" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="31" x="36.83" y="-11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="45" x="8.89" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="44" x="11.43" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="43" x="13.97" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="42" x="16.51" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="41" x="19.05" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="40" x="21.59" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="39" x="24.13" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="38" x="26.67" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="37" x="29.21" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="36" x="31.75" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="35" x="34.29" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<pad name="34" x="36.83" y="11.43" drill="0.8128" shape="long" rot="R90"/>
<text x="-41.275" y="-10.16" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-36.83" y="-2.2352" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="DIL64-ROUND">
<wire x1="-39.37" y1="-1.27" x2="-39.37" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="1.27" x2="-39.37" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="41.91" y1="-10.414" x2="41.91" y2="10.414" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="-10.414" x2="41.91" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="10.414" x2="-39.37" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="10.414" x2="41.91" y2="10.414" width="0.1524" layer="21"/>
<pad name="1" x="-38.1" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="2" x="-35.56" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="3" x="-33.02" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="4" x="-30.48" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="5" x="-27.94" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="6" x="-25.4" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="7" x="-22.86" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="8" x="-20.32" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="9" x="-17.78" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="10" x="-15.24" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="11" x="-12.7" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="12" x="-10.16" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="13" x="-7.62" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="14" x="-5.08" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="15" x="-2.54" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="16" x="0" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="17" x="2.54" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="18" x="5.08" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="19" x="7.62" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="32" x="40.64" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="33" x="40.64" y="11.43" drill="0.8128" rot="R90"/>
<pad name="46" x="7.62" y="11.43" drill="0.8128" rot="R90"/>
<pad name="47" x="5.08" y="11.43" drill="0.8128" rot="R90"/>
<pad name="48" x="2.54" y="11.43" drill="0.8128" rot="R90"/>
<pad name="49" x="0" y="11.43" drill="0.8128" rot="R90"/>
<pad name="50" x="-2.54" y="11.43" drill="0.8128" rot="R90"/>
<pad name="51" x="-5.08" y="11.43" drill="0.8128" rot="R90"/>
<pad name="52" x="-7.62" y="11.43" drill="0.8128" rot="R90"/>
<pad name="53" x="-10.16" y="11.43" drill="0.8128" rot="R90"/>
<pad name="54" x="-12.7" y="11.43" drill="0.8128" rot="R90"/>
<pad name="55" x="-15.24" y="11.43" drill="0.8128" rot="R90"/>
<pad name="56" x="-17.78" y="11.43" drill="0.8128" rot="R90"/>
<pad name="57" x="-20.32" y="11.43" drill="0.8128" rot="R90"/>
<pad name="58" x="-22.86" y="11.43" drill="0.8128" rot="R90"/>
<pad name="59" x="-25.4" y="11.43" drill="0.8128" rot="R90"/>
<pad name="60" x="-27.94" y="11.43" drill="0.8128" rot="R90"/>
<pad name="61" x="-30.48" y="11.43" drill="0.8128" rot="R90"/>
<pad name="62" x="-33.02" y="11.43" drill="0.8128" rot="R90"/>
<pad name="63" x="-35.56" y="11.43" drill="0.8128" rot="R90"/>
<pad name="64" x="-38.1" y="11.43" drill="0.8128" rot="R90"/>
<pad name="20" x="10.16" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="21" x="12.7" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="22" x="15.24" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="23" x="17.78" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="24" x="20.32" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="25" x="22.86" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="26" x="25.4" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="27" x="27.94" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="28" x="30.48" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="29" x="33.02" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="30" x="35.56" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="31" x="38.1" y="-11.43" drill="0.8128" rot="R90"/>
<pad name="45" x="10.16" y="11.43" drill="0.8128" rot="R90"/>
<pad name="44" x="12.7" y="11.43" drill="0.8128" rot="R90"/>
<pad name="43" x="15.24" y="11.43" drill="0.8128" rot="R90"/>
<pad name="42" x="17.78" y="11.43" drill="0.8128" rot="R90"/>
<pad name="41" x="20.32" y="11.43" drill="0.8128" rot="R90"/>
<pad name="40" x="22.86" y="11.43" drill="0.8128" rot="R90"/>
<pad name="39" x="25.4" y="11.43" drill="0.8128" rot="R90"/>
<pad name="38" x="27.94" y="11.43" drill="0.8128" rot="R90"/>
<pad name="37" x="30.48" y="11.43" drill="0.8128" rot="R90"/>
<pad name="36" x="33.02" y="11.43" drill="0.8128" rot="R90"/>
<pad name="35" x="35.56" y="11.43" drill="0.8128" rot="R90"/>
<pad name="34" x="38.1" y="11.43" drill="0.8128" rot="R90"/>
<text x="-40.005" y="-10.16" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="45.085" y="-7.3152" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-37.465" y="-8.89" size="1.27" layer="51" rot="R90">D4</text>
<text x="-37.465" y="5.715" size="1.27" layer="51" rot="R90">D5</text>
<text x="-34.925" y="5.715" size="1.27" layer="51" rot="R90">D6</text>
<text x="-32.385" y="5.715" size="1.27" layer="51" rot="R90">D7</text>
<text x="-29.845" y="5.715" size="1.27" layer="51" rot="R90">D8</text>
<text x="-34.925" y="-8.89" size="1.27" layer="51" rot="R90">D3</text>
<text x="-32.385" y="-8.89" size="1.27" layer="51" rot="R90">D2</text>
<text x="-29.845" y="-8.89" size="1.27" layer="51" rot="R90">D1</text>
<text x="-27.305" y="-8.89" size="1.27" layer="51" rot="R90">D0</text>
<text x="-24.765" y="-8.89" size="1.27" layer="51" rot="R90">!AS</text>
<text x="-22.225" y="-8.89" size="1.27" layer="51" rot="R90">!UDS</text>
<text x="-19.685" y="-8.89" size="1.27" layer="51" rot="R90">!LDS</text>
<text x="-17.145" y="-8.89" size="1.27" layer="51" rot="R90">R/!W</text>
<text x="-14.605" y="-8.89" size="1.27" layer="51" rot="R90">!DTACK</text>
<text x="-12.065" y="-8.89" size="1.27" layer="51" rot="R90">!BG</text>
<text x="-9.525" y="-8.89" size="1.27" layer="51" rot="R90">!BGACK</text>
<text x="-6.985" y="-8.89" size="1.27" layer="51" rot="R90">!BR</text>
<text x="-4.445" y="-8.89" size="1.27" layer="51" rot="R90">Vcc</text>
<text x="-1.905" y="-8.89" size="1.27" layer="51" rot="R90">CLK</text>
<text x="0.635" y="-8.89" size="1.27" layer="51" rot="R90">GND</text>
<text x="3.175" y="-8.89" size="1.27" layer="51" rot="R90">!HALT</text>
<text x="5.715" y="-8.89" size="1.27" layer="51" rot="R90">!RESET</text>
<text x="8.255" y="-8.89" size="1.27" layer="51" rot="R90">!VMA</text>
<text x="10.795" y="-8.89" size="1.27" layer="51" rot="R90">E</text>
<text x="13.335" y="-8.89" size="1.27" layer="51" rot="R90">!VPA</text>
<text x="15.875" y="-8.89" size="1.27" layer="51" rot="R90">!BERR</text>
<text x="18.415" y="-8.89" size="1.27" layer="51" rot="R90">!IPL2</text>
<text x="20.955" y="-8.89" size="1.27" layer="51" rot="R90">!IPL1</text>
<text x="23.495" y="-8.89" size="1.27" layer="51" rot="R90">!IPL0</text>
<text x="26.035" y="-8.89" size="1.27" layer="51" rot="R90">FC2</text>
<text x="28.575" y="-8.89" size="1.27" layer="51" rot="R90">FC1</text>
<text x="31.115" y="-8.89" size="1.27" layer="51" rot="R90">FC0</text>
<text x="33.655" y="-8.89" size="1.27" layer="51" rot="R90">A1</text>
<text x="36.195" y="-8.89" size="1.27" layer="51" rot="R90">A2</text>
<text x="38.735" y="-8.89" size="1.27" layer="51" rot="R90">A3</text>
<text x="41.275" y="-8.89" size="1.27" layer="51" rot="R90">A4</text>
<text x="41.275" y="5.715" size="1.27" layer="51" rot="R90">A5</text>
<text x="38.735" y="5.715" size="1.27" layer="51" rot="R90">A6</text>
<text x="36.195" y="5.715" size="1.27" layer="51" rot="R90">A7</text>
<text x="33.655" y="5.715" size="1.27" layer="51" rot="R90">A8</text>
<text x="31.115" y="5.715" size="1.27" layer="51" rot="R90">A9</text>
<text x="28.575" y="5.715" size="1.27" layer="51" rot="R90">A10</text>
<text x="26.035" y="5.715" size="1.27" layer="51" rot="R90">A11</text>
<text x="23.495" y="5.715" size="1.27" layer="51" rot="R90">A12</text>
<text x="20.955" y="5.715" size="1.27" layer="51" rot="R90">A13</text>
<text x="18.415" y="5.715" size="1.27" layer="51" rot="R90">A14</text>
<text x="15.875" y="5.715" size="1.27" layer="51" rot="R90">A15</text>
<text x="13.335" y="5.715" size="1.27" layer="51" rot="R90">A16</text>
<text x="10.795" y="5.715" size="1.27" layer="51" rot="R90">A17</text>
<text x="8.255" y="5.715" size="1.27" layer="51" rot="R90">A18</text>
<text x="5.715" y="5.715" size="1.27" layer="51" rot="R90">A19</text>
<text x="3.175" y="5.715" size="1.27" layer="51" rot="R90">A20</text>
<text x="0.635" y="5.715" size="1.27" layer="51" rot="R90">Vcc</text>
<text x="-1.905" y="5.715" size="1.27" layer="51" rot="R90">A21</text>
<text x="-4.445" y="5.715" size="1.27" layer="51" rot="R90">A22</text>
<text x="-6.985" y="5.715" size="1.27" layer="51" rot="R90">A23</text>
<text x="-9.525" y="5.715" size="1.27" layer="51" rot="R90">GND</text>
<text x="-12.065" y="5.715" size="1.27" layer="51" rot="R90">D15</text>
<text x="-14.605" y="5.715" size="1.27" layer="51" rot="R90">D14</text>
<text x="-17.145" y="5.715" size="1.27" layer="51" rot="R90">D13</text>
<text x="-19.685" y="5.715" size="1.27" layer="51" rot="R90">D12</text>
<text x="-22.225" y="5.715" size="1.27" layer="51" rot="R90">D11</text>
<text x="-24.765" y="5.715" size="1.27" layer="51" rot="R90">D10</text>
<text x="-27.305" y="5.715" size="1.27" layer="51" rot="R90">D9</text>
</package>
<package name="DIL64-ROUND-BIGHOLE">
<wire x1="-39.37" y1="-1.27" x2="-39.37" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="1.27" x2="-39.37" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="41.91" y1="-10.414" x2="41.91" y2="10.414" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="-10.414" x2="41.91" y2="-10.414" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="10.414" x2="-39.37" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-39.37" y1="10.414" x2="41.91" y2="10.414" width="0.1524" layer="21"/>
<pad name="1" x="-38.1" y="-11.43" drill="1.1" rot="R90"/>
<pad name="2" x="-35.56" y="-11.43" drill="1.1" rot="R90"/>
<pad name="3" x="-33.02" y="-11.43" drill="1.1" rot="R90"/>
<pad name="4" x="-30.48" y="-11.43" drill="1.1" rot="R90"/>
<pad name="5" x="-27.94" y="-11.43" drill="1.1" rot="R90"/>
<pad name="6" x="-25.4" y="-11.43" drill="1.1" rot="R90"/>
<pad name="7" x="-22.86" y="-11.43" drill="1.1" rot="R90"/>
<pad name="8" x="-20.32" y="-11.43" drill="1.1" rot="R90"/>
<pad name="9" x="-17.78" y="-11.43" drill="1.1" rot="R90"/>
<pad name="10" x="-15.24" y="-11.43" drill="1.1" rot="R90"/>
<pad name="11" x="-12.7" y="-11.43" drill="1.1" rot="R90"/>
<pad name="12" x="-10.16" y="-11.43" drill="1.1" rot="R90"/>
<pad name="13" x="-7.62" y="-11.43" drill="1.1" rot="R90"/>
<pad name="14" x="-5.08" y="-11.43" drill="1.1" rot="R90"/>
<pad name="15" x="-2.54" y="-11.43" drill="1.1" rot="R90"/>
<pad name="16" x="0" y="-11.43" drill="1.1" rot="R90"/>
<pad name="17" x="2.54" y="-11.43" drill="1.1" rot="R90"/>
<pad name="18" x="5.08" y="-11.43" drill="1.1" rot="R90"/>
<pad name="19" x="7.62" y="-11.43" drill="1.1" rot="R90"/>
<pad name="32" x="40.64" y="-11.43" drill="1.1" rot="R90"/>
<pad name="33" x="40.64" y="11.43" drill="1.1" rot="R90"/>
<pad name="46" x="7.62" y="11.43" drill="1.1" rot="R90"/>
<pad name="47" x="5.08" y="11.43" drill="1.1" rot="R90"/>
<pad name="48" x="2.54" y="11.43" drill="1.1" rot="R90"/>
<pad name="49" x="0" y="11.43" drill="1.1" rot="R90"/>
<pad name="50" x="-2.54" y="11.43" drill="1.1" rot="R90"/>
<pad name="51" x="-5.08" y="11.43" drill="1.1" rot="R90"/>
<pad name="52" x="-7.62" y="11.43" drill="1.1" rot="R90"/>
<pad name="53" x="-10.16" y="11.43" drill="1.1" rot="R90"/>
<pad name="54" x="-12.7" y="11.43" drill="1.1" rot="R90"/>
<pad name="55" x="-15.24" y="11.43" drill="1.1" rot="R90"/>
<pad name="56" x="-17.78" y="11.43" drill="1.1" rot="R90"/>
<pad name="57" x="-20.32" y="11.43" drill="1.1" rot="R90"/>
<pad name="58" x="-22.86" y="11.43" drill="1.1" rot="R90"/>
<pad name="59" x="-25.4" y="11.43" drill="1.1" rot="R90"/>
<pad name="60" x="-27.94" y="11.43" drill="1.1" rot="R90"/>
<pad name="61" x="-30.48" y="11.43" drill="1.1" rot="R90"/>
<pad name="62" x="-33.02" y="11.43" drill="1.1" rot="R90"/>
<pad name="63" x="-35.56" y="11.43" drill="1.1" rot="R90"/>
<pad name="64" x="-38.1" y="11.43" drill="1.1" rot="R90"/>
<pad name="20" x="10.16" y="-11.43" drill="1.1" rot="R90"/>
<pad name="21" x="12.7" y="-11.43" drill="1.1" rot="R90"/>
<pad name="22" x="15.24" y="-11.43" drill="1.1" rot="R90"/>
<pad name="23" x="17.78" y="-11.43" drill="1.1" rot="R90"/>
<pad name="24" x="20.32" y="-11.43" drill="1.1" rot="R90"/>
<pad name="25" x="22.86" y="-11.43" drill="1.1" rot="R90"/>
<pad name="26" x="25.4" y="-11.43" drill="1.1" rot="R90"/>
<pad name="27" x="27.94" y="-11.43" drill="1.1" rot="R90"/>
<pad name="28" x="30.48" y="-11.43" drill="1.1" rot="R90"/>
<pad name="29" x="33.02" y="-11.43" drill="1.1" rot="R90"/>
<pad name="30" x="35.56" y="-11.43" drill="1.1" rot="R90"/>
<pad name="31" x="38.1" y="-11.43" drill="1.1" rot="R90"/>
<pad name="45" x="10.16" y="11.43" drill="1.1" rot="R90"/>
<pad name="44" x="12.7" y="11.43" drill="1.1" rot="R90"/>
<pad name="43" x="15.24" y="11.43" drill="1.1" rot="R90"/>
<pad name="42" x="17.78" y="11.43" drill="1.1" rot="R90"/>
<pad name="41" x="20.32" y="11.43" drill="1.1" rot="R90"/>
<pad name="40" x="22.86" y="11.43" drill="1.1" rot="R90"/>
<pad name="39" x="25.4" y="11.43" drill="1.1" rot="R90"/>
<pad name="38" x="27.94" y="11.43" drill="1.1" rot="R90"/>
<pad name="37" x="30.48" y="11.43" drill="1.1" rot="R90"/>
<pad name="36" x="33.02" y="11.43" drill="1.1" rot="R90"/>
<pad name="35" x="35.56" y="11.43" drill="1.1" rot="R90"/>
<pad name="34" x="38.1" y="11.43" drill="1.1" rot="R90"/>
<text x="-40.005" y="-10.16" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-35.56" y="-2.2352" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="68000">
<wire x1="-12.7" y1="-50.8" x2="10.16" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="10.16" y1="48.26" x2="10.16" y2="-50.8" width="0.4064" layer="94"/>
<wire x1="10.16" y1="48.26" x2="-12.7" y2="48.26" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-50.8" x2="-12.7" y2="48.26" width="0.4064" layer="94"/>
<text x="-12.7" y="-53.34" size="1.778" layer="96">&gt;VALUE</text>
<text x="-12.7" y="48.895" size="1.778" layer="95">&gt;NAME</text>
<pin name="CLK" x="-17.78" y="45.72" length="middle" direction="in" function="clk"/>
<pin name="VPA" x="-17.78" y="30.48" length="middle" direction="in" function="dot"/>
<pin name="BERR" x="-17.78" y="40.64" length="middle" direction="in" function="dot"/>
<pin name="RESET" x="-17.78" y="38.1" length="middle" function="dot"/>
<pin name="HALT" x="-17.78" y="35.56" length="middle" function="dot"/>
<pin name="DTACK" x="-17.78" y="17.78" length="middle" direction="in" function="dot"/>
<pin name="BR" x="-17.78" y="25.4" length="middle" direction="in" function="dot"/>
<pin name="BGACK" x="-17.78" y="22.86" length="middle" direction="in" function="dot"/>
<pin name="IPL0" x="-17.78" y="12.7" length="middle" direction="in" function="dot"/>
<pin name="D0" x="-17.78" y="-10.16" length="middle"/>
<pin name="D1" x="-17.78" y="-12.7" length="middle"/>
<pin name="D2" x="-17.78" y="-15.24" length="middle"/>
<pin name="D3" x="-17.78" y="-17.78" length="middle"/>
<pin name="D4" x="-17.78" y="-20.32" length="middle"/>
<pin name="D5" x="-17.78" y="-22.86" length="middle"/>
<pin name="D6" x="-17.78" y="-25.4" length="middle"/>
<pin name="D7" x="-17.78" y="-27.94" length="middle"/>
<pin name="D8" x="-17.78" y="-30.48" length="middle"/>
<pin name="D9" x="-17.78" y="-33.02" length="middle"/>
<pin name="D10" x="-17.78" y="-35.56" length="middle"/>
<pin name="D11" x="-17.78" y="-38.1" length="middle"/>
<pin name="D12" x="-17.78" y="-40.64" length="middle"/>
<pin name="D13" x="-17.78" y="-43.18" length="middle"/>
<pin name="D14" x="-17.78" y="-45.72" length="middle"/>
<pin name="D15" x="-17.78" y="-48.26" length="middle"/>
<pin name="E" x="15.24" y="33.02" length="middle" direction="out" rot="R180"/>
<pin name="VMA" x="15.24" y="30.48" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="FC0" x="15.24" y="45.72" length="middle" direction="out" rot="R180"/>
<pin name="FC1" x="15.24" y="43.18" length="middle" direction="out" rot="R180"/>
<pin name="FC2" x="15.24" y="40.64" length="middle" direction="out" rot="R180"/>
<pin name="AS" x="15.24" y="20.32" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="R/W" x="15.24" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="UDS" x="15.24" y="15.24" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="LDS" x="15.24" y="12.7" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="BG" x="15.24" y="25.4" length="middle" direction="out" function="dot" rot="R180"/>
<pin name="A1" x="15.24" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="A2" x="15.24" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="A3" x="15.24" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="A4" x="15.24" y="0" length="middle" direction="out" rot="R180"/>
<pin name="A5" x="15.24" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="A6" x="15.24" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="A7" x="15.24" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="A9" x="15.24" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="A8" x="15.24" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="A10" x="15.24" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="A11" x="15.24" y="-17.78" length="middle" direction="out" rot="R180"/>
<pin name="A12" x="15.24" y="-20.32" length="middle" direction="out" rot="R180"/>
<pin name="A13" x="15.24" y="-22.86" length="middle" direction="out" rot="R180"/>
<pin name="A14" x="15.24" y="-25.4" length="middle" direction="out" rot="R180"/>
<pin name="A15" x="15.24" y="-27.94" length="middle" direction="out" rot="R180"/>
<pin name="A16" x="15.24" y="-30.48" length="middle" direction="out" rot="R180"/>
<pin name="A17" x="15.24" y="-33.02" length="middle" direction="out" rot="R180"/>
<pin name="A18" x="15.24" y="-35.56" length="middle" direction="out" rot="R180"/>
<pin name="A19" x="15.24" y="-38.1" length="middle" direction="out" rot="R180"/>
<pin name="A20" x="15.24" y="-40.64" length="middle" direction="out" rot="R180"/>
<pin name="A21" x="15.24" y="-43.18" length="middle" direction="out" rot="R180"/>
<pin name="A22" x="15.24" y="-45.72" length="middle" direction="out" rot="R180"/>
<pin name="A23" x="15.24" y="-48.26" length="middle" direction="out" rot="R180"/>
<pin name="IPL1" x="-17.78" y="10.16" length="middle" direction="in" function="dot"/>
<pin name="IPL2" x="-17.78" y="7.62" length="middle" direction="in" function="dot"/>
</symbol>
<symbol name="2PWR2GND">
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="4.445" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<text x="4.445" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<pin name="VCC@1" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC@2" x="2.54" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="GND@1" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@2" x="2.54" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MC68000P" prefix="IC" uservalue="yes">
<description>&lt;b&gt;68xxx PROCESSOR&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="68000" x="0" y="0"/>
<gate name="P" symbol="2PWR2GND" x="-40.64" y="12.7" addlevel="request"/>
</gates>
<devices>
<device name="" package="DIL64">
<connects>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="38"/>
<connect gate="G$1" pin="A11" pad="39"/>
<connect gate="G$1" pin="A12" pad="40"/>
<connect gate="G$1" pin="A13" pad="41"/>
<connect gate="G$1" pin="A14" pad="42"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="44"/>
<connect gate="G$1" pin="A17" pad="45"/>
<connect gate="G$1" pin="A18" pad="46"/>
<connect gate="G$1" pin="A19" pad="47"/>
<connect gate="G$1" pin="A2" pad="30"/>
<connect gate="G$1" pin="A20" pad="48"/>
<connect gate="G$1" pin="A21" pad="50"/>
<connect gate="G$1" pin="A22" pad="51"/>
<connect gate="G$1" pin="A23" pad="52"/>
<connect gate="G$1" pin="A3" pad="31"/>
<connect gate="G$1" pin="A4" pad="32"/>
<connect gate="G$1" pin="A5" pad="33"/>
<connect gate="G$1" pin="A6" pad="34"/>
<connect gate="G$1" pin="A7" pad="35"/>
<connect gate="G$1" pin="A8" pad="36"/>
<connect gate="G$1" pin="A9" pad="37"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="22"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="59"/>
<connect gate="G$1" pin="D11" pad="58"/>
<connect gate="G$1" pin="D12" pad="57"/>
<connect gate="G$1" pin="D13" pad="56"/>
<connect gate="G$1" pin="D14" pad="55"/>
<connect gate="G$1" pin="D15" pad="54"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="64"/>
<connect gate="G$1" pin="D6" pad="63"/>
<connect gate="G$1" pin="D7" pad="62"/>
<connect gate="G$1" pin="D8" pad="61"/>
<connect gate="G$1" pin="D9" pad="60"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="20"/>
<connect gate="G$1" pin="FC0" pad="28"/>
<connect gate="G$1" pin="FC1" pad="27"/>
<connect gate="G$1" pin="FC2" pad="26"/>
<connect gate="G$1" pin="HALT" pad="17"/>
<connect gate="G$1" pin="IPL0" pad="25"/>
<connect gate="G$1" pin="IPL1" pad="24"/>
<connect gate="G$1" pin="IPL2" pad="23"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="18"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="19"/>
<connect gate="G$1" pin="VPA" pad="21"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="53"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="49"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="ROUND-PADS" package="DIL64-ROUND">
<connects>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="38"/>
<connect gate="G$1" pin="A11" pad="39"/>
<connect gate="G$1" pin="A12" pad="40"/>
<connect gate="G$1" pin="A13" pad="41"/>
<connect gate="G$1" pin="A14" pad="42"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="44"/>
<connect gate="G$1" pin="A17" pad="45"/>
<connect gate="G$1" pin="A18" pad="46"/>
<connect gate="G$1" pin="A19" pad="47"/>
<connect gate="G$1" pin="A2" pad="30"/>
<connect gate="G$1" pin="A20" pad="48"/>
<connect gate="G$1" pin="A21" pad="50"/>
<connect gate="G$1" pin="A22" pad="51"/>
<connect gate="G$1" pin="A23" pad="52"/>
<connect gate="G$1" pin="A3" pad="31"/>
<connect gate="G$1" pin="A4" pad="32"/>
<connect gate="G$1" pin="A5" pad="33"/>
<connect gate="G$1" pin="A6" pad="34"/>
<connect gate="G$1" pin="A7" pad="35"/>
<connect gate="G$1" pin="A8" pad="36"/>
<connect gate="G$1" pin="A9" pad="37"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="22"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="59"/>
<connect gate="G$1" pin="D11" pad="58"/>
<connect gate="G$1" pin="D12" pad="57"/>
<connect gate="G$1" pin="D13" pad="56"/>
<connect gate="G$1" pin="D14" pad="55"/>
<connect gate="G$1" pin="D15" pad="54"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="64"/>
<connect gate="G$1" pin="D6" pad="63"/>
<connect gate="G$1" pin="D7" pad="62"/>
<connect gate="G$1" pin="D8" pad="61"/>
<connect gate="G$1" pin="D9" pad="60"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="20"/>
<connect gate="G$1" pin="FC0" pad="28"/>
<connect gate="G$1" pin="FC1" pad="27"/>
<connect gate="G$1" pin="FC2" pad="26"/>
<connect gate="G$1" pin="HALT" pad="17"/>
<connect gate="G$1" pin="IPL0" pad="25"/>
<connect gate="G$1" pin="IPL1" pad="24"/>
<connect gate="G$1" pin="IPL2" pad="23"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="18"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="19"/>
<connect gate="G$1" pin="VPA" pad="21"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="53"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="49"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ROUND-PAD-BIG-HOLE" package="DIL64-ROUND-BIGHOLE">
<connects>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="38"/>
<connect gate="G$1" pin="A11" pad="39"/>
<connect gate="G$1" pin="A12" pad="40"/>
<connect gate="G$1" pin="A13" pad="41"/>
<connect gate="G$1" pin="A14" pad="42"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="44"/>
<connect gate="G$1" pin="A17" pad="45"/>
<connect gate="G$1" pin="A18" pad="46"/>
<connect gate="G$1" pin="A19" pad="47"/>
<connect gate="G$1" pin="A2" pad="30"/>
<connect gate="G$1" pin="A20" pad="48"/>
<connect gate="G$1" pin="A21" pad="50"/>
<connect gate="G$1" pin="A22" pad="51"/>
<connect gate="G$1" pin="A23" pad="52"/>
<connect gate="G$1" pin="A3" pad="31"/>
<connect gate="G$1" pin="A4" pad="32"/>
<connect gate="G$1" pin="A5" pad="33"/>
<connect gate="G$1" pin="A6" pad="34"/>
<connect gate="G$1" pin="A7" pad="35"/>
<connect gate="G$1" pin="A8" pad="36"/>
<connect gate="G$1" pin="A9" pad="37"/>
<connect gate="G$1" pin="AS" pad="6"/>
<connect gate="G$1" pin="BERR" pad="22"/>
<connect gate="G$1" pin="BG" pad="11"/>
<connect gate="G$1" pin="BGACK" pad="12"/>
<connect gate="G$1" pin="BR" pad="13"/>
<connect gate="G$1" pin="CLK" pad="15"/>
<connect gate="G$1" pin="D0" pad="5"/>
<connect gate="G$1" pin="D1" pad="4"/>
<connect gate="G$1" pin="D10" pad="59"/>
<connect gate="G$1" pin="D11" pad="58"/>
<connect gate="G$1" pin="D12" pad="57"/>
<connect gate="G$1" pin="D13" pad="56"/>
<connect gate="G$1" pin="D14" pad="55"/>
<connect gate="G$1" pin="D15" pad="54"/>
<connect gate="G$1" pin="D2" pad="3"/>
<connect gate="G$1" pin="D3" pad="2"/>
<connect gate="G$1" pin="D4" pad="1"/>
<connect gate="G$1" pin="D5" pad="64"/>
<connect gate="G$1" pin="D6" pad="63"/>
<connect gate="G$1" pin="D7" pad="62"/>
<connect gate="G$1" pin="D8" pad="61"/>
<connect gate="G$1" pin="D9" pad="60"/>
<connect gate="G$1" pin="DTACK" pad="10"/>
<connect gate="G$1" pin="E" pad="20"/>
<connect gate="G$1" pin="FC0" pad="28"/>
<connect gate="G$1" pin="FC1" pad="27"/>
<connect gate="G$1" pin="FC2" pad="26"/>
<connect gate="G$1" pin="HALT" pad="17"/>
<connect gate="G$1" pin="IPL0" pad="25"/>
<connect gate="G$1" pin="IPL1" pad="24"/>
<connect gate="G$1" pin="IPL2" pad="23"/>
<connect gate="G$1" pin="LDS" pad="8"/>
<connect gate="G$1" pin="R/W" pad="9"/>
<connect gate="G$1" pin="RESET" pad="18"/>
<connect gate="G$1" pin="UDS" pad="7"/>
<connect gate="G$1" pin="VMA" pad="19"/>
<connect gate="G$1" pin="VPA" pad="21"/>
<connect gate="P" pin="GND@1" pad="16"/>
<connect gate="P" pin="GND@2" pad="53"/>
<connect gate="P" pin="VCC@1" pad="14"/>
<connect gate="P" pin="VCC@2" pad="49"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-ml">
<description>&lt;b&gt;Harting  Connectors&lt;/b&gt;&lt;p&gt;
Low profile connectors, straight&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="ML20">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-15.24" y1="3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="-15.24" y1="3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-16.51" y2="-4.445" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="10.922" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.858" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.699" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="13.97" y1="4.445" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.445" x2="13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="-15.24" y1="4.699" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="9.398" y2="-3.429" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="10.922" y2="-3.429" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="9.398" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="-3.429" x2="15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="3.429" x2="-15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="3.429" x2="-15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="-3.429" x2="-8.382" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.937" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.937" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-4.445" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-9.271" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.429" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-8.382" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-6.858" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-8.382" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.937" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.937" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-9.271" y1="-4.445" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-5.969" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.969" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-16.51" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-13.97" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-13.97" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">20</text>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="20P">
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="13" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="17" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="19" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="10" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="14" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="16" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="18" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas"/>
<pin name="20" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ML20" prefix="SV" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="20P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML20">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="17" pad="17"/>
<connect gate="1" pin="18" pad="18"/>
<connect gate="1" pin="19" pad="19"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="20" pad="20"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead-2">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de, Modified by Cougar@CasaDelGato.Com&lt;/author&gt;</description>
<packages>
<package name="2X06">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54</description>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="-1.27" drill="1.016" shape="square"/>
<pad name="2" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<text x="-7.62" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
</package>
<package name="2X06/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 90°</description>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-6.35" y="-6.35" drill="1.016" shape="square"/>
<pad name="3" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-8.255" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="9.525" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
</package>
<package name="2X06M">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2</description>
<wire x1="6.5" y1="2.25" x2="6.5" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="6.5" y1="-2.25" x2="-6.25" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="-2.25" x2="-6.25" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-6.25" y1="2.25" x2="6.5" y2="2.25" width="0.2032" layer="21"/>
<pad name="1" x="-5" y="-1" drill="0.9144" shape="square"/>
<pad name="2" x="-5" y="1" drill="0.9144"/>
<pad name="3" x="-3" y="-1" drill="0.9144"/>
<pad name="4" x="-3" y="1" drill="0.9144"/>
<pad name="5" x="-1" y="-1" drill="0.9144"/>
<pad name="6" x="-1" y="1" drill="0.9144"/>
<pad name="7" x="1" y="-1" drill="0.9144"/>
<pad name="8" x="1" y="1" drill="0.9144"/>
<pad name="9" x="3" y="-1" drill="0.9144"/>
<pad name="10" x="3" y="1" drill="0.9144"/>
<pad name="11" x="5" y="-1" drill="0.9144"/>
<pad name="12" x="5" y="1" drill="0.9144"/>
<text x="-7" y="-2" size="1.016" layer="25" ratio="14" rot="R90">&gt;NAME</text>
<text x="8" y="-2" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.25" y1="0.75" x2="-4.75" y2="1.25" layer="51"/>
<rectangle x1="-5.25" y1="-1.25" x2="-4.75" y2="-0.75" layer="51"/>
<rectangle x1="-3.25" y1="0.75" x2="-2.75" y2="1.25" layer="51"/>
<rectangle x1="-3.25" y1="-1.25" x2="-2.75" y2="-0.75" layer="51"/>
<rectangle x1="-1.25" y1="0.75" x2="-0.75" y2="1.25" layer="51"/>
<rectangle x1="-1.25" y1="-1.25" x2="-0.75" y2="-0.75" layer="51"/>
<rectangle x1="0.75" y1="0.75" x2="1.25" y2="1.25" layer="51"/>
<rectangle x1="0.75" y1="-1.25" x2="1.25" y2="-0.75" layer="51"/>
<rectangle x1="2.75" y1="0.75" x2="3.25" y2="1.25" layer="51"/>
<rectangle x1="2.75" y1="-1.25" x2="3.25" y2="-0.75" layer="51"/>
<rectangle x1="4.75" y1="0.75" x2="5.25" y2="1.25" layer="51"/>
<rectangle x1="4.75" y1="-1.25" x2="5.25" y2="-0.75" layer="51"/>
</package>
<package name="2X06SMD">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 SMD</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="0" y1="-3.175" x2="-0.635" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-3.81" x2="-1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-3.81" x2="1.905" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-3.81" x2="0" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="4.445" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-3.81" x2="2.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-3.81" x2="4.445" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-3.81" x2="6.985" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-3.175" x2="6.985" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-3.81" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.1524" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-3.81" x2="7.62" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-3.175" x2="9.525" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-3.81" x2="9.525" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="12.7" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="12.065" y1="-1.27" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-0.635" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="12.065" y1="1.27" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.795" y2="1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="10.16" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="12.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-3.81" x2="12.065" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-3.175" x2="12.065" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-3.81" x2="10.16" y2="-3.175" width="0.1524" layer="21"/>
<smd name="1" x="-1.27" y="-3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="2" x="-1.27" y="1.27" dx="3.302" dy="1.016" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="-3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="4" x="1.27" y="1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="5" x="3.81" y="-3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="6" x="3.81" y="1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="7" x="6.35" y="-3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="8" x="6.35" y="1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="9" x="8.89" y="-3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="10" x="8.89" y="1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="11" x="11.43" y="-3.81" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<smd name="12" x="11.43" y="1.27" dx="3.302" dy="1.016" layer="1" rot="R270"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-5.715" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-2.794" x2="4.064" y2="-2.286" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-2.794" x2="6.604" y2="-2.286" layer="51"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<rectangle x1="8.636" y1="-2.794" x2="9.144" y2="-2.286" layer="51"/>
<rectangle x1="11.176" y1="-0.254" x2="11.684" y2="0.254" layer="51"/>
<rectangle x1="11.176" y1="-2.794" x2="11.684" y2="-2.286" layer="51"/>
</package>
<package name="MICROMATCH-12">
<description>&lt;b&gt;MicroMaTch 12 &lt;/b&gt; 8-215464-2 / 1-215464-2&lt;p&gt;
Drawing Number C-215464&lt;br&gt;
DDEController.pdf</description>
<wire x1="-8.64" y1="0.7" x2="-9.04" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-9.04" y1="0.7" x2="-9.04" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-9.04" y1="-0.7" x2="-8.64" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="-8.64" y1="-1.2" x2="-9.04" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-9.04" y1="-1.2" x2="-9.04" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-9.04" y1="-1.6" x2="-8.64" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-8.64" y1="1.6" x2="-9.04" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-9.04" y1="1.6" x2="-9.04" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-9.04" y1="1.2" x2="-8.64" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-8.54" y1="2.4" x2="-8.54" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="-7.74" y1="2" x2="-7.74" y2="-2" width="0.2032" layer="21"/>
<wire x1="8.44" y1="1.6" x2="8.14" y2="1.6" width="0.1016" layer="21"/>
<wire x1="8.14" y1="1.6" x2="7.84" y2="1.6" width="0.1016" layer="21"/>
<wire x1="8.44" y1="0" x2="8.14" y2="0" width="0.1016" layer="21"/>
<wire x1="8.14" y1="0" x2="7.84" y2="0" width="0.1016" layer="21"/>
<wire x1="8.14" y1="0" x2="7.84" y2="0.3" width="0.1016" layer="21"/>
<wire x1="8.14" y1="1.6" x2="7.84" y2="1.3" width="0.1016" layer="21"/>
<wire x1="8.04" y1="1.4" x2="8.14" y2="1.3" width="0.1016" layer="21"/>
<wire x1="8.44" y1="1.3" x2="8.14" y2="1.3" width="0.1016" layer="21"/>
<wire x1="8.14" y1="1.3" x2="8.14" y2="0.3" width="0.1016" layer="21"/>
<wire x1="8.14" y1="0.3" x2="8.44" y2="0.3" width="0.1016" layer="21"/>
<wire x1="8.04" y1="0.2" x2="8.14" y2="0.3" width="0.1016" layer="21"/>
<wire x1="8.64" y1="-0.7" x2="9.04" y2="-0.7" width="0.2032" layer="21"/>
<wire x1="9.04" y1="-0.7" x2="9.04" y2="0.7" width="0.2032" layer="21"/>
<wire x1="9.04" y1="0.7" x2="8.64" y2="0.7" width="0.2032" layer="21"/>
<wire x1="8.64" y1="1.2" x2="9.04" y2="1.2" width="0.2032" layer="21"/>
<wire x1="9.04" y1="1.2" x2="9.04" y2="1.6" width="0.2032" layer="21"/>
<wire x1="9.04" y1="1.6" x2="8.64" y2="1.6" width="0.2032" layer="21"/>
<wire x1="8.64" y1="-1.6" x2="9.04" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="9.04" y1="-1.6" x2="9.04" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="9.04" y1="-1.2" x2="8.64" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="8.54" y1="-2.4" x2="8.54" y2="2.4" width="0.2032" layer="21"/>
<wire x1="7.74" y1="2" x2="-7.74" y2="2" width="0.2032" layer="21"/>
<wire x1="8.54" y1="2.4" x2="-8.54" y2="2.4" width="0.2032" layer="21"/>
<wire x1="8.54" y1="-2.4" x2="-8.54" y2="-2.4" width="0.2032" layer="21"/>
<wire x1="7.74" y1="-2" x2="7.74" y2="2" width="0.2032" layer="21"/>
<wire x1="7.74" y1="-2" x2="-7.74" y2="-2" width="0.2032" layer="21"/>
<pad name="1" x="6.985" y="1.27" drill="0.8128"/>
<pad name="2" x="5.715" y="-1.27" drill="0.8128"/>
<pad name="3" x="4.445" y="1.27" drill="0.8128"/>
<pad name="4" x="3.175" y="-1.27" drill="0.8128"/>
<pad name="5" x="1.905" y="1.27" drill="0.8128"/>
<pad name="6" x="0.635" y="-1.27" drill="0.8128"/>
<pad name="7" x="-0.635" y="1.27" drill="0.8128"/>
<pad name="8" x="-1.905" y="-1.27" drill="0.8128"/>
<pad name="9" x="-3.175" y="1.27" drill="0.8128"/>
<pad name="10" x="-4.445" y="-1.27" drill="0.8128"/>
<pad name="11" x="-5.715" y="1.27" drill="0.8128"/>
<pad name="12" x="-6.985" y="-1.27" drill="0.8128"/>
<text x="-6.985" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.985" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="2X06SMD/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt; - 2.54 90°</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="8.5725" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-8.89" width="0.4064" layer="21"/>
<wire x1="1.27" y1="8.5725" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-8.89" width="0.4064" layer="21"/>
<wire x1="3.81" y1="8.5725" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="-8.89" width="0.4064" layer="21"/>
<wire x1="6.35" y1="8.5725" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="6.35" y2="-8.89" width="0.4064" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="8.89" y2="-8.89" width="0.4064" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="8.5725" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="11.43" y1="8.5725" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="11.43" y1="-2.54" x2="11.43" y2="-8.89" width="0.4064" layer="21"/>
<smd name="2" x="-1.27" y="-5.08" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="1" x="-1.27" y="-8.89" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="3" x="1.27" y="-8.89" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="4" x="1.27" y="-5.08" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="5" x="3.81" y="-8.89" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="6" x="3.81" y="-5.08" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="7" x="6.35" y="-8.89" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="8" x="6.35" y="-5.08" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="9" x="8.89" y="-8.89" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="10" x="8.89" y="-5.08" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="11" x="11.43" y="-8.89" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<smd name="12" x="11.43" y="-5.08" dx="1.778" dy="1.016" layer="1" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="14.605" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X06">
<wire x1="-6.35" y1="-10.16" x2="8.89" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X06" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X06" x="0" y="0"/>
</gates>
<devices>
<device name="_2.54" package="2X06">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-90°" package="2X06/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.00" package="2X06M">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-SMD" package="2X06SMD">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_MICROMATCH" package="MICROMATCH-12">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_2.54-SMD-90°" package="2X06SMD/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND55" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND56" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="P+12" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="P+14" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="GND62" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND63" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND64" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND65" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND66" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND67" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND68" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND69" library="SUPPLY" deviceset="GND" device="" value="GND"/>
<part name="IC1" library="micro-mc68000" deviceset="MC68000P" device="ROUND-PADS"/>
<part name="GND1" library="SUPPLY" deviceset="GND" device="" value="GND"/>
<part name="GND2" library="SUPPLY" deviceset="GND" device="" value="GND"/>
<part name="P+1" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="P+2" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="X1" library="con-ml" deviceset="ML20" device=""/>
<part name="GND20" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND21" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND22" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND23" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X2" library="con-ml" deviceset="ML20" device=""/>
<part name="GND3" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND4" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND5" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND6" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X3" library="con-ml" deviceset="ML20" device=""/>
<part name="GND7" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND8" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND9" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND10" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X4" library="con-ml" deviceset="ML20" device=""/>
<part name="GND11" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND12" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND13" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND14" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X5" library="con-ml" deviceset="ML20" device=""/>
<part name="GND15" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND16" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND17" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND18" library="SUPPLY1" deviceset="GND" device=""/>
<part name="JP1" library="pinhead-2" deviceset="PINHD-2X06" device="_2.54"/>
<part name="IC2" library="micro-mc68000" deviceset="MC68000P" device="ROUND-PAD-BIG-HOLE"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="40.64" y="117.1575" size="1.27" layer="95">/CCKQ</text>
<text x="40.64" y="114.6175" size="1.27" layer="95">/CCK</text>
<text x="40.64" y="117.1575" size="1.27" layer="95">/CCKQ</text>
<text x="40.64" y="106.9975" size="1.27" layer="95">/INT6</text>
<text x="40.64" y="73.9775" size="1.27" layer="95">/VPA</text>
<text x="40.64" y="71.4375" size="1.27" layer="95">E</text>
<text x="40.64" y="53.6575" size="1.27" layer="95">/BG</text>
<text x="40.64" y="56.1975" size="1.27" layer="95">/BGACK</text>
<text x="40.64" y="51.1175" size="1.27" layer="95">/DTACK</text>
<text x="40.64" y="58.7375" size="1.27" layer="95">/BR</text>
</plain>
<instances>
<instance part="GND55" gate="1" x="15.24" y="132.08" rot="R270"/>
<instance part="GND56" gate="1" x="40.64" y="132.08" rot="R90"/>
<instance part="P+12" gate="VCC" x="40.64" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="37.7825" y="127.3175" size="1.016" layer="96"/>
</instance>
<instance part="P+14" gate="VCC" x="12.7" y="127" smashed="yes" rot="MR270">
<attribute name="VALUE" x="15.5575" y="127.635" size="1.016" layer="96" rot="MR0"/>
</instance>
<instance part="GND62" gate="1" x="12.7" y="25.4" rot="R270"/>
<instance part="GND63" gate="1" x="15.24" y="40.64" rot="R270"/>
<instance part="GND64" gate="1" x="15.24" y="55.88" rot="R270"/>
<instance part="GND65" gate="1" x="15.24" y="71.12" rot="R270"/>
<instance part="GND66" gate="1" x="15.24" y="116.84" rot="R270"/>
<instance part="GND67" gate="1" x="15.24" y="101.6" rot="R270"/>
<instance part="GND68" gate="1" x="15.24" y="86.36" rot="R270"/>
<instance part="GND69" gate="1" x="43.18" y="22.86" rot="R90"/>
<instance part="IC1" gate="G$1" x="133.35" y="77.47"/>
<instance part="IC1" gate="P" x="167.64" y="16.51"/>
<instance part="GND1" gate="1" x="167.64" y="3.81"/>
<instance part="GND2" gate="1" x="170.18" y="3.81"/>
<instance part="P+1" gate="VCC" x="167.64" y="29.21" smashed="yes" rot="MR0">
<attribute name="VALUE" x="167.9575" y="26.3525" size="1.016" layer="96" rot="MR90"/>
</instance>
<instance part="P+2" gate="VCC" x="170.18" y="29.21" smashed="yes" rot="MR0">
<attribute name="VALUE" x="170.4975" y="26.3525" size="1.016" layer="96" rot="MR90"/>
</instance>
<instance part="X1" gate="1" x="184.15" y="106.68"/>
<instance part="GND20" gate="1" x="171.45" y="116.84" rot="R270"/>
<instance part="GND21" gate="1" x="171.45" y="93.98" rot="R270"/>
<instance part="GND22" gate="1" x="196.85" y="93.98" rot="R90"/>
<instance part="GND23" gate="1" x="196.85" y="116.84" rot="R90"/>
<instance part="X2" gate="1" x="222.25" y="106.68"/>
<instance part="GND3" gate="1" x="209.55" y="116.84" rot="R270"/>
<instance part="GND4" gate="1" x="209.55" y="93.98" rot="R270"/>
<instance part="GND5" gate="1" x="234.95" y="93.98" rot="R90"/>
<instance part="GND6" gate="1" x="234.95" y="116.84" rot="R90"/>
<instance part="X3" gate="1" x="184.15" y="73.66"/>
<instance part="GND7" gate="1" x="171.45" y="83.82" rot="R270"/>
<instance part="GND8" gate="1" x="171.45" y="60.96" rot="R270"/>
<instance part="GND9" gate="1" x="196.85" y="60.96" rot="R90"/>
<instance part="GND10" gate="1" x="196.85" y="83.82" rot="R90"/>
<instance part="X4" gate="1" x="222.25" y="73.66"/>
<instance part="GND11" gate="1" x="209.55" y="83.82" rot="R270"/>
<instance part="GND12" gate="1" x="209.55" y="60.96" rot="R270"/>
<instance part="GND13" gate="1" x="234.95" y="60.96" rot="R90"/>
<instance part="GND14" gate="1" x="234.95" y="83.82" rot="R90"/>
<instance part="X5" gate="1" x="222.25" y="31.75"/>
<instance part="GND15" gate="1" x="209.55" y="41.91" rot="R270"/>
<instance part="GND16" gate="1" x="209.55" y="19.05" rot="R270"/>
<instance part="GND17" gate="1" x="234.95" y="19.05" rot="R90"/>
<instance part="GND18" gate="1" x="234.95" y="41.91" rot="R90"/>
<instance part="JP1" gate="A" x="279.4" y="33.02"/>
<instance part="IC2" gate="G$1" x="293.37" y="97.79"/>
</instances>
<busses>
<bus name="/OWN,/SLAVE,CFGOUT,/CCKQ,/CCK,CDAC,/OVR,XRDY,/INT2,/INT6,A[1..23],/IPL[0..2],FC[0..2],/BERR,/VPA,E,/VMA,/RESET,/HALT,/BR,/BGACK,/BG,DA[0..15],/DTACK,R/W,/LDS,/UDS,7MHZ,IORST,DOE,/EINT1,/GBG,/FCS,/DS1,Z3,GND,VCC,CFGIN,/AS,28M,BOSS,PROBE[0..3]">
<segment>
<wire x1="53.34" y1="132.08" x2="55.88" y2="132.08" width="0.762" layer="92"/>
<wire x1="55.88" y1="132.08" x2="55.88" y2="2.54" width="0.762" layer="92"/>
<wire x1="55.88" y1="2.54" x2="106.68" y2="2.54" width="0.762" layer="92"/>
<wire x1="106.68" y1="2.54" x2="106.68" y2="134.62" width="0.762" layer="92"/>
<wire x1="106.68" y1="2.54" x2="157.48" y2="2.54" width="0.762" layer="92"/>
<wire x1="157.48" y1="2.54" x2="157.48" y2="134.62" width="0.762" layer="92"/>
<wire x1="55.88" y1="2.54" x2="2.54" y2="2.54" width="0.762" layer="92"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="137.16" width="0.762" layer="92"/>
<wire x1="157.48" y1="2.54" x2="205.74" y2="2.54" width="0.762" layer="92"/>
<wire x1="205.74" y1="2.54" x2="247.65" y2="2.54" width="0.762" layer="92"/>
<wire x1="247.65" y1="2.54" x2="250.19" y2="5.08" width="0.762" layer="92"/>
<wire x1="250.19" y1="5.08" x2="250.19" y2="121.92" width="0.762" layer="92"/>
<wire x1="205.74" y1="2.54" x2="205.74" y2="123.19" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="R/W" class="0">
<segment>
<label x="40.64" y="48.5775" size="1.27" layer="95"/>
<wire x1="35.56" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="53.34" y1="48.26" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="119.38" x2="58.42" y2="119.38" width="0.1524" layer="91"/>
<wire x1="58.42" y1="119.38" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<label x="58.42" y="119.38" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="R/W"/>
<wire x1="148.59" y1="95.25" x2="154.94" y2="95.25" width="0.1524" layer="91"/>
<wire x1="154.94" y1="95.25" x2="157.48" y2="92.71" width="0.1524" layer="91"/>
<label x="149.86" y="95.25" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="9"/>
<wire x1="229.87" y1="104.14" x2="247.65" y2="104.14" width="0.1524" layer="91"/>
<wire x1="247.65" y1="104.14" x2="250.19" y2="101.6" width="0.1524" layer="91"/>
<label x="234.95" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="R/W"/>
<wire x1="308.61" y1="115.57" x2="314.96" y2="115.57" width="0.1524" layer="91"/>
<label x="309.88" y="115.57" size="1.27" layer="95"/>
</segment>
</net>
<net name="/UDS" class="0">
<segment>
<label x="40.64" y="43.4975" size="1.27" layer="95"/>
<wire x1="35.56" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="43.18" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="116.84" x2="58.42" y2="116.84" width="0.1524" layer="91"/>
<wire x1="58.42" y1="116.84" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<label x="58.42" y="116.84" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="UDS"/>
<wire x1="148.59" y1="92.71" x2="154.94" y2="92.71" width="0.1524" layer="91"/>
<wire x1="154.94" y1="92.71" x2="157.48" y2="90.17" width="0.1524" layer="91"/>
<label x="149.86" y="92.71" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="5"/>
<wire x1="229.87" y1="99.06" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="246.38" y1="99.06" x2="250.19" y2="95.25" width="0.1524" layer="91"/>
<label x="234.95" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="UDS"/>
<wire x1="308.61" y1="113.03" x2="314.96" y2="113.03" width="0.1524" layer="91"/>
<label x="309.88" y="113.03" size="1.27" layer="95"/>
</segment>
</net>
<net name="/LDS" class="0">
<segment>
<label x="40.64" y="46.0375" size="1.27" layer="95"/>
<wire x1="35.56" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="45.72" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="114.3" x2="58.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="58.42" y1="114.3" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<label x="58.42" y="114.3" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="LDS"/>
<wire x1="148.59" y1="90.17" x2="154.94" y2="90.17" width="0.1524" layer="91"/>
<wire x1="154.94" y1="90.17" x2="157.48" y2="87.63" width="0.1524" layer="91"/>
<label x="149.86" y="90.17" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="7"/>
<wire x1="229.87" y1="101.6" x2="247.65" y2="101.6" width="0.1524" layer="91"/>
<wire x1="247.65" y1="101.6" x2="250.19" y2="99.06" width="0.1524" layer="91"/>
<label x="234.95" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="LDS"/>
<wire x1="308.61" y1="110.49" x2="314.96" y2="110.49" width="0.1524" layer="91"/>
<label x="309.88" y="110.49" size="1.27" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="17.78" y1="132.08" x2="19.05" y2="132.08" width="0.1524" layer="91"/>
<wire x1="19.05" y1="132.08" x2="20.32" y2="132.08" width="0.1524" layer="91"/>
<wire x1="20.32" y1="129.54" x2="19.05" y2="129.54" width="0.1524" layer="91"/>
<wire x1="19.05" y1="129.54" x2="19.05" y2="132.08" width="0.1524" layer="91"/>
<junction x="19.05" y="132.08"/>
<pinref part="GND55" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="38.1" y1="132.08" x2="36.83" y2="132.08" width="0.1524" layer="91"/>
<wire x1="36.83" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<wire x1="36.83" y1="132.08" x2="36.83" y2="129.54" width="0.1524" layer="91"/>
<wire x1="36.83" y1="129.54" x2="35.56" y2="129.54" width="0.1524" layer="91"/>
<junction x="36.83" y="132.08"/>
<pinref part="GND56" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="40.64" x2="20.32" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND63" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="55.88" x2="20.32" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND64" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="71.12" x2="20.32" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND65" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="116.84" x2="20.32" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND66" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="101.6" x2="20.32" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND67" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="17.78" y1="86.36" x2="20.32" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND68" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="20.32" y1="25.4" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<wire x1="17.78" y1="25.4" x2="15.24" y2="25.4" width="0.1524" layer="91"/>
<wire x1="17.78" y1="20.32" x2="17.78" y2="22.86" width="0.1524" layer="91"/>
<wire x1="17.78" y1="22.86" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
<wire x1="20.32" y1="22.86" x2="17.78" y2="22.86" width="0.1524" layer="91"/>
<wire x1="20.32" y1="20.32" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<wire x1="20.32" y1="7.62" x2="17.78" y2="7.62" width="0.1524" layer="91"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="20.32" width="0.1524" layer="91"/>
<junction x="17.78" y="20.32"/>
<junction x="17.78" y="22.86"/>
<junction x="17.78" y="25.4"/>
<pinref part="GND62" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="40.64" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<wire x1="38.1" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="38.1" y1="20.32" x2="38.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="38.1" y1="7.62" x2="35.56" y2="7.62" width="0.1524" layer="91"/>
<junction x="38.1" y="20.32"/>
<junction x="38.1" y="22.86"/>
<pinref part="GND69" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="95.25" y1="38.1" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="38.1" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<label x="99.06" y="38.1" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="35.56" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<label x="99.06" y="35.56" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="33.02" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="33.02" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<label x="99.06" y="33.02" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="30.48" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<wire x1="104.14" y1="30.48" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<label x="99.06" y="30.48" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="27.94" x2="104.14" y2="27.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="27.94" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
<label x="99.06" y="27.94" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="25.4" x2="104.14" y2="25.4" width="0.1524" layer="91"/>
<wire x1="104.14" y1="25.4" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="22.86" x2="104.14" y2="22.86" width="0.1524" layer="91"/>
<wire x1="104.14" y1="22.86" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<label x="99.06" y="22.86" size="1.27" layer="95" rot="R180"/>
<label x="99.06" y="25.4" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<wire x1="104.14" y1="20.32" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<label x="99.06" y="20.32" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="17.78" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="17.78" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
<label x="99.06" y="17.78" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="15.24" x2="104.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="104.14" y1="15.24" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<label x="99.06" y="15.24" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="12.7" x2="106.68" y2="10.16" width="0.1524" layer="91"/>
<label x="99.06" y="12.7" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="10.16" x2="104.14" y2="10.16" width="0.1524" layer="91"/>
<wire x1="104.14" y1="10.16" x2="106.68" y2="7.62" width="0.1524" layer="91"/>
<label x="99.06" y="10.16" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="P" pin="GND@1"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="167.64" y1="6.35" x2="167.64" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="P" pin="GND@2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="170.18" y1="6.35" x2="170.18" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="20"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="173.99" y1="116.84" x2="176.53" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="173.99" y1="93.98" x2="176.53" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="194.31" y1="93.98" x2="191.77" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="19"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="194.31" y1="116.84" x2="191.77" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="20"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="212.09" y1="116.84" x2="214.63" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="212.09" y1="93.98" x2="214.63" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="232.41" y1="93.98" x2="229.87" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="19"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="232.41" y1="116.84" x2="229.87" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="20"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="173.99" y1="83.82" x2="176.53" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="173.99" y1="60.96" x2="176.53" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="194.31" y1="60.96" x2="191.77" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="19"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="194.31" y1="83.82" x2="191.77" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="20"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="212.09" y1="83.82" x2="214.63" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="212.09" y1="60.96" x2="214.63" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="1"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="232.41" y1="60.96" x2="229.87" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="19"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="232.41" y1="83.82" x2="229.87" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="20"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="212.09" y1="41.91" x2="214.63" y2="41.91" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="212.09" y1="19.05" x2="214.63" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="232.41" y1="19.05" x2="229.87" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="19"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="232.41" y1="41.91" x2="229.87" y2="41.91" width="0.1524" layer="91"/>
</segment>
</net>
<net name="A17" class="0">
<segment>
<label x="16.51" y="73.66" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="73.66" x2="3.81" y2="73.66" width="0.1524" layer="91"/>
<wire x1="3.81" y1="73.66" x2="2.54" y2="72.39" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="25.4" x2="58.42" y2="25.4" width="0.1524" layer="91"/>
<wire x1="58.42" y1="25.4" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<label x="58.42" y="25.4" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="44.45" x2="154.94" y2="44.45" width="0.1524" layer="91"/>
<wire x1="154.94" y1="44.45" x2="157.48" y2="41.91" width="0.1524" layer="91"/>
<label x="154.94" y="44.45" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A17"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="12"/>
<wire x1="176.53" y1="73.66" x2="160.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="73.66" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="165.1" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="64.77" x2="314.96" y2="64.77" width="0.1524" layer="91"/>
<wire x1="314.96" y1="64.77" x2="317.5" y2="62.23" width="0.1524" layer="91"/>
<label x="314.96" y="64.77" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A17"/>
</segment>
</net>
<net name="A18" class="0">
<segment>
<label x="40.64" y="68.8975" size="1.27" layer="95"/>
<wire x1="35.56" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="68.58" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="22.86" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="22.86" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<label x="58.42" y="22.86" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="41.91" x2="154.94" y2="41.91" width="0.1524" layer="91"/>
<wire x1="154.94" y1="41.91" x2="157.48" y2="39.37" width="0.1524" layer="91"/>
<label x="154.94" y="41.91" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A18"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="10"/>
<wire x1="176.53" y1="71.12" x2="160.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="160.02" y1="71.12" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<label x="165.1" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="62.23" x2="314.96" y2="62.23" width="0.1524" layer="91"/>
<wire x1="314.96" y1="62.23" x2="317.5" y2="59.69" width="0.1524" layer="91"/>
<label x="314.96" y="62.23" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A18"/>
</segment>
</net>
<net name="A20" class="0">
<segment>
<label x="40.64" y="63.8175" size="1.27" layer="95"/>
<wire x1="35.56" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<wire x1="53.34" y1="63.5" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="17.78" x2="58.42" y2="17.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="17.78" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
<label x="58.42" y="17.78" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="36.83" x2="154.94" y2="36.83" width="0.1524" layer="91"/>
<wire x1="154.94" y1="36.83" x2="157.48" y2="34.29" width="0.1524" layer="91"/>
<label x="154.94" y="36.83" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A20"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="6"/>
<wire x1="176.53" y1="66.04" x2="161.29" y2="66.04" width="0.1524" layer="91"/>
<wire x1="161.29" y1="66.04" x2="157.48" y2="62.23" width="0.1524" layer="91"/>
<label x="165.1" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="57.15" x2="314.96" y2="57.15" width="0.1524" layer="91"/>
<wire x1="314.96" y1="57.15" x2="317.5" y2="54.61" width="0.1524" layer="91"/>
<label x="314.96" y="57.15" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A20"/>
</segment>
</net>
<net name="A19" class="0">
<segment>
<label x="40.64" y="66.3575" size="1.27" layer="95"/>
<wire x1="35.56" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<wire x1="53.34" y1="66.04" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="20.32" x2="58.42" y2="20.32" width="0.1524" layer="91"/>
<wire x1="58.42" y1="20.32" x2="55.88" y2="17.78" width="0.1524" layer="91"/>
<label x="58.42" y="20.32" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="39.37" x2="154.94" y2="39.37" width="0.1524" layer="91"/>
<wire x1="154.94" y1="39.37" x2="157.48" y2="36.83" width="0.1524" layer="91"/>
<label x="154.94" y="39.37" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A19"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="8"/>
<wire x1="176.53" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="160.02" y1="68.58" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<label x="165.1" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="59.69" x2="314.96" y2="59.69" width="0.1524" layer="91"/>
<wire x1="314.96" y1="59.69" x2="317.5" y2="57.15" width="0.1524" layer="91"/>
<label x="314.96" y="59.69" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A19"/>
</segment>
</net>
<net name="A16" class="0">
<segment>
<label x="16.51" y="76.2" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="76.2" x2="3.81" y2="76.2" width="0.1524" layer="91"/>
<wire x1="3.81" y1="76.2" x2="2.54" y2="74.93" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="27.94" x2="58.42" y2="27.94" width="0.1524" layer="91"/>
<wire x1="58.42" y1="27.94" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<label x="58.42" y="27.94" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="46.99" x2="154.94" y2="46.99" width="0.1524" layer="91"/>
<wire x1="154.94" y1="46.99" x2="157.48" y2="44.45" width="0.1524" layer="91"/>
<label x="154.94" y="46.99" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A16"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="14"/>
<wire x1="176.53" y1="76.2" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
<wire x1="160.02" y1="76.2" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<label x="165.1" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="67.31" x2="314.96" y2="67.31" width="0.1524" layer="91"/>
<wire x1="314.96" y1="67.31" x2="317.5" y2="64.77" width="0.1524" layer="91"/>
<label x="314.96" y="67.31" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A16"/>
</segment>
</net>
<net name="A15" class="0">
<segment>
<label x="16.51" y="78.74" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="78.74" x2="3.81" y2="78.74" width="0.1524" layer="91"/>
<wire x1="3.81" y1="78.74" x2="2.54" y2="77.47" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="30.48" x2="58.42" y2="30.48" width="0.1524" layer="91"/>
<wire x1="58.42" y1="30.48" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
<label x="58.42" y="30.48" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="49.53" x2="154.94" y2="49.53" width="0.1524" layer="91"/>
<wire x1="154.94" y1="49.53" x2="157.48" y2="46.99" width="0.1524" layer="91"/>
<label x="154.94" y="49.53" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A15"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="16"/>
<wire x1="176.53" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<label x="165.1" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="69.85" x2="314.96" y2="69.85" width="0.1524" layer="91"/>
<wire x1="314.96" y1="69.85" x2="317.5" y2="67.31" width="0.1524" layer="91"/>
<label x="314.96" y="69.85" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A15"/>
</segment>
</net>
<net name="A14" class="0">
<segment>
<label x="16.51" y="81.28" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="81.28" x2="3.81" y2="81.28" width="0.1524" layer="91"/>
<wire x1="3.81" y1="81.28" x2="2.54" y2="80.01" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="33.02" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
<wire x1="58.42" y1="33.02" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<label x="58.42" y="33.02" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="52.07" x2="154.94" y2="52.07" width="0.1524" layer="91"/>
<wire x1="154.94" y1="52.07" x2="157.48" y2="49.53" width="0.1524" layer="91"/>
<label x="154.94" y="52.07" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A14"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="18"/>
<wire x1="176.53" y1="81.28" x2="160.02" y2="81.28" width="0.1524" layer="91"/>
<wire x1="160.02" y1="81.28" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<label x="165.1" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="72.39" x2="314.96" y2="72.39" width="0.1524" layer="91"/>
<wire x1="314.96" y1="72.39" x2="317.5" y2="69.85" width="0.1524" layer="91"/>
<label x="314.96" y="72.39" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A14"/>
</segment>
</net>
<net name="A13" class="0">
<segment>
<label x="16.51" y="83.82" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="83.82" x2="3.81" y2="83.82" width="0.1524" layer="91"/>
<wire x1="3.81" y1="83.82" x2="2.54" y2="82.55" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="35.56" x2="58.42" y2="35.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="35.56" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
<label x="58.42" y="35.56" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="54.61" x2="154.94" y2="54.61" width="0.1524" layer="91"/>
<wire x1="154.94" y1="54.61" x2="157.48" y2="52.07" width="0.1524" layer="91"/>
<label x="154.94" y="54.61" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A13"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="4"/>
<wire x1="214.63" y1="63.5" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<wire x1="208.28" y1="63.5" x2="205.74" y2="60.96" width="0.1524" layer="91"/>
<label x="209.55" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="74.93" x2="314.96" y2="74.93" width="0.1524" layer="91"/>
<wire x1="314.96" y1="74.93" x2="317.5" y2="72.39" width="0.1524" layer="91"/>
<label x="314.96" y="74.93" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A13"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<label x="40.64" y="86.6775" size="1.27" layer="95"/>
<wire x1="35.56" y1="86.36" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="53.34" y1="86.36" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="38.1" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="38.1" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<label x="58.42" y="38.1" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="57.15" x2="154.94" y2="57.15" width="0.1524" layer="91"/>
<wire x1="154.94" y1="57.15" x2="157.48" y2="54.61" width="0.1524" layer="91"/>
<label x="154.94" y="57.15" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A12"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="6"/>
<wire x1="214.63" y1="66.04" x2="208.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="208.28" y1="66.04" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<label x="209.55" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="77.47" x2="314.96" y2="77.47" width="0.1524" layer="91"/>
<wire x1="314.96" y1="77.47" x2="317.5" y2="74.93" width="0.1524" layer="91"/>
<label x="314.96" y="77.47" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A12"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<label x="40.64" y="89.2175" size="1.27" layer="95"/>
<wire x1="35.56" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="40.64" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="58.42" y1="40.64" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<label x="58.42" y="40.64" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="59.69" x2="154.94" y2="59.69" width="0.1524" layer="91"/>
<wire x1="154.94" y1="59.69" x2="157.48" y2="57.15" width="0.1524" layer="91"/>
<label x="154.94" y="59.69" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A11"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="8"/>
<wire x1="214.63" y1="68.58" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
<wire x1="208.28" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<label x="209.55" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="80.01" x2="314.96" y2="80.01" width="0.1524" layer="91"/>
<wire x1="314.96" y1="80.01" x2="317.5" y2="77.47" width="0.1524" layer="91"/>
<label x="314.96" y="80.01" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A11"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<label x="40.64" y="91.7575" size="1.27" layer="95"/>
<wire x1="35.56" y1="91.44" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="43.18" x2="58.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="58.42" y1="43.18" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<label x="58.42" y="43.18" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="62.23" x2="154.94" y2="62.23" width="0.1524" layer="91"/>
<wire x1="154.94" y1="62.23" x2="157.48" y2="59.69" width="0.1524" layer="91"/>
<label x="154.94" y="62.23" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A10"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="10"/>
<wire x1="214.63" y1="71.12" x2="208.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<label x="209.55" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="82.55" x2="314.96" y2="82.55" width="0.1524" layer="91"/>
<wire x1="314.96" y1="82.55" x2="317.5" y2="80.01" width="0.1524" layer="91"/>
<label x="314.96" y="82.55" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A10"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<label x="40.64" y="94.2975" size="1.27" layer="95"/>
<wire x1="35.56" y1="93.98" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="93.98" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="45.72" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<label x="58.42" y="45.72" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="64.77" x2="154.94" y2="64.77" width="0.1524" layer="91"/>
<wire x1="154.94" y1="64.77" x2="157.48" y2="62.23" width="0.1524" layer="91"/>
<label x="154.94" y="64.77" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A9"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="12"/>
<wire x1="214.63" y1="73.66" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<label x="209.55" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="85.09" x2="314.96" y2="85.09" width="0.1524" layer="91"/>
<wire x1="314.96" y1="85.09" x2="317.5" y2="82.55" width="0.1524" layer="91"/>
<label x="314.96" y="85.09" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A9"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<label x="40.64" y="96.8375" size="1.27" layer="95"/>
<wire x1="35.56" y1="96.52" x2="53.34" y2="96.52" width="0.1524" layer="91"/>
<wire x1="53.34" y1="96.52" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="48.26" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="48.26" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<label x="58.42" y="48.26" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="67.31" x2="154.94" y2="67.31" width="0.1524" layer="91"/>
<wire x1="154.94" y1="67.31" x2="157.48" y2="64.77" width="0.1524" layer="91"/>
<label x="154.94" y="67.31" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A8"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="14"/>
<wire x1="214.63" y1="76.2" x2="208.28" y2="76.2" width="0.1524" layer="91"/>
<wire x1="208.28" y1="76.2" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<label x="209.55" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="87.63" x2="314.96" y2="87.63" width="0.1524" layer="91"/>
<wire x1="314.96" y1="87.63" x2="317.5" y2="85.09" width="0.1524" layer="91"/>
<label x="314.96" y="87.63" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A8"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<label x="40.64" y="99.3775" size="1.27" layer="95"/>
<wire x1="35.56" y1="99.06" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="53.34" y1="99.06" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<label x="58.42" y="50.8" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="69.85" x2="154.94" y2="69.85" width="0.1524" layer="91"/>
<wire x1="154.94" y1="69.85" x2="157.48" y2="67.31" width="0.1524" layer="91"/>
<label x="154.94" y="69.85" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A7"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="16"/>
<wire x1="214.63" y1="78.74" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<label x="209.55" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="90.17" x2="314.96" y2="90.17" width="0.1524" layer="91"/>
<wire x1="314.96" y1="90.17" x2="317.5" y2="87.63" width="0.1524" layer="91"/>
<label x="314.96" y="90.17" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A7"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<label x="16.51" y="104.14" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="104.14" x2="5.08" y2="104.14" width="0.1524" layer="91"/>
<wire x1="5.08" y1="104.14" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="58.42" y1="53.34" x2="55.88" y2="50.8" width="0.1524" layer="91"/>
<label x="58.42" y="53.34" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="72.39" x2="154.94" y2="72.39" width="0.1524" layer="91"/>
<wire x1="154.94" y1="72.39" x2="157.48" y2="69.85" width="0.1524" layer="91"/>
<label x="154.94" y="72.39" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A6"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="18"/>
<wire x1="214.63" y1="81.28" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="208.28" y1="81.28" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<label x="209.55" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="92.71" x2="314.96" y2="92.71" width="0.1524" layer="91"/>
<wire x1="314.96" y1="92.71" x2="317.5" y2="90.17" width="0.1524" layer="91"/>
<label x="314.96" y="92.71" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A6"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<label x="16.51" y="106.68" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="106.68" x2="5.08" y2="106.68" width="0.1524" layer="91"/>
<wire x1="5.08" y1="106.68" x2="2.54" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="55.88" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<label x="58.42" y="55.88" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="74.93" x2="154.94" y2="74.93" width="0.1524" layer="91"/>
<wire x1="154.94" y1="74.93" x2="157.48" y2="72.39" width="0.1524" layer="91"/>
<label x="154.94" y="74.93" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A5"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="4"/>
<wire x1="214.63" y1="21.59" x2="208.28" y2="21.59" width="0.1524" layer="91"/>
<wire x1="208.28" y1="21.59" x2="205.74" y2="19.05" width="0.1524" layer="91"/>
<label x="209.55" y="21.59" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="95.25" x2="314.96" y2="95.25" width="0.1524" layer="91"/>
<wire x1="314.96" y1="95.25" x2="317.5" y2="92.71" width="0.1524" layer="91"/>
<label x="314.96" y="95.25" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A5"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<label x="40.64" y="104.4575" size="1.27" layer="95"/>
<wire x1="35.56" y1="104.14" x2="53.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="53.34" y1="104.14" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="58.42" x2="58.42" y2="58.42" width="0.1524" layer="91"/>
<wire x1="58.42" y1="58.42" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<label x="58.42" y="58.42" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="77.47" x2="154.94" y2="77.47" width="0.1524" layer="91"/>
<wire x1="154.94" y1="77.47" x2="157.48" y2="74.93" width="0.1524" layer="91"/>
<label x="154.94" y="77.47" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A4"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="3"/>
<wire x1="229.87" y1="21.59" x2="246.38" y2="21.59" width="0.1524" layer="91"/>
<wire x1="246.38" y1="21.59" x2="250.19" y2="17.78" width="0.1524" layer="91"/>
<label x="234.95" y="21.59" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="97.79" x2="314.96" y2="97.79" width="0.1524" layer="91"/>
<wire x1="314.96" y1="97.79" x2="317.5" y2="95.25" width="0.1524" layer="91"/>
<label x="314.96" y="97.79" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A4"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<label x="40.64" y="101.9175" size="1.27" layer="95"/>
<wire x1="35.56" y1="101.6" x2="53.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="53.34" y1="101.6" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="60.96" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<label x="58.42" y="60.96" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="80.01" x2="154.94" y2="80.01" width="0.1524" layer="91"/>
<wire x1="154.94" y1="80.01" x2="157.48" y2="77.47" width="0.1524" layer="91"/>
<label x="154.94" y="80.01" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A3"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="17"/>
<wire x1="229.87" y1="81.28" x2="247.65" y2="81.28" width="0.1524" layer="91"/>
<wire x1="247.65" y1="81.28" x2="250.19" y2="78.74" width="0.1524" layer="91"/>
<label x="234.95" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="100.33" x2="314.96" y2="100.33" width="0.1524" layer="91"/>
<wire x1="314.96" y1="100.33" x2="317.5" y2="97.79" width="0.1524" layer="91"/>
<label x="314.96" y="100.33" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A3"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<label x="16.51" y="99.06" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="99.06" x2="3.81" y2="99.06" width="0.1524" layer="91"/>
<wire x1="3.81" y1="99.06" x2="2.54" y2="97.79" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="63.5" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="58.42" y1="63.5" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
<label x="58.42" y="63.5" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="82.55" x2="154.94" y2="82.55" width="0.1524" layer="91"/>
<wire x1="154.94" y1="82.55" x2="157.48" y2="80.01" width="0.1524" layer="91"/>
<label x="154.94" y="82.55" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A2"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="15"/>
<wire x1="229.87" y1="78.74" x2="247.65" y2="78.74" width="0.1524" layer="91"/>
<wire x1="247.65" y1="78.74" x2="250.19" y2="76.2" width="0.1524" layer="91"/>
<label x="234.95" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="102.87" x2="314.96" y2="102.87" width="0.1524" layer="91"/>
<wire x1="314.96" y1="102.87" x2="317.5" y2="100.33" width="0.1524" layer="91"/>
<label x="314.96" y="102.87" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A2"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<label x="16.51" y="96.52" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="96.52" x2="3.81" y2="96.52" width="0.1524" layer="91"/>
<wire x1="3.81" y1="96.52" x2="2.54" y2="95.25" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="66.04" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
<label x="58.42" y="66.04" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="85.09" x2="154.94" y2="85.09" width="0.1524" layer="91"/>
<wire x1="154.94" y1="85.09" x2="157.48" y2="82.55" width="0.1524" layer="91"/>
<label x="154.94" y="85.09" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A1"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="13"/>
<wire x1="229.87" y1="76.2" x2="247.65" y2="76.2" width="0.1524" layer="91"/>
<wire x1="247.65" y1="76.2" x2="250.19" y2="73.66" width="0.1524" layer="91"/>
<label x="234.95" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="105.41" x2="314.96" y2="105.41" width="0.1524" layer="91"/>
<wire x1="314.96" y1="105.41" x2="317.5" y2="102.87" width="0.1524" layer="91"/>
<label x="314.96" y="105.41" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="/AS" class="0">
<segment>
<label x="40.64" y="40.9575" size="1.27" layer="95"/>
<wire x1="35.56" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<wire x1="53.34" y1="40.64" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="124.46" x2="58.42" y2="124.46" width="0.1524" layer="91"/>
<wire x1="58.42" y1="124.46" x2="55.88" y2="121.92" width="0.1524" layer="91"/>
<label x="58.42" y="124.46" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="AS"/>
<wire x1="148.59" y1="97.79" x2="154.94" y2="97.79" width="0.1524" layer="91"/>
<wire x1="154.94" y1="97.79" x2="157.48" y2="95.25" width="0.1524" layer="91"/>
<label x="151.13" y="97.79" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="3"/>
<wire x1="229.87" y1="96.52" x2="246.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="246.38" y1="96.52" x2="250.19" y2="92.71" width="0.1524" layer="91"/>
<label x="234.95" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="AS"/>
<wire x1="308.61" y1="118.11" x2="314.96" y2="118.11" width="0.1524" layer="91"/>
<label x="311.15" y="118.11" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA0" class="0">
<segment>
<label x="16.51" y="38.1" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="38.1" x2="6.35" y2="38.1" width="0.1524" layer="91"/>
<wire x1="6.35" y1="38.1" x2="2.54" y2="34.29" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="109.22" x2="58.42" y2="109.22" width="0.1524" layer="91"/>
<wire x1="58.42" y1="109.22" x2="55.88" y2="106.68" width="0.1524" layer="91"/>
<label x="58.42" y="109.22" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="67.31" x2="109.22" y2="67.31" width="0.1524" layer="91"/>
<wire x1="109.22" y1="67.31" x2="106.68" y2="64.77" width="0.1524" layer="91"/>
<label x="109.22" y="67.31" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D0"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="17"/>
<wire x1="191.77" y1="114.3" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
<wire x1="203.2" y1="114.3" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<label x="194.31" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="87.63" x2="269.24" y2="87.63" width="0.1524" layer="91"/>
<wire x1="269.24" y1="87.63" x2="266.7" y2="85.09" width="0.1524" layer="91"/>
<label x="269.24" y="87.63" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D0"/>
</segment>
</net>
<net name="DA2" class="0">
<segment>
<label x="16.51" y="33.02" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="33.02" x2="6.35" y2="33.02" width="0.1524" layer="91"/>
<wire x1="6.35" y1="33.02" x2="2.54" y2="29.21" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<wire x1="58.42" y1="104.14" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<label x="58.42" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="62.23" x2="109.22" y2="62.23" width="0.1524" layer="91"/>
<wire x1="109.22" y1="62.23" x2="106.68" y2="59.69" width="0.1524" layer="91"/>
<label x="109.22" y="62.23" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D2"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="13"/>
<wire x1="191.77" y1="109.22" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<wire x1="203.2" y1="109.22" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<label x="194.31" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="82.55" x2="269.24" y2="82.55" width="0.1524" layer="91"/>
<wire x1="269.24" y1="82.55" x2="266.7" y2="80.01" width="0.1524" layer="91"/>
<label x="269.24" y="82.55" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D2"/>
</segment>
</net>
<net name="DA1" class="0">
<segment>
<label x="16.51" y="35.56" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="35.56" x2="6.35" y2="35.56" width="0.1524" layer="91"/>
<wire x1="6.35" y1="35.56" x2="2.54" y2="31.75" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="106.68" x2="58.42" y2="106.68" width="0.1524" layer="91"/>
<wire x1="58.42" y1="106.68" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<label x="58.42" y="106.68" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="64.77" x2="109.22" y2="64.77" width="0.1524" layer="91"/>
<wire x1="109.22" y1="64.77" x2="106.68" y2="62.23" width="0.1524" layer="91"/>
<label x="109.22" y="64.77" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D1"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="15"/>
<wire x1="191.77" y1="111.76" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="203.2" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<label x="194.31" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="85.09" x2="269.24" y2="85.09" width="0.1524" layer="91"/>
<wire x1="269.24" y1="85.09" x2="266.7" y2="82.55" width="0.1524" layer="91"/>
<label x="269.24" y="85.09" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D1"/>
</segment>
</net>
<net name="DA3" class="0">
<segment>
<label x="16.51" y="30.48" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="30.48" x2="7.62" y2="30.48" width="0.1524" layer="91"/>
<wire x1="7.62" y1="30.48" x2="2.54" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="101.6" x2="58.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="58.42" y1="101.6" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
<label x="58.42" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="59.69" x2="109.22" y2="59.69" width="0.1524" layer="91"/>
<wire x1="109.22" y1="59.69" x2="106.68" y2="57.15" width="0.1524" layer="91"/>
<label x="109.22" y="59.69" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D3"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="11"/>
<wire x1="191.77" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="106.68" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<label x="194.31" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="80.01" x2="269.24" y2="80.01" width="0.1524" layer="91"/>
<wire x1="269.24" y1="80.01" x2="266.7" y2="77.47" width="0.1524" layer="91"/>
<label x="269.24" y="80.01" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D3"/>
</segment>
</net>
<net name="DA4" class="0">
<segment>
<label x="16.51" y="27.94" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="27.94" x2="7.62" y2="27.94" width="0.1524" layer="91"/>
<wire x1="7.62" y1="27.94" x2="2.54" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<label x="58.42" y="99.06" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="57.15" x2="109.22" y2="57.15" width="0.1524" layer="91"/>
<wire x1="109.22" y1="57.15" x2="106.68" y2="54.61" width="0.1524" layer="91"/>
<label x="109.22" y="57.15" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D4"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="9"/>
<wire x1="191.77" y1="104.14" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="203.2" y1="104.14" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<label x="194.31" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="77.47" x2="269.24" y2="77.47" width="0.1524" layer="91"/>
<wire x1="269.24" y1="77.47" x2="266.7" y2="74.93" width="0.1524" layer="91"/>
<label x="269.24" y="77.47" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D4"/>
</segment>
</net>
<net name="DA5" class="0">
<segment>
<label x="40.64" y="25.7175" size="1.27" layer="95"/>
<wire x1="35.56" y1="25.4" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="53.34" y1="25.4" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="96.52" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<wire x1="58.42" y1="96.52" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
<label x="58.42" y="96.52" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="54.61" x2="109.22" y2="54.61" width="0.1524" layer="91"/>
<wire x1="109.22" y1="54.61" x2="106.68" y2="52.07" width="0.1524" layer="91"/>
<label x="109.22" y="54.61" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D5"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="10"/>
<wire x1="176.53" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<wire x1="160.02" y1="104.14" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<label x="165.1" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="74.93" x2="269.24" y2="74.93" width="0.1524" layer="91"/>
<wire x1="269.24" y1="74.93" x2="266.7" y2="72.39" width="0.1524" layer="91"/>
<label x="269.24" y="74.93" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D5"/>
</segment>
</net>
<net name="DA6" class="0">
<segment>
<label x="40.64" y="28.2575" size="1.27" layer="95"/>
<wire x1="35.56" y1="27.94" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="27.94" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="93.98" x2="58.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="58.42" y1="93.98" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<label x="58.42" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="52.07" x2="109.22" y2="52.07" width="0.1524" layer="91"/>
<wire x1="109.22" y1="52.07" x2="106.68" y2="49.53" width="0.1524" layer="91"/>
<label x="109.22" y="52.07" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D6"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="12"/>
<wire x1="176.53" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="106.68" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
<label x="165.1" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="72.39" x2="269.24" y2="72.39" width="0.1524" layer="91"/>
<wire x1="269.24" y1="72.39" x2="266.7" y2="69.85" width="0.1524" layer="91"/>
<label x="269.24" y="72.39" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D6"/>
</segment>
</net>
<net name="DA7" class="0">
<segment>
<label x="40.64" y="30.7975" size="1.27" layer="95"/>
<wire x1="35.56" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="58.42" y1="91.44" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<label x="58.42" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="49.53" x2="109.22" y2="49.53" width="0.1524" layer="91"/>
<wire x1="109.22" y1="49.53" x2="106.68" y2="46.99" width="0.1524" layer="91"/>
<label x="109.22" y="49.53" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D7"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="14"/>
<wire x1="176.53" y1="109.22" x2="160.02" y2="109.22" width="0.1524" layer="91"/>
<wire x1="160.02" y1="109.22" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<label x="165.1" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="69.85" x2="269.24" y2="69.85" width="0.1524" layer="91"/>
<wire x1="269.24" y1="69.85" x2="266.7" y2="67.31" width="0.1524" layer="91"/>
<label x="269.24" y="69.85" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D7"/>
</segment>
</net>
<net name="DA8" class="0">
<segment>
<label x="40.64" y="33.3375" size="1.27" layer="95"/>
<wire x1="35.56" y1="33.02" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="33.02" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="58.42" y1="88.9" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<label x="58.42" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="46.99" x2="109.22" y2="46.99" width="0.1524" layer="91"/>
<wire x1="109.22" y1="46.99" x2="106.68" y2="44.45" width="0.1524" layer="91"/>
<label x="109.22" y="46.99" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D8"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="16"/>
<wire x1="176.53" y1="111.76" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="160.02" y1="111.76" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<label x="165.1" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="67.31" x2="269.24" y2="67.31" width="0.1524" layer="91"/>
<wire x1="269.24" y1="67.31" x2="266.7" y2="64.77" width="0.1524" layer="91"/>
<label x="269.24" y="67.31" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D8"/>
</segment>
</net>
<net name="DA9" class="0">
<segment>
<label x="40.64" y="35.8775" size="1.27" layer="95"/>
<wire x1="35.56" y1="35.56" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
<wire x1="53.34" y1="35.56" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="86.36" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
<label x="58.42" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="44.45" x2="109.22" y2="44.45" width="0.1524" layer="91"/>
<wire x1="109.22" y1="44.45" x2="106.68" y2="41.91" width="0.1524" layer="91"/>
<label x="109.22" y="44.45" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D9"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="18"/>
<wire x1="176.53" y1="114.3" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<label x="165.1" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="64.77" x2="269.24" y2="64.77" width="0.1524" layer="91"/>
<wire x1="269.24" y1="64.77" x2="266.7" y2="62.23" width="0.1524" layer="91"/>
<label x="269.24" y="64.77" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D9"/>
</segment>
</net>
<net name="DA10" class="0">
<segment>
<label x="40.64" y="38.4175" size="1.27" layer="95"/>
<wire x1="35.56" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<wire x1="53.34" y1="38.1" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="58.42" y1="83.82" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<label x="58.42" y="83.82" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="41.91" x2="109.22" y2="41.91" width="0.1524" layer="91"/>
<wire x1="109.22" y1="41.91" x2="106.68" y2="39.37" width="0.1524" layer="91"/>
<label x="109.22" y="41.91" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D10"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="4"/>
<wire x1="214.63" y1="96.52" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="208.28" y1="96.52" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<label x="209.55" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="62.23" x2="269.24" y2="62.23" width="0.1524" layer="91"/>
<wire x1="269.24" y1="62.23" x2="266.7" y2="59.69" width="0.1524" layer="91"/>
<label x="269.24" y="62.23" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D10"/>
</segment>
</net>
<net name="DA11" class="0">
<segment>
<label x="16.51" y="43.18" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="43.18" x2="6.35" y2="43.18" width="0.1524" layer="91"/>
<wire x1="6.35" y1="43.18" x2="2.54" y2="39.37" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="81.28" x2="58.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="58.42" y1="81.28" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<label x="58.42" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="39.37" x2="109.22" y2="39.37" width="0.1524" layer="91"/>
<wire x1="109.22" y1="39.37" x2="106.68" y2="36.83" width="0.1524" layer="91"/>
<label x="109.22" y="39.37" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D11"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="6"/>
<wire x1="214.63" y1="99.06" x2="208.28" y2="99.06" width="0.1524" layer="91"/>
<wire x1="208.28" y1="99.06" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<label x="209.55" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="59.69" x2="269.24" y2="59.69" width="0.1524" layer="91"/>
<wire x1="269.24" y1="59.69" x2="266.7" y2="57.15" width="0.1524" layer="91"/>
<label x="269.24" y="59.69" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D11"/>
</segment>
</net>
<net name="DA12" class="0">
<segment>
<label x="16.51" y="45.72" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="45.72" x2="6.35" y2="45.72" width="0.1524" layer="91"/>
<wire x1="6.35" y1="45.72" x2="2.54" y2="41.91" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="78.74" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="58.42" y1="78.74" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<label x="58.42" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="36.83" x2="109.22" y2="36.83" width="0.1524" layer="91"/>
<wire x1="109.22" y1="36.83" x2="106.68" y2="34.29" width="0.1524" layer="91"/>
<label x="109.22" y="36.83" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D12"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="8"/>
<wire x1="214.63" y1="101.6" x2="208.28" y2="101.6" width="0.1524" layer="91"/>
<wire x1="208.28" y1="101.6" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<label x="209.55" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="57.15" x2="269.24" y2="57.15" width="0.1524" layer="91"/>
<wire x1="269.24" y1="57.15" x2="266.7" y2="54.61" width="0.1524" layer="91"/>
<label x="269.24" y="57.15" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D12"/>
</segment>
</net>
<net name="DA13" class="0">
<segment>
<label x="16.51" y="48.26" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="48.26" x2="6.35" y2="48.26" width="0.1524" layer="91"/>
<wire x1="6.35" y1="48.26" x2="2.54" y2="44.45" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="58.42" y1="76.2" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<label x="58.42" y="76.2" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="34.29" x2="109.22" y2="34.29" width="0.1524" layer="91"/>
<wire x1="109.22" y1="34.29" x2="106.68" y2="31.75" width="0.1524" layer="91"/>
<label x="109.22" y="34.29" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D13"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="10"/>
<wire x1="214.63" y1="104.14" x2="208.28" y2="104.14" width="0.1524" layer="91"/>
<wire x1="208.28" y1="104.14" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<label x="209.55" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="54.61" x2="269.24" y2="54.61" width="0.1524" layer="91"/>
<wire x1="269.24" y1="54.61" x2="266.7" y2="52.07" width="0.1524" layer="91"/>
<label x="269.24" y="54.61" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D13"/>
</segment>
</net>
<net name="DA14" class="0">
<segment>
<label x="16.51" y="50.8" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="50.8" x2="6.35" y2="50.8" width="0.1524" layer="91"/>
<wire x1="6.35" y1="50.8" x2="2.54" y2="46.99" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="73.66" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<label x="58.42" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="31.75" x2="109.22" y2="31.75" width="0.1524" layer="91"/>
<wire x1="109.22" y1="31.75" x2="106.68" y2="29.21" width="0.1524" layer="91"/>
<label x="109.22" y="31.75" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D14"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="12"/>
<wire x1="214.63" y1="106.68" x2="208.28" y2="106.68" width="0.1524" layer="91"/>
<wire x1="208.28" y1="106.68" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<label x="209.55" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="52.07" x2="269.24" y2="52.07" width="0.1524" layer="91"/>
<wire x1="269.24" y1="52.07" x2="266.7" y2="49.53" width="0.1524" layer="91"/>
<label x="269.24" y="52.07" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D14"/>
</segment>
</net>
<net name="DA15" class="0">
<segment>
<label x="16.51" y="53.34" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="53.34" x2="6.35" y2="53.34" width="0.1524" layer="91"/>
<wire x1="6.35" y1="53.34" x2="2.54" y2="49.53" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="58.42" y1="71.12" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
<label x="58.42" y="71.12" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="115.57" y1="29.21" x2="109.22" y2="29.21" width="0.1524" layer="91"/>
<wire x1="109.22" y1="29.21" x2="106.68" y2="26.67" width="0.1524" layer="91"/>
<label x="109.22" y="29.21" size="1.27" layer="95" rot="R180"/>
<pinref part="IC1" gate="G$1" pin="D15"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="14"/>
<wire x1="214.63" y1="109.22" x2="208.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="208.28" y1="109.22" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<label x="209.55" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="275.59" y1="49.53" x2="269.24" y2="49.53" width="0.1524" layer="91"/>
<wire x1="269.24" y1="49.53" x2="266.7" y2="46.99" width="0.1524" layer="91"/>
<label x="269.24" y="49.53" size="1.27" layer="95" rot="R180"/>
<pinref part="IC2" gate="G$1" pin="D15"/>
</segment>
</net>
<net name="XRDY" class="0">
<segment>
<label x="40.64" y="112.0775" size="1.27" layer="95"/>
<wire x1="35.56" y1="111.76" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
<wire x1="53.34" y1="111.76" x2="55.88" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="129.54" x2="57.15" y2="129.54" width="0.1524" layer="91"/>
<wire x1="57.15" y1="129.54" x2="55.88" y2="128.27" width="0.1524" layer="91"/>
<label x="57.15" y="129.54" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="7"/>
<wire x1="229.87" y1="26.67" x2="247.65" y2="26.67" width="0.1524" layer="91"/>
<wire x1="247.65" y1="26.67" x2="250.19" y2="24.13" width="0.1524" layer="91"/>
<label x="234.95" y="26.67" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="284.48" y1="27.94" x2="302.26" y2="27.94" width="0.1524" layer="91"/>
<wire x1="302.26" y1="27.94" x2="304.8" y2="25.4" width="0.1524" layer="91"/>
<label x="289.56" y="27.94" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="10"/>
</segment>
</net>
<net name="A23" class="0">
<segment>
<label x="16.51" y="58.42" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="58.42" x2="6.35" y2="58.42" width="0.1524" layer="91"/>
<wire x1="6.35" y1="58.42" x2="2.54" y2="54.61" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="10.16" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="10.16" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<label x="58.42" y="10.16" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="29.21" x2="154.94" y2="29.21" width="0.1524" layer="91"/>
<wire x1="154.94" y1="29.21" x2="157.48" y2="26.67" width="0.1524" layer="91"/>
<label x="154.94" y="29.21" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A23"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="16"/>
<wire x1="214.63" y1="111.76" x2="208.28" y2="111.76" width="0.1524" layer="91"/>
<wire x1="208.28" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<label x="209.55" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="49.53" x2="314.96" y2="49.53" width="0.1524" layer="91"/>
<wire x1="314.96" y1="49.53" x2="317.5" y2="46.99" width="0.1524" layer="91"/>
<label x="314.96" y="49.53" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A23"/>
</segment>
</net>
<net name="A22" class="0">
<segment>
<label x="16.51" y="60.96" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="60.96" x2="6.35" y2="60.96" width="0.1524" layer="91"/>
<wire x1="6.35" y1="60.96" x2="2.54" y2="57.15" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="12.7" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="12.7" x2="55.88" y2="10.16" width="0.1524" layer="91"/>
<label x="58.42" y="12.7" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="31.75" x2="154.94" y2="31.75" width="0.1524" layer="91"/>
<wire x1="154.94" y1="31.75" x2="157.48" y2="29.21" width="0.1524" layer="91"/>
<label x="154.94" y="31.75" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A22"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="18"/>
<wire x1="214.63" y1="114.3" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<wire x1="208.28" y1="114.3" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<label x="209.55" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="52.07" x2="314.96" y2="52.07" width="0.1524" layer="91"/>
<wire x1="314.96" y1="52.07" x2="317.5" y2="49.53" width="0.1524" layer="91"/>
<label x="314.96" y="52.07" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A22"/>
</segment>
</net>
<net name="A21" class="0">
<segment>
<label x="40.64" y="61.2775" size="1.27" layer="95"/>
<wire x1="35.56" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<wire x1="53.34" y1="60.96" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="15.24" x2="58.42" y2="15.24" width="0.1524" layer="91"/>
<wire x1="58.42" y1="15.24" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<label x="58.42" y="15.24" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="148.59" y1="34.29" x2="154.94" y2="34.29" width="0.1524" layer="91"/>
<wire x1="154.94" y1="34.29" x2="157.48" y2="31.75" width="0.1524" layer="91"/>
<label x="154.94" y="34.29" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC1" gate="G$1" pin="A21"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="4"/>
<wire x1="176.53" y1="63.5" x2="161.29" y2="63.5" width="0.1524" layer="91"/>
<wire x1="161.29" y1="63.5" x2="157.48" y2="59.69" width="0.1524" layer="91"/>
<label x="165.1" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="308.61" y1="54.61" x2="314.96" y2="54.61" width="0.1524" layer="91"/>
<wire x1="314.96" y1="54.61" x2="317.5" y2="52.07" width="0.1524" layer="91"/>
<label x="314.96" y="54.61" size="1.27" layer="95" rot="MR180"/>
<pinref part="IC2" gate="G$1" pin="A21"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="38.1" y1="127" x2="35.56" y2="127" width="0.1524" layer="91"/>
<pinref part="P+12" gate="VCC" pin="VCC"/>
</segment>
<segment>
<wire x1="15.24" y1="127" x2="20.32" y2="127" width="0.1524" layer="91"/>
<pinref part="P+14" gate="VCC" pin="VCC"/>
</segment>
<segment>
<wire x1="95.25" y1="45.72" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<wire x1="104.14" y1="45.72" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<label x="99.06" y="45.72" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="95.25" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<wire x1="104.14" y1="43.18" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<label x="99.06" y="43.18" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="P" pin="VCC@1"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="167.64" y1="26.67" x2="167.64" y2="24.13" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="P" pin="VCC@2"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="170.18" y1="26.67" x2="170.18" y2="24.13" width="0.1524" layer="91"/>
</segment>
</net>
<net name="/RESET" class="0">
<segment>
<label x="16.51" y="66.04" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="66.04" x2="6.35" y2="66.04" width="0.1524" layer="91"/>
<wire x1="6.35" y1="66.04" x2="2.54" y2="62.23" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<wire x1="104.14" y1="99.06" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<label x="97.79" y="99.06" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="RESET"/>
<wire x1="115.57" y1="115.57" x2="109.22" y2="115.57" width="0.1524" layer="91"/>
<wire x1="109.22" y1="115.57" x2="106.68" y2="113.03" width="0.1524" layer="91"/>
<label x="109.22" y="115.57" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="7"/>
<wire x1="191.77" y1="68.58" x2="203.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="203.2" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<label x="194.31" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="RESET"/>
<wire x1="275.59" y1="135.89" x2="269.24" y2="135.89" width="0.1524" layer="91"/>
<wire x1="269.24" y1="135.89" x2="266.7" y2="133.35" width="0.1524" layer="91"/>
<label x="269.24" y="135.89" size="1.27" layer="95"/>
</segment>
</net>
<net name="/BERR" class="0">
<segment>
<label x="40.64" y="76.5175" size="1.27" layer="95"/>
<wire x1="35.56" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="73.66" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<label x="99.06" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="BERR"/>
<wire x1="115.57" y1="118.11" x2="109.22" y2="118.11" width="0.1524" layer="91"/>
<wire x1="109.22" y1="118.11" x2="106.68" y2="115.57" width="0.1524" layer="91"/>
<label x="109.22" y="118.11" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="15"/>
<wire x1="191.77" y1="78.74" x2="203.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="203.2" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<label x="194.31" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="BERR"/>
<wire x1="275.59" y1="138.43" x2="269.24" y2="138.43" width="0.1524" layer="91"/>
<wire x1="269.24" y1="138.43" x2="266.7" y2="135.89" width="0.1524" layer="91"/>
<label x="269.24" y="138.43" size="1.27" layer="95"/>
</segment>
</net>
<net name="/SLAVE" class="0">
<segment>
<label x="16.51" y="121.92" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="121.92" x2="6.35" y2="121.92" width="0.1524" layer="91"/>
<wire x1="6.35" y1="121.92" x2="2.54" y2="118.11" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="14"/>
<wire x1="214.63" y1="34.29" x2="208.28" y2="34.29" width="0.1524" layer="91"/>
<wire x1="208.28" y1="34.29" x2="205.74" y2="31.75" width="0.1524" layer="91"/>
<label x="209.55" y="34.29" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="35.56" x2="270.51" y2="35.56" width="0.1524" layer="91"/>
<wire x1="270.51" y1="35.56" x2="267.97" y2="33.02" width="0.1524" layer="91"/>
<label x="271.78" y="35.56" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
</net>
<net name="CFGOUT" class="0">
<segment>
<label x="16.51" y="119.38" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="119.38" x2="6.35" y2="119.38" width="0.1524" layer="91"/>
<wire x1="6.35" y1="119.38" x2="2.54" y2="115.57" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="132.08" x2="57.15" y2="132.08" width="0.1524" layer="91"/>
<wire x1="57.15" y1="132.08" x2="55.88" y2="130.81" width="0.1524" layer="91"/>
<label x="57.15" y="132.08" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="12"/>
<wire x1="214.63" y1="31.75" x2="208.28" y2="31.75" width="0.1524" layer="91"/>
<wire x1="208.28" y1="31.75" x2="205.74" y2="29.21" width="0.1524" layer="91"/>
<label x="209.55" y="31.75" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="33.02" x2="270.51" y2="33.02" width="0.1524" layer="91"/>
<wire x1="270.51" y1="33.02" x2="267.97" y2="30.48" width="0.1524" layer="91"/>
<label x="271.78" y="33.02" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
</net>
<net name="CFGIN" class="0">
<segment>
<label x="40.64" y="119.6975" size="1.27" layer="95"/>
<wire x1="35.56" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="13"/>
<wire x1="229.87" y1="34.29" x2="247.65" y2="34.29" width="0.1524" layer="91"/>
<wire x1="247.65" y1="34.29" x2="250.19" y2="31.75" width="0.1524" layer="91"/>
<label x="234.95" y="34.29" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="284.48" y1="35.56" x2="302.26" y2="35.56" width="0.1524" layer="91"/>
<wire x1="302.26" y1="35.56" x2="304.8" y2="33.02" width="0.1524" layer="91"/>
<label x="289.56" y="35.56" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
</net>
<net name="CDAC" class="0">
<segment>
<label x="16.51" y="114.3" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="114.3" x2="5.08" y2="114.3" width="0.1524" layer="91"/>
<wire x1="5.08" y1="114.3" x2="2.54" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="60.96" x2="102.87" y2="60.96" width="0.1524" layer="91"/>
<wire x1="102.87" y1="60.96" x2="106.68" y2="57.15" width="0.1524" layer="91"/>
<label x="99.06" y="60.96" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="10"/>
<wire x1="214.63" y1="29.21" x2="208.28" y2="29.21" width="0.1524" layer="91"/>
<wire x1="208.28" y1="29.21" x2="205.74" y2="26.67" width="0.1524" layer="91"/>
<label x="209.55" y="29.21" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="30.48" x2="270.51" y2="30.48" width="0.1524" layer="91"/>
<wire x1="270.51" y1="30.48" x2="267.97" y2="27.94" width="0.1524" layer="91"/>
<label x="271.78" y="30.48" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="7"/>
</segment>
</net>
<net name="/OVR" class="0">
<segment>
<label x="16.51" y="111.76" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="111.76" x2="6.35" y2="111.76" width="0.1524" layer="91"/>
<wire x1="6.35" y1="111.76" x2="2.54" y2="107.95" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="127" x2="58.42" y2="127" width="0.1524" layer="91"/>
<wire x1="58.42" y1="127" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<label x="58.42" y="127" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="8"/>
<wire x1="214.63" y1="26.67" x2="208.28" y2="26.67" width="0.1524" layer="91"/>
<wire x1="208.28" y1="26.67" x2="205.74" y2="24.13" width="0.1524" layer="91"/>
<label x="209.55" y="26.67" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="27.94" x2="270.51" y2="27.94" width="0.1524" layer="91"/>
<wire x1="270.51" y1="27.94" x2="267.97" y2="25.4" width="0.1524" layer="91"/>
<label x="271.78" y="27.94" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
</net>
<net name="/INT2" class="0">
<segment>
<label x="16.51" y="109.22" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="109.22" x2="6.35" y2="109.22" width="0.1524" layer="91"/>
<wire x1="6.35" y1="109.22" x2="2.54" y2="105.41" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="101.6" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="101.6" x2="106.68" y2="99.06" width="0.1524" layer="91"/>
<label x="99.06" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="6"/>
<wire x1="214.63" y1="24.13" x2="208.28" y2="24.13" width="0.1524" layer="91"/>
<wire x1="208.28" y1="24.13" x2="205.74" y2="21.59" width="0.1524" layer="91"/>
<label x="209.55" y="24.13" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="25.4" x2="270.51" y2="25.4" width="0.1524" layer="91"/>
<wire x1="270.51" y1="25.4" x2="267.97" y2="22.86" width="0.1524" layer="91"/>
<label x="271.78" y="25.4" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="11"/>
</segment>
</net>
<net name="FC0" class="0">
<segment>
<label x="16.51" y="93.98" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="93.98" x2="5.08" y2="93.98" width="0.1524" layer="91"/>
<wire x1="5.08" y1="93.98" x2="2.54" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="116.84" x2="104.14" y2="116.84" width="0.1524" layer="91"/>
<wire x1="104.14" y1="116.84" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<label x="99.06" y="116.84" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="FC0"/>
<wire x1="148.59" y1="123.19" x2="156.21" y2="123.19" width="0.1524" layer="91"/>
<wire x1="156.21" y1="123.19" x2="157.48" y2="121.92" width="0.1524" layer="91"/>
<label x="151.13" y="123.19" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="11"/>
<wire x1="229.87" y1="73.66" x2="247.65" y2="73.66" width="0.1524" layer="91"/>
<wire x1="247.65" y1="73.66" x2="250.19" y2="71.12" width="0.1524" layer="91"/>
<label x="234.95" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="FC0"/>
<wire x1="308.61" y1="143.51" x2="316.23" y2="143.51" width="0.1524" layer="91"/>
<label x="311.15" y="143.51" size="1.27" layer="95"/>
</segment>
</net>
<net name="FC1" class="0">
<segment>
<label x="16.51" y="91.44" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="91.44" x2="5.08" y2="91.44" width="0.1524" layer="91"/>
<wire x1="5.08" y1="91.44" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="119.38" x2="104.14" y2="119.38" width="0.1524" layer="91"/>
<wire x1="104.14" y1="119.38" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<label x="99.06" y="119.38" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="FC1"/>
<wire x1="148.59" y1="120.65" x2="154.94" y2="120.65" width="0.1524" layer="91"/>
<wire x1="154.94" y1="120.65" x2="157.48" y2="118.11" width="0.1524" layer="91"/>
<label x="151.13" y="120.65" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="9"/>
<wire x1="229.87" y1="71.12" x2="247.65" y2="71.12" width="0.1524" layer="91"/>
<wire x1="247.65" y1="71.12" x2="250.19" y2="68.58" width="0.1524" layer="91"/>
<label x="234.95" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="FC1"/>
<wire x1="308.61" y1="140.97" x2="314.96" y2="140.97" width="0.1524" layer="91"/>
<label x="311.15" y="140.97" size="1.27" layer="95"/>
</segment>
</net>
<net name="FC2" class="0">
<segment>
<label x="16.51" y="88.9" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="88.9" x2="5.08" y2="88.9" width="0.1524" layer="91"/>
<wire x1="5.08" y1="88.9" x2="2.54" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="121.92" x2="104.14" y2="121.92" width="0.1524" layer="91"/>
<wire x1="104.14" y1="121.92" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="99.06" y="121.92" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="FC2"/>
<wire x1="148.59" y1="118.11" x2="154.94" y2="118.11" width="0.1524" layer="91"/>
<wire x1="154.94" y1="118.11" x2="157.48" y2="115.57" width="0.1524" layer="91"/>
<label x="149.86" y="118.11" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="7"/>
<wire x1="229.87" y1="68.58" x2="247.65" y2="68.58" width="0.1524" layer="91"/>
<wire x1="247.65" y1="68.58" x2="250.19" y2="66.04" width="0.1524" layer="91"/>
<label x="234.95" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="FC2"/>
<wire x1="308.61" y1="138.43" x2="314.96" y2="138.43" width="0.1524" layer="91"/>
<label x="309.88" y="138.43" size="1.27" layer="95"/>
</segment>
</net>
<net name="/CCK" class="0">
<segment>
<wire x1="35.56" y1="114.3" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="53.34" y1="114.3" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="63.5" x2="102.87" y2="63.5" width="0.1524" layer="91"/>
<wire x1="102.87" y1="63.5" x2="106.68" y2="59.69" width="0.1524" layer="91"/>
<label x="99.06" y="63.5" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="9"/>
<wire x1="229.87" y1="29.21" x2="247.65" y2="29.21" width="0.1524" layer="91"/>
<wire x1="247.65" y1="29.21" x2="250.19" y2="26.67" width="0.1524" layer="91"/>
<label x="234.95" y="29.21" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="284.48" y1="30.48" x2="302.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="302.26" y1="30.48" x2="304.8" y2="27.94" width="0.1524" layer="91"/>
<label x="289.56" y="30.48" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="8"/>
</segment>
</net>
<net name="/INT6" class="0">
<segment>
<wire x1="35.56" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<wire x1="53.34" y1="106.68" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="104.14" y1="104.14" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<label x="99.06" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="5"/>
<wire x1="229.87" y1="24.13" x2="246.38" y2="24.13" width="0.1524" layer="91"/>
<wire x1="246.38" y1="24.13" x2="250.19" y2="20.32" width="0.1524" layer="91"/>
<label x="234.95" y="24.13" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="284.48" y1="25.4" x2="300.99" y2="25.4" width="0.1524" layer="91"/>
<wire x1="300.99" y1="25.4" x2="304.8" y2="21.59" width="0.1524" layer="91"/>
<label x="289.56" y="25.4" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="12"/>
</segment>
</net>
<net name="/VMA" class="0">
<segment>
<label x="16.51" y="68.58" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="68.58" x2="6.35" y2="68.58" width="0.1524" layer="91"/>
<wire x1="6.35" y1="68.58" x2="2.54" y2="64.77" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="78.74" x2="104.14" y2="78.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="78.74" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="99.06" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VMA"/>
<wire x1="148.59" y1="107.95" x2="154.94" y2="107.95" width="0.1524" layer="91"/>
<wire x1="154.94" y1="107.95" x2="157.48" y2="105.41" width="0.1524" layer="91"/>
<label x="151.13" y="107.95" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="9"/>
<wire x1="191.77" y1="71.12" x2="203.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="203.2" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<label x="194.31" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VMA"/>
<wire x1="308.61" y1="128.27" x2="314.96" y2="128.27" width="0.1524" layer="91"/>
<label x="311.15" y="128.27" size="1.27" layer="95"/>
</segment>
</net>
<net name="/HALT" class="0">
<segment>
<label x="16.51" y="63.5" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="63.5" x2="6.35" y2="63.5" width="0.1524" layer="91"/>
<wire x1="6.35" y1="63.5" x2="2.54" y2="59.69" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="81.28" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<wire x1="104.14" y1="81.28" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<label x="99.06" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="HALT"/>
<wire x1="115.57" y1="113.03" x2="109.22" y2="113.03" width="0.1524" layer="91"/>
<wire x1="109.22" y1="113.03" x2="106.68" y2="110.49" width="0.1524" layer="91"/>
<label x="109.22" y="113.03" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="5"/>
<wire x1="191.77" y1="66.04" x2="203.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="203.2" y1="66.04" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<label x="194.31" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="HALT"/>
<wire x1="275.59" y1="133.35" x2="269.24" y2="133.35" width="0.1524" layer="91"/>
<wire x1="269.24" y1="133.35" x2="266.7" y2="130.81" width="0.1524" layer="91"/>
<label x="269.24" y="133.35" size="1.27" layer="95"/>
</segment>
</net>
<net name="/BR" class="0">
<segment>
<wire x1="35.56" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<wire x1="53.34" y1="58.42" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="88.9" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="88.9" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="99.06" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="BR"/>
<wire x1="115.57" y1="102.87" x2="110.49" y2="102.87" width="0.1524" layer="91"/>
<wire x1="110.49" y1="102.87" x2="106.68" y2="99.06" width="0.1524" layer="91"/>
<label x="110.49" y="102.87" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="17"/>
<wire x1="229.87" y1="114.3" x2="247.65" y2="114.3" width="0.1524" layer="91"/>
<wire x1="247.65" y1="114.3" x2="250.19" y2="111.76" width="0.1524" layer="91"/>
<label x="234.95" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="BR"/>
<wire x1="275.59" y1="123.19" x2="270.51" y2="123.19" width="0.1524" layer="91"/>
<wire x1="270.51" y1="123.19" x2="266.7" y2="119.38" width="0.1524" layer="91"/>
<label x="270.51" y="123.19" size="1.27" layer="95"/>
</segment>
</net>
<net name="/BGACK" class="0">
<segment>
<wire x1="35.56" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="55.88" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="93.98" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<label x="99.06" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="BGACK"/>
<wire x1="115.57" y1="100.33" x2="110.49" y2="100.33" width="0.1524" layer="91"/>
<wire x1="110.49" y1="100.33" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<label x="111.76" y="100.33" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="15"/>
<wire x1="229.87" y1="111.76" x2="247.65" y2="111.76" width="0.1524" layer="91"/>
<wire x1="247.65" y1="111.76" x2="250.19" y2="109.22" width="0.1524" layer="91"/>
<label x="234.95" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="BGACK"/>
<wire x1="275.59" y1="120.65" x2="270.51" y2="120.65" width="0.1524" layer="91"/>
<wire x1="270.51" y1="120.65" x2="266.7" y2="116.84" width="0.1524" layer="91"/>
<label x="271.78" y="120.65" size="1.27" layer="95"/>
</segment>
</net>
<net name="/BG" class="0">
<segment>
<wire x1="35.56" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="53.34" x2="55.88" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="104.14" y1="91.44" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<label x="99.06" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="BG"/>
<wire x1="148.59" y1="102.87" x2="154.94" y2="102.87" width="0.1524" layer="91"/>
<wire x1="154.94" y1="102.87" x2="157.48" y2="100.33" width="0.1524" layer="91"/>
<label x="151.13" y="102.87" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="13"/>
<wire x1="229.87" y1="109.22" x2="247.65" y2="109.22" width="0.1524" layer="91"/>
<wire x1="247.65" y1="109.22" x2="250.19" y2="106.68" width="0.1524" layer="91"/>
<label x="234.95" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="BG"/>
<wire x1="308.61" y1="123.19" x2="314.96" y2="123.19" width="0.1524" layer="91"/>
<label x="311.15" y="123.19" size="1.27" layer="95"/>
</segment>
</net>
<net name="/DTACK" class="0">
<segment>
<wire x1="35.56" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
<wire x1="53.34" y1="50.8" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="64.77" y1="121.92" x2="58.42" y2="121.92" width="0.1524" layer="91"/>
<wire x1="58.42" y1="121.92" x2="55.88" y2="119.38" width="0.1524" layer="91"/>
<label x="58.42" y="121.92" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="DTACK"/>
<wire x1="115.57" y1="95.25" x2="109.22" y2="95.25" width="0.1524" layer="91"/>
<wire x1="109.22" y1="95.25" x2="106.68" y2="92.71" width="0.1524" layer="91"/>
<label x="109.22" y="95.25" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="11"/>
<wire x1="229.87" y1="106.68" x2="247.65" y2="106.68" width="0.1524" layer="91"/>
<wire x1="247.65" y1="106.68" x2="250.19" y2="104.14" width="0.1524" layer="91"/>
<label x="234.95" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="DTACK"/>
<wire x1="275.59" y1="115.57" x2="269.24" y2="115.57" width="0.1524" layer="91"/>
<wire x1="269.24" y1="115.57" x2="266.7" y2="113.03" width="0.1524" layer="91"/>
<label x="269.24" y="115.57" size="1.27" layer="95"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<wire x1="35.56" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<wire x1="53.34" y1="71.12" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<wire x1="104.14" y1="68.58" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
<label x="99.06" y="68.58" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="E"/>
<wire x1="148.59" y1="110.49" x2="154.94" y2="110.49" width="0.1524" layer="91"/>
<wire x1="154.94" y1="110.49" x2="157.48" y2="107.95" width="0.1524" layer="91"/>
<label x="151.13" y="110.49" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="11"/>
<wire x1="191.77" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<label x="194.31" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="E"/>
<wire x1="308.61" y1="130.81" x2="314.96" y2="130.81" width="0.1524" layer="91"/>
<label x="311.15" y="130.81" size="1.27" layer="95"/>
</segment>
</net>
<net name="/VPA" class="0">
<segment>
<wire x1="35.56" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="73.66" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<label x="99.06" y="76.2" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VPA"/>
<wire x1="115.57" y1="107.95" x2="109.22" y2="107.95" width="0.1524" layer="91"/>
<wire x1="109.22" y1="107.95" x2="106.68" y2="105.41" width="0.1524" layer="91"/>
<label x="109.22" y="107.95" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="13"/>
<wire x1="191.77" y1="76.2" x2="203.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="203.2" y1="76.2" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<label x="194.31" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VPA"/>
<wire x1="275.59" y1="128.27" x2="269.24" y2="128.27" width="0.1524" layer="91"/>
<wire x1="269.24" y1="128.27" x2="266.7" y2="125.73" width="0.1524" layer="91"/>
<label x="269.24" y="128.27" size="1.27" layer="95"/>
</segment>
</net>
<net name="/IPL0" class="0">
<segment>
<wire x1="35.56" y1="83.82" x2="53.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="53.34" y1="83.82" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<label x="40.64" y="83.82" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="95.25" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="106.68" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<label x="99.06" y="106.68" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="IPL0"/>
<wire x1="115.57" y1="90.17" x2="109.22" y2="90.17" width="0.1524" layer="91"/>
<wire x1="109.22" y1="90.17" x2="106.68" y2="87.63" width="0.1524" layer="91"/>
<label x="109.22" y="90.17" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="5"/>
<wire x1="229.87" y1="66.04" x2="247.65" y2="66.04" width="0.1524" layer="91"/>
<wire x1="247.65" y1="66.04" x2="250.19" y2="63.5" width="0.1524" layer="91"/>
<label x="234.95" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="IPL0"/>
<wire x1="275.59" y1="110.49" x2="269.24" y2="110.49" width="0.1524" layer="91"/>
<wire x1="269.24" y1="110.49" x2="266.7" y2="107.95" width="0.1524" layer="91"/>
<label x="269.24" y="110.49" size="1.27" layer="95"/>
</segment>
</net>
<net name="/IPL1" class="0">
<segment>
<wire x1="35.56" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<label x="40.64" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="95.25" y1="109.22" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<wire x1="104.14" y1="109.22" x2="106.68" y2="106.68" width="0.1524" layer="91"/>
<label x="99.06" y="109.22" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="IPL1"/>
<wire x1="115.57" y1="87.63" x2="109.22" y2="87.63" width="0.1524" layer="91"/>
<wire x1="109.22" y1="87.63" x2="106.68" y2="85.09" width="0.1524" layer="91"/>
<label x="109.22" y="87.63" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="3"/>
<wire x1="229.87" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="246.38" y1="63.5" x2="250.19" y2="59.69" width="0.1524" layer="91"/>
<label x="234.95" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="IPL1"/>
<wire x1="275.59" y1="107.95" x2="269.24" y2="107.95" width="0.1524" layer="91"/>
<wire x1="269.24" y1="107.95" x2="266.7" y2="105.41" width="0.1524" layer="91"/>
<label x="269.24" y="107.95" size="1.27" layer="95"/>
</segment>
</net>
<net name="/IPL2" class="0">
<segment>
<wire x1="35.56" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="78.74" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<label x="40.64" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="95.25" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
<wire x1="104.14" y1="111.76" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<label x="99.06" y="111.76" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="IPL2"/>
<wire x1="115.57" y1="85.09" x2="110.49" y2="85.09" width="0.1524" layer="91"/>
<wire x1="110.49" y1="85.09" x2="106.68" y2="81.28" width="0.1524" layer="91"/>
<label x="110.49" y="85.09" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="17"/>
<wire x1="191.77" y1="81.28" x2="203.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="203.2" y1="81.28" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<label x="194.31" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="IPL2"/>
<wire x1="275.59" y1="105.41" x2="270.51" y2="105.41" width="0.1524" layer="91"/>
<wire x1="270.51" y1="105.41" x2="266.7" y2="101.6" width="0.1524" layer="91"/>
<label x="270.51" y="105.41" size="1.27" layer="95"/>
</segment>
</net>
<net name="/OWN" class="0">
<segment>
<label x="16.51" y="124.46" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="124.46" x2="5.08" y2="124.46" width="0.1524" layer="91"/>
<wire x1="5.08" y1="124.46" x2="2.54" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="16"/>
<wire x1="214.63" y1="36.83" x2="208.28" y2="36.83" width="0.1524" layer="91"/>
<wire x1="208.28" y1="36.83" x2="205.74" y2="34.29" width="0.1524" layer="91"/>
<label x="209.55" y="36.83" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="38.1" x2="270.51" y2="38.1" width="0.1524" layer="91"/>
<wire x1="270.51" y1="38.1" x2="267.97" y2="35.56" width="0.1524" layer="91"/>
<label x="271.78" y="38.1" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
</net>
<net name="DOE" class="0">
<segment>
<label x="16.51" y="15.24" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="15.24" x2="5.08" y2="15.24" width="0.1524" layer="91"/>
<wire x1="5.08" y1="15.24" x2="2.54" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="7"/>
<wire x1="191.77" y1="101.6" x2="203.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="203.2" y1="101.6" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<label x="194.31" y="101.6" size="1.27" layer="95"/>
</segment>
</net>
<net name="7MHZ" class="0">
<segment>
<label x="40.64" y="18.0975" size="1.27" layer="95"/>
<wire x1="35.56" y1="17.78" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<wire x1="53.34" y1="17.78" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="106.68" y2="53.34" width="0.1524" layer="91"/>
<label x="99.06" y="58.42" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="CLK"/>
<wire x1="115.57" y1="123.19" x2="109.22" y2="123.19" width="0.1524" layer="91"/>
<wire x1="109.22" y1="123.19" x2="106.68" y2="120.65" width="0.1524" layer="91"/>
<label x="109.22" y="123.19" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="3"/>
<wire x1="191.77" y1="63.5" x2="201.93" y2="63.5" width="0.1524" layer="91"/>
<wire x1="201.93" y1="63.5" x2="205.74" y2="59.69" width="0.1524" layer="91"/>
<label x="194.31" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="CLK"/>
<wire x1="275.59" y1="143.51" x2="269.24" y2="143.51" width="0.1524" layer="91"/>
<wire x1="269.24" y1="143.51" x2="266.7" y2="140.97" width="0.1524" layer="91"/>
<label x="269.24" y="143.51" size="1.27" layer="95"/>
</segment>
</net>
<net name="/EINT1" class="0">
<segment>
<label x="40.64" y="13.0175" size="1.27" layer="95"/>
<wire x1="35.56" y1="12.7" x2="52.07" y2="12.7" width="0.1524" layer="91"/>
<wire x1="52.07" y1="12.7" x2="55.88" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="6"/>
<wire x1="176.53" y1="99.06" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<wire x1="160.02" y1="99.06" x2="157.48" y2="96.52" width="0.1524" layer="91"/>
<label x="165.1" y="99.06" size="1.27" layer="95"/>
</segment>
</net>
<net name="/GBG" class="0">
<segment>
<label x="16.51" y="12.7" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="12.7" x2="6.35" y2="12.7" width="0.1524" layer="91"/>
<wire x1="6.35" y1="12.7" x2="2.54" y2="8.89" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="5"/>
<wire x1="191.77" y1="99.06" x2="203.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="99.06" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<label x="194.31" y="99.06" size="1.27" layer="95"/>
</segment>
</net>
<net name="/FCS" class="0">
<segment>
<label x="16.51" y="10.16" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="10.16" x2="6.35" y2="10.16" width="0.1524" layer="91"/>
<wire x1="6.35" y1="10.16" x2="2.54" y2="6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="3"/>
<wire x1="191.77" y1="96.52" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="203.2" y1="96.52" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<label x="194.31" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="/DS1" class="0">
<segment>
<label x="40.64" y="10.4775" size="1.27" layer="95"/>
<wire x1="35.56" y1="10.16" x2="52.07" y2="10.16" width="0.1524" layer="91"/>
<wire x1="52.07" y1="10.16" x2="55.88" y2="6.35" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="4"/>
<wire x1="176.53" y1="96.52" x2="161.29" y2="96.52" width="0.1524" layer="91"/>
<wire x1="161.29" y1="96.52" x2="157.48" y2="92.71" width="0.1524" layer="91"/>
<label x="165.1" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="IORST" class="0">
<segment>
<label x="40.64" y="15.5575" size="1.27" layer="95"/>
<wire x1="35.56" y1="15.24" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="15.24" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="8"/>
<wire x1="176.53" y1="101.6" x2="160.02" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="101.6" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<label x="165.1" y="101.6" size="1.27" layer="95"/>
</segment>
</net>
<net name="/CCKQ" class="0">
<segment>
<wire x1="35.56" y1="116.84" x2="53.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="53.34" y1="116.84" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="95.25" y1="66.04" x2="102.87" y2="66.04" width="0.1524" layer="91"/>
<wire x1="102.87" y1="66.04" x2="106.68" y2="62.23" width="0.1524" layer="91"/>
<label x="99.06" y="66.04" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="11"/>
<wire x1="229.87" y1="31.75" x2="247.65" y2="31.75" width="0.1524" layer="91"/>
<wire x1="247.65" y1="31.75" x2="250.19" y2="29.21" width="0.1524" layer="91"/>
<label x="234.95" y="31.75" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="284.48" y1="33.02" x2="302.26" y2="33.02" width="0.1524" layer="91"/>
<wire x1="302.26" y1="33.02" x2="304.8" y2="30.48" width="0.1524" layer="91"/>
<label x="289.56" y="33.02" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="6"/>
</segment>
</net>
<net name="Z3" class="0">
<segment>
<label x="16.51" y="17.78" size="1.27" layer="95" rot="R180"/>
<wire x1="20.32" y1="17.78" x2="5.08" y2="17.78" width="0.1524" layer="91"/>
<wire x1="5.08" y1="17.78" x2="2.54" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BOSS" class="0">
<segment>
<wire x1="95.25" y1="86.36" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
<wire x1="104.14" y1="86.36" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<label x="99.06" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="18"/>
<wire x1="214.63" y1="39.37" x2="208.28" y2="39.37" width="0.1524" layer="91"/>
<wire x1="208.28" y1="39.37" x2="205.74" y2="36.83" width="0.1524" layer="91"/>
</segment>
</net>
<net name="28M" class="0">
<segment>
<wire x1="95.25" y1="55.88" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="101.6" y1="55.88" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<label x="99.06" y="55.88" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="15"/>
<wire x1="229.87" y1="36.83" x2="247.65" y2="36.83" width="0.1524" layer="91"/>
<wire x1="247.65" y1="36.83" x2="250.19" y2="34.29" width="0.1524" layer="91"/>
<label x="234.95" y="36.83" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="284.48" y1="38.1" x2="302.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="302.26" y1="38.1" x2="304.8" y2="35.56" width="0.1524" layer="91"/>
<label x="289.56" y="38.1" size="1.27" layer="95"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
