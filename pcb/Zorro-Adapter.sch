<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.05" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="3" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="5" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="7" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="tdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="bdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SUPPLY1">
<packages>
</packages>
<symbols>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-1.27" size="1.016" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND" uservalue="yes">
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-ml">
<description>&lt;b&gt;Harting  Connectors&lt;/b&gt;&lt;p&gt;
Low profile connectors, straight&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="ML20">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-15.24" y1="3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="-15.24" y1="3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-16.51" y2="-4.445" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="10.922" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.858" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.699" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="13.97" y1="4.445" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.445" x2="13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="-15.24" y1="4.699" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="9.398" y2="-3.429" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="10.922" y2="-3.429" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="9.398" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="-3.429" x2="15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="3.429" x2="-15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="3.429" x2="-15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="-3.429" x2="-8.382" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.937" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.937" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-4.445" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-9.271" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.429" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-8.382" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-6.858" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-8.382" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.937" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.937" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-9.271" y1="-4.445" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-5.969" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.969" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-16.51" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-13.97" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-13.97" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">20</text>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="20P">
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="13" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="17" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="19" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="10" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="14" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="16" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="18" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas"/>
<pin name="20" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ML20" prefix="SV" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="20P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML20">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="17" pad="17"/>
<connect gate="1" pin="18" pad="18"/>
<connect gate="1" pin="19" pad="19"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="20" pad="20"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Amiga">
<packages>
<package name="LKONT_50">
<wire x1="129.54" y1="9.9378" x2="129.54" y2="0" width="0" layer="20"/>
<wire x1="129.54" y1="0" x2="-0.0001" y2="0" width="0" layer="20"/>
<wire x1="-0.0001" y1="0" x2="-0.0001" y2="10.0965" width="0" layer="20"/>
<smd name="1" x="127" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="2" x="127" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="3" x="124.46" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="4" x="124.46" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="5" x="121.92" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="6" x="121.92" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="7" x="119.38" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="8" x="119.38" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="9" x="116.84" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="10" x="116.84" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="11" x="114.3" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="12" x="114.3" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="13" x="111.76" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="14" x="111.76" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="15" x="109.22" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="16" x="109.22" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="17" x="106.68" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="18" x="106.68" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="19" x="104.14" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="20" x="104.14" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="21" x="101.6" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="22" x="101.6" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="23" x="99.06" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="24" x="99.06" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="25" x="96.52" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="26" x="96.52" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="27" x="93.98" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="28" x="93.98" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="29" x="91.44" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="30" x="91.44" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="31" x="88.9" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="32" x="88.9" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="33" x="86.36" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="34" x="86.36" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="35" x="83.82" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="36" x="83.82" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="37" x="81.28" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="38" x="81.28" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="39" x="78.74" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="40" x="78.74" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="41" x="76.2" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="42" x="76.2" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="43" x="73.66" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="44" x="73.66" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="45" x="71.12" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="46" x="71.12" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="47" x="68.58" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="48" x="68.58" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="49" x="66.04" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="50" x="66.04" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="51" x="63.5" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="52" x="63.5" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="53" x="60.96" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="54" x="60.96" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="55" x="58.42" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="56" x="58.42" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="57" x="55.88" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="58" x="55.88" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="59" x="53.34" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="60" x="53.34" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="61" x="50.8" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="62" x="50.8" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="63" x="48.26" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="64" x="48.26" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="65" x="45.72" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="66" x="45.72" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="67" x="43.18" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="68" x="43.18" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="69" x="40.64" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="70" x="40.64" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="71" x="38.1" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="72" x="38.1" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="73" x="35.56" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="74" x="35.56" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="75" x="33.02" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="76" x="33.02" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="77" x="30.48" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="78" x="30.48" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="79" x="27.94" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="80" x="27.94" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="81" x="25.4" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="82" x="25.4" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="83" x="22.86" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="84" x="22.86" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="85" x="20.32" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="86" x="20.32" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="87" x="17.78" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="88" x="17.78" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="89" x="15.24" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="90" x="15.24" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="91" x="12.7" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="92" x="12.7" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="93" x="10.16" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="94" x="10.16" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="95" x="7.62" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="96" x="7.62" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="97" x="5.08" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="98" x="5.08" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<smd name="99" x="2.54" y="5.334" dx="1.65" dy="9.5" layer="16" rot="R180"/>
<smd name="100" x="2.54" y="5.334" dx="1.65" dy="9.5" layer="1" rot="R180"/>
<text x="129.2225" y="2.0002" size="1.27" layer="22" rot="MR0">1</text>
<text x="2.0637" y="10.922" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="10.1599" y="10.9537" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LKONT_50_2">
<wire x1="129.286" y1="8.5409" x2="129.286" y2="0.0317" width="0" layer="20"/>
<wire x1="129.286" y1="0.0317" x2="-0.2541" y2="0.0318" width="0" layer="20"/>
<wire x1="-0.2541" y1="0.0318" x2="-0.2541" y2="8.5407" width="0" layer="20"/>
<smd name="1" x="127" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="2" x="127" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="3" x="124.46" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="4" x="124.46" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="5" x="121.92" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="6" x="121.92" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="7" x="119.38" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="8" x="119.38" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="9" x="116.84" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="10" x="116.84" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="11" x="114.3" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="12" x="114.3" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="13" x="111.76" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="14" x="111.76" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="15" x="109.22" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="16" x="109.22" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="17" x="106.68" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="18" x="106.68" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="19" x="104.14" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="20" x="104.14" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="21" x="101.6" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="22" x="101.6" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="23" x="99.06" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="24" x="99.06" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="25" x="96.52" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="26" x="96.52" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="27" x="93.98" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="28" x="93.98" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="29" x="91.44" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="30" x="91.44" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="31" x="88.9" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="32" x="88.9" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="33" x="86.36" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="34" x="86.36" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="35" x="83.82" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="36" x="83.82" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="37" x="81.28" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="38" x="81.28" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="39" x="78.74" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="40" x="78.74" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="41" x="76.2" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="42" x="76.2" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="43" x="73.66" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="44" x="73.66" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="45" x="71.12" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="46" x="71.12" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="47" x="68.58" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="48" x="68.58" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="49" x="66.04" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="50" x="66.04" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="51" x="63.5" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="52" x="63.5" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="53" x="60.96" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="54" x="60.96" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="55" x="58.42" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="56" x="58.42" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="57" x="55.88" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="58" x="55.88" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="59" x="53.34" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="60" x="53.34" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="61" x="50.8" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="62" x="50.8" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="63" x="48.26" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="64" x="48.26" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="65" x="45.72" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="66" x="45.72" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="67" x="43.18" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="68" x="43.18" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="69" x="40.64" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="70" x="40.64" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="71" x="38.1" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="72" x="38.1" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="73" x="35.56" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="74" x="35.56" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="75" x="33.02" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="76" x="33.02" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="77" x="30.48" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="78" x="30.48" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="79" x="27.94" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="80" x="27.94" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="81" x="25.4" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="82" x="25.4" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="83" x="22.86" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="84" x="22.86" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="85" x="20.32" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="86" x="20.32" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="87" x="17.78" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="88" x="17.78" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="89" x="15.24" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="90" x="15.24" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="91" x="12.7" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="92" x="12.7" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="93" x="10.16" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="94" x="10.16" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="95" x="7.62" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="96" x="7.62" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="97" x="5.08" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="98" x="5.08" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<smd name="99" x="2.54" y="3.6195" dx="1.65" dy="6" layer="16" rot="R180"/>
<smd name="100" x="2.54" y="3.6195" dx="1.65" dy="6" layer="1" rot="R180"/>
<text x="129.2225" y="1.2382" size="1.27" layer="22" rot="MR0">1</text>
<text x="3.0163" y="8.636" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="20.3201" y="8.6677" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SLOT_100">
<wire x1="69.85" y1="4.6038" x2="-64.77" y2="4.6038" width="0.127" layer="21"/>
<wire x1="-64.77" y1="4.6038" x2="-64.77" y2="-4.6038" width="0.127" layer="21"/>
<wire x1="-64.77" y1="-4.6038" x2="69.85" y2="-4.6038" width="0.127" layer="21"/>
<wire x1="69.85" y1="-4.6038" x2="69.85" y2="4.6038" width="0.127" layer="21"/>
<wire x1="-40.005" y1="3.175" x2="-38.735" y2="3.175" width="0.127" layer="21"/>
<wire x1="-38.735" y1="3.175" x2="-38.1" y2="2.54" width="0.127" layer="21"/>
<wire x1="-38.1" y1="2.54" x2="-37.465" y2="3.175" width="0.127" layer="21"/>
<wire x1="-37.465" y1="3.175" x2="-36.195" y2="3.175" width="0.127" layer="21"/>
<wire x1="-36.195" y1="3.175" x2="-35.56" y2="2.54" width="0.127" layer="21"/>
<wire x1="-40.005" y1="3.175" x2="-40.64" y2="2.54" width="0.127" layer="21"/>
<wire x1="-38.1" y1="-2.54" x2="-38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-35.56" y1="-2.54" x2="-36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-36.195" y1="-3.175" x2="-37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-37.465" y1="-3.175" x2="-38.1" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-40.64" y1="-2.54" x2="-40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-38.735" y1="-3.175" x2="-40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-60.325" y1="3.175" x2="-59.055" y2="3.175" width="0.127" layer="21"/>
<wire x1="-59.055" y1="3.175" x2="-58.42" y2="2.54" width="0.127" layer="21"/>
<wire x1="-58.42" y1="2.54" x2="-57.785" y2="3.175" width="0.127" layer="21"/>
<wire x1="-57.785" y1="3.175" x2="-56.515" y2="3.175" width="0.127" layer="21"/>
<wire x1="-56.515" y1="3.175" x2="-55.88" y2="2.54" width="0.127" layer="21"/>
<wire x1="-60.325" y1="3.175" x2="-60.96" y2="2.54" width="0.127" layer="21"/>
<wire x1="-55.88" y1="2.54" x2="-55.245" y2="3.175" width="0.127" layer="21"/>
<wire x1="-55.245" y1="3.175" x2="-53.975" y2="3.175" width="0.127" layer="21"/>
<wire x1="-53.975" y1="3.175" x2="-53.34" y2="2.54" width="0.127" layer="21"/>
<wire x1="-52.705" y1="3.175" x2="-51.435" y2="3.175" width="0.127" layer="21"/>
<wire x1="-51.435" y1="3.175" x2="-50.8" y2="2.54" width="0.127" layer="21"/>
<wire x1="-50.8" y1="2.54" x2="-50.165" y2="3.175" width="0.127" layer="21"/>
<wire x1="-50.165" y1="3.175" x2="-48.895" y2="3.175" width="0.127" layer="21"/>
<wire x1="-48.895" y1="3.175" x2="-48.26" y2="2.54" width="0.127" layer="21"/>
<wire x1="-52.705" y1="3.175" x2="-53.34" y2="2.54" width="0.127" layer="21"/>
<wire x1="-48.26" y1="2.54" x2="-47.625" y2="3.175" width="0.127" layer="21"/>
<wire x1="-47.625" y1="3.175" x2="-46.355" y2="3.175" width="0.127" layer="21"/>
<wire x1="-46.355" y1="3.175" x2="-45.72" y2="2.54" width="0.127" layer="21"/>
<wire x1="-58.42" y1="-2.54" x2="-59.055" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-55.88" y1="-2.54" x2="-56.515" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-56.515" y1="-3.175" x2="-57.785" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-57.785" y1="-3.175" x2="-58.42" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-60.96" y1="2.54" x2="-60.96" y2="1.27" width="0.127" layer="21"/>
<wire x1="-60.96" y1="1.27" x2="-62.23" y2="1.27" width="0.127" layer="21"/>
<wire x1="-62.23" y1="1.27" x2="-62.23" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-62.23" y1="-1.27" x2="-60.96" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-60.96" y1="-1.27" x2="-60.96" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-60.96" y1="-2.54" x2="-60.325" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-59.055" y1="-3.175" x2="-60.325" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-53.34" y1="-2.54" x2="-53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-53.975" y1="-3.175" x2="-55.245" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-55.245" y1="-3.175" x2="-55.88" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-50.8" y1="-2.54" x2="-51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-48.26" y1="-2.54" x2="-48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-48.895" y1="-3.175" x2="-50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-50.165" y1="-3.175" x2="-50.8" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-53.34" y1="-2.54" x2="-52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-51.435" y1="-3.175" x2="-52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-45.72" y1="-2.54" x2="-46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-46.355" y1="-3.175" x2="-47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-47.625" y1="-3.175" x2="-48.26" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-45.085" y1="3.175" x2="-43.815" y2="3.175" width="0.127" layer="21"/>
<wire x1="-43.815" y1="3.175" x2="-43.18" y2="2.54" width="0.127" layer="21"/>
<wire x1="-43.18" y1="2.54" x2="-42.545" y2="3.175" width="0.127" layer="21"/>
<wire x1="-42.545" y1="3.175" x2="-41.275" y2="3.175" width="0.127" layer="21"/>
<wire x1="-41.275" y1="3.175" x2="-40.64" y2="2.54" width="0.127" layer="21"/>
<wire x1="-45.085" y1="3.175" x2="-45.72" y2="2.54" width="0.127" layer="21"/>
<wire x1="-43.18" y1="-2.54" x2="-43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-40.64" y1="-2.54" x2="-41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-41.275" y1="-3.175" x2="-42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-42.545" y1="-3.175" x2="-43.18" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-45.72" y1="-2.54" x2="-45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-43.815" y1="-3.175" x2="-45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="3.175" x2="-15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="-34.925" y1="3.175" x2="-33.655" y2="3.175" width="0.127" layer="21"/>
<wire x1="-32.385" y1="3.175" x2="-31.115" y2="3.175" width="0.127" layer="21"/>
<wire x1="-29.845" y1="3.175" x2="-28.575" y2="3.175" width="0.127" layer="21"/>
<wire x1="-27.305" y1="3.175" x2="-26.035" y2="3.175" width="0.127" layer="21"/>
<wire x1="-24.765" y1="3.175" x2="-23.495" y2="3.175" width="0.127" layer="21"/>
<wire x1="-22.225" y1="3.175" x2="-20.955" y2="3.175" width="0.127" layer="21"/>
<wire x1="-19.685" y1="3.175" x2="-18.415" y2="3.175" width="0.127" layer="21"/>
<wire x1="-17.145" y1="3.175" x2="-15.875" y2="3.175" width="0.127" layer="21"/>
<wire x1="-17.145" y1="-3.175" x2="-15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-19.685" y1="-3.175" x2="-18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-22.225" y1="-3.175" x2="-20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-24.765" y1="-3.175" x2="-23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-27.305" y1="-3.175" x2="-26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-29.845" y1="-3.175" x2="-28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-32.385" y1="-3.175" x2="-31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-34.925" y1="-3.175" x2="-33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-33.655" y1="3.175" x2="-33.02" y2="2.54" width="0.127" layer="21"/>
<wire x1="-31.115" y1="3.175" x2="-30.48" y2="2.54" width="0.127" layer="21"/>
<wire x1="-28.575" y1="3.175" x2="-27.94" y2="2.54" width="0.127" layer="21"/>
<wire x1="-26.035" y1="3.175" x2="-25.4" y2="2.54" width="0.127" layer="21"/>
<wire x1="-23.495" y1="3.175" x2="-22.86" y2="2.54" width="0.127" layer="21"/>
<wire x1="-20.955" y1="3.175" x2="-20.32" y2="2.54" width="0.127" layer="21"/>
<wire x1="-18.415" y1="3.175" x2="-17.78" y2="2.54" width="0.127" layer="21"/>
<wire x1="-34.925" y1="3.175" x2="-35.56" y2="2.54" width="0.127" layer="21"/>
<wire x1="-32.385" y1="3.175" x2="-33.02" y2="2.54" width="0.127" layer="21"/>
<wire x1="-29.845" y1="3.175" x2="-30.48" y2="2.54" width="0.127" layer="21"/>
<wire x1="-27.305" y1="3.175" x2="-27.94" y2="2.54" width="0.127" layer="21"/>
<wire x1="-24.765" y1="3.175" x2="-25.4" y2="2.54" width="0.127" layer="21"/>
<wire x1="-22.225" y1="3.175" x2="-22.86" y2="2.54" width="0.127" layer="21"/>
<wire x1="-19.685" y1="3.175" x2="-20.32" y2="2.54" width="0.127" layer="21"/>
<wire x1="-17.145" y1="3.175" x2="-17.78" y2="2.54" width="0.127" layer="21"/>
<wire x1="-33.02" y1="-2.54" x2="-33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-30.48" y1="-2.54" x2="-31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-2.54" x2="-28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-2.54" x2="-26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-22.86" y1="-2.54" x2="-23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-20.32" y1="-2.54" x2="-20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-17.78" y1="-2.54" x2="-18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.24" y1="-2.54" x2="-15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-33.02" y1="-2.54" x2="-32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-35.56" y1="-2.54" x2="-34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-30.48" y1="-2.54" x2="-29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-27.94" y1="-2.54" x2="-27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-25.4" y1="-2.54" x2="-24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-22.86" y1="-2.54" x2="-22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-20.32" y1="-2.54" x2="-19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-17.78" y1="-2.54" x2="-17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-10.795" y1="3.175" x2="-10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="-14.605" y1="3.175" x2="-13.335" y2="3.175" width="0.127" layer="21"/>
<wire x1="-12.065" y1="3.175" x2="-10.795" y2="3.175" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-3.175" x2="-10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-14.605" y1="-3.175" x2="-13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-12.7" y2="2.54" width="0.127" layer="21"/>
<wire x1="-14.605" y1="3.175" x2="-15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="-12.065" y1="3.175" x2="-12.7" y2="2.54" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="-13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="-10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.24" y1="-2.54" x2="-14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-12.7" y1="-2.54" x2="-12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="3.175" x2="-8.255" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.985" y1="3.175" x2="-5.715" y2="3.175" width="0.127" layer="21"/>
<wire x1="-6.985" y1="-3.175" x2="-5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-3.175" x2="-8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.175" x2="-7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="3.175" x2="-10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="-6.985" y1="3.175" x2="-7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-10.16" y1="-2.54" x2="-9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-2.54" x2="-6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="-3.175" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.905" y1="3.175" x2="-0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-3.175" x2="-0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-3.175" x2="-3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.175" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.905" y1="3.175" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.175" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="3.175" x2="1.905" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.175" y1="3.175" x2="4.445" y2="3.175" width="0.127" layer="21"/>
<wire x1="3.175" y1="-3.175" x2="4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="0.635" y1="-3.175" x2="1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.175" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="3.175" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.175" y1="3.175" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="9.525" y1="3.175" x2="10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.175" x2="6.985" y2="3.175" width="0.127" layer="21"/>
<wire x1="8.255" y1="3.175" x2="9.525" y2="3.175" width="0.127" layer="21"/>
<wire x1="8.255" y1="-3.175" x2="9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.715" y1="-3.175" x2="6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="6.985" y1="3.175" x2="7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.175" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="8.255" y1="3.175" x2="7.62" y2="2.54" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.16" y1="-2.54" x2="9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="7.62" y1="-2.54" x2="8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="14.605" y1="3.175" x2="15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="10.795" y1="3.175" x2="12.065" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.335" y1="3.175" x2="14.605" y2="3.175" width="0.127" layer="21"/>
<wire x1="13.335" y1="-3.175" x2="14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.795" y1="-3.175" x2="12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.175" x2="12.7" y2="2.54" width="0.127" layer="21"/>
<wire x1="10.795" y1="3.175" x2="10.16" y2="2.54" width="0.127" layer="21"/>
<wire x1="13.335" y1="3.175" x2="12.7" y2="2.54" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="15.24" y1="-2.54" x2="14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.16" y1="-2.54" x2="10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="12.7" y1="-2.54" x2="13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="20.32" y2="2.54" width="0.127" layer="21"/>
<wire x1="15.875" y1="3.175" x2="17.145" y2="3.175" width="0.127" layer="21"/>
<wire x1="18.415" y1="3.175" x2="19.685" y2="3.175" width="0.127" layer="21"/>
<wire x1="18.415" y1="-3.175" x2="19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="15.875" y1="-3.175" x2="17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="17.145" y1="3.175" x2="17.78" y2="2.54" width="0.127" layer="21"/>
<wire x1="15.875" y1="3.175" x2="15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="18.415" y1="3.175" x2="17.78" y2="2.54" width="0.127" layer="21"/>
<wire x1="17.78" y1="-2.54" x2="17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="20.32" y1="-2.54" x2="19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="15.24" y1="-2.54" x2="15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="17.78" y1="-2.54" x2="18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="24.765" y1="3.175" x2="25.4" y2="2.54" width="0.127" layer="21"/>
<wire x1="20.955" y1="3.175" x2="22.225" y2="3.175" width="0.127" layer="21"/>
<wire x1="23.495" y1="3.175" x2="24.765" y2="3.175" width="0.127" layer="21"/>
<wire x1="23.495" y1="-3.175" x2="24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="20.955" y1="-3.175" x2="22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="22.225" y1="3.175" x2="22.86" y2="2.54" width="0.127" layer="21"/>
<wire x1="20.955" y1="3.175" x2="20.32" y2="2.54" width="0.127" layer="21"/>
<wire x1="23.495" y1="3.175" x2="22.86" y2="2.54" width="0.127" layer="21"/>
<wire x1="22.86" y1="-2.54" x2="22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="25.4" y1="-2.54" x2="24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="20.32" y1="-2.54" x2="20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="22.86" y1="-2.54" x2="23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.845" y1="3.175" x2="30.48" y2="2.54" width="0.127" layer="21"/>
<wire x1="26.035" y1="3.175" x2="27.305" y2="3.175" width="0.127" layer="21"/>
<wire x1="28.575" y1="3.175" x2="29.845" y2="3.175" width="0.127" layer="21"/>
<wire x1="28.575" y1="-3.175" x2="29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="26.035" y1="-3.175" x2="27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="27.305" y1="3.175" x2="27.94" y2="2.54" width="0.127" layer="21"/>
<wire x1="26.035" y1="3.175" x2="25.4" y2="2.54" width="0.127" layer="21"/>
<wire x1="28.575" y1="3.175" x2="27.94" y2="2.54" width="0.127" layer="21"/>
<wire x1="27.94" y1="-2.54" x2="27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="30.48" y1="-2.54" x2="29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="25.4" y1="-2.54" x2="26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="27.94" y1="-2.54" x2="28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.925" y1="3.175" x2="35.56" y2="2.54" width="0.127" layer="21"/>
<wire x1="31.115" y1="3.175" x2="32.385" y2="3.175" width="0.127" layer="21"/>
<wire x1="33.655" y1="3.175" x2="34.925" y2="3.175" width="0.127" layer="21"/>
<wire x1="33.655" y1="-3.175" x2="34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="31.115" y1="-3.175" x2="32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="32.385" y1="3.175" x2="33.02" y2="2.54" width="0.127" layer="21"/>
<wire x1="31.115" y1="3.175" x2="30.48" y2="2.54" width="0.127" layer="21"/>
<wire x1="33.655" y1="3.175" x2="33.02" y2="2.54" width="0.127" layer="21"/>
<wire x1="33.02" y1="-2.54" x2="32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="35.56" y1="-2.54" x2="34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="30.48" y1="-2.54" x2="31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="33.02" y1="-2.54" x2="33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="40.005" y1="3.175" x2="40.64" y2="2.54" width="0.127" layer="21"/>
<wire x1="36.195" y1="3.175" x2="37.465" y2="3.175" width="0.127" layer="21"/>
<wire x1="38.735" y1="3.175" x2="40.005" y2="3.175" width="0.127" layer="21"/>
<wire x1="38.735" y1="-3.175" x2="40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="36.195" y1="-3.175" x2="37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="37.465" y1="3.175" x2="38.1" y2="2.54" width="0.127" layer="21"/>
<wire x1="36.195" y1="3.175" x2="35.56" y2="2.54" width="0.127" layer="21"/>
<wire x1="38.735" y1="3.175" x2="38.1" y2="2.54" width="0.127" layer="21"/>
<wire x1="38.1" y1="-2.54" x2="37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="40.64" y1="-2.54" x2="40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="35.56" y1="-2.54" x2="36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="38.1" y1="-2.54" x2="38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="45.085" y1="3.175" x2="45.72" y2="2.54" width="0.127" layer="21"/>
<wire x1="41.275" y1="3.175" x2="42.545" y2="3.175" width="0.127" layer="21"/>
<wire x1="43.815" y1="3.175" x2="45.085" y2="3.175" width="0.127" layer="21"/>
<wire x1="43.815" y1="-3.175" x2="45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="41.275" y1="-3.175" x2="42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="42.545" y1="3.175" x2="43.18" y2="2.54" width="0.127" layer="21"/>
<wire x1="41.275" y1="3.175" x2="40.64" y2="2.54" width="0.127" layer="21"/>
<wire x1="43.815" y1="3.175" x2="43.18" y2="2.54" width="0.127" layer="21"/>
<wire x1="43.18" y1="-2.54" x2="42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="45.72" y1="-2.54" x2="45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="40.64" y1="-2.54" x2="41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="43.18" y1="-2.54" x2="43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="50.165" y1="3.175" x2="50.8" y2="2.54" width="0.127" layer="21"/>
<wire x1="46.355" y1="3.175" x2="47.625" y2="3.175" width="0.127" layer="21"/>
<wire x1="48.895" y1="3.175" x2="50.165" y2="3.175" width="0.127" layer="21"/>
<wire x1="48.895" y1="-3.175" x2="50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="46.355" y1="-3.175" x2="47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="47.625" y1="3.175" x2="48.26" y2="2.54" width="0.127" layer="21"/>
<wire x1="46.355" y1="3.175" x2="45.72" y2="2.54" width="0.127" layer="21"/>
<wire x1="48.895" y1="3.175" x2="48.26" y2="2.54" width="0.127" layer="21"/>
<wire x1="48.26" y1="-2.54" x2="47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="50.8" y1="-2.54" x2="50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="45.72" y1="-2.54" x2="46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="48.26" y1="-2.54" x2="48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="55.245" y1="3.175" x2="55.88" y2="2.54" width="0.127" layer="21"/>
<wire x1="51.435" y1="3.175" x2="52.705" y2="3.175" width="0.127" layer="21"/>
<wire x1="53.975" y1="3.175" x2="55.245" y2="3.175" width="0.127" layer="21"/>
<wire x1="53.975" y1="-3.175" x2="55.245" y2="-3.175" width="0.127" layer="21"/>
<wire x1="51.435" y1="-3.175" x2="52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="52.705" y1="3.175" x2="53.34" y2="2.54" width="0.127" layer="21"/>
<wire x1="51.435" y1="3.175" x2="50.8" y2="2.54" width="0.127" layer="21"/>
<wire x1="53.975" y1="3.175" x2="53.34" y2="2.54" width="0.127" layer="21"/>
<wire x1="53.34" y1="-2.54" x2="52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="55.88" y1="-2.54" x2="55.245" y2="-3.175" width="0.127" layer="21"/>
<wire x1="50.8" y1="-2.54" x2="51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="53.34" y1="-2.54" x2="53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="60.325" y1="3.175" x2="60.96" y2="2.54" width="0.127" layer="21"/>
<wire x1="56.515" y1="3.175" x2="57.785" y2="3.175" width="0.127" layer="21"/>
<wire x1="59.055" y1="3.175" x2="60.325" y2="3.175" width="0.127" layer="21"/>
<wire x1="59.055" y1="-3.175" x2="60.325" y2="-3.175" width="0.127" layer="21"/>
<wire x1="56.515" y1="-3.175" x2="57.785" y2="-3.175" width="0.127" layer="21"/>
<wire x1="57.785" y1="3.175" x2="58.42" y2="2.54" width="0.127" layer="21"/>
<wire x1="56.515" y1="3.175" x2="55.88" y2="2.54" width="0.127" layer="21"/>
<wire x1="59.055" y1="3.175" x2="58.42" y2="2.54" width="0.127" layer="21"/>
<wire x1="58.42" y1="-2.54" x2="57.785" y2="-3.175" width="0.127" layer="21"/>
<wire x1="60.96" y1="-2.54" x2="60.325" y2="-3.175" width="0.127" layer="21"/>
<wire x1="55.88" y1="-2.54" x2="56.515" y2="-3.175" width="0.127" layer="21"/>
<wire x1="58.42" y1="-2.54" x2="59.055" y2="-3.175" width="0.127" layer="21"/>
<wire x1="65.405" y1="3.175" x2="66.04" y2="2.54" width="0.127" layer="21"/>
<wire x1="61.595" y1="3.175" x2="62.865" y2="3.175" width="0.127" layer="21"/>
<wire x1="64.135" y1="3.175" x2="65.405" y2="3.175" width="0.127" layer="21"/>
<wire x1="64.135" y1="-3.175" x2="65.405" y2="-3.175" width="0.127" layer="21"/>
<wire x1="61.595" y1="-3.175" x2="62.865" y2="-3.175" width="0.127" layer="21"/>
<wire x1="62.865" y1="3.175" x2="63.5" y2="2.54" width="0.127" layer="21"/>
<wire x1="61.595" y1="3.175" x2="60.96" y2="2.54" width="0.127" layer="21"/>
<wire x1="64.135" y1="3.175" x2="63.5" y2="2.54" width="0.127" layer="21"/>
<wire x1="63.5" y1="-2.54" x2="62.865" y2="-3.175" width="0.127" layer="21"/>
<wire x1="66.04" y1="-2.54" x2="65.405" y2="-3.175" width="0.127" layer="21"/>
<wire x1="60.96" y1="-2.54" x2="61.595" y2="-3.175" width="0.127" layer="21"/>
<wire x1="63.5" y1="-2.54" x2="64.135" y2="-3.175" width="0.127" layer="21"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="1.27" width="0.127" layer="21"/>
<wire x1="66.04" y1="1.27" x2="67.31" y2="1.27" width="0.127" layer="21"/>
<wire x1="67.31" y1="1.27" x2="67.31" y2="-1.27" width="0.127" layer="21"/>
<wire x1="67.31" y1="-1.27" x2="66.04" y2="-1.27" width="0.127" layer="21"/>
<wire x1="66.04" y1="-1.27" x2="66.04" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-59.69" y="-2.54" drill="0.9" diameter="1.5"/>
<pad name="2" x="-59.69" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="3" x="-57.15" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="4" x="-57.15" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="5" x="-54.61" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="6" x="-54.61" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="7" x="-52.07" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="8" x="-52.07" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="9" x="-49.53" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="10" x="-49.53" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="11" x="-46.99" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="12" x="-46.99" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="13" x="-44.45" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="14" x="-44.45" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="15" x="-41.91" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="16" x="-41.91" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="17" x="-39.37" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="18" x="-39.37" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="19" x="-36.83" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="20" x="-36.83" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="21" x="-34.29" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="22" x="-34.29" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="23" x="-31.75" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="24" x="-31.75" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="25" x="-29.21" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="26" x="-29.21" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="27" x="-26.67" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="28" x="-26.67" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="29" x="-24.13" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="30" x="-24.13" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="31" x="-21.59" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="32" x="-21.59" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="33" x="-19.05" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="34" x="-19.05" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="35" x="-16.51" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="36" x="-16.51" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="37" x="-13.97" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="38" x="-13.97" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="39" x="-11.43" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="40" x="-11.43" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="41" x="-8.89" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="42" x="-8.89" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="43" x="-6.35" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="44" x="-6.35" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="45" x="-3.81" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="46" x="-3.81" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="47" x="-1.27" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="48" x="-1.27" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="49" x="1.27" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="50" x="1.27" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="51" x="3.81" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="52" x="3.81" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="53" x="6.35" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="54" x="6.35" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="55" x="8.89" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="56" x="8.89" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="57" x="11.43" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="58" x="11.43" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="59" x="13.97" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="60" x="13.97" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="61" x="16.51" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="62" x="16.51" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="63" x="19.05" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="64" x="19.05" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="65" x="21.59" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="66" x="21.59" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="67" x="24.13" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="68" x="24.13" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="69" x="26.67" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="70" x="26.67" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="71" x="29.21" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="72" x="29.21" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="73" x="31.75" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="74" x="31.75" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="75" x="34.29" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="76" x="34.29" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="77" x="36.83" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="78" x="36.83" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="79" x="39.37" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="80" x="39.37" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="81" x="41.91" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="82" x="41.91" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="83" x="44.45" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="84" x="44.45" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="85" x="46.99" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="86" x="46.99" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="87" x="49.53" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="88" x="49.53" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="89" x="52.07" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="90" x="52.07" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="91" x="54.61" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="92" x="54.61" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="93" x="57.15" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="94" x="57.15" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="95" x="59.69" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="96" x="59.69" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="97" x="62.23" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="98" x="62.23" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="99" x="64.77" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="100" x="64.77" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<text x="-62.103" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-48.895" y="-0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-58.42" y="-0.381" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SLOT_100_S">
<wire x1="68.58" y1="4.6038" x2="-66.04" y2="4.6038" width="0.127" layer="21"/>
<wire x1="-66.04" y1="4.6038" x2="-66.04" y2="-4.6038" width="0.127" layer="21"/>
<wire x1="-66.04" y1="-4.6038" x2="68.58" y2="-4.6038" width="0.127" layer="21"/>
<wire x1="68.58" y1="-4.6038" x2="68.58" y2="4.6038" width="0.127" layer="21"/>
<wire x1="-41.275" y1="3.175" x2="-40.005" y2="3.175" width="0.127" layer="21"/>
<wire x1="-40.005" y1="3.175" x2="-39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="-39.37" y1="2.54" x2="-38.735" y2="3.175" width="0.127" layer="21"/>
<wire x1="-38.735" y1="3.175" x2="-37.465" y2="3.175" width="0.127" layer="21"/>
<wire x1="-37.465" y1="3.175" x2="-36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="-41.275" y1="3.175" x2="-41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="-39.37" y1="-2.54" x2="-40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-36.83" y1="-2.54" x2="-37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-37.465" y1="-3.175" x2="-38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-38.735" y1="-3.175" x2="-39.37" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-41.91" y1="-2.54" x2="-41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-40.005" y1="-3.175" x2="-41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-61.595" y1="3.175" x2="-60.325" y2="3.175" width="0.127" layer="21"/>
<wire x1="-60.325" y1="3.175" x2="-59.69" y2="2.54" width="0.127" layer="21"/>
<wire x1="-59.69" y1="2.54" x2="-59.055" y2="3.175" width="0.127" layer="21"/>
<wire x1="-59.055" y1="3.175" x2="-57.785" y2="3.175" width="0.127" layer="21"/>
<wire x1="-57.785" y1="3.175" x2="-57.15" y2="2.54" width="0.127" layer="21"/>
<wire x1="-61.595" y1="3.175" x2="-62.23" y2="2.54" width="0.127" layer="21"/>
<wire x1="-57.15" y1="2.54" x2="-56.515" y2="3.175" width="0.127" layer="21"/>
<wire x1="-56.515" y1="3.175" x2="-55.245" y2="3.175" width="0.127" layer="21"/>
<wire x1="-55.245" y1="3.175" x2="-54.61" y2="2.54" width="0.127" layer="21"/>
<wire x1="-53.975" y1="3.175" x2="-52.705" y2="3.175" width="0.127" layer="21"/>
<wire x1="-52.705" y1="3.175" x2="-52.07" y2="2.54" width="0.127" layer="21"/>
<wire x1="-52.07" y1="2.54" x2="-51.435" y2="3.175" width="0.127" layer="21"/>
<wire x1="-51.435" y1="3.175" x2="-50.165" y2="3.175" width="0.127" layer="21"/>
<wire x1="-50.165" y1="3.175" x2="-49.53" y2="2.54" width="0.127" layer="21"/>
<wire x1="-53.975" y1="3.175" x2="-54.61" y2="2.54" width="0.127" layer="21"/>
<wire x1="-49.53" y1="2.54" x2="-48.895" y2="3.175" width="0.127" layer="21"/>
<wire x1="-48.895" y1="3.175" x2="-47.625" y2="3.175" width="0.127" layer="21"/>
<wire x1="-47.625" y1="3.175" x2="-46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="-59.69" y1="-2.54" x2="-60.325" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-57.15" y1="-2.54" x2="-57.785" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-57.785" y1="-3.175" x2="-59.055" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-59.055" y1="-3.175" x2="-59.69" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-62.23" y1="2.54" x2="-62.23" y2="1.27" width="0.127" layer="21"/>
<wire x1="-62.23" y1="1.27" x2="-63.5" y2="1.27" width="0.127" layer="21"/>
<wire x1="-63.5" y1="1.27" x2="-63.5" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-63.5" y1="-1.27" x2="-62.23" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-62.23" y1="-1.27" x2="-62.23" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-62.23" y1="-2.54" x2="-61.595" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-60.325" y1="-3.175" x2="-61.595" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-54.61" y1="-2.54" x2="-55.245" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-55.245" y1="-3.175" x2="-56.515" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-56.515" y1="-3.175" x2="-57.15" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-52.07" y1="-2.54" x2="-52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-49.53" y1="-2.54" x2="-50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-50.165" y1="-3.175" x2="-51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-51.435" y1="-3.175" x2="-52.07" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-54.61" y1="-2.54" x2="-53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-52.705" y1="-3.175" x2="-53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-46.99" y1="-2.54" x2="-47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-47.625" y1="-3.175" x2="-48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-48.895" y1="-3.175" x2="-49.53" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-46.355" y1="3.175" x2="-45.085" y2="3.175" width="0.127" layer="21"/>
<wire x1="-45.085" y1="3.175" x2="-44.45" y2="2.54" width="0.127" layer="21"/>
<wire x1="-44.45" y1="2.54" x2="-43.815" y2="3.175" width="0.127" layer="21"/>
<wire x1="-43.815" y1="3.175" x2="-42.545" y2="3.175" width="0.127" layer="21"/>
<wire x1="-42.545" y1="3.175" x2="-41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="-46.355" y1="3.175" x2="-46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="-44.45" y1="-2.54" x2="-45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-41.91" y1="-2.54" x2="-42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-42.545" y1="-3.175" x2="-43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-43.815" y1="-3.175" x2="-44.45" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-46.99" y1="-2.54" x2="-46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-45.085" y1="-3.175" x2="-46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-17.145" y1="3.175" x2="-16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="-36.195" y1="3.175" x2="-34.925" y2="3.175" width="0.127" layer="21"/>
<wire x1="-33.655" y1="3.175" x2="-32.385" y2="3.175" width="0.127" layer="21"/>
<wire x1="-31.115" y1="3.175" x2="-29.845" y2="3.175" width="0.127" layer="21"/>
<wire x1="-28.575" y1="3.175" x2="-27.305" y2="3.175" width="0.127" layer="21"/>
<wire x1="-26.035" y1="3.175" x2="-24.765" y2="3.175" width="0.127" layer="21"/>
<wire x1="-23.495" y1="3.175" x2="-22.225" y2="3.175" width="0.127" layer="21"/>
<wire x1="-20.955" y1="3.175" x2="-19.685" y2="3.175" width="0.127" layer="21"/>
<wire x1="-18.415" y1="3.175" x2="-17.145" y2="3.175" width="0.127" layer="21"/>
<wire x1="-18.415" y1="-3.175" x2="-17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-20.955" y1="-3.175" x2="-19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-23.495" y1="-3.175" x2="-22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.035" y1="-3.175" x2="-24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-28.575" y1="-3.175" x2="-27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-31.115" y1="-3.175" x2="-29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-33.655" y1="-3.175" x2="-32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-36.195" y1="-3.175" x2="-34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-34.925" y1="3.175" x2="-34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="-32.385" y1="3.175" x2="-31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="-29.845" y1="3.175" x2="-29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="-27.305" y1="3.175" x2="-26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="-24.765" y1="3.175" x2="-24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-22.225" y1="3.175" x2="-21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="-19.685" y1="3.175" x2="-19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="-36.195" y1="3.175" x2="-36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="-33.655" y1="3.175" x2="-34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="-31.115" y1="3.175" x2="-31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="-28.575" y1="3.175" x2="-29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="-26.035" y1="3.175" x2="-26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="-23.495" y1="3.175" x2="-24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-20.955" y1="3.175" x2="-21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="-18.415" y1="3.175" x2="-19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="-34.29" y1="-2.54" x2="-34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-31.75" y1="-2.54" x2="-32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-29.21" y1="-2.54" x2="-29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.67" y1="-2.54" x2="-27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-2.54" x2="-24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-2.54" x2="-22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-2.54" x2="-19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-2.54" x2="-17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-34.29" y1="-2.54" x2="-33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-36.83" y1="-2.54" x2="-36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-31.75" y1="-2.54" x2="-31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-29.21" y1="-2.54" x2="-28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.67" y1="-2.54" x2="-26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-2.54" x2="-23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-2.54" x2="-20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-2.54" x2="-18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-12.065" y1="3.175" x2="-11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="-15.875" y1="3.175" x2="-14.605" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-12.065" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-3.175" x2="-12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-3.175" x2="-14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-14.605" y1="3.175" x2="-13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="-15.875" y1="3.175" x2="-16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-2.54" x2="-14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-2.54" x2="-12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-2.54" x2="-15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-2.54" x2="-13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.985" y1="3.175" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-10.795" y1="3.175" x2="-9.525" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.175" x2="-6.985" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-3.175" x2="-6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-3.175" x2="-9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-9.525" y1="3.175" x2="-8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="-10.795" y1="3.175" x2="-11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.175" x2="-8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-2.54" x2="-9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-2.54" x2="-10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-2.54" x2="-8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.905" y1="3.175" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="-4.445" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.175" x2="-1.905" y2="3.175" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-3.175" x2="-1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-3.175" x2="-4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.175" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.175" y1="3.175" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.175" x2="3.175" y2="3.175" width="0.127" layer="21"/>
<wire x1="1.905" y1="-3.175" x2="3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-3.175" x2="0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="0.635" y1="3.175" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.175" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.255" y1="3.175" x2="8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.175" x2="5.715" y2="3.175" width="0.127" layer="21"/>
<wire x1="6.985" y1="3.175" x2="8.255" y2="3.175" width="0.127" layer="21"/>
<wire x1="6.985" y1="-3.175" x2="8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="4.445" y1="-3.175" x2="5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.175" x2="6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.175" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.985" y1="3.175" x2="6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="13.335" y1="3.175" x2="13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="9.525" y1="3.175" x2="10.795" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.175" x2="13.335" y2="3.175" width="0.127" layer="21"/>
<wire x1="12.065" y1="-3.175" x2="13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="9.525" y1="-3.175" x2="10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.795" y1="3.175" x2="11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="9.525" y1="3.175" x2="8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.175" x2="11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="11.43" y1="-2.54" x2="10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="11.43" y1="-2.54" x2="12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="18.415" y1="3.175" x2="19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="14.605" y1="3.175" x2="15.875" y2="3.175" width="0.127" layer="21"/>
<wire x1="17.145" y1="3.175" x2="18.415" y2="3.175" width="0.127" layer="21"/>
<wire x1="17.145" y1="-3.175" x2="18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="14.605" y1="-3.175" x2="15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="15.875" y1="3.175" x2="16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="14.605" y1="3.175" x2="13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="17.145" y1="3.175" x2="16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="16.51" y1="-2.54" x2="15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="19.05" y1="-2.54" x2="18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="16.51" y1="-2.54" x2="17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="23.495" y1="3.175" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="20.955" y2="3.175" width="0.127" layer="21"/>
<wire x1="22.225" y1="3.175" x2="23.495" y2="3.175" width="0.127" layer="21"/>
<wire x1="22.225" y1="-3.175" x2="23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="19.685" y1="-3.175" x2="20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="20.955" y1="3.175" x2="21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="22.225" y1="3.175" x2="21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="21.59" y1="-2.54" x2="20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="24.13" y1="-2.54" x2="23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="19.05" y1="-2.54" x2="19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="-2.54" x2="22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="28.575" y1="3.175" x2="29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="24.765" y1="3.175" x2="26.035" y2="3.175" width="0.127" layer="21"/>
<wire x1="27.305" y1="3.175" x2="28.575" y2="3.175" width="0.127" layer="21"/>
<wire x1="27.305" y1="-3.175" x2="28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="24.765" y1="-3.175" x2="26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="26.035" y1="3.175" x2="26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="24.765" y1="3.175" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="27.305" y1="3.175" x2="26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="26.67" y1="-2.54" x2="26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.21" y1="-2.54" x2="28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="24.13" y1="-2.54" x2="24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="26.67" y1="-2.54" x2="27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="33.655" y1="3.175" x2="34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="29.845" y1="3.175" x2="31.115" y2="3.175" width="0.127" layer="21"/>
<wire x1="32.385" y1="3.175" x2="33.655" y2="3.175" width="0.127" layer="21"/>
<wire x1="32.385" y1="-3.175" x2="33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.845" y1="-3.175" x2="31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="31.115" y1="3.175" x2="31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="29.845" y1="3.175" x2="29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="32.385" y1="3.175" x2="31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="31.75" y1="-2.54" x2="31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.29" y1="-2.54" x2="33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.21" y1="-2.54" x2="29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="31.75" y1="-2.54" x2="32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="38.735" y1="3.175" x2="39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="34.925" y1="3.175" x2="36.195" y2="3.175" width="0.127" layer="21"/>
<wire x1="37.465" y1="3.175" x2="38.735" y2="3.175" width="0.127" layer="21"/>
<wire x1="37.465" y1="-3.175" x2="38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.925" y1="-3.175" x2="36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="36.195" y1="3.175" x2="36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="34.925" y1="3.175" x2="34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="37.465" y1="3.175" x2="36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="36.83" y1="-2.54" x2="36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="39.37" y1="-2.54" x2="38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.29" y1="-2.54" x2="34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="36.83" y1="-2.54" x2="37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="43.815" y1="3.175" x2="44.45" y2="2.54" width="0.127" layer="21"/>
<wire x1="40.005" y1="3.175" x2="41.275" y2="3.175" width="0.127" layer="21"/>
<wire x1="42.545" y1="3.175" x2="43.815" y2="3.175" width="0.127" layer="21"/>
<wire x1="42.545" y1="-3.175" x2="43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="40.005" y1="-3.175" x2="41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="41.275" y1="3.175" x2="41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="40.005" y1="3.175" x2="39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="42.545" y1="3.175" x2="41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="41.91" y1="-2.54" x2="41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="44.45" y1="-2.54" x2="43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="39.37" y1="-2.54" x2="40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="41.91" y1="-2.54" x2="42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="48.895" y1="3.175" x2="49.53" y2="2.54" width="0.127" layer="21"/>
<wire x1="45.085" y1="3.175" x2="46.355" y2="3.175" width="0.127" layer="21"/>
<wire x1="47.625" y1="3.175" x2="48.895" y2="3.175" width="0.127" layer="21"/>
<wire x1="47.625" y1="-3.175" x2="48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="45.085" y1="-3.175" x2="46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="46.355" y1="3.175" x2="46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="45.085" y1="3.175" x2="44.45" y2="2.54" width="0.127" layer="21"/>
<wire x1="47.625" y1="3.175" x2="46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="46.99" y1="-2.54" x2="46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="49.53" y1="-2.54" x2="48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="44.45" y1="-2.54" x2="45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="46.99" y1="-2.54" x2="47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="53.975" y1="3.175" x2="54.61" y2="2.54" width="0.127" layer="21"/>
<wire x1="50.165" y1="3.175" x2="51.435" y2="3.175" width="0.127" layer="21"/>
<wire x1="52.705" y1="3.175" x2="53.975" y2="3.175" width="0.127" layer="21"/>
<wire x1="52.705" y1="-3.175" x2="53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="50.165" y1="-3.175" x2="51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="51.435" y1="3.175" x2="52.07" y2="2.54" width="0.127" layer="21"/>
<wire x1="50.165" y1="3.175" x2="49.53" y2="2.54" width="0.127" layer="21"/>
<wire x1="52.705" y1="3.175" x2="52.07" y2="2.54" width="0.127" layer="21"/>
<wire x1="52.07" y1="-2.54" x2="51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="54.61" y1="-2.54" x2="53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="49.53" y1="-2.54" x2="50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="52.07" y1="-2.54" x2="52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="59.055" y1="3.175" x2="59.69" y2="2.54" width="0.127" layer="21"/>
<wire x1="55.245" y1="3.175" x2="56.515" y2="3.175" width="0.127" layer="21"/>
<wire x1="57.785" y1="3.175" x2="59.055" y2="3.175" width="0.127" layer="21"/>
<wire x1="57.785" y1="-3.175" x2="59.055" y2="-3.175" width="0.127" layer="21"/>
<wire x1="55.245" y1="-3.175" x2="56.515" y2="-3.175" width="0.127" layer="21"/>
<wire x1="56.515" y1="3.175" x2="57.15" y2="2.54" width="0.127" layer="21"/>
<wire x1="55.245" y1="3.175" x2="54.61" y2="2.54" width="0.127" layer="21"/>
<wire x1="57.785" y1="3.175" x2="57.15" y2="2.54" width="0.127" layer="21"/>
<wire x1="57.15" y1="-2.54" x2="56.515" y2="-3.175" width="0.127" layer="21"/>
<wire x1="59.69" y1="-2.54" x2="59.055" y2="-3.175" width="0.127" layer="21"/>
<wire x1="54.61" y1="-2.54" x2="55.245" y2="-3.175" width="0.127" layer="21"/>
<wire x1="57.15" y1="-2.54" x2="57.785" y2="-3.175" width="0.127" layer="21"/>
<wire x1="64.135" y1="3.175" x2="64.77" y2="2.54" width="0.127" layer="21"/>
<wire x1="60.325" y1="3.175" x2="61.595" y2="3.175" width="0.127" layer="21"/>
<wire x1="62.865" y1="3.175" x2="64.135" y2="3.175" width="0.127" layer="21"/>
<wire x1="62.865" y1="-3.175" x2="64.135" y2="-3.175" width="0.127" layer="21"/>
<wire x1="60.325" y1="-3.175" x2="61.595" y2="-3.175" width="0.127" layer="21"/>
<wire x1="61.595" y1="3.175" x2="62.23" y2="2.54" width="0.127" layer="21"/>
<wire x1="60.325" y1="3.175" x2="59.69" y2="2.54" width="0.127" layer="21"/>
<wire x1="62.865" y1="3.175" x2="62.23" y2="2.54" width="0.127" layer="21"/>
<wire x1="62.23" y1="-2.54" x2="61.595" y2="-3.175" width="0.127" layer="21"/>
<wire x1="64.77" y1="-2.54" x2="64.135" y2="-3.175" width="0.127" layer="21"/>
<wire x1="59.69" y1="-2.54" x2="60.325" y2="-3.175" width="0.127" layer="21"/>
<wire x1="62.23" y1="-2.54" x2="62.865" y2="-3.175" width="0.127" layer="21"/>
<wire x1="64.77" y1="2.54" x2="64.77" y2="1.27" width="0.127" layer="21"/>
<wire x1="64.77" y1="1.27" x2="66.04" y2="1.27" width="0.127" layer="21"/>
<wire x1="66.04" y1="1.27" x2="66.04" y2="-1.27" width="0.127" layer="21"/>
<wire x1="66.04" y1="-1.27" x2="64.77" y2="-1.27" width="0.127" layer="21"/>
<wire x1="64.77" y1="-1.27" x2="64.77" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-60.96" y="-2.54" drill="0.9" diameter="1.397"/>
<pad name="2" x="-60.96" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="3" x="-58.42" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="4" x="-58.42" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="5" x="-55.88" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="6" x="-55.88" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="7" x="-53.34" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="8" x="-53.34" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="9" x="-50.8" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="10" x="-50.8" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="11" x="-48.26" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="12" x="-48.26" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="13" x="-45.72" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="14" x="-45.72" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="15" x="-43.18" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="16" x="-43.18" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="17" x="-40.64" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="18" x="-40.64" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="19" x="-38.1" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="20" x="-38.1" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="21" x="-35.56" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="22" x="-35.56" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="23" x="-33.02" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="24" x="-33.02" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="25" x="-30.48" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="26" x="-30.48" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="27" x="-27.94" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="28" x="-27.94" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="29" x="-25.4" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="30" x="-25.4" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="31" x="-22.86" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="32" x="-22.86" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="33" x="-20.32" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="34" x="-20.32" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="35" x="-17.78" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="36" x="-17.78" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="37" x="-15.24" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="38" x="-15.24" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="39" x="-12.7" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="40" x="-12.7" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="41" x="-10.16" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="42" x="-10.16" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="43" x="-7.62" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="44" x="-7.62" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="45" x="-5.08" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="46" x="-5.08" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="47" x="-2.54" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="48" x="-2.54" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="49" x="0" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="50" x="0" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="51" x="2.54" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="52" x="2.54" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="53" x="5.08" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="54" x="5.08" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="55" x="7.62" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="56" x="7.62" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="57" x="10.16" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="58" x="10.16" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="59" x="12.7" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="60" x="12.7" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="61" x="15.24" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="62" x="15.24" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="63" x="17.78" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="64" x="17.78" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="65" x="20.32" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="66" x="20.32" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="67" x="22.86" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="68" x="22.86" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="69" x="25.4" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="70" x="25.4" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="71" x="27.94" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="72" x="27.94" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="73" x="30.48" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="74" x="30.48" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="75" x="33.02" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="76" x="33.02" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="77" x="35.56" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="78" x="35.56" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="79" x="38.1" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="80" x="38.1" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="81" x="40.64" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="82" x="40.64" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="83" x="43.18" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="84" x="43.18" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="85" x="45.72" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="86" x="45.72" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="87" x="48.26" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="88" x="48.26" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="89" x="50.8" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="90" x="50.8" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="91" x="53.34" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="92" x="53.34" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="93" x="55.88" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="94" x="55.88" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="95" x="58.42" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="96" x="58.42" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="97" x="60.96" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="98" x="60.96" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="99" x="63.5" y="-2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<pad name="100" x="63.5" y="2.54" drill="0.9" diameter="1.397" shape="octagon"/>
<text x="-63.373" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-50.165" y="-0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-59.69" y="-0.381" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LKONT_50_R">
<smd name="1" x="64.77" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="2" x="64.77" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="62.23" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="4" x="62.23" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="59.69" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="6" x="59.69" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="57.15" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="8" x="57.15" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="54.61" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="10" x="54.61" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="52.07" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="12" x="52.07" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="49.53" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="14" x="49.53" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="46.99" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="16" x="46.99" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="44.45" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="18" x="44.45" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="41.91" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="20" x="41.91" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="39.37" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="22" x="39.37" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="23" x="36.83" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="24" x="36.83" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="25" x="34.29" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="26" x="34.29" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="31.75" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="28" x="31.75" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="29.21" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="30" x="29.21" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="26.67" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="32" x="26.67" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="33" x="24.13" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="34" x="24.13" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="35" x="21.59" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="36" x="21.59" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="37" x="19.05" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="38" x="19.05" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="39" x="16.51" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="40" x="16.51" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="41" x="13.97" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="42" x="13.97" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="43" x="11.43" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="44" x="11.43" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="45" x="8.89" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="46" x="8.89" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="47" x="6.35" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="48" x="6.35" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="49" x="3.81" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="50" x="3.81" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="51" x="1.27" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="52" x="1.27" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="53" x="-1.27" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="54" x="-1.27" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="55" x="-3.81" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="56" x="-3.81" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="57" x="-6.35" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="58" x="-6.35" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="59" x="-8.89" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="60" x="-8.89" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="61" x="-11.43" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="62" x="-11.43" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="63" x="-13.97" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="64" x="-13.97" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="65" x="-16.51" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="66" x="-16.51" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="67" x="-19.05" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="68" x="-19.05" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="69" x="-21.59" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="70" x="-21.59" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="71" x="-24.13" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="72" x="-24.13" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="73" x="-26.67" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="74" x="-26.67" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="75" x="-29.21" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="76" x="-29.21" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="77" x="-31.75" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="78" x="-31.75" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="79" x="-34.29" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="80" x="-34.29" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="81" x="-36.83" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="82" x="-36.83" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="83" x="-39.37" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="84" x="-39.37" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="85" x="-41.91" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="86" x="-41.91" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="87" x="-44.45" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="88" x="-44.45" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="89" x="-46.99" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="90" x="-46.99" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="91" x="-49.53" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="92" x="-49.53" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="93" x="-52.07" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="94" x="-52.07" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="95" x="-54.61" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="96" x="-54.61" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="97" x="-57.15" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="98" x="-57.15" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="99" x="-59.69" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="100" x="-59.69" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<wire x1="-62.23" y1="4.6038" x2="-62.23" y2="-5.08" width="0" layer="20"/>
<wire x1="-62.23" y1="-5.08" x2="67.3101" y2="-5.08" width="0" layer="20"/>
<wire x1="67.3101" y1="-5.08" x2="67.3101" y2="4.7625" width="0" layer="20"/>
<text x="66.3575" y="5.5562" size="1.27" layer="22" rot="MR0">1</text>
<text x="-57.9437" y="6.096" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-49.5299" y="6.1277" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="ZORRO">
<description>&lt;B&gt;ZORRO PIC small&lt;/B&gt;
&lt;P&gt;Plug-In Card for the Amiga Zorro bus&lt;/B&gt;</description>
<wire x1="140.1754" y1="-7.747" x2="137.7116" y2="-7.747" width="0" layer="47"/>
<wire x1="137.7116" y1="-7.747" x2="138.3974" y2="-6.1214" width="0" layer="47"/>
<wire x1="137.7116" y1="-7.747" x2="133.2666" y2="-7.747" width="0" layer="47"/>
<wire x1="137.0258" y1="-6.1214" x2="137.7116" y2="-7.747" width="0" layer="47"/>
<wire x1="137.7116" y1="0.127" x2="137.0004" y2="-1.3716" width="0" layer="47"/>
<wire x1="137.0004" y1="-1.3716" x2="138.4228" y2="-1.3716" width="0" layer="47"/>
<wire x1="138.4228" y1="-1.3716" x2="137.7116" y2="0.127" width="0" layer="47"/>
<wire x1="137.7116" y1="0.127" x2="137.7116" y2="-7.747" width="0" layer="47"/>
<wire x1="138.3974" y1="-6.1214" x2="137.0258" y2="-6.1214" width="0" layer="47"/>
<wire x1="132.911" y1="-7.112" x2="132.276" y2="-7.747" width="0.2032" layer="20"/>
<wire x1="132.276" y1="-7.747" x2="4.514" y2="-7.747" width="0.2032" layer="20"/>
<wire x1="132.911" y1="0.127" x2="132.911" y2="-7.112" width="0.2032" layer="20"/>
<wire x1="4.514" y1="-7.747" x2="3.879" y2="-7.112" width="0.2032" layer="20"/>
<wire x1="3.879" y1="0.127" x2="3.879" y2="-7.112" width="0.2032" layer="20"/>
<wire x1="3.879" y1="0.127" x2="0.175" y2="0.127" width="0.2038" layer="20"/>
<wire x1="0.175" y1="0.127" x2="0.175" y2="50.8" width="0.2038" layer="20"/>
<wire x1="0.175" y1="50.8" x2="136.96" y2="50.8" width="0.2038" layer="20"/>
<wire x1="136.96" y1="50.8" x2="136.96" y2="0.127" width="0.2038" layer="20"/>
<wire x1="3.879" y1="-8.763" x2="3.879" y2="-16.002" width="0" layer="47"/>
<wire x1="133.0126" y1="-14.605" x2="3.9044" y2="-14.605" width="0" layer="47"/>
<wire x1="3.9044" y1="-14.605" x2="6.292" y2="-13.335" width="0" layer="47"/>
<wire x1="6.292" y1="-13.335" x2="6.292" y2="-15.875" width="0" layer="47"/>
<wire x1="6.292" y1="-15.875" x2="3.9044" y2="-14.605" width="0" layer="47"/>
<wire x1="133.0126" y1="-14.605" x2="130.625" y2="-13.335" width="0" layer="47"/>
<wire x1="130.625" y1="-13.335" x2="130.625" y2="-15.875" width="0" layer="47"/>
<wire x1="130.625" y1="-15.875" x2="133.0126" y2="-14.605" width="0" layer="47"/>
<wire x1="133.038" y1="-17.78" x2="133.038" y2="-8.255" width="0" layer="47"/>
<wire x1="132.911" y1="0.127" x2="136.96" y2="0.127" width="0.2038" layer="20"/>
<smd name="2" x="130.625" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="4" x="128.085" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="6" x="125.545" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="8" x="123.005" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="10" x="120.465" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="12" x="117.925" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="14" x="115.385" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="16" x="112.845" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="18" x="110.305" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="20" x="107.765" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="22" x="105.225" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="24" x="102.685" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="26" x="100.145" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="28" x="97.605" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="30" x="95.065" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="32" x="92.525" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="34" x="89.985" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="36" x="87.445" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="38" x="84.905" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="40" x="82.365" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="42" x="79.825" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="44" x="77.285" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="46" x="74.745" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="48" x="72.205" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="50" x="69.665" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="52" x="67.125" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="54" x="64.585" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="56" x="62.045" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="58" x="59.505" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="60" x="56.965" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="62" x="54.425" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="64" x="51.885" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="66" x="49.345" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="68" x="46.805" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="70" x="44.265" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="72" x="41.725" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="74" x="39.185" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="76" x="36.645" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="78" x="34.105" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="80" x="31.565" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="82" x="29.025" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="84" x="26.485" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="86" x="23.945" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="88" x="21.405" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="90" x="18.865" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="92" x="16.325" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="94" x="13.785" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="96" x="11.245" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="98" x="8.705" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="100" x="6.165" y="-3.175" dx="1.524" dy="6.477" layer="1"/>
<smd name="1" x="130.625" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="3" x="128.085" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="5" x="125.545" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="7" x="123.005" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="9" x="120.465" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="11" x="117.925" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="13" x="115.385" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="15" x="112.845" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="17" x="110.305" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="19" x="107.765" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="21" x="105.225" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="23" x="102.685" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="25" x="100.145" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="27" x="97.605" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="29" x="95.065" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="31" x="92.525" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="33" x="89.985" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="35" x="87.445" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="37" x="84.905" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="39" x="82.365" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="41" x="79.825" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="43" x="77.285" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="45" x="74.745" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="47" x="72.205" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="49" x="69.665" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="51" x="67.125" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="53" x="64.585" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="55" x="62.045" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="57" x="59.505" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="59" x="56.965" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="61" x="54.425" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="63" x="51.885" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="65" x="49.345" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="67" x="46.805" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="69" x="44.265" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="71" x="41.725" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="73" x="39.185" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="75" x="36.645" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="77" x="34.105" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="79" x="31.565" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="81" x="29.025" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="83" x="26.485" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="85" x="23.945" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="87" x="21.405" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="89" x="18.865" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="91" x="16.325" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="93" x="13.785" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="95" x="11.245" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="97" x="8.705" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<smd name="99" x="6.165" y="-3.175" dx="1.524" dy="6.477" layer="16"/>
<text x="5.715" y="-10.795" size="1.778" layer="25">&gt;NAME</text>
<text x="142.2582" y="-6.8834" size="1.778" layer="47" rot="R90">7,87</text>
<text x="19.685" y="-10.795" size="1.778" layer="27">&gt;VALUE</text>
<text x="63.7468" y="-16.9164" size="1.778" layer="47">129,03</text>
</package>
</packages>
<symbols>
<symbol name="SLOT-50">
<wire x1="12.7" y1="-66.04" x2="-13.97" y2="-66.04" width="0.4064" layer="94"/>
<wire x1="-13.97" y1="62.23" x2="-13.97" y2="-66.04" width="0.4064" layer="94"/>
<wire x1="-13.97" y1="62.23" x2="-12.7" y2="63.5" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="63.5" x2="12.7" y2="63.5" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-66.04" x2="12.7" y2="63.5" width="0.4064" layer="94"/>
<wire x1="10.16" y1="17.78" x2="11.43" y2="17.78" width="0.6096" layer="94"/>
<wire x1="10.16" y1="20.32" x2="11.43" y2="20.32" width="0.6096" layer="94"/>
<wire x1="10.16" y1="22.86" x2="11.43" y2="22.86" width="0.6096" layer="94"/>
<wire x1="10.16" y1="25.4" x2="11.43" y2="25.4" width="0.6096" layer="94"/>
<wire x1="10.16" y1="27.94" x2="11.43" y2="27.94" width="0.6096" layer="94"/>
<wire x1="10.16" y1="30.48" x2="11.43" y2="30.48" width="0.6096" layer="94"/>
<wire x1="10.16" y1="33.02" x2="11.43" y2="33.02" width="0.6096" layer="94"/>
<wire x1="10.16" y1="35.56" x2="11.43" y2="35.56" width="0.6096" layer="94"/>
<wire x1="10.16" y1="38.1" x2="11.43" y2="38.1" width="0.6096" layer="94"/>
<wire x1="10.16" y1="40.64" x2="11.43" y2="40.64" width="0.6096" layer="94"/>
<wire x1="10.16" y1="43.18" x2="11.43" y2="43.18" width="0.6096" layer="94"/>
<wire x1="10.16" y1="45.72" x2="11.43" y2="45.72" width="0.6096" layer="94"/>
<wire x1="10.16" y1="48.26" x2="11.43" y2="48.26" width="0.6096" layer="94"/>
<wire x1="10.16" y1="50.8" x2="11.43" y2="50.8" width="0.6096" layer="94"/>
<wire x1="10.16" y1="53.34" x2="11.43" y2="53.34" width="0.6096" layer="94"/>
<wire x1="10.16" y1="55.88" x2="11.43" y2="55.88" width="0.6096" layer="94"/>
<wire x1="10.16" y1="58.42" x2="11.43" y2="58.42" width="0.6096" layer="94"/>
<wire x1="10.16" y1="60.96" x2="11.43" y2="60.96" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="17.78" x2="-11.43" y2="17.78" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="20.32" x2="-11.43" y2="20.32" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="22.86" x2="-11.43" y2="22.86" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="25.4" x2="-11.43" y2="25.4" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="27.94" x2="-11.43" y2="27.94" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="30.48" x2="-11.43" y2="30.48" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="33.02" x2="-11.43" y2="33.02" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="35.56" x2="-11.43" y2="35.56" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="38.1" x2="-11.43" y2="38.1" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="40.64" x2="-11.43" y2="40.64" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="43.18" x2="-11.43" y2="43.18" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="45.72" x2="-11.43" y2="45.72" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="48.26" x2="-11.43" y2="48.26" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="50.8" x2="-11.43" y2="50.8" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="53.34" x2="-11.43" y2="53.34" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="55.88" x2="-11.43" y2="55.88" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="58.42" x2="-11.43" y2="58.42" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="60.96" x2="-11.43" y2="60.96" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-11.43" y2="15.24" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="12.7" x2="-11.43" y2="12.7" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="-11.43" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-11.43" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="-11.43" y2="5.08" width="0.6096" layer="94"/>
<wire x1="10.16" y1="5.08" x2="11.43" y2="5.08" width="0.6096" layer="94"/>
<wire x1="10.16" y1="7.62" x2="11.43" y2="7.62" width="0.6096" layer="94"/>
<wire x1="10.16" y1="10.16" x2="11.43" y2="10.16" width="0.6096" layer="94"/>
<wire x1="10.16" y1="12.7" x2="11.43" y2="12.7" width="0.6096" layer="94"/>
<wire x1="10.16" y1="15.24" x2="11.43" y2="15.24" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="-11.43" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="0" x2="-11.43" y2="0" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-2.54" x2="-11.43" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-11.43" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-11.43" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-11.43" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="-11.43" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-11.43" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-17.78" x2="-11.43" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-20.32" x2="-11.43" y2="-20.32" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="11.43" y2="-20.32" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="11.43" y2="-17.78" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="11.43" y2="-15.24" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="11.43" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="11.43" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="11.43" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="11.43" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="11.43" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="10.16" y1="0" x2="11.43" y2="0" width="0.6096" layer="94"/>
<wire x1="10.16" y1="2.54" x2="11.43" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="-11.43" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="-11.43" y2="-25.4" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="-11.43" y2="-27.94" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-30.48" x2="-11.43" y2="-30.48" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-33.02" x2="-11.43" y2="-33.02" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-35.56" x2="-11.43" y2="-35.56" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-38.1" x2="-11.43" y2="-38.1" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-40.64" x2="-11.43" y2="-40.64" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-43.18" x2="-11.43" y2="-43.18" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-45.72" x2="-11.43" y2="-45.72" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-45.72" x2="11.43" y2="-45.72" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-43.18" x2="11.43" y2="-43.18" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-40.64" x2="11.43" y2="-40.64" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-38.1" x2="11.43" y2="-38.1" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-35.56" x2="11.43" y2="-35.56" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-33.02" x2="11.43" y2="-33.02" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-30.48" x2="11.43" y2="-30.48" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-27.94" x2="11.43" y2="-27.94" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-25.4" x2="11.43" y2="-25.4" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-22.86" x2="11.43" y2="-22.86" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-48.26" x2="-11.43" y2="-48.26" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-50.8" x2="-11.43" y2="-50.8" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-53.34" x2="-11.43" y2="-53.34" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-55.88" x2="-11.43" y2="-55.88" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-58.42" x2="-11.43" y2="-58.42" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-60.96" x2="-11.43" y2="-60.96" width="0.6096" layer="94"/>
<wire x1="-12.7" y1="-63.5" x2="-11.43" y2="-63.5" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-63.5" x2="11.43" y2="-63.5" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-60.96" x2="11.43" y2="-60.96" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-58.42" x2="11.43" y2="-58.42" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-55.88" x2="11.43" y2="-55.88" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-53.34" x2="11.43" y2="-53.34" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-50.8" x2="11.43" y2="-50.8" width="0.6096" layer="94"/>
<wire x1="10.16" y1="-48.26" x2="11.43" y2="-48.26" width="0.6096" layer="94"/>
<pin name="GND@1" x="-17.78" y="60.96" length="middle" direction="pas"/>
<pin name="GND@2" x="16.51" y="60.96" length="middle" direction="pas" rot="R180"/>
<pin name="GND@3" x="-17.78" y="58.42" length="middle" direction="pas"/>
<pin name="GND@4" x="16.51" y="58.42" length="middle" direction="pas" rot="R180"/>
<pin name="5V@1" x="-17.78" y="55.88" length="middle" direction="pas"/>
<pin name="5V@2" x="16.51" y="55.88" length="middle" direction="pas" rot="R180"/>
<pin name="!OWN!" x="-17.78" y="53.34" length="middle" direction="pas"/>
<pin name="-5V" x="16.51" y="53.34" length="middle" direction="pas" rot="R180"/>
<pin name="!SLAVE!" x="-17.78" y="50.8" length="middle" direction="pas"/>
<pin name="12V" x="16.51" y="50.8" length="middle" direction="pas" rot="R180"/>
<pin name="!CFOUT!" x="-17.78" y="48.26" length="middle" direction="pas"/>
<pin name="!CFIN!" x="16.51" y="48.26" length="middle" direction="pas" rot="R180"/>
<pin name="GND@5" x="-17.78" y="45.72" length="middle" direction="pas"/>
<pin name="!CCKQ!" x="16.51" y="45.72" length="middle" direction="pas" rot="R180"/>
<pin name="CDAC" x="-17.78" y="43.18" length="middle" direction="pas"/>
<pin name="!CCK!" x="16.51" y="43.18" length="middle" direction="pas" rot="R180"/>
<pin name="!OVR!" x="-17.78" y="40.64" length="middle" direction="pas"/>
<pin name="XRDY" x="16.51" y="40.64" length="middle" direction="pas" rot="R180"/>
<pin name="!INT2!" x="-17.78" y="38.1" length="middle" direction="pas"/>
<pin name="-12V" x="16.51" y="38.1" length="middle" direction="pas" rot="R180"/>
<pin name="A5" x="-17.78" y="35.56" length="middle" direction="pas"/>
<pin name="!INT6!" x="16.51" y="35.56" length="middle" direction="pas" rot="R180"/>
<pin name="A6" x="-17.78" y="33.02" length="middle" direction="pas"/>
<pin name="A4" x="16.51" y="33.02" length="middle" direction="pas" rot="R180"/>
<pin name="GND@6" x="-17.78" y="30.48" length="middle" direction="pas"/>
<pin name="A3" x="16.51" y="30.48" length="middle" direction="pas" rot="R180"/>
<pin name="A2" x="-17.78" y="27.94" length="middle" direction="pas"/>
<pin name="A7" x="16.51" y="27.94" length="middle" direction="pas" rot="R180"/>
<pin name="A1" x="-17.78" y="25.4" length="middle" direction="pas"/>
<pin name="A8" x="16.51" y="25.4" length="middle" direction="pas" rot="R180"/>
<pin name="FC0" x="-17.78" y="22.86" length="middle" direction="pas"/>
<pin name="A9" x="16.51" y="22.86" length="middle" direction="pas" rot="R180"/>
<pin name="FC1" x="-17.78" y="20.32" length="middle" direction="pas"/>
<pin name="A10" x="16.51" y="20.32" length="middle" direction="pas" rot="R180"/>
<pin name="FC2" x="-17.78" y="17.78" length="middle" direction="pas"/>
<pin name="A11" x="16.51" y="17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GND@7" x="-17.78" y="15.24" length="middle" direction="pas"/>
<pin name="A12" x="16.51" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="A13" x="-17.78" y="12.7" length="middle" direction="pas"/>
<pin name="!EINT7!" x="16.51" y="12.7" length="middle" direction="pas" rot="R180"/>
<pin name="A14" x="-17.78" y="10.16" length="middle" direction="pas"/>
<pin name="!EINT5!" x="16.51" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="A15" x="-17.78" y="7.62" length="middle" direction="pas"/>
<pin name="!EINT4!" x="16.51" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="A16" x="-17.78" y="5.08" length="middle" direction="pas"/>
<pin name="!BEER!" x="16.51" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="A17" x="-17.78" y="2.54" length="middle" direction="pas"/>
<pin name="!VPA!" x="16.51" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GND@8" x="-17.78" y="0" length="middle" direction="pas"/>
<pin name="E" x="16.51" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="!VMA!" x="-17.78" y="-2.54" length="middle" direction="pas"/>
<pin name="A18" x="16.51" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="!RESET!" x="-17.78" y="-5.08" length="middle" direction="pas"/>
<pin name="A19" x="16.51" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="!HALT!" x="-17.78" y="-7.62" length="middle" direction="pas"/>
<pin name="A20" x="16.51" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="A22" x="-17.78" y="-10.16" length="middle" direction="pas"/>
<pin name="A21" x="16.51" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="A23" x="-17.78" y="-12.7" length="middle" direction="pas"/>
<pin name="!BR!" x="16.51" y="-12.7" length="middle" direction="pas" rot="R180"/>
<pin name="GND@9" x="-17.78" y="-15.24" length="middle" direction="pas"/>
<pin name="!BGACK!" x="16.51" y="-15.24" length="middle" direction="pas" rot="R180"/>
<pin name="DA15" x="-17.78" y="-17.78" length="middle" direction="pas"/>
<pin name="!BG!" x="16.51" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="DA14" x="-17.78" y="-20.32" length="middle" direction="pas"/>
<pin name="!DTACK!" x="16.51" y="-20.32" length="middle" direction="pas" rot="R180"/>
<pin name="DA13" x="-17.78" y="-22.86" length="middle" direction="pas"/>
<pin name="R/!W!" x="16.51" y="-22.86" length="middle" direction="pas" rot="R180"/>
<pin name="DA12" x="-17.78" y="-25.4" length="middle" direction="pas"/>
<pin name="!LDS!" x="16.51" y="-25.4" length="middle" direction="pas" rot="R180"/>
<pin name="DA11" x="-17.78" y="-27.94" length="middle" direction="pas"/>
<pin name="!UDS!" x="16.51" y="-27.94" length="middle" direction="pas" rot="R180"/>
<pin name="GND@10" x="-17.78" y="-30.48" length="middle" direction="pas"/>
<pin name="!AS!" x="16.51" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="DA0" x="-17.78" y="-33.02" length="middle" direction="pas"/>
<pin name="DA10" x="16.51" y="-33.02" length="middle" direction="pas" rot="R180"/>
<pin name="DA1" x="-17.78" y="-35.56" length="middle" direction="pas"/>
<pin name="DA9" x="16.51" y="-35.56" length="middle" direction="pas" rot="R180"/>
<pin name="DA2" x="-17.78" y="-38.1" length="middle" direction="pas"/>
<pin name="DA8" x="16.51" y="-38.1" length="middle" direction="pas" rot="R180"/>
<pin name="DA3" x="-17.78" y="-40.64" length="middle" direction="pas"/>
<pin name="DA7" x="16.51" y="-40.64" length="middle" direction="pas" rot="R180"/>
<pin name="DA4" x="-17.78" y="-43.18" length="middle" direction="pas"/>
<pin name="DA6" x="16.51" y="-43.18" length="middle" direction="pas" rot="R180"/>
<pin name="GND@11" x="-17.78" y="-45.72" length="middle" direction="pas"/>
<pin name="DA5" x="16.51" y="-45.72" length="middle" direction="pas" rot="R180"/>
<pin name="GND@12" x="-17.78" y="-48.26" length="middle" direction="pas"/>
<pin name="GND@13" x="16.51" y="-48.26" length="middle" direction="pas" rot="R180"/>
<pin name="GND@14" x="-17.78" y="-50.8" length="middle" direction="pas"/>
<pin name="GND@15" x="16.51" y="-50.8" length="middle" direction="pas" rot="R180"/>
<pin name="Z3/GND" x="-17.78" y="-53.34" length="middle" direction="pas"/>
<pin name="7M" x="16.51" y="-53.34" length="middle" direction="pas" rot="R180"/>
<pin name="DOE" x="-17.78" y="-55.88" length="middle" direction="pas"/>
<pin name="!BUSRST!" x="16.51" y="-55.88" length="middle" direction="pas" rot="R180"/>
<pin name="!GBG!" x="-17.78" y="-58.42" length="middle" direction="pas"/>
<pin name="!EINT1!" x="16.51" y="-58.42" length="middle" direction="pas" rot="R180"/>
<pin name="!FCS!" x="-17.78" y="-60.96" length="middle" direction="pas"/>
<pin name="!DS1!" x="16.51" y="-60.96" length="middle" direction="pas" rot="R180"/>
<pin name="GND@16" x="-17.78" y="-63.5" length="middle" direction="pas"/>
<pin name="GND@17" x="16.51" y="-63.5" length="middle" direction="pas" rot="R180"/>
<text x="-15.24" y="-68.58" size="1.778" layer="96">&gt;VALUE</text>
<text x="-13.97" y="64.262" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ZORROII" prefix="CN">
<gates>
<gate name="G$1" symbol="SLOT-50" x="0" y="0"/>
</gates>
<devices>
<device name="_EDGE_FULL_HEIGHT" package="LKONT_50">
<connects>
<connect gate="G$1" pin="!AS!" pad="74"/>
<connect gate="G$1" pin="!BEER!" pad="46"/>
<connect gate="G$1" pin="!BG!" pad="64"/>
<connect gate="G$1" pin="!BGACK!" pad="62"/>
<connect gate="G$1" pin="!BR!" pad="60"/>
<connect gate="G$1" pin="!BUSRST!" pad="94"/>
<connect gate="G$1" pin="!CCK!" pad="16"/>
<connect gate="G$1" pin="!CCKQ!" pad="14"/>
<connect gate="G$1" pin="!CFIN!" pad="12"/>
<connect gate="G$1" pin="!CFOUT!" pad="11"/>
<connect gate="G$1" pin="!DS1!" pad="98"/>
<connect gate="G$1" pin="!DTACK!" pad="66"/>
<connect gate="G$1" pin="!EINT1!" pad="96"/>
<connect gate="G$1" pin="!EINT4!" pad="44"/>
<connect gate="G$1" pin="!EINT5!" pad="42"/>
<connect gate="G$1" pin="!EINT7!" pad="40"/>
<connect gate="G$1" pin="!FCS!" pad="97"/>
<connect gate="G$1" pin="!GBG!" pad="95"/>
<connect gate="G$1" pin="!HALT!" pad="55"/>
<connect gate="G$1" pin="!INT2!" pad="19"/>
<connect gate="G$1" pin="!INT6!" pad="22"/>
<connect gate="G$1" pin="!LDS!" pad="70"/>
<connect gate="G$1" pin="!OVR!" pad="17"/>
<connect gate="G$1" pin="!OWN!" pad="7"/>
<connect gate="G$1" pin="!RESET!" pad="53"/>
<connect gate="G$1" pin="!SLAVE!" pad="9"/>
<connect gate="G$1" pin="!UDS!" pad="72"/>
<connect gate="G$1" pin="!VMA!" pad="51"/>
<connect gate="G$1" pin="!VPA!" pad="48"/>
<connect gate="G$1" pin="-12V" pad="20"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="12V" pad="10"/>
<connect gate="G$1" pin="5V@1" pad="5"/>
<connect gate="G$1" pin="5V@2" pad="6"/>
<connect gate="G$1" pin="7M" pad="92"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="DA0" pad="75"/>
<connect gate="G$1" pin="DA1" pad="77"/>
<connect gate="G$1" pin="DA10" pad="76"/>
<connect gate="G$1" pin="DA11" pad="71"/>
<connect gate="G$1" pin="DA12" pad="69"/>
<connect gate="G$1" pin="DA13" pad="67"/>
<connect gate="G$1" pin="DA14" pad="65"/>
<connect gate="G$1" pin="DA15" pad="63"/>
<connect gate="G$1" pin="DA2" pad="79"/>
<connect gate="G$1" pin="DA3" pad="81"/>
<connect gate="G$1" pin="DA4" pad="83"/>
<connect gate="G$1" pin="DA5" pad="86"/>
<connect gate="G$1" pin="DA6" pad="84"/>
<connect gate="G$1" pin="DA7" pad="82"/>
<connect gate="G$1" pin="DA8" pad="80"/>
<connect gate="G$1" pin="DA9" pad="78"/>
<connect gate="G$1" pin="DOE" pad="93"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@12" pad="87"/>
<connect gate="G$1" pin="GND@13" pad="88"/>
<connect gate="G$1" pin="GND@14" pad="89"/>
<connect gate="G$1" pin="GND@15" pad="90"/>
<connect gate="G$1" pin="GND@16" pad="99"/>
<connect gate="G$1" pin="GND@17" pad="100"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/!W!" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
<connect gate="G$1" pin="Z3/GND" pad="91"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_EDGE_SMALL_HEIGHT" package="LKONT_50_2">
<connects>
<connect gate="G$1" pin="!AS!" pad="74"/>
<connect gate="G$1" pin="!BEER!" pad="46"/>
<connect gate="G$1" pin="!BG!" pad="64"/>
<connect gate="G$1" pin="!BGACK!" pad="62"/>
<connect gate="G$1" pin="!BR!" pad="60"/>
<connect gate="G$1" pin="!BUSRST!" pad="94"/>
<connect gate="G$1" pin="!CCK!" pad="16"/>
<connect gate="G$1" pin="!CCKQ!" pad="14"/>
<connect gate="G$1" pin="!CFIN!" pad="12"/>
<connect gate="G$1" pin="!CFOUT!" pad="11"/>
<connect gate="G$1" pin="!DS1!" pad="98"/>
<connect gate="G$1" pin="!DTACK!" pad="66"/>
<connect gate="G$1" pin="!EINT1!" pad="96"/>
<connect gate="G$1" pin="!EINT4!" pad="44"/>
<connect gate="G$1" pin="!EINT5!" pad="42"/>
<connect gate="G$1" pin="!EINT7!" pad="40"/>
<connect gate="G$1" pin="!FCS!" pad="97"/>
<connect gate="G$1" pin="!GBG!" pad="95"/>
<connect gate="G$1" pin="!HALT!" pad="55"/>
<connect gate="G$1" pin="!INT2!" pad="19"/>
<connect gate="G$1" pin="!INT6!" pad="22"/>
<connect gate="G$1" pin="!LDS!" pad="70"/>
<connect gate="G$1" pin="!OVR!" pad="17"/>
<connect gate="G$1" pin="!OWN!" pad="7"/>
<connect gate="G$1" pin="!RESET!" pad="53"/>
<connect gate="G$1" pin="!SLAVE!" pad="9"/>
<connect gate="G$1" pin="!UDS!" pad="72"/>
<connect gate="G$1" pin="!VMA!" pad="51"/>
<connect gate="G$1" pin="!VPA!" pad="48"/>
<connect gate="G$1" pin="-12V" pad="20"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="12V" pad="10"/>
<connect gate="G$1" pin="5V@1" pad="5"/>
<connect gate="G$1" pin="5V@2" pad="6"/>
<connect gate="G$1" pin="7M" pad="92"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="DA0" pad="75"/>
<connect gate="G$1" pin="DA1" pad="77"/>
<connect gate="G$1" pin="DA10" pad="76"/>
<connect gate="G$1" pin="DA11" pad="71"/>
<connect gate="G$1" pin="DA12" pad="69"/>
<connect gate="G$1" pin="DA13" pad="67"/>
<connect gate="G$1" pin="DA14" pad="65"/>
<connect gate="G$1" pin="DA15" pad="63"/>
<connect gate="G$1" pin="DA2" pad="79"/>
<connect gate="G$1" pin="DA3" pad="81"/>
<connect gate="G$1" pin="DA4" pad="83"/>
<connect gate="G$1" pin="DA5" pad="86"/>
<connect gate="G$1" pin="DA6" pad="84"/>
<connect gate="G$1" pin="DA7" pad="82"/>
<connect gate="G$1" pin="DA8" pad="80"/>
<connect gate="G$1" pin="DA9" pad="78"/>
<connect gate="G$1" pin="DOE" pad="93"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@12" pad="87"/>
<connect gate="G$1" pin="GND@13" pad="88"/>
<connect gate="G$1" pin="GND@14" pad="89"/>
<connect gate="G$1" pin="GND@15" pad="90"/>
<connect gate="G$1" pin="GND@16" pad="99"/>
<connect gate="G$1" pin="GND@17" pad="100"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/!W!" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
<connect gate="G$1" pin="Z3/GND" pad="91"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SLOT" package="SLOT_100">
<connects>
<connect gate="G$1" pin="!AS!" pad="74"/>
<connect gate="G$1" pin="!BEER!" pad="46"/>
<connect gate="G$1" pin="!BG!" pad="64"/>
<connect gate="G$1" pin="!BGACK!" pad="62"/>
<connect gate="G$1" pin="!BR!" pad="60"/>
<connect gate="G$1" pin="!BUSRST!" pad="94"/>
<connect gate="G$1" pin="!CCK!" pad="16"/>
<connect gate="G$1" pin="!CCKQ!" pad="14"/>
<connect gate="G$1" pin="!CFIN!" pad="12"/>
<connect gate="G$1" pin="!CFOUT!" pad="11"/>
<connect gate="G$1" pin="!DS1!" pad="98"/>
<connect gate="G$1" pin="!DTACK!" pad="66"/>
<connect gate="G$1" pin="!EINT1!" pad="96"/>
<connect gate="G$1" pin="!EINT4!" pad="44"/>
<connect gate="G$1" pin="!EINT5!" pad="42"/>
<connect gate="G$1" pin="!EINT7!" pad="40"/>
<connect gate="G$1" pin="!FCS!" pad="97"/>
<connect gate="G$1" pin="!GBG!" pad="95"/>
<connect gate="G$1" pin="!HALT!" pad="55"/>
<connect gate="G$1" pin="!INT2!" pad="19"/>
<connect gate="G$1" pin="!INT6!" pad="22"/>
<connect gate="G$1" pin="!LDS!" pad="70"/>
<connect gate="G$1" pin="!OVR!" pad="17"/>
<connect gate="G$1" pin="!OWN!" pad="7"/>
<connect gate="G$1" pin="!RESET!" pad="53"/>
<connect gate="G$1" pin="!SLAVE!" pad="9"/>
<connect gate="G$1" pin="!UDS!" pad="72"/>
<connect gate="G$1" pin="!VMA!" pad="51"/>
<connect gate="G$1" pin="!VPA!" pad="48"/>
<connect gate="G$1" pin="-12V" pad="20"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="12V" pad="10"/>
<connect gate="G$1" pin="5V@1" pad="5"/>
<connect gate="G$1" pin="5V@2" pad="6"/>
<connect gate="G$1" pin="7M" pad="92"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="DA0" pad="75"/>
<connect gate="G$1" pin="DA1" pad="77"/>
<connect gate="G$1" pin="DA10" pad="76"/>
<connect gate="G$1" pin="DA11" pad="71"/>
<connect gate="G$1" pin="DA12" pad="69"/>
<connect gate="G$1" pin="DA13" pad="67"/>
<connect gate="G$1" pin="DA14" pad="65"/>
<connect gate="G$1" pin="DA15" pad="63"/>
<connect gate="G$1" pin="DA2" pad="79"/>
<connect gate="G$1" pin="DA3" pad="81"/>
<connect gate="G$1" pin="DA4" pad="83"/>
<connect gate="G$1" pin="DA5" pad="86"/>
<connect gate="G$1" pin="DA6" pad="84"/>
<connect gate="G$1" pin="DA7" pad="82"/>
<connect gate="G$1" pin="DA8" pad="80"/>
<connect gate="G$1" pin="DA9" pad="78"/>
<connect gate="G$1" pin="DOE" pad="93"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@12" pad="87"/>
<connect gate="G$1" pin="GND@13" pad="88"/>
<connect gate="G$1" pin="GND@14" pad="89"/>
<connect gate="G$1" pin="GND@15" pad="90"/>
<connect gate="G$1" pin="GND@16" pad="99"/>
<connect gate="G$1" pin="GND@17" pad="100"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/!W!" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
<connect gate="G$1" pin="Z3/GND" pad="91"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SLOT_SMALL_HOLES" package="SLOT_100_S">
<connects>
<connect gate="G$1" pin="!AS!" pad="74"/>
<connect gate="G$1" pin="!BEER!" pad="46"/>
<connect gate="G$1" pin="!BG!" pad="64"/>
<connect gate="G$1" pin="!BGACK!" pad="62"/>
<connect gate="G$1" pin="!BR!" pad="60"/>
<connect gate="G$1" pin="!BUSRST!" pad="94"/>
<connect gate="G$1" pin="!CCK!" pad="16"/>
<connect gate="G$1" pin="!CCKQ!" pad="14"/>
<connect gate="G$1" pin="!CFIN!" pad="12"/>
<connect gate="G$1" pin="!CFOUT!" pad="11"/>
<connect gate="G$1" pin="!DS1!" pad="98"/>
<connect gate="G$1" pin="!DTACK!" pad="66"/>
<connect gate="G$1" pin="!EINT1!" pad="96"/>
<connect gate="G$1" pin="!EINT4!" pad="44"/>
<connect gate="G$1" pin="!EINT5!" pad="42"/>
<connect gate="G$1" pin="!EINT7!" pad="40"/>
<connect gate="G$1" pin="!FCS!" pad="97"/>
<connect gate="G$1" pin="!GBG!" pad="95"/>
<connect gate="G$1" pin="!HALT!" pad="55"/>
<connect gate="G$1" pin="!INT2!" pad="19"/>
<connect gate="G$1" pin="!INT6!" pad="22"/>
<connect gate="G$1" pin="!LDS!" pad="70"/>
<connect gate="G$1" pin="!OVR!" pad="17"/>
<connect gate="G$1" pin="!OWN!" pad="7"/>
<connect gate="G$1" pin="!RESET!" pad="53"/>
<connect gate="G$1" pin="!SLAVE!" pad="9"/>
<connect gate="G$1" pin="!UDS!" pad="72"/>
<connect gate="G$1" pin="!VMA!" pad="51"/>
<connect gate="G$1" pin="!VPA!" pad="48"/>
<connect gate="G$1" pin="-12V" pad="20"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="12V" pad="10"/>
<connect gate="G$1" pin="5V@1" pad="5"/>
<connect gate="G$1" pin="5V@2" pad="6"/>
<connect gate="G$1" pin="7M" pad="92"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="DA0" pad="75"/>
<connect gate="G$1" pin="DA1" pad="77"/>
<connect gate="G$1" pin="DA10" pad="76"/>
<connect gate="G$1" pin="DA11" pad="71"/>
<connect gate="G$1" pin="DA12" pad="69"/>
<connect gate="G$1" pin="DA13" pad="67"/>
<connect gate="G$1" pin="DA14" pad="65"/>
<connect gate="G$1" pin="DA15" pad="63"/>
<connect gate="G$1" pin="DA2" pad="79"/>
<connect gate="G$1" pin="DA3" pad="81"/>
<connect gate="G$1" pin="DA4" pad="83"/>
<connect gate="G$1" pin="DA5" pad="86"/>
<connect gate="G$1" pin="DA6" pad="84"/>
<connect gate="G$1" pin="DA7" pad="82"/>
<connect gate="G$1" pin="DA8" pad="80"/>
<connect gate="G$1" pin="DA9" pad="78"/>
<connect gate="G$1" pin="DOE" pad="93"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@12" pad="87"/>
<connect gate="G$1" pin="GND@13" pad="88"/>
<connect gate="G$1" pin="GND@14" pad="89"/>
<connect gate="G$1" pin="GND@15" pad="90"/>
<connect gate="G$1" pin="GND@16" pad="99"/>
<connect gate="G$1" pin="GND@17" pad="100"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/!W!" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
<connect gate="G$1" pin="Z3/GND" pad="91"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_EDGE_ROUND_CONTACTS" package="LKONT_50_R">
<connects>
<connect gate="G$1" pin="!AS!" pad="74"/>
<connect gate="G$1" pin="!BEER!" pad="46"/>
<connect gate="G$1" pin="!BG!" pad="64"/>
<connect gate="G$1" pin="!BGACK!" pad="62"/>
<connect gate="G$1" pin="!BR!" pad="60"/>
<connect gate="G$1" pin="!BUSRST!" pad="94"/>
<connect gate="G$1" pin="!CCK!" pad="16"/>
<connect gate="G$1" pin="!CCKQ!" pad="14"/>
<connect gate="G$1" pin="!CFIN!" pad="12"/>
<connect gate="G$1" pin="!CFOUT!" pad="11"/>
<connect gate="G$1" pin="!DS1!" pad="98"/>
<connect gate="G$1" pin="!DTACK!" pad="66"/>
<connect gate="G$1" pin="!EINT1!" pad="96"/>
<connect gate="G$1" pin="!EINT4!" pad="44"/>
<connect gate="G$1" pin="!EINT5!" pad="42"/>
<connect gate="G$1" pin="!EINT7!" pad="40"/>
<connect gate="G$1" pin="!FCS!" pad="97"/>
<connect gate="G$1" pin="!GBG!" pad="95"/>
<connect gate="G$1" pin="!HALT!" pad="55"/>
<connect gate="G$1" pin="!INT2!" pad="19"/>
<connect gate="G$1" pin="!INT6!" pad="22"/>
<connect gate="G$1" pin="!LDS!" pad="70"/>
<connect gate="G$1" pin="!OVR!" pad="17"/>
<connect gate="G$1" pin="!OWN!" pad="7"/>
<connect gate="G$1" pin="!RESET!" pad="53"/>
<connect gate="G$1" pin="!SLAVE!" pad="9"/>
<connect gate="G$1" pin="!UDS!" pad="72"/>
<connect gate="G$1" pin="!VMA!" pad="51"/>
<connect gate="G$1" pin="!VPA!" pad="48"/>
<connect gate="G$1" pin="-12V" pad="20"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="12V" pad="10"/>
<connect gate="G$1" pin="5V@1" pad="5"/>
<connect gate="G$1" pin="5V@2" pad="6"/>
<connect gate="G$1" pin="7M" pad="92"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="DA0" pad="75"/>
<connect gate="G$1" pin="DA1" pad="77"/>
<connect gate="G$1" pin="DA10" pad="76"/>
<connect gate="G$1" pin="DA11" pad="71"/>
<connect gate="G$1" pin="DA12" pad="69"/>
<connect gate="G$1" pin="DA13" pad="67"/>
<connect gate="G$1" pin="DA14" pad="65"/>
<connect gate="G$1" pin="DA15" pad="63"/>
<connect gate="G$1" pin="DA2" pad="79"/>
<connect gate="G$1" pin="DA3" pad="81"/>
<connect gate="G$1" pin="DA4" pad="83"/>
<connect gate="G$1" pin="DA5" pad="86"/>
<connect gate="G$1" pin="DA6" pad="84"/>
<connect gate="G$1" pin="DA7" pad="82"/>
<connect gate="G$1" pin="DA8" pad="80"/>
<connect gate="G$1" pin="DA9" pad="78"/>
<connect gate="G$1" pin="DOE" pad="93"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@12" pad="87"/>
<connect gate="G$1" pin="GND@13" pad="88"/>
<connect gate="G$1" pin="GND@14" pad="89"/>
<connect gate="G$1" pin="GND@15" pad="90"/>
<connect gate="G$1" pin="GND@16" pad="99"/>
<connect gate="G$1" pin="GND@17" pad="100"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/!W!" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
<connect gate="G$1" pin="Z3/GND" pad="91"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_EDGE_WITH_OUTLINE" package="ZORRO">
<connects>
<connect gate="G$1" pin="!AS!" pad="74"/>
<connect gate="G$1" pin="!BEER!" pad="46"/>
<connect gate="G$1" pin="!BG!" pad="64"/>
<connect gate="G$1" pin="!BGACK!" pad="62"/>
<connect gate="G$1" pin="!BR!" pad="60"/>
<connect gate="G$1" pin="!BUSRST!" pad="94"/>
<connect gate="G$1" pin="!CCK!" pad="16"/>
<connect gate="G$1" pin="!CCKQ!" pad="14"/>
<connect gate="G$1" pin="!CFIN!" pad="12"/>
<connect gate="G$1" pin="!CFOUT!" pad="11"/>
<connect gate="G$1" pin="!DS1!" pad="98"/>
<connect gate="G$1" pin="!DTACK!" pad="66"/>
<connect gate="G$1" pin="!EINT1!" pad="96"/>
<connect gate="G$1" pin="!EINT4!" pad="44"/>
<connect gate="G$1" pin="!EINT5!" pad="42"/>
<connect gate="G$1" pin="!EINT7!" pad="40"/>
<connect gate="G$1" pin="!FCS!" pad="97"/>
<connect gate="G$1" pin="!GBG!" pad="95"/>
<connect gate="G$1" pin="!HALT!" pad="55"/>
<connect gate="G$1" pin="!INT2!" pad="19"/>
<connect gate="G$1" pin="!INT6!" pad="22"/>
<connect gate="G$1" pin="!LDS!" pad="70"/>
<connect gate="G$1" pin="!OVR!" pad="17"/>
<connect gate="G$1" pin="!OWN!" pad="7"/>
<connect gate="G$1" pin="!RESET!" pad="53"/>
<connect gate="G$1" pin="!SLAVE!" pad="9"/>
<connect gate="G$1" pin="!UDS!" pad="72"/>
<connect gate="G$1" pin="!VMA!" pad="51"/>
<connect gate="G$1" pin="!VPA!" pad="48"/>
<connect gate="G$1" pin="-12V" pad="20"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="12V" pad="10"/>
<connect gate="G$1" pin="5V@1" pad="5"/>
<connect gate="G$1" pin="5V@2" pad="6"/>
<connect gate="G$1" pin="7M" pad="92"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="DA0" pad="75"/>
<connect gate="G$1" pin="DA1" pad="77"/>
<connect gate="G$1" pin="DA10" pad="76"/>
<connect gate="G$1" pin="DA11" pad="71"/>
<connect gate="G$1" pin="DA12" pad="69"/>
<connect gate="G$1" pin="DA13" pad="67"/>
<connect gate="G$1" pin="DA14" pad="65"/>
<connect gate="G$1" pin="DA15" pad="63"/>
<connect gate="G$1" pin="DA2" pad="79"/>
<connect gate="G$1" pin="DA3" pad="81"/>
<connect gate="G$1" pin="DA4" pad="83"/>
<connect gate="G$1" pin="DA5" pad="86"/>
<connect gate="G$1" pin="DA6" pad="84"/>
<connect gate="G$1" pin="DA7" pad="82"/>
<connect gate="G$1" pin="DA8" pad="80"/>
<connect gate="G$1" pin="DA9" pad="78"/>
<connect gate="G$1" pin="DOE" pad="93"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@1" pad="1"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@12" pad="87"/>
<connect gate="G$1" pin="GND@13" pad="88"/>
<connect gate="G$1" pin="GND@14" pad="89"/>
<connect gate="G$1" pin="GND@15" pad="90"/>
<connect gate="G$1" pin="GND@16" pad="99"/>
<connect gate="G$1" pin="GND@17" pad="100"/>
<connect gate="G$1" pin="GND@2" pad="2"/>
<connect gate="G$1" pin="GND@3" pad="3"/>
<connect gate="G$1" pin="GND@4" pad="4"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/!W!" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
<connect gate="G$1" pin="Z3/GND" pad="91"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND55" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND56" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="P+12" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="P+14" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="GND62" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND63" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND64" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND65" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND66" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND67" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND68" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND69" library="SUPPLY" deviceset="GND" device="" value="GND"/>
<part name="X1" library="con-ml" deviceset="ML20" device=""/>
<part name="GND20" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND21" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND22" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND23" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X2" library="con-ml" deviceset="ML20" device=""/>
<part name="GND3" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND4" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND5" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND6" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X3" library="con-ml" deviceset="ML20" device=""/>
<part name="GND7" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND8" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND9" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND10" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X4" library="con-ml" deviceset="ML20" device=""/>
<part name="GND11" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND12" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND13" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND14" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X5" library="con-ml" deviceset="ML20" device=""/>
<part name="GND15" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND16" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND17" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND18" library="SUPPLY1" deviceset="GND" device=""/>
<part name="CN1" library="Amiga" deviceset="ZORROII" device="_SLOT_SMALL_HOLES"/>
<part name="GND1" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND2" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="P+1" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="P+2" library="SUPPLY1" deviceset="VCC" device="" value="VCC"/>
<part name="GND19" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND24" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND25" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND26" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND27" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND28" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND29" library="SUPPLY1" deviceset="GND" device="" value="GND"/>
<part name="GND30" library="SUPPLY" deviceset="GND" device="" value="GND"/>
<part name="CN2" library="Amiga" deviceset="ZORROII" device="_EDGE_ROUND_CONTACTS"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="40.64" y="117.1575" size="1.27" layer="95">/CCKQ</text>
<text x="40.64" y="114.6175" size="1.27" layer="95">/CCK</text>
<text x="40.64" y="117.1575" size="1.27" layer="95">/CCKQ</text>
<text x="40.64" y="106.9975" size="1.27" layer="95">/INT6</text>
<text x="40.64" y="73.9775" size="1.27" layer="95">/VPA</text>
<text x="40.64" y="71.4375" size="1.27" layer="95">E</text>
<text x="40.64" y="53.6575" size="1.27" layer="95">/BG</text>
<text x="40.64" y="56.1975" size="1.27" layer="95">/BGACK</text>
<text x="40.64" y="51.1175" size="1.27" layer="95">/DTACK</text>
<text x="40.64" y="58.7375" size="1.27" layer="95">/BR</text>
</plain>
<instances>
<instance part="GND55" gate="1" x="-3.81" y="132.08" rot="R270"/>
<instance part="GND56" gate="1" x="40.64" y="132.08" rot="R90"/>
<instance part="P+12" gate="VCC" x="40.64" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="37.7825" y="127.3175" size="1.016" layer="96"/>
</instance>
<instance part="P+14" gate="VCC" x="-6.35" y="127" smashed="yes" rot="MR270">
<attribute name="VALUE" x="-3.4925" y="127.635" size="1.016" layer="96" rot="MR0"/>
</instance>
<instance part="GND62" gate="1" x="-6.35" y="25.4" rot="R270"/>
<instance part="GND63" gate="1" x="-3.81" y="40.64" rot="R270"/>
<instance part="GND64" gate="1" x="-3.81" y="55.88" rot="R270"/>
<instance part="GND65" gate="1" x="-3.81" y="71.12" rot="R270"/>
<instance part="GND66" gate="1" x="-3.81" y="116.84" rot="R270"/>
<instance part="GND67" gate="1" x="-3.81" y="101.6" rot="R270"/>
<instance part="GND68" gate="1" x="-3.81" y="86.36" rot="R270"/>
<instance part="GND69" gate="1" x="43.18" y="22.86" rot="R90"/>
<instance part="X1" gate="1" x="184.15" y="106.68"/>
<instance part="GND20" gate="1" x="171.45" y="116.84" rot="R270"/>
<instance part="GND21" gate="1" x="171.45" y="93.98" rot="R270"/>
<instance part="GND22" gate="1" x="196.85" y="93.98" rot="R90"/>
<instance part="GND23" gate="1" x="196.85" y="116.84" rot="R90"/>
<instance part="X2" gate="1" x="222.25" y="106.68"/>
<instance part="GND3" gate="1" x="209.55" y="116.84" rot="R270"/>
<instance part="GND4" gate="1" x="209.55" y="93.98" rot="R270"/>
<instance part="GND5" gate="1" x="234.95" y="93.98" rot="R90"/>
<instance part="GND6" gate="1" x="234.95" y="116.84" rot="R90"/>
<instance part="X3" gate="1" x="184.15" y="73.66"/>
<instance part="GND7" gate="1" x="171.45" y="83.82" rot="R270"/>
<instance part="GND8" gate="1" x="171.45" y="60.96" rot="R270"/>
<instance part="GND9" gate="1" x="196.85" y="60.96" rot="R90"/>
<instance part="GND10" gate="1" x="196.85" y="83.82" rot="R90"/>
<instance part="X4" gate="1" x="222.25" y="73.66"/>
<instance part="GND11" gate="1" x="209.55" y="83.82" rot="R270"/>
<instance part="GND12" gate="1" x="209.55" y="60.96" rot="R270"/>
<instance part="GND13" gate="1" x="234.95" y="60.96" rot="R90"/>
<instance part="GND14" gate="1" x="234.95" y="83.82" rot="R90"/>
<instance part="X5" gate="1" x="222.25" y="31.75"/>
<instance part="GND15" gate="1" x="209.55" y="41.91" rot="R270"/>
<instance part="GND16" gate="1" x="209.55" y="19.05" rot="R270"/>
<instance part="GND17" gate="1" x="234.95" y="19.05" rot="R90"/>
<instance part="GND18" gate="1" x="234.95" y="41.91" rot="R90"/>
<instance part="CN1" gate="G$1" x="101.6" y="71.12"/>
<instance part="GND1" gate="1" x="78.74" y="132.08" rot="R270"/>
<instance part="GND2" gate="1" x="123.19" y="132.08" rot="R90"/>
<instance part="P+1" gate="VCC" x="123.19" y="127" smashed="yes" rot="R270">
<attribute name="VALUE" x="120.3325" y="127.3175" size="1.016" layer="96"/>
</instance>
<instance part="P+2" gate="VCC" x="76.2" y="127" smashed="yes" rot="MR270">
<attribute name="VALUE" x="79.0575" y="127.635" size="1.016" layer="96" rot="MR0"/>
</instance>
<instance part="GND19" gate="1" x="76.2" y="25.4" rot="R270"/>
<instance part="GND24" gate="1" x="78.74" y="40.64" rot="R270"/>
<instance part="GND25" gate="1" x="78.74" y="55.88" rot="R270"/>
<instance part="GND26" gate="1" x="78.74" y="71.12" rot="R270"/>
<instance part="GND27" gate="1" x="78.74" y="116.84" rot="R270"/>
<instance part="GND28" gate="1" x="78.74" y="101.6" rot="R270"/>
<instance part="GND29" gate="1" x="78.74" y="86.36" rot="R270"/>
<instance part="GND30" gate="1" x="125.73" y="22.86" rot="R90"/>
<instance part="CN2" gate="G$1" x="19.05" y="71.12"/>
</instances>
<busses>
<bus name="/OWN,/SLAVE,CFGOUT,/CCKQ,/CCK,CDAC,/OVR,XRDY,/INT2,/INT6,A[1..23],/IPL[0..2],FC[0..2],/BERR,/VPA,E,/VMA,/RESET,/HALT,/BR,/BGACK,/BG,DA[0..15],/DTACK,R/W,/LDS,/UDS,7MHZ,IORST,DOE,/EINT1,/GBG,/FCS,/DS1,Z3,GND,VCC,CFGIN,/AS,28M,BOSS,PROBE[0..3]">
<segment>
<wire x1="53.34" y1="132.08" x2="55.88" y2="132.08" width="0.762" layer="92"/>
<wire x1="55.88" y1="132.08" x2="55.88" y2="2.54" width="0.762" layer="92"/>
<wire x1="55.88" y1="2.54" x2="157.48" y2="2.54" width="0.762" layer="92"/>
<wire x1="157.48" y1="2.54" x2="157.48" y2="134.62" width="0.762" layer="92"/>
<wire x1="55.88" y1="2.54" x2="-16.51" y2="2.54" width="0.762" layer="92"/>
<wire x1="-16.51" y1="2.54" x2="-16.51" y2="137.16" width="0.762" layer="92"/>
<wire x1="157.48" y1="2.54" x2="205.74" y2="2.54" width="0.762" layer="92"/>
<wire x1="205.74" y1="2.54" x2="247.65" y2="2.54" width="0.762" layer="92"/>
<wire x1="247.65" y1="2.54" x2="250.19" y2="5.08" width="0.762" layer="92"/>
<wire x1="250.19" y1="5.08" x2="250.19" y2="121.92" width="0.762" layer="92"/>
<wire x1="205.74" y1="2.54" x2="205.74" y2="123.19" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="R/W" class="0">
<segment>
<label x="40.64" y="48.5775" size="1.27" layer="95"/>
<wire x1="35.56" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<wire x1="53.34" y1="48.26" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="R/!W!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="9"/>
<wire x1="229.87" y1="104.14" x2="247.65" y2="104.14" width="0.1524" layer="91"/>
<wire x1="247.65" y1="104.14" x2="250.19" y2="101.6" width="0.1524" layer="91"/>
<label x="234.95" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="48.26" x2="135.89" y2="48.26" width="0.1524" layer="91"/>
<wire x1="135.89" y1="48.26" x2="138.43" y2="45.72" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="R/!W!"/>
<label x="123.19" y="48.26" size="1.27" layer="95"/>
</segment>
</net>
<net name="/UDS" class="0">
<segment>
<label x="40.64" y="43.4975" size="1.27" layer="95"/>
<wire x1="35.56" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="53.34" y1="43.18" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!UDS!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="5"/>
<wire x1="229.87" y1="99.06" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="246.38" y1="99.06" x2="250.19" y2="95.25" width="0.1524" layer="91"/>
<label x="234.95" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="43.18" x2="135.89" y2="43.18" width="0.1524" layer="91"/>
<wire x1="135.89" y1="43.18" x2="138.43" y2="40.64" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!UDS!"/>
<label x="123.19" y="43.18" size="1.27" layer="95"/>
</segment>
</net>
<net name="/LDS" class="0">
<segment>
<label x="40.64" y="46.0375" size="1.27" layer="95"/>
<wire x1="35.56" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="53.34" y1="45.72" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!LDS!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="7"/>
<wire x1="229.87" y1="101.6" x2="247.65" y2="101.6" width="0.1524" layer="91"/>
<wire x1="247.65" y1="101.6" x2="250.19" y2="99.06" width="0.1524" layer="91"/>
<label x="234.95" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="45.72" x2="135.89" y2="45.72" width="0.1524" layer="91"/>
<wire x1="135.89" y1="45.72" x2="138.43" y2="43.18" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!LDS!"/>
<label x="123.19" y="45.72" size="1.27" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-1.27" y1="132.08" x2="0" y2="132.08" width="0.1524" layer="91"/>
<wire x1="0" y1="132.08" x2="1.27" y2="132.08" width="0.1524" layer="91"/>
<wire x1="1.27" y1="129.54" x2="0" y2="129.54" width="0.1524" layer="91"/>
<wire x1="0" y1="129.54" x2="0" y2="132.08" width="0.1524" layer="91"/>
<junction x="0" y="132.08"/>
<pinref part="GND55" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@1"/>
<pinref part="CN2" gate="G$1" pin="GND@3"/>
</segment>
<segment>
<wire x1="38.1" y1="132.08" x2="36.83" y2="132.08" width="0.1524" layer="91"/>
<wire x1="36.83" y1="132.08" x2="35.56" y2="132.08" width="0.1524" layer="91"/>
<wire x1="36.83" y1="132.08" x2="36.83" y2="129.54" width="0.1524" layer="91"/>
<wire x1="36.83" y1="129.54" x2="35.56" y2="129.54" width="0.1524" layer="91"/>
<junction x="36.83" y="132.08"/>
<pinref part="GND56" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@2"/>
<pinref part="CN2" gate="G$1" pin="GND@4"/>
</segment>
<segment>
<wire x1="-1.27" y1="40.64" x2="1.27" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND63" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@10"/>
</segment>
<segment>
<wire x1="-1.27" y1="55.88" x2="1.27" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND64" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@9"/>
</segment>
<segment>
<wire x1="-1.27" y1="71.12" x2="1.27" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND65" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@8"/>
</segment>
<segment>
<wire x1="-1.27" y1="116.84" x2="1.27" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND66" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@5"/>
</segment>
<segment>
<wire x1="-1.27" y1="101.6" x2="1.27" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND67" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@6"/>
</segment>
<segment>
<wire x1="-1.27" y1="86.36" x2="1.27" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND68" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@7"/>
</segment>
<segment>
<wire x1="1.27" y1="25.4" x2="-1.27" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="25.4" x2="-3.81" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="20.32" x2="-1.27" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="22.86" x2="-1.27" y2="25.4" width="0.1524" layer="91"/>
<wire x1="1.27" y1="22.86" x2="-1.27" y2="22.86" width="0.1524" layer="91"/>
<wire x1="1.27" y1="20.32" x2="-1.27" y2="20.32" width="0.1524" layer="91"/>
<wire x1="1.27" y1="7.62" x2="-1.27" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="20.32" width="0.1524" layer="91"/>
<junction x="-1.27" y="20.32"/>
<junction x="-1.27" y="22.86"/>
<junction x="-1.27" y="25.4"/>
<pinref part="GND62" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@11"/>
<pinref part="CN2" gate="G$1" pin="GND@12"/>
<pinref part="CN2" gate="G$1" pin="GND@14"/>
<pinref part="CN2" gate="G$1" pin="GND@16"/>
</segment>
<segment>
<wire x1="40.64" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="38.1" y1="22.86" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<wire x1="38.1" y1="20.32" x2="35.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="38.1" y1="20.32" x2="38.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="38.1" y1="7.62" x2="35.56" y2="7.62" width="0.1524" layer="91"/>
<junction x="38.1" y="20.32"/>
<junction x="38.1" y="22.86"/>
<pinref part="GND69" gate="1" pin="GND"/>
<pinref part="CN2" gate="G$1" pin="GND@13"/>
<pinref part="CN2" gate="G$1" pin="GND@15"/>
<pinref part="CN2" gate="G$1" pin="GND@17"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="20"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="173.99" y1="116.84" x2="176.53" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="173.99" y1="93.98" x2="176.53" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="194.31" y1="93.98" x2="191.77" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="19"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="194.31" y1="116.84" x2="191.77" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="20"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="212.09" y1="116.84" x2="214.63" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="212.09" y1="93.98" x2="214.63" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="232.41" y1="93.98" x2="229.87" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="19"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="232.41" y1="116.84" x2="229.87" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="20"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="173.99" y1="83.82" x2="176.53" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="173.99" y1="60.96" x2="176.53" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="194.31" y1="60.96" x2="191.77" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="19"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="194.31" y1="83.82" x2="191.77" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="20"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="212.09" y1="83.82" x2="214.63" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="212.09" y1="60.96" x2="214.63" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="1"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="232.41" y1="60.96" x2="229.87" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="19"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="232.41" y1="83.82" x2="229.87" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="20"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="212.09" y1="41.91" x2="214.63" y2="41.91" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="212.09" y1="19.05" x2="214.63" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="232.41" y1="19.05" x2="229.87" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="19"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="232.41" y1="41.91" x2="229.87" y2="41.91" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="81.28" y1="132.08" x2="82.55" y2="132.08" width="0.1524" layer="91"/>
<wire x1="82.55" y1="132.08" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<wire x1="83.82" y1="129.54" x2="82.55" y2="129.54" width="0.1524" layer="91"/>
<wire x1="82.55" y1="129.54" x2="82.55" y2="132.08" width="0.1524" layer="91"/>
<junction x="82.55" y="132.08"/>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@1"/>
<pinref part="CN1" gate="G$1" pin="GND@3"/>
</segment>
<segment>
<wire x1="120.65" y1="132.08" x2="119.38" y2="132.08" width="0.1524" layer="91"/>
<wire x1="119.38" y1="132.08" x2="118.11" y2="132.08" width="0.1524" layer="91"/>
<wire x1="119.38" y1="132.08" x2="119.38" y2="129.54" width="0.1524" layer="91"/>
<wire x1="119.38" y1="129.54" x2="118.11" y2="129.54" width="0.1524" layer="91"/>
<junction x="119.38" y="132.08"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@2"/>
<pinref part="CN1" gate="G$1" pin="GND@4"/>
</segment>
<segment>
<wire x1="81.28" y1="40.64" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@10"/>
</segment>
<segment>
<wire x1="81.28" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@9"/>
</segment>
<segment>
<wire x1="81.28" y1="71.12" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@8"/>
</segment>
<segment>
<wire x1="81.28" y1="116.84" x2="83.82" y2="116.84" width="0.1524" layer="91"/>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@5"/>
</segment>
<segment>
<wire x1="81.28" y1="101.6" x2="83.82" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND28" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@6"/>
</segment>
<segment>
<wire x1="81.28" y1="86.36" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@7"/>
</segment>
<segment>
<wire x1="83.82" y1="25.4" x2="81.28" y2="25.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="25.4" x2="78.74" y2="25.4" width="0.1524" layer="91"/>
<wire x1="81.28" y1="20.32" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<wire x1="81.28" y1="22.86" x2="81.28" y2="25.4" width="0.1524" layer="91"/>
<wire x1="83.82" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<wire x1="83.82" y1="20.32" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<wire x1="83.82" y1="7.62" x2="81.28" y2="7.62" width="0.1524" layer="91"/>
<wire x1="81.28" y1="7.62" x2="81.28" y2="20.32" width="0.1524" layer="91"/>
<junction x="81.28" y="20.32"/>
<junction x="81.28" y="22.86"/>
<junction x="81.28" y="25.4"/>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@11"/>
<pinref part="CN1" gate="G$1" pin="GND@12"/>
<pinref part="CN1" gate="G$1" pin="GND@14"/>
<pinref part="CN1" gate="G$1" pin="GND@16"/>
</segment>
<segment>
<wire x1="123.19" y1="22.86" x2="120.65" y2="22.86" width="0.1524" layer="91"/>
<wire x1="120.65" y1="22.86" x2="118.11" y2="22.86" width="0.1524" layer="91"/>
<wire x1="120.65" y1="22.86" x2="120.65" y2="20.32" width="0.1524" layer="91"/>
<wire x1="120.65" y1="20.32" x2="118.11" y2="20.32" width="0.1524" layer="91"/>
<wire x1="120.65" y1="20.32" x2="120.65" y2="7.62" width="0.1524" layer="91"/>
<wire x1="120.65" y1="7.62" x2="118.11" y2="7.62" width="0.1524" layer="91"/>
<junction x="120.65" y="20.32"/>
<junction x="120.65" y="22.86"/>
<pinref part="GND30" gate="1" pin="GND"/>
<pinref part="CN1" gate="G$1" pin="GND@13"/>
<pinref part="CN1" gate="G$1" pin="GND@15"/>
<pinref part="CN1" gate="G$1" pin="GND@17"/>
</segment>
</net>
<net name="A17" class="0">
<segment>
<label x="-2.54" y="73.66" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="73.66" x2="-15.24" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="73.66" x2="-16.51" y2="72.39" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A17"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="12"/>
<wire x1="176.53" y1="73.66" x2="160.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="73.66" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="165.1" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="73.66" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="73.66" x2="67.31" y2="73.66" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A17"/>
</segment>
</net>
<net name="A18" class="0">
<segment>
<label x="40.64" y="68.8975" size="1.27" layer="95"/>
<wire x1="35.56" y1="68.58" x2="53.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="53.34" y1="68.58" x2="55.88" y2="66.04" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A18"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="10"/>
<wire x1="176.53" y1="71.12" x2="160.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="160.02" y1="71.12" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<label x="165.1" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="68.58" x2="135.89" y2="68.58" width="0.1524" layer="91"/>
<wire x1="135.89" y1="68.58" x2="138.43" y2="66.04" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A18"/>
<label x="123.19" y="68.58" size="1.27" layer="95"/>
</segment>
</net>
<net name="A20" class="0">
<segment>
<label x="40.64" y="63.8175" size="1.27" layer="95"/>
<wire x1="35.56" y1="63.5" x2="53.34" y2="63.5" width="0.1524" layer="91"/>
<wire x1="53.34" y1="63.5" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A20"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="6"/>
<wire x1="176.53" y1="66.04" x2="161.29" y2="66.04" width="0.1524" layer="91"/>
<wire x1="161.29" y1="66.04" x2="157.48" y2="62.23" width="0.1524" layer="91"/>
<label x="165.1" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="63.5" x2="135.89" y2="63.5" width="0.1524" layer="91"/>
<wire x1="135.89" y1="63.5" x2="138.43" y2="60.96" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A20"/>
<label x="123.19" y="63.5" size="1.27" layer="95"/>
</segment>
</net>
<net name="A19" class="0">
<segment>
<label x="40.64" y="66.3575" size="1.27" layer="95"/>
<wire x1="35.56" y1="66.04" x2="53.34" y2="66.04" width="0.1524" layer="91"/>
<wire x1="53.34" y1="66.04" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A19"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="8"/>
<wire x1="176.53" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="160.02" y1="68.58" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<label x="165.1" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="66.04" x2="135.89" y2="66.04" width="0.1524" layer="91"/>
<wire x1="135.89" y1="66.04" x2="138.43" y2="63.5" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A19"/>
<label x="123.19" y="66.04" size="1.27" layer="95"/>
</segment>
</net>
<net name="A16" class="0">
<segment>
<label x="-2.54" y="76.2" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="76.2" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="76.2" x2="-16.51" y2="74.93" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A16"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="14"/>
<wire x1="176.53" y1="76.2" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
<wire x1="160.02" y1="76.2" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<label x="165.1" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="76.2" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="76.2" x2="67.31" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A16"/>
</segment>
</net>
<net name="A15" class="0">
<segment>
<label x="-2.54" y="78.74" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="78.74" x2="-15.24" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="78.74" x2="-16.51" y2="77.47" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A15"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="16"/>
<wire x1="176.53" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<label x="165.1" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="78.74" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="78.74" x2="67.31" y2="78.74" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A15"/>
</segment>
</net>
<net name="A14" class="0">
<segment>
<label x="-2.54" y="81.28" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="81.28" x2="-15.24" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="81.28" x2="-16.51" y2="80.01" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A14"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="18"/>
<wire x1="176.53" y1="81.28" x2="160.02" y2="81.28" width="0.1524" layer="91"/>
<wire x1="160.02" y1="81.28" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<label x="165.1" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="81.28" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="81.28" x2="67.31" y2="81.28" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A14"/>
</segment>
</net>
<net name="A13" class="0">
<segment>
<label x="-2.54" y="83.82" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="83.82" x2="-15.24" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="83.82" x2="-16.51" y2="82.55" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A13"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="4"/>
<wire x1="214.63" y1="63.5" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<wire x1="208.28" y1="63.5" x2="205.74" y2="60.96" width="0.1524" layer="91"/>
<label x="209.55" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="83.82" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="83.82" x2="67.31" y2="83.82" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A13"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<label x="40.64" y="86.6775" size="1.27" layer="95"/>
<wire x1="35.56" y1="86.36" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="53.34" y1="86.36" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A12"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="6"/>
<wire x1="214.63" y1="66.04" x2="208.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="208.28" y1="66.04" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<label x="209.55" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="86.36" x2="135.89" y2="86.36" width="0.1524" layer="91"/>
<wire x1="135.89" y1="86.36" x2="138.43" y2="83.82" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A12"/>
<label x="123.19" y="86.36" size="1.27" layer="95"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<label x="40.64" y="89.2175" size="1.27" layer="95"/>
<wire x1="35.56" y1="88.9" x2="53.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="53.34" y1="88.9" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A11"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="8"/>
<wire x1="214.63" y1="68.58" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
<wire x1="208.28" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<label x="209.55" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="88.9" x2="135.89" y2="88.9" width="0.1524" layer="91"/>
<wire x1="135.89" y1="88.9" x2="138.43" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A11"/>
<label x="123.19" y="88.9" size="1.27" layer="95"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<label x="40.64" y="91.7575" size="1.27" layer="95"/>
<wire x1="35.56" y1="91.44" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A10"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="10"/>
<wire x1="214.63" y1="71.12" x2="208.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<label x="209.55" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="91.44" x2="135.89" y2="91.44" width="0.1524" layer="91"/>
<wire x1="135.89" y1="91.44" x2="138.43" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A10"/>
<label x="123.19" y="91.44" size="1.27" layer="95"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<label x="40.64" y="94.2975" size="1.27" layer="95"/>
<wire x1="35.56" y1="93.98" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="93.98" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A9"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="12"/>
<wire x1="214.63" y1="73.66" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<label x="209.55" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="93.98" x2="135.89" y2="93.98" width="0.1524" layer="91"/>
<wire x1="135.89" y1="93.98" x2="138.43" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A9"/>
<label x="123.19" y="93.98" size="1.27" layer="95"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<label x="40.64" y="96.8375" size="1.27" layer="95"/>
<wire x1="35.56" y1="96.52" x2="53.34" y2="96.52" width="0.1524" layer="91"/>
<wire x1="53.34" y1="96.52" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A8"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="14"/>
<wire x1="214.63" y1="76.2" x2="208.28" y2="76.2" width="0.1524" layer="91"/>
<wire x1="208.28" y1="76.2" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<label x="209.55" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="96.52" x2="135.89" y2="96.52" width="0.1524" layer="91"/>
<wire x1="135.89" y1="96.52" x2="138.43" y2="93.98" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A8"/>
<label x="123.19" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<label x="40.64" y="99.3775" size="1.27" layer="95"/>
<wire x1="35.56" y1="99.06" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="53.34" y1="99.06" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A7"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="16"/>
<wire x1="214.63" y1="78.74" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<label x="209.55" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="99.06" x2="135.89" y2="99.06" width="0.1524" layer="91"/>
<wire x1="135.89" y1="99.06" x2="138.43" y2="96.52" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A7"/>
<label x="123.19" y="99.06" size="1.27" layer="95"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<label x="-2.54" y="104.14" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="104.14" x2="-13.97" y2="104.14" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="104.14" x2="-16.51" y2="101.6" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A6"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="18"/>
<wire x1="214.63" y1="81.28" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="208.28" y1="81.28" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<label x="209.55" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="104.14" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="104.14" x2="68.58" y2="104.14" width="0.1524" layer="91"/>
<wire x1="68.58" y1="104.14" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A6"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<label x="-2.54" y="106.68" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="106.68" x2="-13.97" y2="106.68" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="106.68" x2="-16.51" y2="104.14" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A5"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="4"/>
<wire x1="214.63" y1="21.59" x2="208.28" y2="21.59" width="0.1524" layer="91"/>
<wire x1="208.28" y1="21.59" x2="205.74" y2="19.05" width="0.1524" layer="91"/>
<label x="209.55" y="21.59" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="106.68" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<wire x1="68.58" y1="106.68" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A5"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<label x="40.64" y="104.4575" size="1.27" layer="95"/>
<wire x1="35.56" y1="104.14" x2="53.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="53.34" y1="104.14" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A4"/>
</segment>
<segment>
<wire x1="118.11" y1="104.14" x2="135.89" y2="104.14" width="0.1524" layer="91"/>
<wire x1="135.89" y1="104.14" x2="138.43" y2="101.6" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A4"/>
<label x="123.19" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="3"/>
<wire x1="229.87" y1="21.59" x2="246.38" y2="21.59" width="0.1524" layer="91"/>
<wire x1="246.38" y1="21.59" x2="250.19" y2="17.78" width="0.1524" layer="91"/>
<label x="234.95" y="21.59" size="1.27" layer="95"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<label x="40.64" y="101.9175" size="1.27" layer="95"/>
<wire x1="35.56" y1="101.6" x2="53.34" y2="101.6" width="0.1524" layer="91"/>
<wire x1="53.34" y1="101.6" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A3"/>
</segment>
<segment>
<wire x1="118.11" y1="101.6" x2="135.89" y2="101.6" width="0.1524" layer="91"/>
<wire x1="135.89" y1="101.6" x2="138.43" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A3"/>
<label x="123.19" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="17"/>
<wire x1="229.87" y1="81.28" x2="247.65" y2="81.28" width="0.1524" layer="91"/>
<wire x1="247.65" y1="81.28" x2="250.19" y2="78.74" width="0.1524" layer="91"/>
<label x="234.95" y="81.28" size="1.27" layer="95"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<label x="-2.54" y="99.06" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="99.06" x2="-15.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="99.06" x2="-16.51" y2="97.79" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A2"/>
</segment>
<segment>
<label x="80.01" y="99.06" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="99.06" x2="67.31" y2="99.06" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A2"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="15"/>
<wire x1="229.87" y1="78.74" x2="247.65" y2="78.74" width="0.1524" layer="91"/>
<wire x1="247.65" y1="78.74" x2="250.19" y2="76.2" width="0.1524" layer="91"/>
<label x="234.95" y="78.74" size="1.27" layer="95"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<label x="-2.54" y="96.52" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="96.52" x2="-15.24" y2="96.52" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="96.52" x2="-16.51" y2="95.25" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A1"/>
</segment>
<segment>
<label x="80.01" y="96.52" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="96.52" x2="67.31" y2="96.52" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A1"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="13"/>
<wire x1="229.87" y1="76.2" x2="247.65" y2="76.2" width="0.1524" layer="91"/>
<wire x1="247.65" y1="76.2" x2="250.19" y2="73.66" width="0.1524" layer="91"/>
<label x="234.95" y="76.2" size="1.27" layer="95"/>
</segment>
</net>
<net name="/AS" class="0">
<segment>
<label x="40.64" y="40.9575" size="1.27" layer="95"/>
<wire x1="35.56" y1="40.64" x2="53.34" y2="40.64" width="0.1524" layer="91"/>
<wire x1="53.34" y1="40.64" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!AS!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="3"/>
<wire x1="229.87" y1="96.52" x2="246.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="246.38" y1="96.52" x2="250.19" y2="92.71" width="0.1524" layer="91"/>
<label x="234.95" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="40.64" x2="135.89" y2="40.64" width="0.1524" layer="91"/>
<wire x1="135.89" y1="40.64" x2="138.43" y2="38.1" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!AS!"/>
<label x="123.19" y="40.64" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA0" class="0">
<segment>
<label x="-2.54" y="38.1" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="38.1" x2="-12.7" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="38.1" x2="-16.51" y2="34.29" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA0"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="17"/>
<wire x1="191.77" y1="114.3" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
<wire x1="203.2" y1="114.3" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<label x="194.31" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="38.1" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="38.1" x2="69.85" y2="38.1" width="0.1524" layer="91"/>
<wire x1="69.85" y1="38.1" x2="66.04" y2="34.29" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA0"/>
</segment>
</net>
<net name="DA2" class="0">
<segment>
<label x="-2.54" y="33.02" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="33.02" x2="-12.7" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="33.02" x2="-16.51" y2="29.21" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA2"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="13"/>
<wire x1="191.77" y1="109.22" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<wire x1="203.2" y1="109.22" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<label x="194.31" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="33.02" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="33.02" x2="69.85" y2="33.02" width="0.1524" layer="91"/>
<wire x1="69.85" y1="33.02" x2="66.04" y2="29.21" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA2"/>
</segment>
</net>
<net name="DA1" class="0">
<segment>
<label x="-2.54" y="35.56" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="35.56" x2="-12.7" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="35.56" x2="-16.51" y2="31.75" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA1"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="15"/>
<wire x1="191.77" y1="111.76" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="203.2" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<label x="194.31" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="35.56" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="35.56" x2="69.85" y2="35.56" width="0.1524" layer="91"/>
<wire x1="69.85" y1="35.56" x2="66.04" y2="31.75" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA1"/>
</segment>
</net>
<net name="DA3" class="0">
<segment>
<label x="-2.54" y="30.48" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="30.48" x2="-11.43" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-11.43" y1="30.48" x2="-16.51" y2="25.4" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA3"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="11"/>
<wire x1="191.77" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="106.68" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<label x="194.31" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="30.48" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="30.48" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<wire x1="71.12" y1="30.48" x2="66.04" y2="25.4" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA3"/>
</segment>
</net>
<net name="DA4" class="0">
<segment>
<label x="-2.54" y="27.94" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="27.94" x2="-11.43" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-11.43" y1="27.94" x2="-16.51" y2="22.86" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA4"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="9"/>
<wire x1="191.77" y1="104.14" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="203.2" y1="104.14" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<label x="194.31" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="27.94" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="27.94" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="27.94" x2="66.04" y2="22.86" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA4"/>
</segment>
</net>
<net name="DA5" class="0">
<segment>
<label x="40.64" y="25.7175" size="1.27" layer="95"/>
<wire x1="35.56" y1="25.4" x2="53.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="53.34" y1="25.4" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA5"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="10"/>
<wire x1="176.53" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<wire x1="160.02" y1="104.14" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<label x="165.1" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="25.4" x2="135.89" y2="25.4" width="0.1524" layer="91"/>
<wire x1="135.89" y1="25.4" x2="138.43" y2="22.86" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA5"/>
<label x="124.46" y="25.4" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA6" class="0">
<segment>
<label x="40.64" y="28.2575" size="1.27" layer="95"/>
<wire x1="35.56" y1="27.94" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="27.94" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA6"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="12"/>
<wire x1="176.53" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="106.68" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
<label x="165.1" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="27.94" x2="135.89" y2="27.94" width="0.1524" layer="91"/>
<wire x1="135.89" y1="27.94" x2="138.43" y2="25.4" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA6"/>
<label x="123.19" y="27.94" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA7" class="0">
<segment>
<label x="40.64" y="30.7975" size="1.27" layer="95"/>
<wire x1="35.56" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA7"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="14"/>
<wire x1="176.53" y1="109.22" x2="160.02" y2="109.22" width="0.1524" layer="91"/>
<wire x1="160.02" y1="109.22" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<label x="165.1" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="30.48" x2="135.89" y2="30.48" width="0.1524" layer="91"/>
<wire x1="135.89" y1="30.48" x2="138.43" y2="27.94" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA7"/>
<label x="123.19" y="30.48" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA8" class="0">
<segment>
<label x="40.64" y="33.3375" size="1.27" layer="95"/>
<wire x1="35.56" y1="33.02" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="33.02" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA8"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="16"/>
<wire x1="176.53" y1="111.76" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="160.02" y1="111.76" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<label x="165.1" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="33.02" x2="135.89" y2="33.02" width="0.1524" layer="91"/>
<wire x1="135.89" y1="33.02" x2="138.43" y2="30.48" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA8"/>
<label x="123.19" y="33.02" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA9" class="0">
<segment>
<label x="40.64" y="35.8775" size="1.27" layer="95"/>
<wire x1="35.56" y1="35.56" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
<wire x1="53.34" y1="35.56" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA9"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="18"/>
<wire x1="176.53" y1="114.3" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<label x="165.1" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="35.56" x2="135.89" y2="35.56" width="0.1524" layer="91"/>
<wire x1="135.89" y1="35.56" x2="138.43" y2="33.02" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA9"/>
<label x="123.19" y="35.56" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA10" class="0">
<segment>
<label x="40.64" y="38.4175" size="1.27" layer="95"/>
<wire x1="35.56" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<wire x1="53.34" y1="38.1" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA10"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="4"/>
<wire x1="214.63" y1="96.52" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="208.28" y1="96.52" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<label x="209.55" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="38.1" x2="135.89" y2="38.1" width="0.1524" layer="91"/>
<wire x1="135.89" y1="38.1" x2="138.43" y2="35.56" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA10"/>
<label x="123.19" y="38.1" size="1.27" layer="95"/>
</segment>
</net>
<net name="DA11" class="0">
<segment>
<label x="-2.54" y="43.18" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="43.18" x2="-12.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="43.18" x2="-16.51" y2="39.37" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA11"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="6"/>
<wire x1="214.63" y1="99.06" x2="208.28" y2="99.06" width="0.1524" layer="91"/>
<wire x1="208.28" y1="99.06" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<label x="209.55" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="43.18" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="43.18" x2="69.85" y2="43.18" width="0.1524" layer="91"/>
<wire x1="69.85" y1="43.18" x2="66.04" y2="39.37" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA11"/>
</segment>
</net>
<net name="DA12" class="0">
<segment>
<label x="-2.54" y="45.72" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="45.72" x2="-12.7" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="45.72" x2="-16.51" y2="41.91" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA12"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="8"/>
<wire x1="214.63" y1="101.6" x2="208.28" y2="101.6" width="0.1524" layer="91"/>
<wire x1="208.28" y1="101.6" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<label x="209.55" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="45.72" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="45.72" x2="69.85" y2="45.72" width="0.1524" layer="91"/>
<wire x1="69.85" y1="45.72" x2="66.04" y2="41.91" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA12"/>
</segment>
</net>
<net name="DA13" class="0">
<segment>
<label x="-2.54" y="48.26" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="48.26" x2="-12.7" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="48.26" x2="-16.51" y2="44.45" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA13"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="10"/>
<wire x1="214.63" y1="104.14" x2="208.28" y2="104.14" width="0.1524" layer="91"/>
<wire x1="208.28" y1="104.14" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<label x="209.55" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="48.26" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="48.26" x2="69.85" y2="48.26" width="0.1524" layer="91"/>
<wire x1="69.85" y1="48.26" x2="66.04" y2="44.45" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA13"/>
</segment>
</net>
<net name="DA14" class="0">
<segment>
<label x="-2.54" y="50.8" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="50.8" x2="-12.7" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="50.8" x2="-16.51" y2="46.99" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA14"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="12"/>
<wire x1="214.63" y1="106.68" x2="208.28" y2="106.68" width="0.1524" layer="91"/>
<wire x1="208.28" y1="106.68" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<label x="209.55" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="50.8" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="50.8" x2="69.85" y2="50.8" width="0.1524" layer="91"/>
<wire x1="69.85" y1="50.8" x2="66.04" y2="46.99" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA14"/>
</segment>
</net>
<net name="DA15" class="0">
<segment>
<label x="-2.54" y="53.34" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="53.34" x2="-12.7" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="53.34" x2="-16.51" y2="49.53" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DA15"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="14"/>
<wire x1="214.63" y1="109.22" x2="208.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="208.28" y1="109.22" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<label x="209.55" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="53.34" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="53.34" x2="69.85" y2="53.34" width="0.1524" layer="91"/>
<wire x1="69.85" y1="53.34" x2="66.04" y2="49.53" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DA15"/>
</segment>
</net>
<net name="XRDY" class="0">
<segment>
<label x="40.64" y="112.0775" size="1.27" layer="95"/>
<wire x1="35.56" y1="111.76" x2="53.34" y2="111.76" width="0.1524" layer="91"/>
<wire x1="53.34" y1="111.76" x2="55.88" y2="109.22" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="XRDY"/>
</segment>
<segment>
<wire x1="118.11" y1="111.76" x2="135.89" y2="111.76" width="0.1524" layer="91"/>
<wire x1="135.89" y1="111.76" x2="138.43" y2="109.22" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="XRDY"/>
<label x="123.19" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="7"/>
<wire x1="229.87" y1="26.67" x2="247.65" y2="26.67" width="0.1524" layer="91"/>
<wire x1="247.65" y1="26.67" x2="250.19" y2="24.13" width="0.1524" layer="91"/>
<label x="234.95" y="26.67" size="1.27" layer="95"/>
</segment>
</net>
<net name="A23" class="0">
<segment>
<label x="-2.54" y="58.42" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="58.42" x2="-12.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="58.42" x2="-16.51" y2="54.61" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A23"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="16"/>
<wire x1="214.63" y1="111.76" x2="208.28" y2="111.76" width="0.1524" layer="91"/>
<wire x1="208.28" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<label x="209.55" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="58.42" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="58.42" x2="69.85" y2="58.42" width="0.1524" layer="91"/>
<wire x1="69.85" y1="58.42" x2="66.04" y2="54.61" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A23"/>
</segment>
</net>
<net name="A22" class="0">
<segment>
<label x="-2.54" y="60.96" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="60.96" x2="-12.7" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="60.96" x2="-16.51" y2="57.15" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A22"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="18"/>
<wire x1="214.63" y1="114.3" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<wire x1="208.28" y1="114.3" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<label x="209.55" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="60.96" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="60.96" x2="69.85" y2="60.96" width="0.1524" layer="91"/>
<wire x1="69.85" y1="60.96" x2="66.04" y2="57.15" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A22"/>
</segment>
</net>
<net name="A21" class="0">
<segment>
<label x="40.64" y="61.2775" size="1.27" layer="95"/>
<wire x1="35.56" y1="60.96" x2="53.34" y2="60.96" width="0.1524" layer="91"/>
<wire x1="53.34" y1="60.96" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="A21"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="4"/>
<wire x1="176.53" y1="63.5" x2="161.29" y2="63.5" width="0.1524" layer="91"/>
<wire x1="161.29" y1="63.5" x2="157.48" y2="59.69" width="0.1524" layer="91"/>
<label x="165.1" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="60.96" x2="135.89" y2="60.96" width="0.1524" layer="91"/>
<wire x1="135.89" y1="60.96" x2="138.43" y2="58.42" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="A21"/>
<label x="123.19" y="60.96" size="1.27" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="38.1" y1="127" x2="35.56" y2="127" width="0.1524" layer="91"/>
<pinref part="P+12" gate="VCC" pin="VCC"/>
<pinref part="CN2" gate="G$1" pin="5V@2"/>
</segment>
<segment>
<wire x1="-3.81" y1="127" x2="1.27" y2="127" width="0.1524" layer="91"/>
<pinref part="P+14" gate="VCC" pin="VCC"/>
<pinref part="CN2" gate="G$1" pin="5V@1"/>
</segment>
<segment>
<wire x1="120.65" y1="127" x2="118.11" y2="127" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<pinref part="CN1" gate="G$1" pin="5V@2"/>
</segment>
<segment>
<wire x1="78.74" y1="127" x2="83.82" y2="127" width="0.1524" layer="91"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<pinref part="CN1" gate="G$1" pin="5V@1"/>
</segment>
</net>
<net name="/RESET" class="0">
<segment>
<label x="-2.54" y="66.04" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="66.04" x2="-12.7" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="66.04" x2="-16.51" y2="62.23" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!RESET!"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="7"/>
<wire x1="191.77" y1="68.58" x2="203.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="203.2" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<label x="194.31" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="66.04" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="66.04" x2="69.85" y2="66.04" width="0.1524" layer="91"/>
<wire x1="69.85" y1="66.04" x2="66.04" y2="62.23" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!RESET!"/>
</segment>
</net>
<net name="/BERR" class="0">
<segment>
<label x="40.64" y="76.5175" size="1.27" layer="95"/>
<wire x1="35.56" y1="76.2" x2="53.34" y2="76.2" width="0.1524" layer="91"/>
<wire x1="53.34" y1="76.2" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!BEER!"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="15"/>
<wire x1="191.77" y1="78.74" x2="203.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="203.2" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<label x="194.31" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="76.2" x2="135.89" y2="76.2" width="0.1524" layer="91"/>
<wire x1="135.89" y1="76.2" x2="138.43" y2="73.66" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!BEER!"/>
<label x="123.19" y="76.2" size="1.27" layer="95"/>
</segment>
</net>
<net name="/SLAVE" class="0">
<segment>
<label x="-2.54" y="121.92" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="121.92" x2="-12.7" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="121.92" x2="-16.51" y2="118.11" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!SLAVE!"/>
</segment>
<segment>
<label x="80.01" y="121.92" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="121.92" x2="69.85" y2="121.92" width="0.1524" layer="91"/>
<wire x1="69.85" y1="121.92" x2="66.04" y2="118.11" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!SLAVE!"/>
</segment>
<segment>
<wire x1="214.63" y1="34.29" x2="208.28" y2="34.29" width="0.1524" layer="91"/>
<wire x1="208.28" y1="34.29" x2="205.74" y2="31.75" width="0.1524" layer="91"/>
<label x="209.55" y="34.29" size="1.27" layer="95"/>
<pinref part="X5" gate="1" pin="14"/>
</segment>
</net>
<net name="CFGOUT" class="0">
<segment>
<label x="-2.54" y="119.38" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="119.38" x2="-12.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="119.38" x2="-16.51" y2="115.57" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!CFOUT!"/>
</segment>
<segment>
<label x="80.01" y="119.38" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="119.38" x2="69.85" y2="119.38" width="0.1524" layer="91"/>
<wire x1="69.85" y1="119.38" x2="66.04" y2="115.57" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!CFOUT!"/>
</segment>
<segment>
<wire x1="214.63" y1="31.75" x2="208.28" y2="31.75" width="0.1524" layer="91"/>
<wire x1="208.28" y1="31.75" x2="205.74" y2="29.21" width="0.1524" layer="91"/>
<label x="209.55" y="31.75" size="1.27" layer="95"/>
<pinref part="X5" gate="1" pin="12"/>
</segment>
</net>
<net name="CFGIN" class="0">
<segment>
<label x="40.64" y="119.6975" size="1.27" layer="95"/>
<wire x1="35.56" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!CFIN!"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="13"/>
<wire x1="229.87" y1="34.29" x2="247.65" y2="34.29" width="0.1524" layer="91"/>
<wire x1="247.65" y1="34.29" x2="250.19" y2="31.75" width="0.1524" layer="91"/>
<label x="234.95" y="34.29" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="119.38" x2="135.89" y2="119.38" width="0.1524" layer="91"/>
<wire x1="135.89" y1="119.38" x2="138.43" y2="116.84" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!CFIN!"/>
<label x="123.19" y="119.38" size="1.27" layer="95"/>
</segment>
</net>
<net name="CDAC" class="0">
<segment>
<label x="-2.54" y="114.3" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="114.3" x2="-13.97" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="114.3" x2="-16.51" y2="111.76" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="CDAC"/>
</segment>
<segment>
<label x="80.01" y="114.3" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="114.3" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<wire x1="68.58" y1="114.3" x2="66.04" y2="111.76" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="CDAC"/>
</segment>
<segment>
<wire x1="214.63" y1="29.21" x2="208.28" y2="29.21" width="0.1524" layer="91"/>
<wire x1="208.28" y1="29.21" x2="205.74" y2="26.67" width="0.1524" layer="91"/>
<label x="209.55" y="29.21" size="1.27" layer="95"/>
<pinref part="X5" gate="1" pin="10"/>
</segment>
</net>
<net name="/OVR" class="0">
<segment>
<label x="-2.54" y="111.76" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="111.76" x2="-12.7" y2="111.76" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="111.76" x2="-16.51" y2="107.95" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!OVR!"/>
</segment>
<segment>
<label x="80.01" y="111.76" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="111.76" x2="69.85" y2="111.76" width="0.1524" layer="91"/>
<wire x1="69.85" y1="111.76" x2="66.04" y2="107.95" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!OVR!"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="8"/>
<wire x1="214.63" y1="26.67" x2="208.28" y2="26.67" width="0.1524" layer="91"/>
<wire x1="208.28" y1="26.67" x2="205.74" y2="24.13" width="0.1524" layer="91"/>
<label x="209.55" y="26.67" size="1.27" layer="95"/>
</segment>
</net>
<net name="/INT2" class="0">
<segment>
<label x="-2.54" y="109.22" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="109.22" x2="-12.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="109.22" x2="-16.51" y2="105.41" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!INT2!"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="6"/>
<wire x1="214.63" y1="24.13" x2="208.28" y2="24.13" width="0.1524" layer="91"/>
<wire x1="208.28" y1="24.13" x2="205.74" y2="21.59" width="0.1524" layer="91"/>
<label x="209.55" y="24.13" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="109.22" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="109.22" x2="69.85" y2="109.22" width="0.1524" layer="91"/>
<wire x1="69.85" y1="109.22" x2="66.04" y2="105.41" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!INT2!"/>
</segment>
</net>
<net name="FC0" class="0">
<segment>
<label x="-2.54" y="93.98" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="93.98" x2="-13.97" y2="93.98" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="93.98" x2="-16.51" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="FC0"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="11"/>
<wire x1="229.87" y1="73.66" x2="247.65" y2="73.66" width="0.1524" layer="91"/>
<wire x1="247.65" y1="73.66" x2="250.19" y2="71.12" width="0.1524" layer="91"/>
<label x="234.95" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="93.98" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<wire x1="68.58" y1="93.98" x2="66.04" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="FC0"/>
</segment>
</net>
<net name="FC1" class="0">
<segment>
<label x="-2.54" y="91.44" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="91.44" x2="-13.97" y2="91.44" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="91.44" x2="-16.51" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="FC1"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="9"/>
<wire x1="229.87" y1="71.12" x2="247.65" y2="71.12" width="0.1524" layer="91"/>
<wire x1="247.65" y1="71.12" x2="250.19" y2="68.58" width="0.1524" layer="91"/>
<label x="234.95" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="91.44" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="91.44" x2="68.58" y2="91.44" width="0.1524" layer="91"/>
<wire x1="68.58" y1="91.44" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="FC1"/>
</segment>
</net>
<net name="FC2" class="0">
<segment>
<label x="-2.54" y="88.9" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="88.9" x2="-13.97" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="88.9" x2="-16.51" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="FC2"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="7"/>
<wire x1="229.87" y1="68.58" x2="247.65" y2="68.58" width="0.1524" layer="91"/>
<wire x1="247.65" y1="68.58" x2="250.19" y2="66.04" width="0.1524" layer="91"/>
<label x="234.95" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="88.9" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="88.9" x2="68.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="68.58" y1="88.9" x2="66.04" y2="86.36" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="FC2"/>
</segment>
</net>
<net name="/CCK" class="0">
<segment>
<wire x1="35.56" y1="114.3" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="53.34" y1="114.3" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!CCK!"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="9"/>
<wire x1="229.87" y1="29.21" x2="247.65" y2="29.21" width="0.1524" layer="91"/>
<wire x1="247.65" y1="29.21" x2="250.19" y2="26.67" width="0.1524" layer="91"/>
<label x="234.95" y="29.21" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="114.3" x2="135.89" y2="114.3" width="0.1524" layer="91"/>
<wire x1="135.89" y1="114.3" x2="138.43" y2="111.76" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!CCK!"/>
<label x="123.19" y="114.3" size="1.27" layer="95"/>
</segment>
</net>
<net name="/INT6" class="0">
<segment>
<wire x1="35.56" y1="106.68" x2="53.34" y2="106.68" width="0.1524" layer="91"/>
<wire x1="53.34" y1="106.68" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!INT6!"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="5"/>
<wire x1="229.87" y1="24.13" x2="246.38" y2="24.13" width="0.1524" layer="91"/>
<wire x1="246.38" y1="24.13" x2="250.19" y2="20.32" width="0.1524" layer="91"/>
<label x="234.95" y="24.13" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="106.68" x2="135.89" y2="106.68" width="0.1524" layer="91"/>
<wire x1="135.89" y1="106.68" x2="138.43" y2="104.14" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!INT6!"/>
<label x="123.19" y="106.68" size="1.27" layer="95"/>
</segment>
</net>
<net name="/VMA" class="0">
<segment>
<label x="-2.54" y="68.58" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="68.58" x2="-12.7" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="68.58" x2="-16.51" y2="64.77" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!VMA!"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="9"/>
<wire x1="191.77" y1="71.12" x2="203.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="203.2" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<label x="194.31" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="68.58" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="68.58" x2="69.85" y2="68.58" width="0.1524" layer="91"/>
<wire x1="69.85" y1="68.58" x2="66.04" y2="64.77" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!VMA!"/>
</segment>
</net>
<net name="/HALT" class="0">
<segment>
<label x="-2.54" y="63.5" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="63.5" x2="-12.7" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="63.5" x2="-16.51" y2="59.69" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!HALT!"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="5"/>
<wire x1="191.77" y1="66.04" x2="203.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="203.2" y1="66.04" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<label x="194.31" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="63.5" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="63.5" x2="69.85" y2="63.5" width="0.1524" layer="91"/>
<wire x1="69.85" y1="63.5" x2="66.04" y2="59.69" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!HALT!"/>
</segment>
</net>
<net name="/BR" class="0">
<segment>
<wire x1="35.56" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<wire x1="53.34" y1="58.42" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!BR!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="17"/>
<wire x1="229.87" y1="114.3" x2="247.65" y2="114.3" width="0.1524" layer="91"/>
<wire x1="247.65" y1="114.3" x2="250.19" y2="111.76" width="0.1524" layer="91"/>
<label x="234.95" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="58.42" x2="135.89" y2="58.42" width="0.1524" layer="91"/>
<wire x1="135.89" y1="58.42" x2="138.43" y2="55.88" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!BR!"/>
<label x="123.19" y="58.42" size="1.27" layer="95"/>
</segment>
</net>
<net name="/BGACK" class="0">
<segment>
<wire x1="35.56" y1="55.88" x2="53.34" y2="55.88" width="0.1524" layer="91"/>
<wire x1="53.34" y1="55.88" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!BGACK!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="15"/>
<wire x1="229.87" y1="111.76" x2="247.65" y2="111.76" width="0.1524" layer="91"/>
<wire x1="247.65" y1="111.76" x2="250.19" y2="109.22" width="0.1524" layer="91"/>
<label x="234.95" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="55.88" x2="135.89" y2="55.88" width="0.1524" layer="91"/>
<wire x1="135.89" y1="55.88" x2="138.43" y2="53.34" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!BGACK!"/>
<label x="123.19" y="55.88" size="1.27" layer="95"/>
</segment>
</net>
<net name="/BG" class="0">
<segment>
<wire x1="35.56" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="53.34" x2="55.88" y2="50.8" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!BG!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="13"/>
<wire x1="229.87" y1="109.22" x2="247.65" y2="109.22" width="0.1524" layer="91"/>
<wire x1="247.65" y1="109.22" x2="250.19" y2="106.68" width="0.1524" layer="91"/>
<label x="234.95" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="53.34" x2="135.89" y2="53.34" width="0.1524" layer="91"/>
<wire x1="135.89" y1="53.34" x2="138.43" y2="50.8" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!BG!"/>
<label x="123.19" y="53.34" size="1.27" layer="95"/>
</segment>
</net>
<net name="/DTACK" class="0">
<segment>
<wire x1="35.56" y1="50.8" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
<wire x1="53.34" y1="50.8" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!DTACK!"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="11"/>
<wire x1="229.87" y1="106.68" x2="247.65" y2="106.68" width="0.1524" layer="91"/>
<wire x1="247.65" y1="106.68" x2="250.19" y2="104.14" width="0.1524" layer="91"/>
<label x="234.95" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="50.8" x2="135.89" y2="50.8" width="0.1524" layer="91"/>
<wire x1="135.89" y1="50.8" x2="138.43" y2="48.26" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!DTACK!"/>
<label x="123.19" y="50.8" size="1.27" layer="95"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<wire x1="35.56" y1="71.12" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
<wire x1="53.34" y1="71.12" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="E"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="11"/>
<wire x1="191.77" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<label x="194.31" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="71.12" x2="135.89" y2="71.12" width="0.1524" layer="91"/>
<wire x1="135.89" y1="71.12" x2="138.43" y2="68.58" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="E"/>
<label x="123.19" y="71.12" size="1.27" layer="95"/>
</segment>
</net>
<net name="/VPA" class="0">
<segment>
<wire x1="35.56" y1="73.66" x2="53.34" y2="73.66" width="0.1524" layer="91"/>
<wire x1="53.34" y1="73.66" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!VPA!"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="13"/>
<wire x1="191.77" y1="76.2" x2="203.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="203.2" y1="76.2" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<label x="194.31" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="73.66" x2="135.89" y2="73.66" width="0.1524" layer="91"/>
<wire x1="135.89" y1="73.66" x2="138.43" y2="71.12" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!VPA!"/>
<label x="123.19" y="73.66" size="1.27" layer="95"/>
</segment>
</net>
<net name="/IPL0" class="0">
<segment>
<wire x1="35.56" y1="83.82" x2="53.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="53.34" y1="83.82" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<label x="40.64" y="83.82" size="1.27" layer="95"/>
<pinref part="CN2" gate="G$1" pin="!EINT7!"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="5"/>
<wire x1="229.87" y1="66.04" x2="247.65" y2="66.04" width="0.1524" layer="91"/>
<wire x1="247.65" y1="66.04" x2="250.19" y2="63.5" width="0.1524" layer="91"/>
<label x="234.95" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="83.82" x2="135.89" y2="83.82" width="0.1524" layer="91"/>
<wire x1="135.89" y1="83.82" x2="138.43" y2="81.28" width="0.1524" layer="91"/>
<label x="123.19" y="83.82" size="1.27" layer="95"/>
<pinref part="CN1" gate="G$1" pin="!EINT7!"/>
</segment>
</net>
<net name="/IPL1" class="0">
<segment>
<wire x1="35.56" y1="81.28" x2="53.34" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<label x="40.64" y="81.28" size="1.27" layer="95"/>
<pinref part="CN2" gate="G$1" pin="!EINT5!"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="3"/>
<wire x1="229.87" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="246.38" y1="63.5" x2="250.19" y2="59.69" width="0.1524" layer="91"/>
<label x="234.95" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="81.28" x2="135.89" y2="81.28" width="0.1524" layer="91"/>
<wire x1="135.89" y1="81.28" x2="138.43" y2="78.74" width="0.1524" layer="91"/>
<label x="123.19" y="81.28" size="1.27" layer="95"/>
<pinref part="CN1" gate="G$1" pin="!EINT5!"/>
</segment>
</net>
<net name="/IPL2" class="0">
<segment>
<wire x1="35.56" y1="78.74" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
<wire x1="53.34" y1="78.74" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<label x="40.64" y="78.74" size="1.27" layer="95"/>
<pinref part="CN2" gate="G$1" pin="!EINT4!"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="17"/>
<wire x1="191.77" y1="81.28" x2="203.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="203.2" y1="81.28" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<label x="194.31" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="78.74" x2="135.89" y2="78.74" width="0.1524" layer="91"/>
<wire x1="135.89" y1="78.74" x2="138.43" y2="76.2" width="0.1524" layer="91"/>
<label x="123.19" y="78.74" size="1.27" layer="95"/>
<pinref part="CN1" gate="G$1" pin="!EINT4!"/>
</segment>
</net>
<net name="/OWN" class="0">
<segment>
<label x="-2.54" y="124.46" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="124.46" x2="-13.97" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="124.46" x2="-16.51" y2="121.92" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!OWN!"/>
</segment>
<segment>
<label x="80.01" y="124.46" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="124.46" x2="68.58" y2="124.46" width="0.1524" layer="91"/>
<wire x1="68.58" y1="124.46" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!OWN!"/>
</segment>
<segment>
<wire x1="214.63" y1="36.83" x2="208.28" y2="36.83" width="0.1524" layer="91"/>
<wire x1="208.28" y1="36.83" x2="205.74" y2="34.29" width="0.1524" layer="91"/>
<label x="209.55" y="36.83" size="1.27" layer="95"/>
<pinref part="X5" gate="1" pin="16"/>
</segment>
</net>
<net name="DOE" class="0">
<segment>
<label x="-2.54" y="15.24" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="15.24" x2="-13.97" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="15.24" x2="-16.51" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="DOE"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="7"/>
<wire x1="191.77" y1="101.6" x2="203.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="203.2" y1="101.6" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<label x="194.31" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="15.24" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="15.24" x2="68.58" y2="15.24" width="0.1524" layer="91"/>
<wire x1="68.58" y1="15.24" x2="66.04" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="DOE"/>
</segment>
</net>
<net name="7MHZ" class="0">
<segment>
<label x="40.64" y="18.0975" size="1.27" layer="95"/>
<wire x1="35.56" y1="17.78" x2="53.34" y2="17.78" width="0.1524" layer="91"/>
<wire x1="53.34" y1="17.78" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="7M"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="3"/>
<wire x1="191.77" y1="63.5" x2="201.93" y2="63.5" width="0.1524" layer="91"/>
<wire x1="201.93" y1="63.5" x2="205.74" y2="59.69" width="0.1524" layer="91"/>
<label x="194.31" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="17.78" x2="135.89" y2="17.78" width="0.1524" layer="91"/>
<wire x1="135.89" y1="17.78" x2="138.43" y2="15.24" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="7M"/>
<label x="124.46" y="17.78" size="1.27" layer="95"/>
</segment>
</net>
<net name="/EINT1" class="0">
<segment>
<label x="40.64" y="13.0175" size="1.27" layer="95"/>
<wire x1="35.56" y1="12.7" x2="52.07" y2="12.7" width="0.1524" layer="91"/>
<wire x1="52.07" y1="12.7" x2="55.88" y2="8.89" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!EINT1!"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="6"/>
<wire x1="176.53" y1="99.06" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<wire x1="160.02" y1="99.06" x2="157.48" y2="96.52" width="0.1524" layer="91"/>
<label x="165.1" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="12.7" x2="134.62" y2="12.7" width="0.1524" layer="91"/>
<wire x1="134.62" y1="12.7" x2="138.43" y2="8.89" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!EINT1!"/>
<label x="124.46" y="12.7" size="1.27" layer="95"/>
</segment>
</net>
<net name="/GBG" class="0">
<segment>
<label x="-2.54" y="12.7" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="12.7" x2="-12.7" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="12.7" x2="-16.51" y2="8.89" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!GBG!"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="5"/>
<wire x1="191.77" y1="99.06" x2="203.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="99.06" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<label x="194.31" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="12.7" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="12.7" x2="69.85" y2="12.7" width="0.1524" layer="91"/>
<wire x1="69.85" y1="12.7" x2="66.04" y2="8.89" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!GBG!"/>
</segment>
</net>
<net name="/FCS" class="0">
<segment>
<label x="-2.54" y="10.16" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="10.16" x2="-12.7" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="10.16" x2="-16.51" y2="6.35" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!FCS!"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="3"/>
<wire x1="191.77" y1="96.52" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="203.2" y1="96.52" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<label x="194.31" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<label x="80.01" y="10.16" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="10.16" x2="69.85" y2="10.16" width="0.1524" layer="91"/>
<wire x1="69.85" y1="10.16" x2="66.04" y2="6.35" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!FCS!"/>
</segment>
</net>
<net name="/DS1" class="0">
<segment>
<label x="40.64" y="10.4775" size="1.27" layer="95"/>
<wire x1="35.56" y1="10.16" x2="52.07" y2="10.16" width="0.1524" layer="91"/>
<wire x1="52.07" y1="10.16" x2="55.88" y2="6.35" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!DS1!"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="4"/>
<wire x1="176.53" y1="96.52" x2="161.29" y2="96.52" width="0.1524" layer="91"/>
<wire x1="161.29" y1="96.52" x2="157.48" y2="92.71" width="0.1524" layer="91"/>
<label x="165.1" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="10.16" x2="134.62" y2="10.16" width="0.1524" layer="91"/>
<wire x1="134.62" y1="10.16" x2="138.43" y2="6.35" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!DS1!"/>
<label x="124.46" y="10.16" size="1.27" layer="95"/>
</segment>
</net>
<net name="IORST" class="0">
<segment>
<label x="40.64" y="15.5575" size="1.27" layer="95"/>
<wire x1="35.56" y1="15.24" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="15.24" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!BUSRST!"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="8"/>
<wire x1="176.53" y1="101.6" x2="160.02" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="101.6" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<label x="165.1" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="15.24" x2="135.89" y2="15.24" width="0.1524" layer="91"/>
<wire x1="135.89" y1="15.24" x2="138.43" y2="12.7" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!BUSRST!"/>
<label x="124.46" y="15.24" size="1.27" layer="95"/>
</segment>
</net>
<net name="/CCKQ" class="0">
<segment>
<wire x1="35.56" y1="116.84" x2="53.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="53.34" y1="116.84" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="!CCKQ!"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="11"/>
<wire x1="229.87" y1="31.75" x2="247.65" y2="31.75" width="0.1524" layer="91"/>
<wire x1="247.65" y1="31.75" x2="250.19" y2="29.21" width="0.1524" layer="91"/>
<label x="234.95" y="31.75" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="118.11" y1="116.84" x2="135.89" y2="116.84" width="0.1524" layer="91"/>
<wire x1="135.89" y1="116.84" x2="138.43" y2="114.3" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="!CCKQ!"/>
<label x="123.19" y="116.84" size="1.27" layer="95"/>
</segment>
</net>
<net name="Z3" class="0">
<segment>
<label x="-2.54" y="17.78" size="1.27" layer="95" rot="R180"/>
<wire x1="1.27" y1="17.78" x2="-13.97" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-13.97" y1="17.78" x2="-16.51" y2="15.24" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="Z3/GND"/>
</segment>
<segment>
<label x="80.01" y="17.78" size="1.27" layer="95" rot="R180"/>
<wire x1="83.82" y1="17.78" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<wire x1="68.58" y1="17.78" x2="66.04" y2="15.24" width="0.1524" layer="91"/>
<pinref part="CN1" gate="G$1" pin="Z3/GND"/>
</segment>
</net>
<net name="BOSS" class="0">
<segment>
<wire x1="214.63" y1="39.37" x2="208.28" y2="39.37" width="0.1524" layer="91"/>
<wire x1="208.28" y1="39.37" x2="205.74" y2="36.83" width="0.1524" layer="91"/>
<label x="209.55" y="39.37" size="1.27" layer="95"/>
<pinref part="X5" gate="1" pin="18"/>
</segment>
</net>
<net name="28M" class="0">
<segment>
<pinref part="X5" gate="1" pin="15"/>
<wire x1="229.87" y1="36.83" x2="247.65" y2="36.83" width="0.1524" layer="91"/>
<wire x1="247.65" y1="36.83" x2="250.19" y2="34.29" width="0.1524" layer="91"/>
<label x="234.95" y="36.83" size="1.27" layer="95"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="12V"/>
<wire x1="118.11" y1="121.92" x2="135.89" y2="121.92" width="0.1524" layer="91"/>
<wire x1="135.89" y1="121.92" x2="138.43" y2="119.38" width="0.1524" layer="91"/>
<label x="123.19" y="121.92" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="121.92" x2="48.26" y2="121.92" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="12V"/>
</segment>
</net>
<net name="-5V" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="-5V"/>
<wire x1="118.11" y1="124.46" x2="135.89" y2="124.46" width="0.1524" layer="91"/>
<wire x1="135.89" y1="124.46" x2="138.43" y2="121.92" width="0.1524" layer="91"/>
<label x="123.19" y="124.46" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="124.46" x2="48.26" y2="124.46" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="-5V"/>
</segment>
</net>
<net name="-12V" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="-12V"/>
<wire x1="118.11" y1="109.22" x2="135.89" y2="109.22" width="0.1524" layer="91"/>
<wire x1="135.89" y1="109.22" x2="138.43" y2="106.68" width="0.1524" layer="91"/>
<label x="123.19" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="35.56" y1="109.22" x2="48.26" y2="109.22" width="0.1524" layer="91"/>
<pinref part="CN2" gate="G$1" pin="-12V"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
