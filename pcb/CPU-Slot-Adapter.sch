<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="yes" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="yes" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="yes" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="90" name="Modules" color="7" fill="1" visible="no" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="tdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="bdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="SUPPLY1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Amiga">
<packages>
<package name="KONT_43_R">
<wire x1="-54.61" y1="4.6038" x2="-54.61" y2="-5.08" width="0" layer="20"/>
<wire x1="-54.61" y1="-5.08" x2="57.1501" y2="-5.08" width="0" layer="20"/>
<wire x1="57.1501" y1="-5.08" x2="57.1501" y2="4.7625" width="0" layer="20"/>
<smd name="1" x="54.61" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="2" x="54.61" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="52.07" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="4" x="52.07" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="49.53" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="6" x="49.53" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="46.99" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="8" x="46.99" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="44.45" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="10" x="44.45" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="41.91" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="12" x="41.91" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="39.37" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="14" x="39.37" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="36.83" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="16" x="36.83" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="34.29" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="18" x="34.29" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="31.75" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="20" x="31.75" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="29.21" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="22" x="29.21" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="23" x="26.67" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="24" x="26.67" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="25" x="24.13" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="26" x="24.13" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="21.59" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="28" x="21.59" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="19.05" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="30" x="19.05" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="16.51" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="32" x="16.51" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="33" x="13.97" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="34" x="13.97" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="35" x="11.43" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="36" x="11.43" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="37" x="8.89" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="38" x="8.89" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="39" x="6.35" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="40" x="6.35" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="41" x="3.81" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="42" x="3.81" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="43" x="1.27" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="44" x="1.27" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="45" x="-1.27" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="46" x="-1.27" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="47" x="-3.81" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="48" x="-3.81" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="49" x="-6.35" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="50" x="-6.35" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="51" x="-8.89" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="52" x="-8.89" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="53" x="-11.43" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="54" x="-11.43" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="55" x="-13.97" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="56" x="-13.97" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="57" x="-16.51" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="58" x="-16.51" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="59" x="-19.05" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="60" x="-19.05" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="61" x="-21.59" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="62" x="-21.59" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="63" x="-24.13" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="64" x="-24.13" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="65" x="-26.67" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="66" x="-26.67" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="67" x="-29.21" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="68" x="-29.21" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="69" x="-31.75" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="70" x="-31.75" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="71" x="-34.29" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="72" x="-34.29" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="73" x="-36.83" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="74" x="-36.83" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="75" x="-39.37" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="76" x="-39.37" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="77" x="-41.91" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="78" x="-41.91" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="79" x="-44.45" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="80" x="-44.45" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="81" x="-46.99" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="82" x="-46.99" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="83" x="-49.53" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="84" x="-49.53" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="85" x="-52.07" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="86" x="-52.07" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<text x="-52.8637" y="-6.604" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-43.1799" y="-6.5723" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="55.88" y="5.08" size="1.27" layer="51">2</text>
<text x="-53.34" y="5.08" size="1.27" layer="51">86</text>
</package>
<package name="LKONT_43_R">
<wire x1="-51.91125" y1="4.6038" x2="-51.91125" y2="-4.28625" width="0" layer="20"/>
<wire x1="-51.91125" y1="-4.28625" x2="59.53135" y2="-4.28625" width="0" layer="20"/>
<wire x1="59.53135" y1="-4.28625" x2="59.53135" y2="4.7625" width="0" layer="20"/>
<smd name="1" x="57.15" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="2" x="57.15" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="3" x="54.61" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="4" x="54.61" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="5" x="52.07" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="6" x="52.07" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="49.53" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="8" x="49.53" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="46.99" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="10" x="46.99" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="44.45" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="12" x="44.45" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="41.91" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="14" x="41.91" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="39.37" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="16" x="39.37" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="36.83" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="18" x="36.83" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="19" x="34.29" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="20" x="34.29" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="21" x="31.75" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="22" x="31.75" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="23" x="29.21" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="24" x="29.21" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="25" x="26.67" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="26" x="26.67" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="24.13" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="28" x="24.13" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="21.59" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="30" x="21.59" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="19.05" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="32" x="19.05" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="33" x="16.51" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="34" x="16.51" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="35" x="13.97" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="36" x="13.97" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="37" x="11.43" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="38" x="11.43" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="39" x="8.89" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="40" x="8.89" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="41" x="6.35" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="42" x="6.35" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="43" x="3.81" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="44" x="3.81" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="45" x="1.27" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="46" x="1.27" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="47" x="-1.27" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="48" x="-1.27" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="49" x="-3.81" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="50" x="-3.81" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="51" x="-6.35" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="52" x="-6.35" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="53" x="-8.89" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="54" x="-8.89" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="55" x="-11.43" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="56" x="-11.43" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="57" x="-13.97" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="58" x="-13.97" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="59" x="-16.51" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="60" x="-16.51" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="61" x="-19.05" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="62" x="-19.05" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="63" x="-21.59" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="64" x="-21.59" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="65" x="-24.13" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="66" x="-24.13" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="67" x="-26.67" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="68" x="-26.67" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="69" x="-29.21" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="70" x="-29.21" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="71" x="-31.75" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="72" x="-31.75" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="73" x="-34.29" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="74" x="-34.29" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="75" x="-36.83" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="76" x="-36.83" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="77" x="-39.37" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="78" x="-39.37" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="79" x="-41.91" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="80" x="-41.91" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="81" x="-44.45" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="82" x="-44.45" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="83" x="-46.99" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="84" x="-46.99" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<smd name="85" x="-49.53" y="0" dx="7.62" dy="1.27" layer="16" roundness="100" rot="R90"/>
<smd name="86" x="-49.53" y="0" dx="7.62" dy="1.27" layer="1" roundness="100" rot="R90"/>
<text x="-50.8" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-41.91" y="5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SLOT_86">
<wire x1="58.42" y1="4.6038" x2="-58.42" y2="4.6038" width="0.127" layer="21"/>
<wire x1="-58.42" y1="4.6038" x2="-58.42" y2="-4.6038" width="0.127" layer="21"/>
<wire x1="-58.42" y1="-4.6038" x2="58.42" y2="-4.6038" width="0.127" layer="21"/>
<wire x1="58.42" y1="-4.6038" x2="58.42" y2="4.6038" width="0.127" layer="21"/>
<wire x1="-33.655" y1="3.175" x2="-32.385" y2="3.175" width="0.127" layer="21"/>
<wire x1="-32.385" y1="3.175" x2="-31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="-31.75" y1="2.54" x2="-31.115" y2="3.175" width="0.127" layer="21"/>
<wire x1="-31.115" y1="3.175" x2="-29.845" y2="3.175" width="0.127" layer="21"/>
<wire x1="-29.845" y1="3.175" x2="-29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="-33.655" y1="3.175" x2="-34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="-31.75" y1="-2.54" x2="-32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-29.21" y1="-2.54" x2="-29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-29.845" y1="-3.175" x2="-31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-31.115" y1="-3.175" x2="-31.75" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-34.29" y1="-2.54" x2="-33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-32.385" y1="-3.175" x2="-33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-53.975" y1="3.175" x2="-52.705" y2="3.175" width="0.127" layer="21"/>
<wire x1="-52.705" y1="3.175" x2="-52.07" y2="2.54" width="0.127" layer="21"/>
<wire x1="-52.07" y1="2.54" x2="-51.435" y2="3.175" width="0.127" layer="21"/>
<wire x1="-51.435" y1="3.175" x2="-50.165" y2="3.175" width="0.127" layer="21"/>
<wire x1="-50.165" y1="3.175" x2="-49.53" y2="2.54" width="0.127" layer="21"/>
<wire x1="-53.975" y1="3.175" x2="-54.61" y2="2.54" width="0.127" layer="21"/>
<wire x1="-49.53" y1="2.54" x2="-48.895" y2="3.175" width="0.127" layer="21"/>
<wire x1="-48.895" y1="3.175" x2="-47.625" y2="3.175" width="0.127" layer="21"/>
<wire x1="-47.625" y1="3.175" x2="-46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="-46.355" y1="3.175" x2="-45.085" y2="3.175" width="0.127" layer="21"/>
<wire x1="-45.085" y1="3.175" x2="-44.45" y2="2.54" width="0.127" layer="21"/>
<wire x1="-44.45" y1="2.54" x2="-43.815" y2="3.175" width="0.127" layer="21"/>
<wire x1="-43.815" y1="3.175" x2="-42.545" y2="3.175" width="0.127" layer="21"/>
<wire x1="-42.545" y1="3.175" x2="-41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="-46.355" y1="3.175" x2="-46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="-41.91" y1="2.54" x2="-41.275" y2="3.175" width="0.127" layer="21"/>
<wire x1="-41.275" y1="3.175" x2="-40.005" y2="3.175" width="0.127" layer="21"/>
<wire x1="-40.005" y1="3.175" x2="-39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="-52.07" y1="-2.54" x2="-52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-49.53" y1="-2.54" x2="-50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-50.165" y1="-3.175" x2="-51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-51.435" y1="-3.175" x2="-52.07" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-54.61" y1="2.54" x2="-54.61" y2="1.27" width="0.127" layer="21"/>
<wire x1="-54.61" y1="1.27" x2="-55.88" y2="1.27" width="0.127" layer="21"/>
<wire x1="-55.88" y1="1.27" x2="-55.88" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-55.88" y1="-1.27" x2="-54.61" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-54.61" y1="-1.27" x2="-54.61" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-54.61" y1="-2.54" x2="-53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-52.705" y1="-3.175" x2="-53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-46.99" y1="-2.54" x2="-47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-47.625" y1="-3.175" x2="-48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-48.895" y1="-3.175" x2="-49.53" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-44.45" y1="-2.54" x2="-45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-41.91" y1="-2.54" x2="-42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-42.545" y1="-3.175" x2="-43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-43.815" y1="-3.175" x2="-44.45" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-46.99" y1="-2.54" x2="-46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-45.085" y1="-3.175" x2="-46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-39.37" y1="-2.54" x2="-40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-40.005" y1="-3.175" x2="-41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-41.275" y1="-3.175" x2="-41.91" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-38.735" y1="3.175" x2="-37.465" y2="3.175" width="0.127" layer="21"/>
<wire x1="-37.465" y1="3.175" x2="-36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="-36.83" y1="2.54" x2="-36.195" y2="3.175" width="0.127" layer="21"/>
<wire x1="-36.195" y1="3.175" x2="-34.925" y2="3.175" width="0.127" layer="21"/>
<wire x1="-34.925" y1="3.175" x2="-34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="-38.735" y1="3.175" x2="-39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="-36.83" y1="-2.54" x2="-37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-34.29" y1="-2.54" x2="-34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-34.925" y1="-3.175" x2="-36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-36.195" y1="-3.175" x2="-36.83" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-39.37" y1="-2.54" x2="-38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-37.465" y1="-3.175" x2="-38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-9.525" y1="3.175" x2="-8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="-28.575" y1="3.175" x2="-27.305" y2="3.175" width="0.127" layer="21"/>
<wire x1="-26.035" y1="3.175" x2="-24.765" y2="3.175" width="0.127" layer="21"/>
<wire x1="-23.495" y1="3.175" x2="-22.225" y2="3.175" width="0.127" layer="21"/>
<wire x1="-20.955" y1="3.175" x2="-19.685" y2="3.175" width="0.127" layer="21"/>
<wire x1="-18.415" y1="3.175" x2="-17.145" y2="3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="3.175" x2="-14.605" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-12.065" y2="3.175" width="0.127" layer="21"/>
<wire x1="-10.795" y1="3.175" x2="-9.525" y2="3.175" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-3.175" x2="-9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-3.175" x2="-12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-15.875" y1="-3.175" x2="-14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-18.415" y1="-3.175" x2="-17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-20.955" y1="-3.175" x2="-19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-23.495" y1="-3.175" x2="-22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.035" y1="-3.175" x2="-24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-28.575" y1="-3.175" x2="-27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-27.305" y1="3.175" x2="-26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="-24.765" y1="3.175" x2="-24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-22.225" y1="3.175" x2="-21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="-19.685" y1="3.175" x2="-19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="-17.145" y1="3.175" x2="-16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="-14.605" y1="3.175" x2="-13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="-12.065" y1="3.175" x2="-11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="-28.575" y1="3.175" x2="-29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="-26.035" y1="3.175" x2="-26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="-23.495" y1="3.175" x2="-24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="-20.955" y1="3.175" x2="-21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="-18.415" y1="3.175" x2="-19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="-15.875" y1="3.175" x2="-16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="3.175" x2="-13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="-10.795" y1="3.175" x2="-11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="-26.67" y1="-2.54" x2="-27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-2.54" x2="-24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-2.54" x2="-22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-2.54" x2="-19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-2.54" x2="-17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-2.54" x2="-14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-2.54" x2="-12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-2.54" x2="-9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-26.67" y1="-2.54" x2="-26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-29.21" y1="-2.54" x2="-28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-24.13" y1="-2.54" x2="-23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-21.59" y1="-2.54" x2="-20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-19.05" y1="-2.54" x2="-18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-2.54" x2="-15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-2.54" x2="-13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-2.54" x2="-10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.175" x2="-6.985" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="-4.445" y2="3.175" width="0.127" layer="21"/>
<wire x1="-5.715" y1="-3.175" x2="-4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.255" y1="-3.175" x2="-6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.985" y1="3.175" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-8.255" y1="3.175" x2="-8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.715" y1="3.175" x2="-6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-2.54" x2="-8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="0.635" y1="3.175" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.175" x2="-1.905" y2="3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-3.175" x2="0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.175" y1="-3.175" x2="-1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.905" y1="3.175" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.175" y1="3.175" x2="-3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="3.175" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="5.715" y1="3.175" x2="6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.175" x2="3.175" y2="3.175" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.175" x2="5.715" y2="3.175" width="0.127" layer="21"/>
<wire x1="4.445" y1="-3.175" x2="5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.905" y1="-3.175" x2="3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.175" y1="3.175" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="3.175" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="4.445" y1="3.175" x2="3.81" y2="2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.175" y2="-3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="5.715" y2="-3.175" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="-3.175" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="4.445" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.795" y1="3.175" x2="11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.985" y1="3.175" x2="8.255" y2="3.175" width="0.127" layer="21"/>
<wire x1="9.525" y1="3.175" x2="10.795" y2="3.175" width="0.127" layer="21"/>
<wire x1="9.525" y1="-3.175" x2="10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="6.985" y1="-3.175" x2="8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.255" y1="3.175" x2="8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="6.985" y1="3.175" x2="6.35" y2="2.54" width="0.127" layer="21"/>
<wire x1="9.525" y1="3.175" x2="8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="8.255" y2="-3.175" width="0.127" layer="21"/>
<wire x1="11.43" y1="-2.54" x2="10.795" y2="-3.175" width="0.127" layer="21"/>
<wire x1="6.35" y1="-2.54" x2="6.985" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="9.525" y2="-3.175" width="0.127" layer="21"/>
<wire x1="15.875" y1="3.175" x2="16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.175" x2="13.335" y2="3.175" width="0.127" layer="21"/>
<wire x1="14.605" y1="3.175" x2="15.875" y2="3.175" width="0.127" layer="21"/>
<wire x1="14.605" y1="-3.175" x2="15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="12.065" y1="-3.175" x2="13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="13.335" y1="3.175" x2="13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="12.065" y1="3.175" x2="11.43" y2="2.54" width="0.127" layer="21"/>
<wire x1="14.605" y1="3.175" x2="13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="13.335" y2="-3.175" width="0.127" layer="21"/>
<wire x1="16.51" y1="-2.54" x2="15.875" y2="-3.175" width="0.127" layer="21"/>
<wire x1="11.43" y1="-2.54" x2="12.065" y2="-3.175" width="0.127" layer="21"/>
<wire x1="13.97" y1="-2.54" x2="14.605" y2="-3.175" width="0.127" layer="21"/>
<wire x1="20.955" y1="3.175" x2="21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="17.145" y1="3.175" x2="18.415" y2="3.175" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="20.955" y2="3.175" width="0.127" layer="21"/>
<wire x1="19.685" y1="-3.175" x2="20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="17.145" y1="-3.175" x2="18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="18.415" y1="3.175" x2="19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="17.145" y1="3.175" x2="16.51" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.685" y1="3.175" x2="19.05" y2="2.54" width="0.127" layer="21"/>
<wire x1="19.05" y1="-2.54" x2="18.415" y2="-3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="-2.54" x2="20.955" y2="-3.175" width="0.127" layer="21"/>
<wire x1="16.51" y1="-2.54" x2="17.145" y2="-3.175" width="0.127" layer="21"/>
<wire x1="19.05" y1="-2.54" x2="19.685" y2="-3.175" width="0.127" layer="21"/>
<wire x1="22.225" y1="3.175" x2="23.495" y2="3.175" width="0.127" layer="21"/>
<wire x1="22.225" y1="-3.175" x2="23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="23.495" y1="3.175" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="22.225" y1="3.175" x2="21.59" y2="2.54" width="0.127" layer="21"/>
<wire x1="24.765" y1="3.175" x2="24.13" y2="2.54" width="0.127" layer="21"/>
<wire x1="24.13" y1="-2.54" x2="23.495" y2="-3.175" width="0.127" layer="21"/>
<wire x1="21.59" y1="-2.54" x2="22.225" y2="-3.175" width="0.127" layer="21"/>
<wire x1="24.13" y1="-2.54" x2="24.765" y2="-3.175" width="0.127" layer="21"/>
<wire x1="28.575" y1="3.175" x2="29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="24.765" y1="3.175" x2="26.035" y2="3.175" width="0.127" layer="21"/>
<wire x1="27.305" y1="3.175" x2="28.575" y2="3.175" width="0.127" layer="21"/>
<wire x1="27.305" y1="-3.175" x2="28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="24.765" y1="-3.175" x2="26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="26.035" y1="3.175" x2="26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="27.305" y1="3.175" x2="26.67" y2="2.54" width="0.127" layer="21"/>
<wire x1="26.67" y1="-2.54" x2="26.035" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.21" y1="-2.54" x2="28.575" y2="-3.175" width="0.127" layer="21"/>
<wire x1="26.67" y1="-2.54" x2="27.305" y2="-3.175" width="0.127" layer="21"/>
<wire x1="33.655" y1="3.175" x2="34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="29.845" y1="3.175" x2="31.115" y2="3.175" width="0.127" layer="21"/>
<wire x1="32.385" y1="3.175" x2="33.655" y2="3.175" width="0.127" layer="21"/>
<wire x1="32.385" y1="-3.175" x2="33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.845" y1="-3.175" x2="31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="31.115" y1="3.175" x2="31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="29.845" y1="3.175" x2="29.21" y2="2.54" width="0.127" layer="21"/>
<wire x1="32.385" y1="3.175" x2="31.75" y2="2.54" width="0.127" layer="21"/>
<wire x1="31.75" y1="-2.54" x2="31.115" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.29" y1="-2.54" x2="33.655" y2="-3.175" width="0.127" layer="21"/>
<wire x1="29.21" y1="-2.54" x2="29.845" y2="-3.175" width="0.127" layer="21"/>
<wire x1="31.75" y1="-2.54" x2="32.385" y2="-3.175" width="0.127" layer="21"/>
<wire x1="38.735" y1="3.175" x2="39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="34.925" y1="3.175" x2="36.195" y2="3.175" width="0.127" layer="21"/>
<wire x1="37.465" y1="3.175" x2="38.735" y2="3.175" width="0.127" layer="21"/>
<wire x1="37.465" y1="-3.175" x2="38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.925" y1="-3.175" x2="36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="36.195" y1="3.175" x2="36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="34.925" y1="3.175" x2="34.29" y2="2.54" width="0.127" layer="21"/>
<wire x1="37.465" y1="3.175" x2="36.83" y2="2.54" width="0.127" layer="21"/>
<wire x1="36.83" y1="-2.54" x2="36.195" y2="-3.175" width="0.127" layer="21"/>
<wire x1="39.37" y1="-2.54" x2="38.735" y2="-3.175" width="0.127" layer="21"/>
<wire x1="34.29" y1="-2.54" x2="34.925" y2="-3.175" width="0.127" layer="21"/>
<wire x1="36.83" y1="-2.54" x2="37.465" y2="-3.175" width="0.127" layer="21"/>
<wire x1="43.815" y1="3.175" x2="44.45" y2="2.54" width="0.127" layer="21"/>
<wire x1="40.005" y1="3.175" x2="41.275" y2="3.175" width="0.127" layer="21"/>
<wire x1="42.545" y1="3.175" x2="43.815" y2="3.175" width="0.127" layer="21"/>
<wire x1="42.545" y1="-3.175" x2="43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="40.005" y1="-3.175" x2="41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="41.275" y1="3.175" x2="41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="40.005" y1="3.175" x2="39.37" y2="2.54" width="0.127" layer="21"/>
<wire x1="42.545" y1="3.175" x2="41.91" y2="2.54" width="0.127" layer="21"/>
<wire x1="41.91" y1="-2.54" x2="41.275" y2="-3.175" width="0.127" layer="21"/>
<wire x1="44.45" y1="-2.54" x2="43.815" y2="-3.175" width="0.127" layer="21"/>
<wire x1="39.37" y1="-2.54" x2="40.005" y2="-3.175" width="0.127" layer="21"/>
<wire x1="41.91" y1="-2.54" x2="42.545" y2="-3.175" width="0.127" layer="21"/>
<wire x1="48.895" y1="3.175" x2="49.53" y2="2.54" width="0.127" layer="21"/>
<wire x1="45.085" y1="3.175" x2="46.355" y2="3.175" width="0.127" layer="21"/>
<wire x1="47.625" y1="3.175" x2="48.895" y2="3.175" width="0.127" layer="21"/>
<wire x1="47.625" y1="-3.175" x2="48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="45.085" y1="-3.175" x2="46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="46.355" y1="3.175" x2="46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="45.085" y1="3.175" x2="44.45" y2="2.54" width="0.127" layer="21"/>
<wire x1="47.625" y1="3.175" x2="46.99" y2="2.54" width="0.127" layer="21"/>
<wire x1="46.99" y1="-2.54" x2="46.355" y2="-3.175" width="0.127" layer="21"/>
<wire x1="49.53" y1="-2.54" x2="48.895" y2="-3.175" width="0.127" layer="21"/>
<wire x1="44.45" y1="-2.54" x2="45.085" y2="-3.175" width="0.127" layer="21"/>
<wire x1="46.99" y1="-2.54" x2="47.625" y2="-3.175" width="0.127" layer="21"/>
<wire x1="53.975" y1="3.175" x2="54.61" y2="2.54" width="0.127" layer="21"/>
<wire x1="50.165" y1="3.175" x2="51.435" y2="3.175" width="0.127" layer="21"/>
<wire x1="52.705" y1="3.175" x2="53.975" y2="3.175" width="0.127" layer="21"/>
<wire x1="52.705" y1="-3.175" x2="53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="50.165" y1="-3.175" x2="51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="51.435" y1="3.175" x2="52.07" y2="2.54" width="0.127" layer="21"/>
<wire x1="50.165" y1="3.175" x2="49.53" y2="2.54" width="0.127" layer="21"/>
<wire x1="52.705" y1="3.175" x2="52.07" y2="2.54" width="0.127" layer="21"/>
<wire x1="52.07" y1="-2.54" x2="51.435" y2="-3.175" width="0.127" layer="21"/>
<wire x1="54.61" y1="-2.54" x2="53.975" y2="-3.175" width="0.127" layer="21"/>
<wire x1="49.53" y1="-2.54" x2="50.165" y2="-3.175" width="0.127" layer="21"/>
<wire x1="52.07" y1="-2.54" x2="52.705" y2="-3.175" width="0.127" layer="21"/>
<wire x1="54.61" y1="2.54" x2="54.61" y2="1.27" width="0.127" layer="21"/>
<wire x1="54.61" y1="1.27" x2="55.88" y2="1.27" width="0.127" layer="21"/>
<wire x1="55.88" y1="1.27" x2="55.88" y2="-1.27" width="0.127" layer="21"/>
<wire x1="55.88" y1="-1.27" x2="54.61" y2="-1.27" width="0.127" layer="21"/>
<wire x1="54.61" y1="-1.27" x2="54.61" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-53.34" y="-2.54" drill="0.9" diameter="1.5"/>
<pad name="2" x="-53.34" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="3" x="-50.8" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="4" x="-50.8" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="5" x="-48.26" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="6" x="-48.26" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="7" x="-45.72" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="8" x="-45.72" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="9" x="-43.18" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="10" x="-43.18" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="11" x="-40.64" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="12" x="-40.64" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="13" x="-38.1" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="14" x="-38.1" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="15" x="-35.56" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="16" x="-35.56" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="17" x="-33.02" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="18" x="-33.02" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="19" x="-30.48" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="20" x="-30.48" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="21" x="-27.94" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="22" x="-27.94" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="23" x="-25.4" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="24" x="-25.4" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="25" x="-22.86" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="26" x="-22.86" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="27" x="-20.32" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="28" x="-20.32" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="29" x="-17.78" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="30" x="-17.78" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="31" x="-15.24" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="32" x="-15.24" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="33" x="-12.7" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="34" x="-12.7" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="35" x="-10.16" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="36" x="-10.16" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="37" x="-7.62" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="38" x="-7.62" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="39" x="-5.08" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="40" x="-5.08" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="41" x="-2.54" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="42" x="-2.54" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="43" x="0" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="44" x="0" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="45" x="2.54" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="46" x="2.54" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="47" x="5.08" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="48" x="5.08" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="49" x="7.62" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="50" x="7.62" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="51" x="10.16" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="52" x="10.16" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="53" x="12.7" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="54" x="12.7" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="55" x="15.24" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="56" x="15.24" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="57" x="17.78" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="58" x="17.78" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="59" x="20.32" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="60" x="20.32" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="61" x="22.86" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="62" x="22.86" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="63" x="25.4" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="64" x="25.4" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="65" x="27.94" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="66" x="27.94" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="67" x="30.48" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="68" x="30.48" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="69" x="33.02" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="70" x="33.02" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="71" x="35.56" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="72" x="35.56" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="73" x="38.1" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="74" x="38.1" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="75" x="40.64" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="76" x="40.64" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="77" x="43.18" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="78" x="43.18" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="79" x="45.72" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="80" x="45.72" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="81" x="48.26" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="82" x="48.26" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="83" x="50.8" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="84" x="50.8" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="85" x="53.34" y="-2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<pad name="86" x="53.34" y="2.54" drill="0.9" diameter="1.5" shape="octagon"/>
<text x="-56.388" y="-3.556" size="1.27" layer="21" ratio="10">1</text>
<text x="-44.45" y="-0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-52.705" y="-0.381" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="A2K-COPROCESSOR">
<wire x1="-63.5" y1="10.16" x2="-63.5" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-63.5" y1="-10.16" x2="63.5" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-10.16" x2="63.5" y2="10.16" width="0.4064" layer="94"/>
<wire x1="63.5" y1="10.16" x2="-63.5" y2="10.16" width="0.4064" layer="94"/>
<pin name="+5V@0" x="-27.94" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="+5V@1" x="-25.4" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="+12V" x="-20.32" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="-5V" x="-22.86" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="/AS" x="53.34" y="15.24" length="middle" rot="R270"/>
<pin name="/BERR" x="2.54" y="-15.24" length="middle" rot="R90"/>
<pin name="/BG" x="20.32" y="-15.24" length="middle" rot="R90"/>
<pin name="/BGACK" x="22.86" y="-15.24" length="middle" rot="R90"/>
<pin name="/BOSS" x="15.24" y="-15.24" length="middle" rot="R90"/>
<pin name="/BR" x="17.78" y="-15.24" length="middle" rot="R90"/>
<pin name="/C1" x="-7.62" y="-15.24" length="middle" rot="R90"/>
<pin name="/C3" x="-5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="/COPCFG" x="60.96" y="15.24" length="middle" rot="R270"/>
<pin name="/DTACK" x="50.8" y="15.24" length="middle" rot="R270"/>
<pin name="/HLT" x="10.16" y="-15.24" length="middle" rot="R90"/>
<pin name="/INT2" x="30.48" y="-15.24" length="middle" rot="R90"/>
<pin name="/INT6" x="33.02" y="-15.24" length="middle" rot="R90"/>
<pin name="/IPL0" x="35.56" y="-15.24" length="middle" rot="R90"/>
<pin name="/IPL1" x="38.1" y="-15.24" length="middle" rot="R90"/>
<pin name="/IPL2" x="40.64" y="-15.24" length="middle" rot="R90"/>
<pin name="/LDS" x="43.18" y="15.24" length="middle" rot="R270"/>
<pin name="/OVR" x="55.88" y="15.24" length="middle" rot="R270"/>
<pin name="/RESET" x="27.94" y="-15.24" length="middle" rot="R90"/>
<pin name="/UDS" x="45.72" y="15.24" length="middle" rot="R270"/>
<pin name="/VMA" x="7.62" y="-15.24" length="middle" rot="R90"/>
<pin name="/VPA" x="5.08" y="-15.24" length="middle" rot="R90"/>
<pin name="28M" x="-15.24" y="-15.24" length="middle" rot="R90"/>
<pin name="A1" x="-5.08" y="15.24" length="middle" rot="R270"/>
<pin name="A2" x="-7.62" y="15.24" length="middle" rot="R270"/>
<pin name="A3" x="-10.16" y="15.24" length="middle" rot="R270"/>
<pin name="A4" x="-12.7" y="15.24" length="middle" rot="R270"/>
<pin name="A5" x="-15.24" y="15.24" length="middle" rot="R270"/>
<pin name="A6" x="-17.78" y="15.24" length="middle" rot="R270"/>
<pin name="A7" x="-20.32" y="15.24" length="middle" rot="R270"/>
<pin name="A8" x="-22.86" y="15.24" length="middle" rot="R270"/>
<pin name="A9" x="-25.4" y="15.24" length="middle" rot="R270"/>
<pin name="A10" x="-27.94" y="15.24" length="middle" rot="R270"/>
<pin name="A11" x="-30.48" y="15.24" length="middle" rot="R270"/>
<pin name="A12" x="-33.02" y="15.24" length="middle" rot="R270"/>
<pin name="A13" x="-35.56" y="15.24" length="middle" rot="R270"/>
<pin name="A14" x="-38.1" y="15.24" length="middle" rot="R270"/>
<pin name="A15" x="-40.64" y="15.24" length="middle" rot="R270"/>
<pin name="A16" x="-43.18" y="15.24" length="middle" rot="R270"/>
<pin name="A17" x="-45.72" y="15.24" length="middle" rot="R270"/>
<pin name="A18" x="-48.26" y="15.24" length="middle" rot="R270"/>
<pin name="A19" x="-50.8" y="15.24" length="middle" rot="R270"/>
<pin name="A20" x="-53.34" y="15.24" length="middle" rot="R270"/>
<pin name="A21" x="-55.88" y="15.24" length="middle" rot="R270"/>
<pin name="A22" x="-58.42" y="15.24" length="middle" rot="R270"/>
<pin name="A23" x="-60.96" y="15.24" length="middle" rot="R270"/>
<pin name="CDAC" x="-10.16" y="-15.24" length="middle" rot="R90"/>
<pin name="D0" x="38.1" y="15.24" length="middle" rot="R270"/>
<pin name="D1" x="35.56" y="15.24" length="middle" rot="R270"/>
<pin name="D2" x="33.02" y="15.24" length="middle" rot="R270"/>
<pin name="D3" x="30.48" y="15.24" length="middle" rot="R270"/>
<pin name="D4" x="27.94" y="15.24" length="middle" rot="R270"/>
<pin name="D5" x="25.4" y="15.24" length="middle" rot="R270"/>
<pin name="D6" x="22.86" y="15.24" length="middle" rot="R270"/>
<pin name="D7" x="20.32" y="15.24" length="middle" rot="R270"/>
<pin name="D8" x="17.78" y="15.24" length="middle" rot="R270"/>
<pin name="D9" x="15.24" y="15.24" length="middle" rot="R270"/>
<pin name="D10" x="12.7" y="15.24" length="middle" rot="R270"/>
<pin name="D11" x="10.16" y="15.24" length="middle" rot="R270"/>
<pin name="D12" x="7.62" y="15.24" length="middle" rot="R270"/>
<pin name="D13" x="5.08" y="15.24" length="middle" rot="R270"/>
<pin name="D14" x="2.54" y="15.24" length="middle" rot="R270"/>
<pin name="D15" x="0" y="15.24" length="middle" rot="R270"/>
<pin name="E" x="-2.54" y="-15.24" length="middle" rot="R90"/>
<pin name="E7M" x="-12.7" y="-15.24" length="middle" rot="R90"/>
<pin name="FC0" x="45.72" y="-15.24" length="middle" rot="R90"/>
<pin name="FC1" x="48.26" y="-15.24" length="middle" rot="R90"/>
<pin name="FC2" x="50.8" y="-15.24" length="middle" rot="R90"/>
<pin name="GND@0" x="-60.96" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@1" x="-58.42" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@2" x="-55.88" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@3" x="-53.34" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@4" x="-50.8" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@5" x="-48.26" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@6" x="-45.72" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@7" x="-43.18" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@8" x="-40.64" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@9" x="-38.1" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@10" x="-35.56" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@11" x="-33.02" y="-15.24" length="middle" direction="pwr" rot="R90"/>
<pin name="R/W" x="48.26" y="15.24" length="middle" rot="R270"/>
<pin name="XRDY" x="58.42" y="15.24" length="middle" rot="R270"/>
<text x="-63.5" y="-10.16" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="66.04" y="-10.16" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="A2000-EXPANSION" prefix="CN">
<gates>
<gate name="G$1" symbol="A2K-COPROCESSOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="KONT_43_R">
<connects>
<connect gate="G$1" pin="+12V" pad="10"/>
<connect gate="G$1" pin="+5V@0" pad="5"/>
<connect gate="G$1" pin="+5V@1" pad="6"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="/AS" pad="74"/>
<connect gate="G$1" pin="/BERR" pad="46"/>
<connect gate="G$1" pin="/BG" pad="64"/>
<connect gate="G$1" pin="/BGACK" pad="62"/>
<connect gate="G$1" pin="/BOSS" pad="20"/>
<connect gate="G$1" pin="/BR" pad="60"/>
<connect gate="G$1" pin="/C1" pad="16"/>
<connect gate="G$1" pin="/C3" pad="14"/>
<connect gate="G$1" pin="/COPCFG" pad="11"/>
<connect gate="G$1" pin="/DTACK" pad="66"/>
<connect gate="G$1" pin="/HLT" pad="55"/>
<connect gate="G$1" pin="/INT2" pad="19"/>
<connect gate="G$1" pin="/INT6" pad="22"/>
<connect gate="G$1" pin="/IPL0" pad="40"/>
<connect gate="G$1" pin="/IPL1" pad="42"/>
<connect gate="G$1" pin="/IPL2" pad="44"/>
<connect gate="G$1" pin="/LDS" pad="70"/>
<connect gate="G$1" pin="/OVR" pad="17"/>
<connect gate="G$1" pin="/RESET" pad="53"/>
<connect gate="G$1" pin="/UDS" pad="72"/>
<connect gate="G$1" pin="/VMA" pad="51"/>
<connect gate="G$1" pin="/VPA" pad="48"/>
<connect gate="G$1" pin="28M" pad="9"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="D0" pad="75"/>
<connect gate="G$1" pin="D1" pad="77"/>
<connect gate="G$1" pin="D10" pad="76"/>
<connect gate="G$1" pin="D11" pad="71"/>
<connect gate="G$1" pin="D12" pad="69"/>
<connect gate="G$1" pin="D13" pad="67"/>
<connect gate="G$1" pin="D14" pad="65"/>
<connect gate="G$1" pin="D15" pad="63"/>
<connect gate="G$1" pin="D2" pad="79"/>
<connect gate="G$1" pin="D3" pad="81"/>
<connect gate="G$1" pin="D4" pad="83"/>
<connect gate="G$1" pin="D5" pad="86"/>
<connect gate="G$1" pin="D6" pad="84"/>
<connect gate="G$1" pin="D7" pad="82"/>
<connect gate="G$1" pin="D8" pad="80"/>
<connect gate="G$1" pin="D9" pad="78"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="E7M" pad="7"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@0" pad="1"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@2" pad="3"/>
<connect gate="G$1" pin="GND@3" pad="4"/>
<connect gate="G$1" pin="GND@4" pad="12"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/W" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SMALL" package="LKONT_43_R">
<connects>
<connect gate="G$1" pin="+12V" pad="10"/>
<connect gate="G$1" pin="+5V@0" pad="5"/>
<connect gate="G$1" pin="+5V@1" pad="6"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="/AS" pad="74"/>
<connect gate="G$1" pin="/BERR" pad="46"/>
<connect gate="G$1" pin="/BG" pad="64"/>
<connect gate="G$1" pin="/BGACK" pad="62"/>
<connect gate="G$1" pin="/BOSS" pad="20"/>
<connect gate="G$1" pin="/BR" pad="60"/>
<connect gate="G$1" pin="/C1" pad="16"/>
<connect gate="G$1" pin="/C3" pad="14"/>
<connect gate="G$1" pin="/COPCFG" pad="11"/>
<connect gate="G$1" pin="/DTACK" pad="66"/>
<connect gate="G$1" pin="/HLT" pad="55"/>
<connect gate="G$1" pin="/INT2" pad="19"/>
<connect gate="G$1" pin="/INT6" pad="22"/>
<connect gate="G$1" pin="/IPL0" pad="40"/>
<connect gate="G$1" pin="/IPL1" pad="42"/>
<connect gate="G$1" pin="/IPL2" pad="44"/>
<connect gate="G$1" pin="/LDS" pad="70"/>
<connect gate="G$1" pin="/OVR" pad="17"/>
<connect gate="G$1" pin="/RESET" pad="53"/>
<connect gate="G$1" pin="/UDS" pad="72"/>
<connect gate="G$1" pin="/VMA" pad="51"/>
<connect gate="G$1" pin="/VPA" pad="48"/>
<connect gate="G$1" pin="28M" pad="9"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="D0" pad="75"/>
<connect gate="G$1" pin="D1" pad="77"/>
<connect gate="G$1" pin="D10" pad="76"/>
<connect gate="G$1" pin="D11" pad="71"/>
<connect gate="G$1" pin="D12" pad="69"/>
<connect gate="G$1" pin="D13" pad="67"/>
<connect gate="G$1" pin="D14" pad="65"/>
<connect gate="G$1" pin="D15" pad="63"/>
<connect gate="G$1" pin="D2" pad="79"/>
<connect gate="G$1" pin="D3" pad="81"/>
<connect gate="G$1" pin="D4" pad="83"/>
<connect gate="G$1" pin="D5" pad="86"/>
<connect gate="G$1" pin="D6" pad="84"/>
<connect gate="G$1" pin="D7" pad="82"/>
<connect gate="G$1" pin="D8" pad="80"/>
<connect gate="G$1" pin="D9" pad="78"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="E7M" pad="7"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@0" pad="1"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@2" pad="3"/>
<connect gate="G$1" pin="GND@3" pad="4"/>
<connect gate="G$1" pin="GND@4" pad="12"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/W" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_SLOT" package="SLOT_86">
<connects>
<connect gate="G$1" pin="+12V" pad="10"/>
<connect gate="G$1" pin="+5V@0" pad="5"/>
<connect gate="G$1" pin="+5V@1" pad="6"/>
<connect gate="G$1" pin="-5V" pad="8"/>
<connect gate="G$1" pin="/AS" pad="74"/>
<connect gate="G$1" pin="/BERR" pad="46"/>
<connect gate="G$1" pin="/BG" pad="64"/>
<connect gate="G$1" pin="/BGACK" pad="62"/>
<connect gate="G$1" pin="/BOSS" pad="20"/>
<connect gate="G$1" pin="/BR" pad="60"/>
<connect gate="G$1" pin="/C1" pad="16"/>
<connect gate="G$1" pin="/C3" pad="14"/>
<connect gate="G$1" pin="/COPCFG" pad="11"/>
<connect gate="G$1" pin="/DTACK" pad="66"/>
<connect gate="G$1" pin="/HLT" pad="55"/>
<connect gate="G$1" pin="/INT2" pad="19"/>
<connect gate="G$1" pin="/INT6" pad="22"/>
<connect gate="G$1" pin="/IPL0" pad="40"/>
<connect gate="G$1" pin="/IPL1" pad="42"/>
<connect gate="G$1" pin="/IPL2" pad="44"/>
<connect gate="G$1" pin="/LDS" pad="70"/>
<connect gate="G$1" pin="/OVR" pad="17"/>
<connect gate="G$1" pin="/RESET" pad="53"/>
<connect gate="G$1" pin="/UDS" pad="72"/>
<connect gate="G$1" pin="/VMA" pad="51"/>
<connect gate="G$1" pin="/VPA" pad="48"/>
<connect gate="G$1" pin="28M" pad="9"/>
<connect gate="G$1" pin="A1" pad="29"/>
<connect gate="G$1" pin="A10" pad="34"/>
<connect gate="G$1" pin="A11" pad="36"/>
<connect gate="G$1" pin="A12" pad="38"/>
<connect gate="G$1" pin="A13" pad="39"/>
<connect gate="G$1" pin="A14" pad="41"/>
<connect gate="G$1" pin="A15" pad="43"/>
<connect gate="G$1" pin="A16" pad="45"/>
<connect gate="G$1" pin="A17" pad="47"/>
<connect gate="G$1" pin="A18" pad="52"/>
<connect gate="G$1" pin="A19" pad="54"/>
<connect gate="G$1" pin="A2" pad="27"/>
<connect gate="G$1" pin="A20" pad="56"/>
<connect gate="G$1" pin="A21" pad="58"/>
<connect gate="G$1" pin="A22" pad="57"/>
<connect gate="G$1" pin="A23" pad="59"/>
<connect gate="G$1" pin="A3" pad="26"/>
<connect gate="G$1" pin="A4" pad="24"/>
<connect gate="G$1" pin="A5" pad="21"/>
<connect gate="G$1" pin="A6" pad="23"/>
<connect gate="G$1" pin="A7" pad="28"/>
<connect gate="G$1" pin="A8" pad="30"/>
<connect gate="G$1" pin="A9" pad="32"/>
<connect gate="G$1" pin="CDAC" pad="15"/>
<connect gate="G$1" pin="D0" pad="75"/>
<connect gate="G$1" pin="D1" pad="77"/>
<connect gate="G$1" pin="D10" pad="76"/>
<connect gate="G$1" pin="D11" pad="71"/>
<connect gate="G$1" pin="D12" pad="69"/>
<connect gate="G$1" pin="D13" pad="67"/>
<connect gate="G$1" pin="D14" pad="65"/>
<connect gate="G$1" pin="D15" pad="63"/>
<connect gate="G$1" pin="D2" pad="79"/>
<connect gate="G$1" pin="D3" pad="81"/>
<connect gate="G$1" pin="D4" pad="83"/>
<connect gate="G$1" pin="D5" pad="86"/>
<connect gate="G$1" pin="D6" pad="84"/>
<connect gate="G$1" pin="D7" pad="82"/>
<connect gate="G$1" pin="D8" pad="80"/>
<connect gate="G$1" pin="D9" pad="78"/>
<connect gate="G$1" pin="E" pad="50"/>
<connect gate="G$1" pin="E7M" pad="7"/>
<connect gate="G$1" pin="FC0" pad="31"/>
<connect gate="G$1" pin="FC1" pad="33"/>
<connect gate="G$1" pin="FC2" pad="35"/>
<connect gate="G$1" pin="GND@0" pad="1"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@10" pad="73"/>
<connect gate="G$1" pin="GND@11" pad="85"/>
<connect gate="G$1" pin="GND@2" pad="3"/>
<connect gate="G$1" pin="GND@3" pad="4"/>
<connect gate="G$1" pin="GND@4" pad="12"/>
<connect gate="G$1" pin="GND@5" pad="13"/>
<connect gate="G$1" pin="GND@6" pad="25"/>
<connect gate="G$1" pin="GND@7" pad="37"/>
<connect gate="G$1" pin="GND@8" pad="49"/>
<connect gate="G$1" pin="GND@9" pad="61"/>
<connect gate="G$1" pin="R/W" pad="68"/>
<connect gate="G$1" pin="XRDY" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-ml">
<description>&lt;b&gt;Harting  Connectors&lt;/b&gt;&lt;p&gt;
Low profile connectors, straight&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="ML20">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-15.24" y1="3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="-15.24" y1="3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-16.51" y2="-4.445" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="10.922" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.858" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.699" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="13.97" y1="4.445" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.445" x2="13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="-15.24" y1="4.699" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="9.398" y2="-3.429" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="10.922" y2="-3.429" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="9.398" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="-3.429" x2="15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="3.429" x2="-15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="3.429" x2="-15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="-3.429" x2="-8.382" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.937" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.937" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-4.445" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-9.271" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.429" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-8.382" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-6.858" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-8.382" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.937" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.937" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-9.271" y1="-4.445" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-5.969" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.969" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-16.51" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-13.97" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-13.97" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">20</text>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="20P">
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="13" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="17" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="19" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="10" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="14" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="16" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="18" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas"/>
<pin name="20" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ML20" prefix="SV" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="20P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML20">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="17" pad="17"/>
<connect gate="1" pin="18" pad="18"/>
<connect gate="1" pin="19" pad="19"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="20" pad="20"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="CN1" library="Amiga" deviceset="A2000-EXPANSION" device="-SMALL"/>
<part name="X1" library="con-ml" deviceset="ML20" device=""/>
<part name="GND20" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND21" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND22" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND23" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X2" library="con-ml" deviceset="ML20" device=""/>
<part name="GND3" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND4" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND5" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND6" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X3" library="con-ml" deviceset="ML20" device=""/>
<part name="GND7" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND8" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND9" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND10" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X4" library="con-ml" deviceset="ML20" device=""/>
<part name="GND11" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND12" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND13" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND14" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X5" library="con-ml" deviceset="ML20" device=""/>
<part name="GND15" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND16" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND17" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND18" library="SUPPLY1" deviceset="GND" device=""/>
<part name="CN2" library="Amiga" deviceset="A2000-EXPANSION" device="_SLOT" value="A2000-EXPANSION_SLOT"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="CN1" gate="G$1" x="80.01" y="71.12" rot="R90"/>
<instance part="X1" gate="1" x="184.15" y="106.68"/>
<instance part="GND20" gate="1" x="171.45" y="116.84" rot="R270"/>
<instance part="GND21" gate="1" x="171.45" y="93.98" rot="R270"/>
<instance part="GND22" gate="1" x="196.85" y="93.98" rot="R90"/>
<instance part="GND23" gate="1" x="196.85" y="116.84" rot="R90"/>
<instance part="X2" gate="1" x="222.25" y="106.68"/>
<instance part="GND3" gate="1" x="209.55" y="116.84" rot="R270"/>
<instance part="GND4" gate="1" x="209.55" y="93.98" rot="R270"/>
<instance part="GND5" gate="1" x="234.95" y="93.98" rot="R90"/>
<instance part="GND6" gate="1" x="234.95" y="116.84" rot="R90"/>
<instance part="X3" gate="1" x="184.15" y="73.66"/>
<instance part="GND7" gate="1" x="171.45" y="83.82" rot="R270"/>
<instance part="GND8" gate="1" x="171.45" y="60.96" rot="R270"/>
<instance part="GND9" gate="1" x="196.85" y="60.96" rot="R90"/>
<instance part="GND10" gate="1" x="196.85" y="83.82" rot="R90"/>
<instance part="X4" gate="1" x="222.25" y="73.66"/>
<instance part="GND11" gate="1" x="209.55" y="83.82" rot="R270"/>
<instance part="GND12" gate="1" x="209.55" y="60.96" rot="R270"/>
<instance part="GND13" gate="1" x="234.95" y="60.96" rot="R90"/>
<instance part="GND14" gate="1" x="234.95" y="83.82" rot="R90"/>
<instance part="X5" gate="1" x="222.25" y="31.75"/>
<instance part="GND15" gate="1" x="209.55" y="41.91" rot="R270"/>
<instance part="GND16" gate="1" x="209.55" y="19.05" rot="R270"/>
<instance part="GND17" gate="1" x="234.95" y="19.05" rot="R90"/>
<instance part="GND18" gate="1" x="234.95" y="41.91" rot="R90"/>
<instance part="CN2" gate="G$1" x="130.81" y="71.12" rot="R90"/>
</instances>
<busses>
<bus name="/OWN,/SLAVE,CFGOUT,/CCKQ,/CCK,CDAC,/OVR,XRDY,/INT2,/INT6,A[1..23],/IPL[0..2],FC[0..2],/BERR,/VPA,E,/VMA,/RESET,/HALT,/BR,/BGACK,/BG,DA[0..15],/DTACK,R/W,/LDS,/UDS,7MHZ,IORST,DOE,/EINT1,/GBG,/FCS,/DS1,Z3,GND,VCC,CFGIN,/AS,28M,BOSS,PROBE[0..3]">
<segment>
<wire x1="55.88" y1="132.08" x2="55.88" y2="2.54" width="0.762" layer="92"/>
<wire x1="55.88" y1="2.54" x2="106.68" y2="2.54" width="0.762" layer="92"/>
<wire x1="106.68" y1="2.54" x2="106.68" y2="134.62" width="0.762" layer="92"/>
<wire x1="106.68" y1="2.54" x2="157.48" y2="2.54" width="0.762" layer="92"/>
<wire x1="157.48" y1="2.54" x2="157.48" y2="134.62" width="0.762" layer="92"/>
<wire x1="157.48" y1="2.54" x2="205.74" y2="2.54" width="0.762" layer="92"/>
<wire x1="205.74" y1="2.54" x2="247.65" y2="2.54" width="0.762" layer="92"/>
<wire x1="247.65" y1="2.54" x2="250.19" y2="5.08" width="0.762" layer="92"/>
<wire x1="250.19" y1="5.08" x2="250.19" y2="121.92" width="0.762" layer="92"/>
<wire x1="205.74" y1="2.54" x2="205.74" y2="123.19" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="R/W" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="R/W"/>
<wire x1="64.77" y1="119.38" x2="58.42" y2="119.38" width="0.1524" layer="91"/>
<wire x1="58.42" y1="119.38" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
<label x="58.42" y="119.38" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="9"/>
<wire x1="229.87" y1="104.14" x2="247.65" y2="104.14" width="0.1524" layer="91"/>
<wire x1="247.65" y1="104.14" x2="250.19" y2="101.6" width="0.1524" layer="91"/>
<label x="234.95" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="R/W"/>
<wire x1="115.57" y1="119.38" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<wire x1="109.22" y1="119.38" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<label x="109.22" y="119.38" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/UDS" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/UDS"/>
<wire x1="64.77" y1="116.84" x2="58.42" y2="116.84" width="0.1524" layer="91"/>
<wire x1="58.42" y1="116.84" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<label x="58.42" y="116.84" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="5"/>
<wire x1="229.87" y1="99.06" x2="246.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="246.38" y1="99.06" x2="250.19" y2="95.25" width="0.1524" layer="91"/>
<label x="234.95" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/UDS"/>
<wire x1="115.57" y1="116.84" x2="109.22" y2="116.84" width="0.1524" layer="91"/>
<wire x1="109.22" y1="116.84" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<label x="109.22" y="116.84" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/LDS" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/LDS"/>
<wire x1="64.77" y1="114.3" x2="58.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="58.42" y1="114.3" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
<label x="58.42" y="114.3" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="7"/>
<wire x1="229.87" y1="101.6" x2="247.65" y2="101.6" width="0.1524" layer="91"/>
<wire x1="247.65" y1="101.6" x2="250.19" y2="99.06" width="0.1524" layer="91"/>
<label x="234.95" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/LDS"/>
<wire x1="115.57" y1="114.3" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="109.22" y1="114.3" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<label x="109.22" y="114.3" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="GND@11"/>
<wire x1="95.25" y1="38.1" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="38.1" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<label x="99.06" y="38.1" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@10"/>
<wire x1="95.25" y1="35.56" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<wire x1="104.14" y1="35.56" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<label x="99.06" y="35.56" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@9"/>
<wire x1="95.25" y1="33.02" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="33.02" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<label x="99.06" y="33.02" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@8"/>
<wire x1="95.25" y1="30.48" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<wire x1="104.14" y1="30.48" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<label x="99.06" y="30.48" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@7"/>
<wire x1="95.25" y1="27.94" x2="104.14" y2="27.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="27.94" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
<label x="99.06" y="27.94" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@6"/>
<wire x1="95.25" y1="25.4" x2="104.14" y2="25.4" width="0.1524" layer="91"/>
<wire x1="104.14" y1="25.4" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@5"/>
<wire x1="95.25" y1="22.86" x2="104.14" y2="22.86" width="0.1524" layer="91"/>
<wire x1="104.14" y1="22.86" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<label x="99.06" y="22.86" size="1.27" layer="95" rot="R180"/>
<label x="99.06" y="25.4" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@4"/>
<wire x1="95.25" y1="20.32" x2="104.14" y2="20.32" width="0.1524" layer="91"/>
<wire x1="104.14" y1="20.32" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<label x="99.06" y="20.32" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@3"/>
<wire x1="95.25" y1="17.78" x2="104.14" y2="17.78" width="0.1524" layer="91"/>
<wire x1="104.14" y1="17.78" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
<label x="99.06" y="17.78" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@2"/>
<wire x1="95.25" y1="15.24" x2="104.14" y2="15.24" width="0.1524" layer="91"/>
<wire x1="104.14" y1="15.24" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<label x="99.06" y="15.24" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@1"/>
<wire x1="95.25" y1="12.7" x2="104.14" y2="12.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="12.7" x2="106.68" y2="10.16" width="0.1524" layer="91"/>
<label x="99.06" y="12.7" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="GND@0"/>
<wire x1="95.25" y1="10.16" x2="104.14" y2="10.16" width="0.1524" layer="91"/>
<wire x1="104.14" y1="10.16" x2="106.68" y2="7.62" width="0.1524" layer="91"/>
<label x="99.06" y="10.16" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="20"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="173.99" y1="116.84" x2="176.53" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="173.99" y1="93.98" x2="176.53" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="194.31" y1="93.98" x2="191.77" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="19"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="194.31" y1="116.84" x2="191.77" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="20"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="212.09" y1="116.84" x2="214.63" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="212.09" y1="93.98" x2="214.63" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="232.41" y1="93.98" x2="229.87" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="19"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="232.41" y1="116.84" x2="229.87" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="20"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="173.99" y1="83.82" x2="176.53" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="173.99" y1="60.96" x2="176.53" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="194.31" y1="60.96" x2="191.77" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="19"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="194.31" y1="83.82" x2="191.77" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="20"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="212.09" y1="83.82" x2="214.63" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="212.09" y1="60.96" x2="214.63" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="1"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="232.41" y1="60.96" x2="229.87" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="19"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="232.41" y1="83.82" x2="229.87" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="20"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="212.09" y1="41.91" x2="214.63" y2="41.91" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="2"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="212.09" y1="19.05" x2="214.63" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="1"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="232.41" y1="19.05" x2="229.87" y2="19.05" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="19"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="232.41" y1="41.91" x2="229.87" y2="41.91" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@11"/>
<wire x1="146.05" y1="38.1" x2="154.94" y2="38.1" width="0.1524" layer="91"/>
<wire x1="154.94" y1="38.1" x2="157.48" y2="35.56" width="0.1524" layer="91"/>
<label x="149.86" y="38.1" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@10"/>
<wire x1="146.05" y1="35.56" x2="154.94" y2="35.56" width="0.1524" layer="91"/>
<wire x1="154.94" y1="35.56" x2="157.48" y2="33.02" width="0.1524" layer="91"/>
<label x="149.86" y="35.56" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@9"/>
<wire x1="146.05" y1="33.02" x2="154.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="154.94" y1="33.02" x2="157.48" y2="30.48" width="0.1524" layer="91"/>
<label x="149.86" y="33.02" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@8"/>
<wire x1="146.05" y1="30.48" x2="154.94" y2="30.48" width="0.1524" layer="91"/>
<wire x1="154.94" y1="30.48" x2="157.48" y2="27.94" width="0.1524" layer="91"/>
<label x="149.86" y="30.48" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@7"/>
<wire x1="146.05" y1="27.94" x2="154.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="154.94" y1="27.94" x2="157.48" y2="25.4" width="0.1524" layer="91"/>
<label x="149.86" y="27.94" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@6"/>
<wire x1="146.05" y1="25.4" x2="154.94" y2="25.4" width="0.1524" layer="91"/>
<wire x1="154.94" y1="25.4" x2="157.48" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@5"/>
<wire x1="146.05" y1="22.86" x2="154.94" y2="22.86" width="0.1524" layer="91"/>
<wire x1="154.94" y1="22.86" x2="157.48" y2="20.32" width="0.1524" layer="91"/>
<label x="149.86" y="22.86" size="1.27" layer="95" rot="R180"/>
<label x="149.86" y="25.4" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@4"/>
<wire x1="146.05" y1="20.32" x2="154.94" y2="20.32" width="0.1524" layer="91"/>
<wire x1="154.94" y1="20.32" x2="157.48" y2="17.78" width="0.1524" layer="91"/>
<label x="149.86" y="20.32" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@3"/>
<wire x1="146.05" y1="17.78" x2="154.94" y2="17.78" width="0.1524" layer="91"/>
<wire x1="154.94" y1="17.78" x2="157.48" y2="15.24" width="0.1524" layer="91"/>
<label x="149.86" y="17.78" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@2"/>
<wire x1="146.05" y1="15.24" x2="154.94" y2="15.24" width="0.1524" layer="91"/>
<wire x1="154.94" y1="15.24" x2="157.48" y2="12.7" width="0.1524" layer="91"/>
<label x="149.86" y="15.24" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@1"/>
<wire x1="146.05" y1="12.7" x2="154.94" y2="12.7" width="0.1524" layer="91"/>
<wire x1="154.94" y1="12.7" x2="157.48" y2="10.16" width="0.1524" layer="91"/>
<label x="149.86" y="12.7" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="GND@0"/>
<wire x1="146.05" y1="10.16" x2="154.94" y2="10.16" width="0.1524" layer="91"/>
<wire x1="154.94" y1="10.16" x2="157.48" y2="7.62" width="0.1524" layer="91"/>
<label x="149.86" y="10.16" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A17" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A17"/>
<wire x1="64.77" y1="25.4" x2="58.42" y2="25.4" width="0.1524" layer="91"/>
<wire x1="58.42" y1="25.4" x2="55.88" y2="22.86" width="0.1524" layer="91"/>
<label x="58.42" y="25.4" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="12"/>
<wire x1="176.53" y1="73.66" x2="160.02" y2="73.66" width="0.1524" layer="91"/>
<wire x1="160.02" y1="73.66" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="165.1" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A17"/>
<wire x1="115.57" y1="25.4" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<wire x1="109.22" y1="25.4" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<label x="109.22" y="25.4" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A18" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A18"/>
<wire x1="64.77" y1="22.86" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<wire x1="58.42" y1="22.86" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
<label x="58.42" y="22.86" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="10"/>
<wire x1="176.53" y1="71.12" x2="160.02" y2="71.12" width="0.1524" layer="91"/>
<wire x1="160.02" y1="71.12" x2="157.48" y2="68.58" width="0.1524" layer="91"/>
<label x="165.1" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A18"/>
<wire x1="115.57" y1="22.86" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<wire x1="109.22" y1="22.86" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<label x="109.22" y="22.86" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A20" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A20"/>
<wire x1="64.77" y1="17.78" x2="58.42" y2="17.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="17.78" x2="55.88" y2="15.24" width="0.1524" layer="91"/>
<label x="58.42" y="17.78" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="6"/>
<wire x1="176.53" y1="66.04" x2="161.29" y2="66.04" width="0.1524" layer="91"/>
<wire x1="161.29" y1="66.04" x2="157.48" y2="62.23" width="0.1524" layer="91"/>
<label x="165.1" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A20"/>
<wire x1="115.57" y1="17.78" x2="109.22" y2="17.78" width="0.1524" layer="91"/>
<wire x1="109.22" y1="17.78" x2="106.68" y2="15.24" width="0.1524" layer="91"/>
<label x="109.22" y="17.78" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A19" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A19"/>
<wire x1="64.77" y1="20.32" x2="58.42" y2="20.32" width="0.1524" layer="91"/>
<wire x1="58.42" y1="20.32" x2="55.88" y2="17.78" width="0.1524" layer="91"/>
<label x="58.42" y="20.32" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="8"/>
<wire x1="176.53" y1="68.58" x2="160.02" y2="68.58" width="0.1524" layer="91"/>
<wire x1="160.02" y1="68.58" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<label x="165.1" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A19"/>
<wire x1="115.57" y1="20.32" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
<wire x1="109.22" y1="20.32" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<label x="109.22" y="20.32" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A16" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A16"/>
<wire x1="64.77" y1="27.94" x2="58.42" y2="27.94" width="0.1524" layer="91"/>
<wire x1="58.42" y1="27.94" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<label x="58.42" y="27.94" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="14"/>
<wire x1="176.53" y1="76.2" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
<wire x1="160.02" y1="76.2" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<label x="165.1" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A16"/>
<wire x1="115.57" y1="27.94" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
<wire x1="109.22" y1="27.94" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
<label x="109.22" y="27.94" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A15" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A15"/>
<wire x1="64.77" y1="30.48" x2="58.42" y2="30.48" width="0.1524" layer="91"/>
<wire x1="58.42" y1="30.48" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
<label x="58.42" y="30.48" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="16"/>
<wire x1="176.53" y1="78.74" x2="160.02" y2="78.74" width="0.1524" layer="91"/>
<wire x1="160.02" y1="78.74" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<label x="165.1" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A15"/>
<wire x1="115.57" y1="30.48" x2="109.22" y2="30.48" width="0.1524" layer="91"/>
<wire x1="109.22" y1="30.48" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<label x="109.22" y="30.48" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A14" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A14"/>
<wire x1="64.77" y1="33.02" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
<wire x1="58.42" y1="33.02" x2="55.88" y2="30.48" width="0.1524" layer="91"/>
<label x="58.42" y="33.02" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="18"/>
<wire x1="176.53" y1="81.28" x2="160.02" y2="81.28" width="0.1524" layer="91"/>
<wire x1="160.02" y1="81.28" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<label x="165.1" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A14"/>
<wire x1="115.57" y1="33.02" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
<wire x1="109.22" y1="33.02" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<label x="109.22" y="33.02" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A13" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A13"/>
<wire x1="64.77" y1="35.56" x2="58.42" y2="35.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="35.56" x2="55.88" y2="33.02" width="0.1524" layer="91"/>
<label x="58.42" y="35.56" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="4"/>
<wire x1="214.63" y1="63.5" x2="208.28" y2="63.5" width="0.1524" layer="91"/>
<wire x1="208.28" y1="63.5" x2="205.74" y2="60.96" width="0.1524" layer="91"/>
<label x="209.55" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A13"/>
<wire x1="115.57" y1="35.56" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="35.56" x2="106.68" y2="33.02" width="0.1524" layer="91"/>
<label x="109.22" y="35.56" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A12"/>
<wire x1="64.77" y1="38.1" x2="58.42" y2="38.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="38.1" x2="55.88" y2="35.56" width="0.1524" layer="91"/>
<label x="58.42" y="38.1" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="6"/>
<wire x1="214.63" y1="66.04" x2="208.28" y2="66.04" width="0.1524" layer="91"/>
<wire x1="208.28" y1="66.04" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<label x="209.55" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A12"/>
<wire x1="115.57" y1="38.1" x2="109.22" y2="38.1" width="0.1524" layer="91"/>
<wire x1="109.22" y1="38.1" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<label x="109.22" y="38.1" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A11" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A11"/>
<wire x1="64.77" y1="40.64" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<wire x1="58.42" y1="40.64" x2="55.88" y2="38.1" width="0.1524" layer="91"/>
<label x="58.42" y="40.64" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="8"/>
<wire x1="214.63" y1="68.58" x2="208.28" y2="68.58" width="0.1524" layer="91"/>
<wire x1="208.28" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<label x="209.55" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A11"/>
<wire x1="115.57" y1="40.64" x2="109.22" y2="40.64" width="0.1524" layer="91"/>
<wire x1="109.22" y1="40.64" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<label x="109.22" y="40.64" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A10" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A10"/>
<wire x1="64.77" y1="43.18" x2="58.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="58.42" y1="43.18" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<label x="58.42" y="43.18" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="10"/>
<wire x1="214.63" y1="71.12" x2="208.28" y2="71.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<label x="209.55" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A10"/>
<wire x1="115.57" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="43.18" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<label x="109.22" y="43.18" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A9" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A9"/>
<wire x1="64.77" y1="45.72" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="45.72" x2="55.88" y2="43.18" width="0.1524" layer="91"/>
<label x="58.42" y="45.72" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="12"/>
<wire x1="214.63" y1="73.66" x2="208.28" y2="73.66" width="0.1524" layer="91"/>
<wire x1="208.28" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<label x="209.55" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A9"/>
<wire x1="115.57" y1="45.72" x2="109.22" y2="45.72" width="0.1524" layer="91"/>
<wire x1="109.22" y1="45.72" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<label x="109.22" y="45.72" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A8" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A8"/>
<wire x1="64.77" y1="48.26" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<wire x1="58.42" y1="48.26" x2="55.88" y2="45.72" width="0.1524" layer="91"/>
<label x="58.42" y="48.26" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="14"/>
<wire x1="214.63" y1="76.2" x2="208.28" y2="76.2" width="0.1524" layer="91"/>
<wire x1="208.28" y1="76.2" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<label x="209.55" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A8"/>
<wire x1="115.57" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="48.26" x2="106.68" y2="45.72" width="0.1524" layer="91"/>
<label x="109.22" y="48.26" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A7" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A7"/>
<wire x1="64.77" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="55.88" y2="48.26" width="0.1524" layer="91"/>
<label x="58.42" y="50.8" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="16"/>
<wire x1="214.63" y1="78.74" x2="208.28" y2="78.74" width="0.1524" layer="91"/>
<wire x1="208.28" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<label x="209.55" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A7"/>
<wire x1="115.57" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="50.8" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<label x="109.22" y="50.8" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A6" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A6"/>
<wire x1="64.77" y1="53.34" x2="58.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="58.42" y1="53.34" x2="55.88" y2="50.8" width="0.1524" layer="91"/>
<label x="58.42" y="53.34" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="18"/>
<wire x1="214.63" y1="81.28" x2="208.28" y2="81.28" width="0.1524" layer="91"/>
<wire x1="208.28" y1="81.28" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<label x="209.55" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A6"/>
<wire x1="115.57" y1="53.34" x2="109.22" y2="53.34" width="0.1524" layer="91"/>
<wire x1="109.22" y1="53.34" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<label x="109.22" y="53.34" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A5" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A5"/>
<wire x1="64.77" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<wire x1="58.42" y1="55.88" x2="55.88" y2="53.34" width="0.1524" layer="91"/>
<label x="58.42" y="55.88" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="4"/>
<wire x1="214.63" y1="21.59" x2="208.28" y2="21.59" width="0.1524" layer="91"/>
<wire x1="208.28" y1="21.59" x2="205.74" y2="19.05" width="0.1524" layer="91"/>
<label x="209.55" y="21.59" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A5"/>
<wire x1="115.57" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<wire x1="109.22" y1="55.88" x2="106.68" y2="53.34" width="0.1524" layer="91"/>
<label x="109.22" y="55.88" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A4" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A4"/>
<wire x1="64.77" y1="58.42" x2="58.42" y2="58.42" width="0.1524" layer="91"/>
<wire x1="58.42" y1="58.42" x2="55.88" y2="55.88" width="0.1524" layer="91"/>
<label x="58.42" y="58.42" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="3"/>
<wire x1="229.87" y1="21.59" x2="246.38" y2="21.59" width="0.1524" layer="91"/>
<wire x1="246.38" y1="21.59" x2="250.19" y2="17.78" width="0.1524" layer="91"/>
<label x="234.95" y="21.59" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A4"/>
<wire x1="115.57" y1="58.42" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<wire x1="109.22" y1="58.42" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<label x="109.22" y="58.42" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A3" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A3"/>
<wire x1="64.77" y1="60.96" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="55.88" y2="58.42" width="0.1524" layer="91"/>
<label x="58.42" y="60.96" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="17"/>
<wire x1="229.87" y1="81.28" x2="247.65" y2="81.28" width="0.1524" layer="91"/>
<wire x1="247.65" y1="81.28" x2="250.19" y2="78.74" width="0.1524" layer="91"/>
<label x="234.95" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A3"/>
<wire x1="115.57" y1="60.96" x2="109.22" y2="60.96" width="0.1524" layer="91"/>
<wire x1="109.22" y1="60.96" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<label x="109.22" y="60.96" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A2" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A2"/>
<wire x1="64.77" y1="63.5" x2="58.42" y2="63.5" width="0.1524" layer="91"/>
<wire x1="58.42" y1="63.5" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
<label x="58.42" y="63.5" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="15"/>
<wire x1="229.87" y1="78.74" x2="247.65" y2="78.74" width="0.1524" layer="91"/>
<wire x1="247.65" y1="78.74" x2="250.19" y2="76.2" width="0.1524" layer="91"/>
<label x="234.95" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A2"/>
<wire x1="115.57" y1="63.5" x2="109.22" y2="63.5" width="0.1524" layer="91"/>
<wire x1="109.22" y1="63.5" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<label x="109.22" y="63.5" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<wire x1="64.77" y1="66.04" x2="58.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="58.42" y1="66.04" x2="55.88" y2="63.5" width="0.1524" layer="91"/>
<label x="58.42" y="66.04" size="1.27" layer="95" rot="R180"/>
<pinref part="CN1" gate="G$1" pin="A1"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="13"/>
<wire x1="229.87" y1="76.2" x2="247.65" y2="76.2" width="0.1524" layer="91"/>
<wire x1="247.65" y1="76.2" x2="250.19" y2="73.66" width="0.1524" layer="91"/>
<label x="234.95" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="115.57" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="66.04" x2="106.68" y2="63.5" width="0.1524" layer="91"/>
<label x="109.22" y="66.04" size="1.27" layer="95" rot="R180"/>
<pinref part="CN2" gate="G$1" pin="A1"/>
</segment>
</net>
<net name="/AS" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/AS"/>
<wire x1="64.77" y1="124.46" x2="58.42" y2="124.46" width="0.1524" layer="91"/>
<wire x1="58.42" y1="124.46" x2="55.88" y2="121.92" width="0.1524" layer="91"/>
<label x="58.42" y="124.46" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="3"/>
<wire x1="229.87" y1="96.52" x2="246.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="246.38" y1="96.52" x2="250.19" y2="92.71" width="0.1524" layer="91"/>
<label x="234.95" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/AS"/>
<wire x1="115.57" y1="124.46" x2="109.22" y2="124.46" width="0.1524" layer="91"/>
<wire x1="109.22" y1="124.46" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
<label x="109.22" y="124.46" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA0" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D0"/>
<wire x1="64.77" y1="109.22" x2="58.42" y2="109.22" width="0.1524" layer="91"/>
<wire x1="58.42" y1="109.22" x2="55.88" y2="106.68" width="0.1524" layer="91"/>
<label x="58.42" y="109.22" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="17"/>
<wire x1="191.77" y1="114.3" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
<wire x1="203.2" y1="114.3" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<label x="194.31" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D0"/>
<wire x1="115.57" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="109.22" y1="109.22" x2="106.68" y2="106.68" width="0.1524" layer="91"/>
<label x="109.22" y="109.22" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA2" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D2"/>
<wire x1="64.77" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<wire x1="58.42" y1="104.14" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<label x="58.42" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="13"/>
<wire x1="191.77" y1="109.22" x2="203.2" y2="109.22" width="0.1524" layer="91"/>
<wire x1="203.2" y1="109.22" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<label x="194.31" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D2"/>
<wire x1="115.57" y1="104.14" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="109.22" y1="104.14" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<label x="109.22" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA1" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D1"/>
<wire x1="64.77" y1="106.68" x2="58.42" y2="106.68" width="0.1524" layer="91"/>
<wire x1="58.42" y1="106.68" x2="55.88" y2="104.14" width="0.1524" layer="91"/>
<label x="58.42" y="106.68" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="15"/>
<wire x1="191.77" y1="111.76" x2="203.2" y2="111.76" width="0.1524" layer="91"/>
<wire x1="203.2" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<label x="194.31" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D1"/>
<wire x1="115.57" y1="106.68" x2="109.22" y2="106.68" width="0.1524" layer="91"/>
<wire x1="109.22" y1="106.68" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<label x="109.22" y="106.68" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA3" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D3"/>
<wire x1="64.77" y1="101.6" x2="58.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="58.42" y1="101.6" x2="55.88" y2="99.06" width="0.1524" layer="91"/>
<label x="58.42" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="11"/>
<wire x1="191.77" y1="106.68" x2="203.2" y2="106.68" width="0.1524" layer="91"/>
<wire x1="203.2" y1="106.68" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<label x="194.31" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D3"/>
<wire x1="115.57" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<wire x1="109.22" y1="101.6" x2="106.68" y2="99.06" width="0.1524" layer="91"/>
<label x="109.22" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA4" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D4"/>
<wire x1="64.77" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<label x="58.42" y="99.06" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="9"/>
<wire x1="191.77" y1="104.14" x2="203.2" y2="104.14" width="0.1524" layer="91"/>
<wire x1="203.2" y1="104.14" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<label x="194.31" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D4"/>
<wire x1="115.57" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<wire x1="109.22" y1="99.06" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<label x="109.22" y="99.06" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA5" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D5"/>
<wire x1="64.77" y1="96.52" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<wire x1="58.42" y1="96.52" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
<label x="58.42" y="96.52" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="10"/>
<wire x1="176.53" y1="104.14" x2="160.02" y2="104.14" width="0.1524" layer="91"/>
<wire x1="160.02" y1="104.14" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<label x="165.1" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D5"/>
<wire x1="115.57" y1="96.52" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<wire x1="109.22" y1="96.52" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<label x="109.22" y="96.52" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA6" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D6"/>
<wire x1="64.77" y1="93.98" x2="58.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="58.42" y1="93.98" x2="55.88" y2="91.44" width="0.1524" layer="91"/>
<label x="58.42" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="12"/>
<wire x1="176.53" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<wire x1="160.02" y1="106.68" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
<label x="165.1" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D6"/>
<wire x1="115.57" y1="93.98" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="93.98" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<label x="109.22" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA7" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D7"/>
<wire x1="64.77" y1="91.44" x2="58.42" y2="91.44" width="0.1524" layer="91"/>
<wire x1="58.42" y1="91.44" x2="55.88" y2="88.9" width="0.1524" layer="91"/>
<label x="58.42" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="14"/>
<wire x1="176.53" y1="109.22" x2="160.02" y2="109.22" width="0.1524" layer="91"/>
<wire x1="160.02" y1="109.22" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<label x="165.1" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D7"/>
<wire x1="115.57" y1="91.44" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="109.22" y1="91.44" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<label x="109.22" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA8" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D8"/>
<wire x1="64.77" y1="88.9" x2="58.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="58.42" y1="88.9" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<label x="58.42" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="16"/>
<wire x1="176.53" y1="111.76" x2="160.02" y2="111.76" width="0.1524" layer="91"/>
<wire x1="160.02" y1="111.76" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<label x="165.1" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D8"/>
<wire x1="115.57" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="109.22" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA9" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D9"/>
<wire x1="64.77" y1="86.36" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="55.88" y2="83.82" width="0.1524" layer="91"/>
<label x="58.42" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="18"/>
<wire x1="176.53" y1="114.3" x2="160.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="160.02" y1="114.3" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<label x="165.1" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D9"/>
<wire x1="115.57" y1="86.36" x2="109.22" y2="86.36" width="0.1524" layer="91"/>
<wire x1="109.22" y1="86.36" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<label x="109.22" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA10" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D10"/>
<wire x1="64.77" y1="83.82" x2="58.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="58.42" y1="83.82" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<label x="58.42" y="83.82" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="4"/>
<wire x1="214.63" y1="96.52" x2="208.28" y2="96.52" width="0.1524" layer="91"/>
<wire x1="208.28" y1="96.52" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<label x="209.55" y="96.52" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D10"/>
<wire x1="115.57" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="106.68" y2="81.28" width="0.1524" layer="91"/>
<label x="109.22" y="83.82" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA11" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D11"/>
<wire x1="64.77" y1="81.28" x2="58.42" y2="81.28" width="0.1524" layer="91"/>
<wire x1="58.42" y1="81.28" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<label x="58.42" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="6"/>
<wire x1="214.63" y1="99.06" x2="208.28" y2="99.06" width="0.1524" layer="91"/>
<wire x1="208.28" y1="99.06" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<label x="209.55" y="99.06" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D11"/>
<wire x1="115.57" y1="81.28" x2="109.22" y2="81.28" width="0.1524" layer="91"/>
<wire x1="109.22" y1="81.28" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<label x="109.22" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA12" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D12"/>
<wire x1="64.77" y1="78.74" x2="58.42" y2="78.74" width="0.1524" layer="91"/>
<wire x1="58.42" y1="78.74" x2="55.88" y2="76.2" width="0.1524" layer="91"/>
<label x="58.42" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="8"/>
<wire x1="214.63" y1="101.6" x2="208.28" y2="101.6" width="0.1524" layer="91"/>
<wire x1="208.28" y1="101.6" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<label x="209.55" y="101.6" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D12"/>
<wire x1="115.57" y1="78.74" x2="109.22" y2="78.74" width="0.1524" layer="91"/>
<wire x1="109.22" y1="78.74" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="109.22" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA13" class="0">
<segment>
<wire x1="64.77" y1="76.2" x2="58.42" y2="76.2" width="0.1524" layer="91"/>
<wire x1="58.42" y1="76.2" x2="55.88" y2="73.66" width="0.1524" layer="91"/>
<label x="58.42" y="76.2" size="1.27" layer="95" rot="R180"/>
<pinref part="CN1" gate="G$1" pin="D13"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="10"/>
<wire x1="214.63" y1="104.14" x2="208.28" y2="104.14" width="0.1524" layer="91"/>
<wire x1="208.28" y1="104.14" x2="205.74" y2="101.6" width="0.1524" layer="91"/>
<label x="209.55" y="104.14" size="1.27" layer="95"/>
</segment>
<segment>
<wire x1="115.57" y1="76.2" x2="109.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="109.22" y1="76.2" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<label x="109.22" y="76.2" size="1.27" layer="95" rot="R180"/>
<pinref part="CN2" gate="G$1" pin="D13"/>
</segment>
</net>
<net name="DA14" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D14"/>
<wire x1="64.77" y1="73.66" x2="58.42" y2="73.66" width="0.1524" layer="91"/>
<wire x1="58.42" y1="73.66" x2="55.88" y2="71.12" width="0.1524" layer="91"/>
<label x="58.42" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="12"/>
<wire x1="214.63" y1="106.68" x2="208.28" y2="106.68" width="0.1524" layer="91"/>
<wire x1="208.28" y1="106.68" x2="205.74" y2="104.14" width="0.1524" layer="91"/>
<label x="209.55" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D14"/>
<wire x1="115.57" y1="73.66" x2="109.22" y2="73.66" width="0.1524" layer="91"/>
<wire x1="109.22" y1="73.66" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<label x="109.22" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="DA15" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="D15"/>
<wire x1="64.77" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="58.42" y1="71.12" x2="55.88" y2="68.58" width="0.1524" layer="91"/>
<label x="58.42" y="71.12" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="14"/>
<wire x1="214.63" y1="109.22" x2="208.28" y2="109.22" width="0.1524" layer="91"/>
<wire x1="208.28" y1="109.22" x2="205.74" y2="106.68" width="0.1524" layer="91"/>
<label x="209.55" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="D15"/>
<wire x1="115.57" y1="71.12" x2="109.22" y2="71.12" width="0.1524" layer="91"/>
<wire x1="109.22" y1="71.12" x2="106.68" y2="68.58" width="0.1524" layer="91"/>
<label x="109.22" y="71.12" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="XRDY" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="XRDY"/>
<wire x1="64.77" y1="129.54" x2="57.15" y2="129.54" width="0.1524" layer="91"/>
<wire x1="57.15" y1="129.54" x2="55.88" y2="128.27" width="0.1524" layer="91"/>
<label x="57.15" y="129.54" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="7"/>
<wire x1="229.87" y1="26.67" x2="247.65" y2="26.67" width="0.1524" layer="91"/>
<wire x1="247.65" y1="26.67" x2="250.19" y2="24.13" width="0.1524" layer="91"/>
<label x="234.95" y="26.67" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="XRDY"/>
<wire x1="115.57" y1="129.54" x2="107.95" y2="129.54" width="0.1524" layer="91"/>
<wire x1="107.95" y1="129.54" x2="106.68" y2="128.27" width="0.1524" layer="91"/>
<label x="107.95" y="129.54" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A23" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A23"/>
<wire x1="64.77" y1="10.16" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
<wire x1="58.42" y1="10.16" x2="55.88" y2="7.62" width="0.1524" layer="91"/>
<label x="58.42" y="10.16" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="16"/>
<wire x1="214.63" y1="111.76" x2="208.28" y2="111.76" width="0.1524" layer="91"/>
<wire x1="208.28" y1="111.76" x2="205.74" y2="109.22" width="0.1524" layer="91"/>
<label x="209.55" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A23"/>
<wire x1="115.57" y1="10.16" x2="109.22" y2="10.16" width="0.1524" layer="91"/>
<wire x1="109.22" y1="10.16" x2="106.68" y2="7.62" width="0.1524" layer="91"/>
<label x="109.22" y="10.16" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A22" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A22"/>
<wire x1="64.77" y1="12.7" x2="58.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="12.7" x2="55.88" y2="10.16" width="0.1524" layer="91"/>
<label x="58.42" y="12.7" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="18"/>
<wire x1="214.63" y1="114.3" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<wire x1="208.28" y1="114.3" x2="205.74" y2="111.76" width="0.1524" layer="91"/>
<label x="209.55" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A22"/>
<wire x1="115.57" y1="12.7" x2="109.22" y2="12.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="12.7" x2="106.68" y2="10.16" width="0.1524" layer="91"/>
<label x="109.22" y="12.7" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="A21" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="A21"/>
<wire x1="64.77" y1="15.24" x2="58.42" y2="15.24" width="0.1524" layer="91"/>
<wire x1="58.42" y1="15.24" x2="55.88" y2="12.7" width="0.1524" layer="91"/>
<label x="58.42" y="15.24" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="4"/>
<wire x1="176.53" y1="63.5" x2="161.29" y2="63.5" width="0.1524" layer="91"/>
<wire x1="161.29" y1="63.5" x2="157.48" y2="59.69" width="0.1524" layer="91"/>
<label x="165.1" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="A21"/>
<wire x1="115.57" y1="15.24" x2="109.22" y2="15.24" width="0.1524" layer="91"/>
<wire x1="109.22" y1="15.24" x2="106.68" y2="12.7" width="0.1524" layer="91"/>
<label x="109.22" y="15.24" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="+5V@1"/>
<wire x1="95.25" y1="45.72" x2="104.14" y2="45.72" width="0.1524" layer="91"/>
<wire x1="104.14" y1="45.72" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<label x="99.06" y="45.72" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN1" gate="G$1" pin="+5V@0"/>
<wire x1="95.25" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<wire x1="104.14" y1="43.18" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<label x="99.06" y="43.18" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="+5V@1"/>
<wire x1="146.05" y1="45.72" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<wire x1="154.94" y1="45.72" x2="157.48" y2="43.18" width="0.1524" layer="91"/>
<label x="149.86" y="45.72" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="+5V@0"/>
<wire x1="146.05" y1="43.18" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
<wire x1="154.94" y1="43.18" x2="157.48" y2="40.64" width="0.1524" layer="91"/>
<label x="149.86" y="43.18" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/RESET" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/RESET"/>
<wire x1="95.25" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<wire x1="104.14" y1="99.06" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<label x="97.79" y="99.06" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="7"/>
<wire x1="191.77" y1="68.58" x2="203.2" y2="68.58" width="0.1524" layer="91"/>
<wire x1="203.2" y1="68.58" x2="205.74" y2="66.04" width="0.1524" layer="91"/>
<label x="194.31" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/RESET"/>
<wire x1="146.05" y1="99.06" x2="154.94" y2="99.06" width="0.1524" layer="91"/>
<wire x1="154.94" y1="99.06" x2="157.48" y2="96.52" width="0.1524" layer="91"/>
<label x="148.59" y="99.06" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/BERR" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/BERR"/>
<wire x1="95.25" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<wire x1="104.14" y1="73.66" x2="106.68" y2="71.12" width="0.1524" layer="91"/>
<label x="99.06" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="15"/>
<wire x1="191.77" y1="78.74" x2="203.2" y2="78.74" width="0.1524" layer="91"/>
<wire x1="203.2" y1="78.74" x2="205.74" y2="76.2" width="0.1524" layer="91"/>
<label x="194.31" y="78.74" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/BERR"/>
<wire x1="146.05" y1="73.66" x2="154.94" y2="73.66" width="0.1524" layer="91"/>
<wire x1="154.94" y1="73.66" x2="157.48" y2="71.12" width="0.1524" layer="91"/>
<label x="149.86" y="73.66" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/SLAVE" class="0">
<segment>
<pinref part="X5" gate="1" pin="14"/>
<wire x1="214.63" y1="34.29" x2="208.28" y2="34.29" width="0.1524" layer="91"/>
<wire x1="208.28" y1="34.29" x2="205.74" y2="31.75" width="0.1524" layer="91"/>
<label x="209.55" y="34.29" size="1.27" layer="95"/>
</segment>
</net>
<net name="CFGOUT" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/COPCFG"/>
<wire x1="64.77" y1="132.08" x2="57.15" y2="132.08" width="0.1524" layer="91"/>
<wire x1="57.15" y1="132.08" x2="55.88" y2="130.81" width="0.1524" layer="91"/>
<label x="57.15" y="132.08" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="12"/>
<wire x1="214.63" y1="31.75" x2="208.28" y2="31.75" width="0.1524" layer="91"/>
<wire x1="208.28" y1="31.75" x2="205.74" y2="29.21" width="0.1524" layer="91"/>
<label x="209.55" y="31.75" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/COPCFG"/>
<wire x1="115.57" y1="132.08" x2="107.95" y2="132.08" width="0.1524" layer="91"/>
<wire x1="107.95" y1="132.08" x2="106.68" y2="130.81" width="0.1524" layer="91"/>
<label x="107.95" y="132.08" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="CFGIN" class="0">
<segment>
<pinref part="X5" gate="1" pin="13"/>
<wire x1="229.87" y1="34.29" x2="247.65" y2="34.29" width="0.1524" layer="91"/>
<wire x1="247.65" y1="34.29" x2="250.19" y2="31.75" width="0.1524" layer="91"/>
<label x="234.95" y="34.29" size="1.27" layer="95"/>
</segment>
</net>
<net name="CDAC" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="CDAC"/>
<wire x1="95.25" y1="60.96" x2="102.87" y2="60.96" width="0.1524" layer="91"/>
<wire x1="102.87" y1="60.96" x2="106.68" y2="57.15" width="0.1524" layer="91"/>
<label x="99.06" y="60.96" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="10"/>
<wire x1="214.63" y1="29.21" x2="208.28" y2="29.21" width="0.1524" layer="91"/>
<wire x1="208.28" y1="29.21" x2="205.74" y2="26.67" width="0.1524" layer="91"/>
<label x="209.55" y="29.21" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="CDAC"/>
<wire x1="146.05" y1="60.96" x2="153.67" y2="60.96" width="0.1524" layer="91"/>
<wire x1="153.67" y1="60.96" x2="157.48" y2="57.15" width="0.1524" layer="91"/>
<label x="149.86" y="60.96" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/OVR" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/OVR"/>
<wire x1="64.77" y1="127" x2="58.42" y2="127" width="0.1524" layer="91"/>
<wire x1="58.42" y1="127" x2="55.88" y2="124.46" width="0.1524" layer="91"/>
<label x="58.42" y="127" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="8"/>
<wire x1="214.63" y1="26.67" x2="208.28" y2="26.67" width="0.1524" layer="91"/>
<wire x1="208.28" y1="26.67" x2="205.74" y2="24.13" width="0.1524" layer="91"/>
<label x="209.55" y="26.67" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/OVR"/>
<wire x1="115.57" y1="127" x2="109.22" y2="127" width="0.1524" layer="91"/>
<wire x1="109.22" y1="127" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<label x="109.22" y="127" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/INT2" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/INT2"/>
<wire x1="95.25" y1="101.6" x2="104.14" y2="101.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="101.6" x2="106.68" y2="99.06" width="0.1524" layer="91"/>
<label x="99.06" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="6"/>
<wire x1="214.63" y1="24.13" x2="208.28" y2="24.13" width="0.1524" layer="91"/>
<wire x1="208.28" y1="24.13" x2="205.74" y2="21.59" width="0.1524" layer="91"/>
<label x="209.55" y="24.13" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/INT2"/>
<wire x1="146.05" y1="101.6" x2="154.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="154.94" y1="101.6" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<label x="149.86" y="101.6" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="FC0" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="FC0"/>
<wire x1="95.25" y1="116.84" x2="104.14" y2="116.84" width="0.1524" layer="91"/>
<wire x1="104.14" y1="116.84" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<label x="99.06" y="116.84" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="11"/>
<wire x1="229.87" y1="73.66" x2="247.65" y2="73.66" width="0.1524" layer="91"/>
<wire x1="247.65" y1="73.66" x2="250.19" y2="71.12" width="0.1524" layer="91"/>
<label x="234.95" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="FC0"/>
<wire x1="146.05" y1="116.84" x2="154.94" y2="116.84" width="0.1524" layer="91"/>
<wire x1="154.94" y1="116.84" x2="157.48" y2="114.3" width="0.1524" layer="91"/>
<label x="149.86" y="116.84" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="FC1" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="FC1"/>
<wire x1="95.25" y1="119.38" x2="104.14" y2="119.38" width="0.1524" layer="91"/>
<wire x1="104.14" y1="119.38" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<label x="99.06" y="119.38" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="9"/>
<wire x1="229.87" y1="71.12" x2="247.65" y2="71.12" width="0.1524" layer="91"/>
<wire x1="247.65" y1="71.12" x2="250.19" y2="68.58" width="0.1524" layer="91"/>
<label x="234.95" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="FC1"/>
<wire x1="146.05" y1="119.38" x2="154.94" y2="119.38" width="0.1524" layer="91"/>
<wire x1="154.94" y1="119.38" x2="157.48" y2="116.84" width="0.1524" layer="91"/>
<label x="149.86" y="119.38" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="FC2" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="FC2"/>
<wire x1="95.25" y1="121.92" x2="104.14" y2="121.92" width="0.1524" layer="91"/>
<wire x1="104.14" y1="121.92" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="99.06" y="121.92" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="7"/>
<wire x1="229.87" y1="68.58" x2="247.65" y2="68.58" width="0.1524" layer="91"/>
<wire x1="247.65" y1="68.58" x2="250.19" y2="66.04" width="0.1524" layer="91"/>
<label x="234.95" y="68.58" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="FC2"/>
<wire x1="146.05" y1="121.92" x2="154.94" y2="121.92" width="0.1524" layer="91"/>
<wire x1="154.94" y1="121.92" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<label x="149.86" y="121.92" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/CCK" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/C1"/>
<wire x1="95.25" y1="63.5" x2="102.87" y2="63.5" width="0.1524" layer="91"/>
<wire x1="102.87" y1="63.5" x2="106.68" y2="59.69" width="0.1524" layer="91"/>
<label x="99.06" y="63.5" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="9"/>
<wire x1="229.87" y1="29.21" x2="247.65" y2="29.21" width="0.1524" layer="91"/>
<wire x1="247.65" y1="29.21" x2="250.19" y2="26.67" width="0.1524" layer="91"/>
<label x="234.95" y="29.21" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/C1"/>
<wire x1="146.05" y1="63.5" x2="153.67" y2="63.5" width="0.1524" layer="91"/>
<wire x1="153.67" y1="63.5" x2="157.48" y2="59.69" width="0.1524" layer="91"/>
<label x="149.86" y="63.5" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/INT6" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/INT6"/>
<wire x1="95.25" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="104.14" y1="104.14" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<label x="99.06" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="5"/>
<wire x1="229.87" y1="24.13" x2="246.38" y2="24.13" width="0.1524" layer="91"/>
<wire x1="246.38" y1="24.13" x2="250.19" y2="20.32" width="0.1524" layer="91"/>
<label x="234.95" y="24.13" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/INT6"/>
<wire x1="146.05" y1="104.14" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="154.94" y1="104.14" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<label x="149.86" y="104.14" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/VMA" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/VMA"/>
<wire x1="95.25" y1="78.74" x2="104.14" y2="78.74" width="0.1524" layer="91"/>
<wire x1="104.14" y1="78.74" x2="106.68" y2="76.2" width="0.1524" layer="91"/>
<label x="99.06" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="9"/>
<wire x1="191.77" y1="71.12" x2="203.2" y2="71.12" width="0.1524" layer="91"/>
<wire x1="203.2" y1="71.12" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<label x="194.31" y="71.12" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/VMA"/>
<wire x1="146.05" y1="78.74" x2="154.94" y2="78.74" width="0.1524" layer="91"/>
<wire x1="154.94" y1="78.74" x2="157.48" y2="76.2" width="0.1524" layer="91"/>
<label x="149.86" y="78.74" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/HALT" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/HLT"/>
<wire x1="95.25" y1="81.28" x2="104.14" y2="81.28" width="0.1524" layer="91"/>
<wire x1="104.14" y1="81.28" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<label x="99.06" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="5"/>
<wire x1="191.77" y1="66.04" x2="203.2" y2="66.04" width="0.1524" layer="91"/>
<wire x1="203.2" y1="66.04" x2="205.74" y2="63.5" width="0.1524" layer="91"/>
<label x="194.31" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/HLT"/>
<wire x1="146.05" y1="81.28" x2="154.94" y2="81.28" width="0.1524" layer="91"/>
<wire x1="154.94" y1="81.28" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
<label x="149.86" y="81.28" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/BR" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/BR"/>
<wire x1="95.25" y1="88.9" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="88.9" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="99.06" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="17"/>
<wire x1="229.87" y1="114.3" x2="247.65" y2="114.3" width="0.1524" layer="91"/>
<wire x1="247.65" y1="114.3" x2="250.19" y2="111.76" width="0.1524" layer="91"/>
<label x="234.95" y="114.3" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/BR"/>
<wire x1="146.05" y1="88.9" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="154.94" y1="88.9" x2="157.48" y2="86.36" width="0.1524" layer="91"/>
<label x="149.86" y="88.9" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/BGACK" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/BGACK"/>
<wire x1="95.25" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="104.14" y1="93.98" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<label x="99.06" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="15"/>
<wire x1="229.87" y1="111.76" x2="247.65" y2="111.76" width="0.1524" layer="91"/>
<wire x1="247.65" y1="111.76" x2="250.19" y2="109.22" width="0.1524" layer="91"/>
<label x="234.95" y="111.76" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/BGACK"/>
<wire x1="146.05" y1="93.98" x2="154.94" y2="93.98" width="0.1524" layer="91"/>
<wire x1="154.94" y1="93.98" x2="157.48" y2="91.44" width="0.1524" layer="91"/>
<label x="149.86" y="93.98" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/BG" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/BG"/>
<wire x1="95.25" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="104.14" y1="91.44" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<label x="99.06" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="13"/>
<wire x1="229.87" y1="109.22" x2="247.65" y2="109.22" width="0.1524" layer="91"/>
<wire x1="247.65" y1="109.22" x2="250.19" y2="106.68" width="0.1524" layer="91"/>
<label x="234.95" y="109.22" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/BG"/>
<wire x1="146.05" y1="91.44" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="154.94" y1="91.44" x2="157.48" y2="88.9" width="0.1524" layer="91"/>
<label x="149.86" y="91.44" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/DTACK" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/DTACK"/>
<wire x1="64.77" y1="121.92" x2="58.42" y2="121.92" width="0.1524" layer="91"/>
<wire x1="58.42" y1="121.92" x2="55.88" y2="119.38" width="0.1524" layer="91"/>
<label x="58.42" y="121.92" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="11"/>
<wire x1="229.87" y1="106.68" x2="247.65" y2="106.68" width="0.1524" layer="91"/>
<wire x1="247.65" y1="106.68" x2="250.19" y2="104.14" width="0.1524" layer="91"/>
<label x="234.95" y="106.68" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/DTACK"/>
<wire x1="115.57" y1="121.92" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<wire x1="109.22" y1="121.92" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="109.22" y="121.92" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="E" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="E"/>
<wire x1="95.25" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="91"/>
<wire x1="104.14" y1="68.58" x2="106.68" y2="66.04" width="0.1524" layer="91"/>
<label x="99.06" y="68.58" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="11"/>
<wire x1="191.77" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<wire x1="203.2" y1="73.66" x2="205.74" y2="71.12" width="0.1524" layer="91"/>
<label x="194.31" y="73.66" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="E"/>
<wire x1="146.05" y1="68.58" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="154.94" y1="68.58" x2="157.48" y2="66.04" width="0.1524" layer="91"/>
<label x="149.86" y="68.58" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/VPA" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/VPA"/>
<wire x1="95.25" y1="76.2" x2="104.14" y2="76.2" width="0.1524" layer="91"/>
<wire x1="104.14" y1="76.2" x2="106.68" y2="73.66" width="0.1524" layer="91"/>
<label x="99.06" y="76.2" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="13"/>
<wire x1="191.77" y1="76.2" x2="203.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="203.2" y1="76.2" x2="205.74" y2="73.66" width="0.1524" layer="91"/>
<label x="194.31" y="76.2" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/VPA"/>
<wire x1="146.05" y1="76.2" x2="154.94" y2="76.2" width="0.1524" layer="91"/>
<wire x1="154.94" y1="76.2" x2="157.48" y2="73.66" width="0.1524" layer="91"/>
<label x="149.86" y="76.2" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/IPL0" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/IPL0"/>
<wire x1="95.25" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="106.68" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<label x="99.06" y="106.68" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="5"/>
<wire x1="229.87" y1="66.04" x2="247.65" y2="66.04" width="0.1524" layer="91"/>
<wire x1="247.65" y1="66.04" x2="250.19" y2="63.5" width="0.1524" layer="91"/>
<label x="234.95" y="66.04" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/IPL0"/>
<wire x1="146.05" y1="106.68" x2="154.94" y2="106.68" width="0.1524" layer="91"/>
<wire x1="154.94" y1="106.68" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
<label x="149.86" y="106.68" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/IPL1" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/IPL1"/>
<wire x1="95.25" y1="109.22" x2="104.14" y2="109.22" width="0.1524" layer="91"/>
<wire x1="104.14" y1="109.22" x2="106.68" y2="106.68" width="0.1524" layer="91"/>
<label x="99.06" y="109.22" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="3"/>
<wire x1="229.87" y1="63.5" x2="246.38" y2="63.5" width="0.1524" layer="91"/>
<wire x1="246.38" y1="63.5" x2="250.19" y2="59.69" width="0.1524" layer="91"/>
<label x="234.95" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/IPL1"/>
<wire x1="146.05" y1="109.22" x2="154.94" y2="109.22" width="0.1524" layer="91"/>
<wire x1="154.94" y1="109.22" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<label x="149.86" y="109.22" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/IPL2" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/IPL2"/>
<wire x1="95.25" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
<wire x1="104.14" y1="111.76" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<label x="99.06" y="111.76" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="17"/>
<wire x1="191.77" y1="81.28" x2="203.2" y2="81.28" width="0.1524" layer="91"/>
<wire x1="203.2" y1="81.28" x2="205.74" y2="78.74" width="0.1524" layer="91"/>
<label x="194.31" y="81.28" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/IPL2"/>
<wire x1="146.05" y1="111.76" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="154.94" y1="111.76" x2="157.48" y2="109.22" width="0.1524" layer="91"/>
<label x="149.86" y="111.76" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/OWN" class="0">
<segment>
<pinref part="X5" gate="1" pin="16"/>
<wire x1="214.63" y1="36.83" x2="208.28" y2="36.83" width="0.1524" layer="91"/>
<wire x1="208.28" y1="36.83" x2="205.74" y2="34.29" width="0.1524" layer="91"/>
<label x="209.55" y="36.83" size="1.27" layer="95"/>
</segment>
</net>
<net name="DOE" class="0">
<segment>
<pinref part="X1" gate="1" pin="7"/>
<wire x1="191.77" y1="101.6" x2="203.2" y2="101.6" width="0.1524" layer="91"/>
<wire x1="203.2" y1="101.6" x2="205.74" y2="99.06" width="0.1524" layer="91"/>
<label x="194.31" y="101.6" size="1.27" layer="95"/>
</segment>
</net>
<net name="7MHZ" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="E7M"/>
<wire x1="95.25" y1="58.42" x2="101.6" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="58.42" x2="106.68" y2="53.34" width="0.1524" layer="91"/>
<label x="99.06" y="58.42" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="3"/>
<wire x1="191.77" y1="63.5" x2="201.93" y2="63.5" width="0.1524" layer="91"/>
<wire x1="201.93" y1="63.5" x2="205.74" y2="59.69" width="0.1524" layer="91"/>
<label x="194.31" y="63.5" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="E7M"/>
<wire x1="146.05" y1="58.42" x2="152.4" y2="58.42" width="0.1524" layer="91"/>
<wire x1="152.4" y1="58.42" x2="157.48" y2="53.34" width="0.1524" layer="91"/>
<label x="149.86" y="58.42" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="/EINT1" class="0">
<segment>
<pinref part="X1" gate="1" pin="6"/>
<wire x1="176.53" y1="99.06" x2="160.02" y2="99.06" width="0.1524" layer="91"/>
<wire x1="160.02" y1="99.06" x2="157.48" y2="96.52" width="0.1524" layer="91"/>
<label x="165.1" y="99.06" size="1.27" layer="95"/>
</segment>
</net>
<net name="/GBG" class="0">
<segment>
<pinref part="X1" gate="1" pin="5"/>
<wire x1="191.77" y1="99.06" x2="203.2" y2="99.06" width="0.1524" layer="91"/>
<wire x1="203.2" y1="99.06" x2="205.74" y2="96.52" width="0.1524" layer="91"/>
<label x="194.31" y="99.06" size="1.27" layer="95"/>
</segment>
</net>
<net name="/FCS" class="0">
<segment>
<pinref part="X1" gate="1" pin="3"/>
<wire x1="191.77" y1="96.52" x2="203.2" y2="96.52" width="0.1524" layer="91"/>
<wire x1="203.2" y1="96.52" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<label x="194.31" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="/DS1" class="0">
<segment>
<pinref part="X1" gate="1" pin="4"/>
<wire x1="176.53" y1="96.52" x2="161.29" y2="96.52" width="0.1524" layer="91"/>
<wire x1="161.29" y1="96.52" x2="157.48" y2="92.71" width="0.1524" layer="91"/>
<label x="165.1" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="IORST" class="0">
<segment>
<pinref part="X1" gate="1" pin="8"/>
<wire x1="176.53" y1="101.6" x2="160.02" y2="101.6" width="0.1524" layer="91"/>
<wire x1="160.02" y1="101.6" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<label x="165.1" y="101.6" size="1.27" layer="95"/>
</segment>
</net>
<net name="/CCKQ" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/C3"/>
<wire x1="95.25" y1="66.04" x2="102.87" y2="66.04" width="0.1524" layer="91"/>
<wire x1="102.87" y1="66.04" x2="106.68" y2="62.23" width="0.1524" layer="91"/>
<label x="99.06" y="66.04" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="11"/>
<wire x1="229.87" y1="31.75" x2="247.65" y2="31.75" width="0.1524" layer="91"/>
<wire x1="247.65" y1="31.75" x2="250.19" y2="29.21" width="0.1524" layer="91"/>
<label x="234.95" y="31.75" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/C3"/>
<wire x1="146.05" y1="66.04" x2="153.67" y2="66.04" width="0.1524" layer="91"/>
<wire x1="153.67" y1="66.04" x2="157.48" y2="62.23" width="0.1524" layer="91"/>
<label x="149.86" y="66.04" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="BOSS" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="/BOSS"/>
<wire x1="95.25" y1="86.36" x2="104.14" y2="86.36" width="0.1524" layer="91"/>
<wire x1="104.14" y1="86.36" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<label x="99.06" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="18"/>
<wire x1="214.63" y1="39.37" x2="208.28" y2="39.37" width="0.1524" layer="91"/>
<wire x1="208.28" y1="39.37" x2="205.74" y2="36.83" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="/BOSS"/>
<wire x1="146.05" y1="86.36" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="154.94" y1="86.36" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
<label x="149.86" y="86.36" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
<net name="28M" class="0">
<segment>
<pinref part="CN1" gate="G$1" pin="28M"/>
<wire x1="95.25" y1="55.88" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
<wire x1="101.6" y1="55.88" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<label x="99.06" y="55.88" size="1.27" layer="95" rot="R180"/>
</segment>
<segment>
<pinref part="X5" gate="1" pin="15"/>
<wire x1="229.87" y1="36.83" x2="247.65" y2="36.83" width="0.1524" layer="91"/>
<wire x1="247.65" y1="36.83" x2="250.19" y2="34.29" width="0.1524" layer="91"/>
<label x="234.95" y="36.83" size="1.27" layer="95"/>
</segment>
<segment>
<pinref part="CN2" gate="G$1" pin="28M"/>
<wire x1="146.05" y1="55.88" x2="152.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="152.4" y1="55.88" x2="157.48" y2="50.8" width="0.1524" layer="91"/>
<label x="149.86" y="55.88" size="1.27" layer="95" rot="R180"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
