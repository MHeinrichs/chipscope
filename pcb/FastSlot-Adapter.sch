<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="centerline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="tBPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="tdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="bdokum" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="Ports" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="Port2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="Port3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="no" active="no"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="no" active="no"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="no" active="no"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="no" active="no"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="no" active="no"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="no" active="no"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="no" active="no"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="no" active="no"/>
<layer number="149" name="mReference" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="191" name="mNets" color="7" fill="1" visible="no" active="no"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="no" active="no"/>
<layer number="193" name="mPins" color="7" fill="1" visible="no" active="no"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="no" active="no"/>
<layer number="195" name="mNames" color="7" fill="1" visible="no" active="no"/>
<layer number="196" name="mValues" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Amiga">
<packages>
<package name="KEL200-F">
<pad name="P$1" x="-64.135" y="0.9525" drill="0.8" shape="octagon" rot="R180"/>
<pad name="P$2" x="-64.135" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$3" x="-62.865" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$4" x="-62.865" y="-0.9525" drill="0.8" rot="R180"/>
<hole x="-67.31" y="0" drill="2.3"/>
<hole x="67.31" y="0" drill="2.3"/>
<wire x1="-68.6562" y1="3.8481" x2="68.6562" y2="3.8481" width="0.127" layer="21"/>
<wire x1="68.6562" y1="3.8481" x2="68.6562" y2="-3.8481" width="0.127" layer="21"/>
<wire x1="68.6562" y1="-3.8481" x2="-68.6562" y2="-3.8481" width="0.127" layer="21"/>
<wire x1="-68.6562" y1="-3.8481" x2="-68.6562" y2="3.8481" width="0.127" layer="21"/>
<text x="-3.175" y="4.445" size="1.27" layer="25">&gt;NAME</text>
<pad name="P$5" x="-61.595" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$6" x="-61.595" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$7" x="-60.325" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$8" x="-60.325" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$9" x="-59.055" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$10" x="-59.055" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$11" x="-57.785" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$12" x="-57.785" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$13" x="-56.515" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$14" x="-56.515" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$15" x="-55.245" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$16" x="-55.245" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$17" x="-53.975" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$18" x="-53.975" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$19" x="-52.705" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$20" x="-52.705" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$21" x="-51.435" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$22" x="-51.435" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$23" x="-50.165" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$24" x="-50.165" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$25" x="-48.895" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$26" x="-48.895" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$27" x="-47.625" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$28" x="-47.625" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$29" x="-46.355" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$30" x="-46.355" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$31" x="-45.085" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$32" x="-45.085" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$33" x="-43.815" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$34" x="-43.815" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$35" x="-42.545" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$36" x="-42.545" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$37" x="-41.275" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$38" x="-41.275" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$39" x="-40.005" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$40" x="-40.005" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$41" x="-38.735" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$42" x="-38.735" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$43" x="-37.465" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$44" x="-37.465" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$45" x="-36.195" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$46" x="-36.195" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$47" x="-34.925" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$48" x="-34.925" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$49" x="-33.655" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$50" x="-33.655" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$51" x="-32.385" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$52" x="-32.385" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$53" x="-31.115" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$54" x="-31.115" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$55" x="-29.845" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$56" x="-29.845" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$57" x="-28.575" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$58" x="-28.575" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$59" x="-27.305" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$60" x="-27.305" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$61" x="-26.035" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$62" x="-26.035" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$63" x="-24.765" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$64" x="-24.765" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$65" x="-23.495" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$66" x="-23.495" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$67" x="-22.225" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$68" x="-22.225" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$69" x="-20.955" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$70" x="-20.955" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$71" x="-19.685" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$72" x="-19.685" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$73" x="-18.415" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$74" x="-18.415" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$75" x="-17.145" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$76" x="-17.145" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$77" x="-15.875" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$78" x="-15.875" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$79" x="-14.605" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$80" x="-14.605" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$81" x="-13.335" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$82" x="-13.335" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$83" x="-12.065" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$84" x="-12.065" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$85" x="-10.795" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$86" x="-10.795" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$87" x="-9.525" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$88" x="-9.525" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$89" x="-8.255" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$90" x="-8.255" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$91" x="-6.985" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$92" x="-6.985" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$93" x="-5.715" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$94" x="-5.715" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$95" x="-4.445" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$96" x="-4.445" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$97" x="-3.175" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$98" x="-3.175" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$99" x="-1.905" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$100" x="-1.905" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$101" x="1.905" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$102" x="1.905" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$103" x="3.175" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$104" x="3.175" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$105" x="4.445" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$106" x="4.445" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$107" x="5.715" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$108" x="5.715" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$109" x="6.985" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$110" x="6.985" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$111" x="8.255" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$112" x="8.255" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$113" x="9.525" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$114" x="9.525" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$115" x="10.795" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$116" x="10.795" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$117" x="12.065" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$118" x="12.065" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$119" x="13.335" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$120" x="13.335" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$121" x="14.605" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$122" x="14.605" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$123" x="15.875" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$124" x="15.875" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$125" x="17.145" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$126" x="17.145" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$127" x="18.415" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$128" x="18.415" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$129" x="19.685" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$130" x="19.685" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$131" x="20.955" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$132" x="20.955" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$133" x="22.225" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$134" x="22.225" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$135" x="23.495" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$136" x="23.495" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$137" x="24.765" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$138" x="24.765" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$139" x="26.035" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$140" x="26.035" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$141" x="27.305" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$142" x="27.305" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$143" x="28.575" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$144" x="28.575" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$145" x="29.845" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$146" x="29.845" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$147" x="31.115" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$148" x="31.115" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$149" x="32.385" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$150" x="32.385" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$151" x="33.655" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$152" x="33.655" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$153" x="34.925" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$154" x="34.925" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$155" x="36.195" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$156" x="36.195" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$157" x="37.465" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$158" x="37.465" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$159" x="38.735" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$160" x="38.735" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$161" x="40.005" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$162" x="40.005" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$163" x="41.275" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$164" x="41.275" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$165" x="42.545" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$166" x="42.545" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$167" x="43.815" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$168" x="43.815" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$169" x="45.085" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$170" x="45.085" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$171" x="46.355" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$172" x="46.355" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$173" x="47.625" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$174" x="47.625" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$175" x="48.895" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$176" x="48.895" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$177" x="50.165" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$178" x="50.165" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$179" x="51.435" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$180" x="51.435" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$181" x="52.705" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$182" x="52.705" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$183" x="53.975" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$184" x="53.975" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$185" x="55.245" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$186" x="55.245" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$187" x="56.515" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$188" x="56.515" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$189" x="57.785" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$190" x="57.785" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$191" x="59.055" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$192" x="59.055" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$193" x="60.325" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$194" x="60.325" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$195" x="61.595" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$196" x="61.595" y="-0.9525" drill="0.8" rot="R180"/>
<pad name="P$197" x="62.865" y="0.9525" drill="0.8" rot="R180"/>
<pad name="P$198" x="62.865" y="-2.8575" drill="0.8" rot="R180"/>
<pad name="P$199" x="64.135" y="2.8575" drill="0.8" rot="R180"/>
<pad name="P$200" x="64.135" y="-0.9525" drill="0.8" rot="R180"/>
<text x="-64.77" y="4.445" size="1.27" layer="21">1</text>
</package>
<package name="KEL200-M">
<pad name="P$1" x="64.135" y="2.8575" drill="0.8" shape="octagon"/>
<pad name="P$2" x="64.135" y="-0.9525" drill="0.8"/>
<pad name="P$3" x="62.865" y="0.9525" drill="0.8"/>
<pad name="P$4" x="62.865" y="-2.8575" drill="0.8"/>
<pad name="P$5" x="61.595" y="2.8575" drill="0.8"/>
<pad name="P$6" x="61.595" y="-0.9525" drill="0.8"/>
<pad name="P$7" x="60.325" y="0.9525" drill="0.8"/>
<pad name="P$8" x="60.325" y="-2.8575" drill="0.8"/>
<pad name="P$9" x="59.055" y="2.8575" drill="0.8"/>
<pad name="P$10" x="59.055" y="-0.9525" drill="0.8"/>
<pad name="P$11" x="57.785" y="0.9525" drill="0.8"/>
<pad name="P$12" x="57.785" y="-2.8575" drill="0.8"/>
<pad name="P$13" x="56.515" y="2.8575" drill="0.8"/>
<pad name="P$14" x="56.515" y="-0.9525" drill="0.8"/>
<pad name="P$15" x="55.245" y="0.9525" drill="0.8"/>
<pad name="P$16" x="55.245" y="-2.8575" drill="0.8"/>
<pad name="P$17" x="53.975" y="2.8575" drill="0.8"/>
<pad name="P$18" x="53.975" y="-0.9525" drill="0.8"/>
<pad name="P$19" x="52.705" y="0.9525" drill="0.8"/>
<pad name="P$20" x="52.705" y="-2.8575" drill="0.8"/>
<pad name="P$21" x="51.435" y="2.8575" drill="0.8"/>
<pad name="P$22" x="51.435" y="-0.9525" drill="0.8"/>
<pad name="P$23" x="50.165" y="0.9525" drill="0.8"/>
<pad name="P$24" x="50.165" y="-2.8575" drill="0.8"/>
<pad name="P$25" x="48.895" y="2.8575" drill="0.8"/>
<pad name="P$26" x="48.895" y="-0.9525" drill="0.8"/>
<pad name="P$27" x="47.625" y="0.9525" drill="0.8"/>
<pad name="P$28" x="47.625" y="-2.8575" drill="0.8"/>
<pad name="P$29" x="46.355" y="2.8575" drill="0.8"/>
<pad name="P$30" x="46.355" y="-0.9525" drill="0.8"/>
<pad name="P$31" x="45.085" y="0.9525" drill="0.8"/>
<pad name="P$32" x="45.085" y="-2.8575" drill="0.8"/>
<pad name="P$33" x="43.815" y="2.8575" drill="0.8"/>
<pad name="P$34" x="43.815" y="-0.9525" drill="0.8"/>
<pad name="P$35" x="42.545" y="0.9525" drill="0.8"/>
<pad name="P$36" x="42.545" y="-2.8575" drill="0.8"/>
<pad name="P$37" x="41.275" y="2.8575" drill="0.8"/>
<pad name="P$38" x="41.275" y="-0.9525" drill="0.8"/>
<pad name="P$39" x="40.005" y="0.9525" drill="0.8"/>
<pad name="P$40" x="40.005" y="-2.8575" drill="0.8"/>
<pad name="P$41" x="38.735" y="2.8575" drill="0.8"/>
<pad name="P$42" x="38.735" y="-0.9525" drill="0.8"/>
<pad name="P$43" x="37.465" y="0.9525" drill="0.8"/>
<pad name="P$44" x="37.465" y="-2.8575" drill="0.8"/>
<pad name="P$45" x="36.195" y="2.8575" drill="0.8"/>
<pad name="P$46" x="36.195" y="-0.9525" drill="0.8"/>
<pad name="P$47" x="34.925" y="0.9525" drill="0.8"/>
<pad name="P$48" x="34.925" y="-2.8575" drill="0.8"/>
<pad name="P$49" x="33.655" y="2.8575" drill="0.8"/>
<pad name="P$50" x="33.655" y="-0.9525" drill="0.8"/>
<pad name="P$51" x="32.385" y="0.9525" drill="0.8"/>
<pad name="P$52" x="32.385" y="-2.8575" drill="0.8"/>
<pad name="P$53" x="31.115" y="2.8575" drill="0.8"/>
<pad name="P$54" x="31.115" y="-0.9525" drill="0.8"/>
<pad name="P$55" x="29.845" y="0.9525" drill="0.8"/>
<pad name="P$56" x="29.845" y="-2.8575" drill="0.8"/>
<pad name="P$57" x="28.575" y="2.8575" drill="0.8"/>
<pad name="P$58" x="28.575" y="-0.9525" drill="0.8"/>
<pad name="P$59" x="27.305" y="0.9525" drill="0.8"/>
<pad name="P$60" x="27.305" y="-2.8575" drill="0.8"/>
<pad name="P$61" x="26.035" y="2.8575" drill="0.8"/>
<pad name="P$62" x="26.035" y="-0.9525" drill="0.8"/>
<pad name="P$63" x="24.765" y="0.9525" drill="0.8"/>
<pad name="P$64" x="24.765" y="-2.8575" drill="0.8"/>
<pad name="P$65" x="23.495" y="2.8575" drill="0.8"/>
<pad name="P$66" x="23.495" y="-0.9525" drill="0.8"/>
<pad name="P$67" x="22.225" y="0.9525" drill="0.8"/>
<pad name="P$68" x="22.225" y="-2.8575" drill="0.8"/>
<pad name="P$69" x="20.955" y="2.8575" drill="0.8"/>
<pad name="P$70" x="20.955" y="-0.9525" drill="0.8"/>
<pad name="P$71" x="19.685" y="0.9525" drill="0.8"/>
<pad name="P$72" x="19.685" y="-2.8575" drill="0.8"/>
<pad name="P$73" x="18.415" y="2.8575" drill="0.8"/>
<pad name="P$74" x="18.415" y="-0.9525" drill="0.8"/>
<pad name="P$75" x="17.145" y="0.9525" drill="0.8"/>
<pad name="P$76" x="17.145" y="-2.8575" drill="0.8"/>
<pad name="P$77" x="15.875" y="2.8575" drill="0.8"/>
<pad name="P$78" x="15.875" y="-0.9525" drill="0.8"/>
<pad name="P$79" x="14.605" y="0.9525" drill="0.8"/>
<pad name="P$80" x="14.605" y="-2.8575" drill="0.8"/>
<pad name="P$81" x="13.335" y="2.8575" drill="0.8"/>
<pad name="P$82" x="13.335" y="-0.9525" drill="0.8"/>
<pad name="P$83" x="12.065" y="0.9525" drill="0.8"/>
<pad name="P$84" x="12.065" y="-2.8575" drill="0.8"/>
<pad name="P$85" x="10.795" y="2.8575" drill="0.8"/>
<pad name="P$86" x="10.795" y="-0.9525" drill="0.8"/>
<pad name="P$87" x="9.525" y="0.9525" drill="0.8"/>
<pad name="P$88" x="9.525" y="-2.8575" drill="0.8"/>
<pad name="P$89" x="8.255" y="2.8575" drill="0.8"/>
<pad name="P$90" x="8.255" y="-0.9525" drill="0.8"/>
<pad name="P$91" x="6.985" y="0.9525" drill="0.8"/>
<pad name="P$92" x="6.985" y="-2.8575" drill="0.8"/>
<pad name="P$93" x="5.715" y="2.8575" drill="0.8"/>
<pad name="P$94" x="5.715" y="-0.9525" drill="0.8"/>
<pad name="P$95" x="4.445" y="0.9525" drill="0.8"/>
<pad name="P$96" x="4.445" y="-2.8575" drill="0.8"/>
<pad name="P$97" x="3.175" y="2.8575" drill="0.8"/>
<pad name="P$98" x="3.175" y="-0.9525" drill="0.8"/>
<pad name="P$99" x="1.905" y="0.9525" drill="0.8"/>
<pad name="P$100" x="1.905" y="-2.8575" drill="0.8"/>
<pad name="P$101" x="-1.905" y="2.8575" drill="0.8"/>
<pad name="P$102" x="-1.905" y="-0.9525" drill="0.8"/>
<pad name="P$103" x="-3.175" y="0.9525" drill="0.8"/>
<pad name="P$104" x="-3.175" y="-2.8575" drill="0.8"/>
<pad name="P$105" x="-4.445" y="2.8575" drill="0.8"/>
<pad name="P$106" x="-4.445" y="-0.9525" drill="0.8"/>
<pad name="P$107" x="-5.715" y="0.9525" drill="0.8"/>
<pad name="P$108" x="-5.715" y="-2.8575" drill="0.8"/>
<pad name="P$109" x="-6.985" y="2.8575" drill="0.8"/>
<pad name="P$110" x="-6.985" y="-0.9525" drill="0.8"/>
<pad name="P$111" x="-8.255" y="0.9525" drill="0.8"/>
<pad name="P$112" x="-8.255" y="-2.8575" drill="0.8"/>
<pad name="P$113" x="-9.525" y="2.8575" drill="0.8"/>
<pad name="P$114" x="-9.525" y="-0.9525" drill="0.8"/>
<pad name="P$115" x="-10.795" y="0.9525" drill="0.8"/>
<pad name="P$116" x="-10.795" y="-2.8575" drill="0.8"/>
<pad name="P$117" x="-12.065" y="2.8575" drill="0.8"/>
<pad name="P$118" x="-12.065" y="-0.9525" drill="0.8"/>
<pad name="P$119" x="-13.335" y="0.9525" drill="0.8"/>
<pad name="P$120" x="-13.335" y="-2.8575" drill="0.8"/>
<pad name="P$121" x="-14.605" y="2.8575" drill="0.8"/>
<pad name="P$122" x="-14.605" y="-0.9525" drill="0.8"/>
<pad name="P$123" x="-15.875" y="0.9525" drill="0.8"/>
<pad name="P$124" x="-15.875" y="-2.8575" drill="0.8"/>
<pad name="P$125" x="-17.145" y="2.8575" drill="0.8"/>
<pad name="P$126" x="-17.145" y="-0.9525" drill="0.8"/>
<pad name="P$127" x="-18.415" y="0.9525" drill="0.8"/>
<pad name="P$128" x="-18.415" y="-2.8575" drill="0.8"/>
<pad name="P$129" x="-19.685" y="2.8575" drill="0.8"/>
<pad name="P$130" x="-19.685" y="-0.9525" drill="0.8"/>
<pad name="P$131" x="-20.955" y="0.9525" drill="0.8"/>
<pad name="P$132" x="-20.955" y="-2.8575" drill="0.8"/>
<pad name="P$133" x="-22.225" y="2.8575" drill="0.8"/>
<pad name="P$134" x="-22.225" y="-0.9525" drill="0.8"/>
<pad name="P$135" x="-23.495" y="0.9525" drill="0.8"/>
<pad name="P$136" x="-23.495" y="-2.8575" drill="0.8"/>
<pad name="P$137" x="-24.765" y="2.8575" drill="0.8"/>
<pad name="P$138" x="-24.765" y="-0.9525" drill="0.8"/>
<pad name="P$139" x="-26.035" y="0.9525" drill="0.8"/>
<pad name="P$140" x="-26.035" y="-2.8575" drill="0.8"/>
<pad name="P$141" x="-27.305" y="2.8575" drill="0.8"/>
<pad name="P$142" x="-27.305" y="-0.9525" drill="0.8"/>
<pad name="P$143" x="-28.575" y="0.9525" drill="0.8"/>
<pad name="P$144" x="-28.575" y="-2.8575" drill="0.8"/>
<pad name="P$145" x="-29.845" y="2.8575" drill="0.8"/>
<pad name="P$146" x="-29.845" y="-0.9525" drill="0.8"/>
<pad name="P$147" x="-31.115" y="0.9525" drill="0.8"/>
<pad name="P$148" x="-31.115" y="-2.8575" drill="0.8"/>
<pad name="P$149" x="-32.385" y="2.8575" drill="0.8"/>
<pad name="P$150" x="-32.385" y="-0.9525" drill="0.8"/>
<pad name="P$151" x="-33.655" y="0.9525" drill="0.8"/>
<pad name="P$152" x="-33.655" y="-2.8575" drill="0.8"/>
<pad name="P$153" x="-34.925" y="2.8575" drill="0.8"/>
<pad name="P$154" x="-34.925" y="-0.9525" drill="0.8"/>
<pad name="P$155" x="-36.195" y="0.9525" drill="0.8"/>
<pad name="P$156" x="-36.195" y="-2.8575" drill="0.8"/>
<pad name="P$157" x="-37.465" y="2.8575" drill="0.8"/>
<pad name="P$158" x="-37.465" y="-0.9525" drill="0.8"/>
<pad name="P$159" x="-38.735" y="0.9525" drill="0.8"/>
<pad name="P$160" x="-38.735" y="-2.8575" drill="0.8"/>
<pad name="P$161" x="-40.005" y="2.8575" drill="0.8"/>
<pad name="P$162" x="-40.005" y="-0.9525" drill="0.8"/>
<pad name="P$163" x="-41.275" y="0.9525" drill="0.8"/>
<pad name="P$164" x="-41.275" y="-2.8575" drill="0.8"/>
<pad name="P$165" x="-42.545" y="2.8575" drill="0.8"/>
<pad name="P$166" x="-42.545" y="-0.9525" drill="0.8"/>
<pad name="P$167" x="-43.815" y="0.9525" drill="0.8"/>
<pad name="P$168" x="-43.815" y="-2.8575" drill="0.8"/>
<pad name="P$169" x="-45.085" y="2.8575" drill="0.8"/>
<pad name="P$170" x="-45.085" y="-0.9525" drill="0.8"/>
<pad name="P$171" x="-46.355" y="0.9525" drill="0.8"/>
<pad name="P$172" x="-46.355" y="-2.8575" drill="0.8"/>
<pad name="P$173" x="-47.625" y="2.8575" drill="0.8"/>
<pad name="P$174" x="-47.625" y="-0.9525" drill="0.8"/>
<pad name="P$175" x="-48.895" y="0.9525" drill="0.8"/>
<pad name="P$176" x="-48.895" y="-2.8575" drill="0.8"/>
<pad name="P$177" x="-50.165" y="2.8575" drill="0.8"/>
<pad name="P$178" x="-50.165" y="-0.9525" drill="0.8"/>
<pad name="P$179" x="-51.435" y="0.9525" drill="0.8"/>
<pad name="P$180" x="-51.435" y="-2.8575" drill="0.8"/>
<pad name="P$181" x="-52.705" y="2.8575" drill="0.8"/>
<pad name="P$182" x="-52.705" y="-0.9525" drill="0.8"/>
<pad name="P$183" x="-53.975" y="0.9525" drill="0.8"/>
<pad name="P$184" x="-53.975" y="-2.8575" drill="0.8"/>
<pad name="P$185" x="-55.245" y="2.8575" drill="0.8"/>
<pad name="P$186" x="-55.245" y="-0.9525" drill="0.8"/>
<pad name="P$187" x="-56.515" y="0.9525" drill="0.8"/>
<pad name="P$188" x="-56.515" y="-2.8575" drill="0.8"/>
<pad name="P$189" x="-57.785" y="2.8575" drill="0.8"/>
<pad name="P$190" x="-57.785" y="-0.9525" drill="0.8"/>
<pad name="P$191" x="-59.055" y="0.9525" drill="0.8"/>
<pad name="P$192" x="-59.055" y="-2.8575" drill="0.8"/>
<pad name="P$193" x="-60.325" y="2.8575" drill="0.8"/>
<pad name="P$194" x="-60.325" y="-0.9525" drill="0.8"/>
<pad name="P$195" x="-61.595" y="0.9525" drill="0.8"/>
<pad name="P$196" x="-61.595" y="-2.8575" drill="0.8"/>
<pad name="P$197" x="-62.865" y="2.8575" drill="0.8"/>
<pad name="P$198" x="-62.865" y="-0.9525" drill="0.8"/>
<pad name="P$199" x="-64.135" y="0.9525" drill="0.8"/>
<pad name="P$200" x="-64.135" y="-2.8575" drill="0.8"/>
<hole x="67.31" y="0" drill="2.3"/>
<hole x="-67.31" y="0" drill="2.3"/>
<wire x1="-68.6562" y1="3.8481" x2="68.6562" y2="3.8481" width="0.127" layer="21"/>
<wire x1="68.6562" y1="3.8481" x2="68.6562" y2="-3.8481" width="0.127" layer="21"/>
<wire x1="68.6562" y1="-3.8481" x2="-68.6562" y2="-3.8481" width="0.127" layer="21"/>
<wire x1="-68.6562" y1="-3.8481" x2="-68.6562" y2="3.8481" width="0.127" layer="21"/>
<text x="-3.81" y="5.08" size="1.27" layer="25">&gt;NAME</text>
<text x="63.5" y="5.08" size="1.27" layer="21">1</text>
</package>
</packages>
<symbols>
<symbol name="AMIGA-32-BIT-LOCAL-SLOT">
<pin name="!DSACK1" x="129.54" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!HLT" x="127" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="R_W" x="124.46" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!BGACK" x="121.92" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!SBR" x="119.38" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!AVEC" x="116.84" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="EXT90" x="114.3" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!RAMSLOT" x="111.76" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!BOSS" x="109.22" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="FC0" x="106.68" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!STERM" x="104.14" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="FC1" x="101.6" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!BR" x="99.06" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!CBACK" x="96.52" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!BERR" x="93.98" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!EMUL" x="93.98" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!CBREQ" x="91.44" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A8" x="88.9" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A0" x="86.36" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A9" x="83.82" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A1" x="81.28" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A10" x="78.74" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!INT6" x="78.74" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A2" x="76.2" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A11" x="73.66" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A3" x="71.12" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A12" x="68.58" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A4" x="66.04" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A13" x="63.5" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!WAIT" x="63.5" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A5" x="60.96" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A14" x="58.42" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A6" x="55.88" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A15" x="53.34" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A7" x="50.8" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A16" x="48.26" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!SCSI" x="45.72" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!DMAEN" x="48.26" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A24" x="45.72" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A17" x="43.18" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A25" x="40.64" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A18" x="38.1" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A26" x="35.56" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A19" x="33.02" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A27" x="30.48" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A20" x="27.94" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!INT2" x="25.4" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A28" x="25.4" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A21" x="22.86" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A29" x="20.32" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A22" x="17.78" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!DSACK0" x="17.78" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A30" x="15.24" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="A23" x="12.7" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="A31" x="10.16" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!DS" x="7.62" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!ECS" x="5.08" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!CIOUT" x="-5.08" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!DBEN" x="-7.62" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!BG" x="-10.16" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!RMC" x="-12.7" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!CPURST" x="-15.24" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!FPURST" x="-17.78" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="EXTCPU" x="-17.78" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!EBCLR" x="-20.32" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="IPEND" x="-22.86" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!RESET" x="-25.4" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!IPL0" x="-27.94" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="SIZ0" x="-30.48" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!IPL1" x="-33.02" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="FC2" x="-35.56" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="CLK90EXP" x="-38.1" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!IPL2" x="-38.1" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="SIZ1" x="-40.64" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!CIIN" x="-43.18" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!AS" x="-45.72" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!FPUCS" x="-48.26" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="CPUCLKEXP" x="-45.72" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="!OCS" x="-48.26" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D31" x="-50.8" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D15" x="-53.34" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D30" x="-55.88" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D14" x="-58.42" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D29" x="-60.96" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!CBR" x="-60.96" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D13" x="-63.5" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D28" x="-66.04" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D12" x="-68.58" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D27" x="-71.12" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D11" x="-73.66" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D26" x="-76.2" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="!BG30" x="-76.2" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D10" x="-78.74" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D25" x="-81.28" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D9" x="-83.82" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D24" x="-86.36" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D8" x="-88.9" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D16" x="-91.44" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D0" x="-93.98" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D17" x="-96.52" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D1" x="-99.06" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D18" x="-101.6" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D2" x="-104.14" y="15.24" visible="pin" length="middle" rot="R270"/>
<wire x1="-132.08" y1="10.16" x2="-127" y2="10.16" width="0.254" layer="94"/>
<wire x1="-127" y1="10.16" x2="-121.92" y2="10.16" width="0.254" layer="94"/>
<wire x1="-121.92" y1="10.16" x2="-116.84" y2="10.16" width="0.254" layer="94"/>
<wire x1="-116.84" y1="10.16" x2="-111.76" y2="10.16" width="0.254" layer="94"/>
<wire x1="-111.76" y1="10.16" x2="-106.68" y2="10.16" width="0.254" layer="94"/>
<wire x1="-106.68" y1="10.16" x2="-101.6" y2="10.16" width="0.254" layer="94"/>
<wire x1="-101.6" y1="10.16" x2="-96.52" y2="10.16" width="0.254" layer="94"/>
<wire x1="-96.52" y1="10.16" x2="-91.44" y2="10.16" width="0.254" layer="94"/>
<wire x1="-91.44" y1="10.16" x2="-86.36" y2="10.16" width="0.254" layer="94"/>
<wire x1="-86.36" y1="10.16" x2="-81.28" y2="10.16" width="0.254" layer="94"/>
<wire x1="-81.28" y1="10.16" x2="-71.12" y2="10.16" width="0.254" layer="94"/>
<wire x1="-71.12" y1="10.16" x2="-66.04" y2="10.16" width="0.254" layer="94"/>
<wire x1="-66.04" y1="10.16" x2="-55.88" y2="10.16" width="0.254" layer="94"/>
<wire x1="-55.88" y1="10.16" x2="-50.8" y2="10.16" width="0.254" layer="94"/>
<wire x1="-50.8" y1="10.16" x2="-40.64" y2="10.16" width="0.254" layer="94"/>
<wire x1="-40.64" y1="10.16" x2="-35.56" y2="10.16" width="0.254" layer="94"/>
<wire x1="-35.56" y1="10.16" x2="-30.48" y2="10.16" width="0.254" layer="94"/>
<wire x1="-30.48" y1="10.16" x2="-25.4" y2="10.16" width="0.254" layer="94"/>
<wire x1="-25.4" y1="10.16" x2="-20.32" y2="10.16" width="0.254" layer="94"/>
<wire x1="-20.32" y1="10.16" x2="-15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="22.86" y2="10.16" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="27.94" y2="10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="10.16" x2="33.02" y2="10.16" width="0.254" layer="94"/>
<wire x1="33.02" y1="10.16" x2="38.1" y2="10.16" width="0.254" layer="94"/>
<wire x1="38.1" y1="10.16" x2="43.18" y2="10.16" width="0.254" layer="94"/>
<wire x1="43.18" y1="10.16" x2="53.34" y2="10.16" width="0.254" layer="94"/>
<wire x1="53.34" y1="10.16" x2="58.42" y2="10.16" width="0.254" layer="94"/>
<wire x1="58.42" y1="10.16" x2="68.58" y2="10.16" width="0.254" layer="94"/>
<wire x1="68.58" y1="10.16" x2="73.66" y2="10.16" width="0.254" layer="94"/>
<wire x1="73.66" y1="10.16" x2="83.82" y2="10.16" width="0.254" layer="94"/>
<wire x1="83.82" y1="10.16" x2="88.9" y2="10.16" width="0.254" layer="94"/>
<wire x1="88.9" y1="10.16" x2="99.06" y2="10.16" width="0.254" layer="94"/>
<wire x1="99.06" y1="10.16" x2="104.14" y2="10.16" width="0.254" layer="94"/>
<wire x1="104.14" y1="10.16" x2="109.22" y2="10.16" width="0.254" layer="94"/>
<wire x1="109.22" y1="10.16" x2="114.3" y2="10.16" width="0.254" layer="94"/>
<wire x1="114.3" y1="10.16" x2="119.38" y2="10.16" width="0.254" layer="94"/>
<wire x1="119.38" y1="10.16" x2="124.46" y2="10.16" width="0.254" layer="94"/>
<wire x1="124.46" y1="10.16" x2="132.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="132.08" y1="10.16" x2="132.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="132.08" y1="-12.7" x2="127" y2="-12.7" width="0.254" layer="94"/>
<wire x1="127" y1="-12.7" x2="121.92" y2="-12.7" width="0.254" layer="94"/>
<wire x1="121.92" y1="-12.7" x2="116.84" y2="-12.7" width="0.254" layer="94"/>
<wire x1="116.84" y1="-12.7" x2="111.76" y2="-12.7" width="0.254" layer="94"/>
<wire x1="111.76" y1="-12.7" x2="106.68" y2="-12.7" width="0.254" layer="94"/>
<wire x1="106.68" y1="-12.7" x2="101.6" y2="-12.7" width="0.254" layer="94"/>
<wire x1="101.6" y1="-12.7" x2="96.52" y2="-12.7" width="0.254" layer="94"/>
<wire x1="96.52" y1="-12.7" x2="91.44" y2="-12.7" width="0.254" layer="94"/>
<wire x1="91.44" y1="-12.7" x2="86.36" y2="-12.7" width="0.254" layer="94"/>
<wire x1="86.36" y1="-12.7" x2="81.28" y2="-12.7" width="0.254" layer="94"/>
<wire x1="81.28" y1="-12.7" x2="76.2" y2="-12.7" width="0.254" layer="94"/>
<wire x1="76.2" y1="-12.7" x2="71.12" y2="-12.7" width="0.254" layer="94"/>
<wire x1="71.12" y1="-12.7" x2="66.04" y2="-12.7" width="0.254" layer="94"/>
<wire x1="66.04" y1="-12.7" x2="60.96" y2="-12.7" width="0.254" layer="94"/>
<wire x1="60.96" y1="-12.7" x2="55.88" y2="-12.7" width="0.254" layer="94"/>
<wire x1="55.88" y1="-12.7" x2="50.8" y2="-12.7" width="0.254" layer="94"/>
<wire x1="50.8" y1="-12.7" x2="40.64" y2="-12.7" width="0.254" layer="94"/>
<wire x1="40.64" y1="-12.7" x2="35.56" y2="-12.7" width="0.254" layer="94"/>
<wire x1="35.56" y1="-12.7" x2="30.48" y2="-12.7" width="0.254" layer="94"/>
<wire x1="30.48" y1="-12.7" x2="20.32" y2="-12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="-12.7" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="-22.86" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-12.7" x2="-27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-12.7" x2="-33.02" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-33.02" y1="-12.7" x2="-43.18" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-43.18" y1="-12.7" x2="-53.34" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-53.34" y1="-12.7" x2="-58.42" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-58.42" y1="-12.7" x2="-63.5" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-63.5" y1="-12.7" x2="-68.58" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-68.58" y1="-12.7" x2="-73.66" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-73.66" y1="-12.7" x2="-78.74" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-78.74" y1="-12.7" x2="-83.82" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-83.82" y1="-12.7" x2="-88.9" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-88.9" y1="-12.7" x2="-93.98" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-93.98" y1="-12.7" x2="-99.06" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-99.06" y1="-12.7" x2="-104.14" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-104.14" y1="-12.7" x2="-109.22" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-109.22" y1="-12.7" x2="-114.3" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-114.3" y1="-12.7" x2="-119.38" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-119.38" y1="-12.7" x2="-124.46" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-124.46" y1="-12.7" x2="-129.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-129.54" y1="-12.7" x2="-132.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-132.08" y1="-12.7" x2="-132.08" y2="10.16" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.27" layer="104">&gt;NAME</text>
<pin name="D19" x="-106.68" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D3" x="-109.22" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D20" x="-111.76" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D4" x="-114.3" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D21" x="-116.84" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D5" x="-119.38" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D22" x="-121.92" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D6" x="-124.46" y="15.24" visible="pin" length="middle" rot="R270"/>
<pin name="D23" x="-127" y="-17.78" visible="pin" length="middle" rot="R90"/>
<pin name="D7" x="-129.54" y="15.24" visible="pin" length="middle" rot="R270"/>
<wire x1="-127" y1="10.16" x2="-127" y2="12.7" width="0.254" layer="94"/>
<wire x1="-121.92" y1="10.16" x2="-121.92" y2="12.7" width="0.254" layer="94"/>
<wire x1="-116.84" y1="10.16" x2="-116.84" y2="12.7" width="0.254" layer="94"/>
<wire x1="-119.38" y1="-15.24" x2="-119.38" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-124.46" y1="-15.24" x2="-124.46" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-129.54" y1="-15.24" x2="-129.54" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-86.36" y1="10.16" x2="-86.36" y2="12.7" width="0.254" layer="94"/>
<wire x1="-88.9" y1="-15.24" x2="-88.9" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-81.28" y1="10.16" x2="-81.28" y2="12.7" width="0.254" layer="94"/>
<wire x1="-73.66" y1="-15.24" x2="-73.66" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-71.12" y1="10.16" x2="-71.12" y2="12.7" width="0.254" layer="94"/>
<wire x1="-66.04" y1="10.16" x2="-66.04" y2="12.7" width="0.254" layer="94"/>
<wire x1="-55.88" y1="10.16" x2="-55.88" y2="12.7" width="0.254" layer="94"/>
<wire x1="-50.8" y1="10.16" x2="-50.8" y2="12.7" width="0.254" layer="94"/>
<wire x1="-40.64" y1="10.16" x2="-40.64" y2="12.7" width="0.254" layer="94"/>
<wire x1="-30.48" y1="10.16" x2="-30.48" y2="12.7" width="0.254" layer="94"/>
<wire x1="-58.42" y1="-15.24" x2="-58.42" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-53.34" y1="-15.24" x2="-53.34" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-43.18" y1="-15.24" x2="-43.18" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-33.02" y1="-15.24" x2="-33.02" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-25.4" y1="10.16" x2="-25.4" y2="12.7" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-15.24" x2="-27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-20.32" y1="10.16" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="10.16" x2="22.86" y2="12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="-15.24" x2="20.32" y2="-12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="10.16" x2="27.94" y2="12.7" width="0.254" layer="94"/>
<wire x1="38.1" y1="10.16" x2="38.1" y2="12.7" width="0.254" layer="94"/>
<wire x1="43.18" y1="10.16" x2="43.18" y2="12.7" width="0.254" layer="94"/>
<wire x1="53.34" y1="10.16" x2="53.34" y2="12.7" width="0.254" layer="94"/>
<wire x1="58.42" y1="10.16" x2="58.42" y2="12.7" width="0.254" layer="94"/>
<wire x1="68.58" y1="10.16" x2="68.58" y2="12.7" width="0.254" layer="94"/>
<wire x1="35.56" y1="-15.24" x2="35.56" y2="-12.7" width="0.254" layer="94"/>
<wire x1="50.8" y1="-15.24" x2="50.8" y2="-12.7" width="0.254" layer="94"/>
<wire x1="66.04" y1="-15.24" x2="66.04" y2="-12.7" width="0.254" layer="94"/>
<wire x1="124.46" y1="10.16" x2="124.46" y2="12.7" width="0.254" layer="94"/>
<wire x1="119.38" y1="10.16" x2="119.38" y2="12.7" width="0.254" layer="94"/>
<wire x1="116.84" y1="-15.24" x2="116.84" y2="-12.7" width="0.254" layer="94"/>
<wire x1="121.92" y1="-15.24" x2="121.92" y2="-12.7" width="0.254" layer="94"/>
<wire x1="127" y1="-15.24" x2="127" y2="-12.7" width="0.254" layer="94"/>
<wire x1="81.28" y1="-15.24" x2="81.28" y2="-12.7" width="0.254" layer="94"/>
<wire x1="83.82" y1="10.16" x2="83.82" y2="12.7" width="0.254" layer="94"/>
<wire x1="88.9" y1="10.16" x2="88.9" y2="12.7" width="0.254" layer="94"/>
<wire x1="73.66" y1="10.16" x2="73.66" y2="12.7" width="0.254" layer="94"/>
<text x="23.622" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="28.702" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="38.862" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="43.942" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="54.102" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="59.182" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="69.342" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="74.422" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="84.582" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="89.662" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="120.142" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="125.222" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-19.558" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-24.638" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-29.718" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-39.878" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-50.038" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-55.118" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-65.278" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-70.358" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-80.518" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-85.598" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-116.078" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-121.158" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-126.238" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<text x="127.508" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="122.428" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="117.348" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="81.788" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="66.548" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="51.308" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="36.068" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="20.828" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-27.432" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-32.512" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-42.672" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-52.832" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-57.912" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-73.152" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-88.392" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-118.872" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-123.952" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<text x="-129.032" y="-10.16" size="1.4224" layer="97" rot="R90">GND</text>
<wire x1="-35.56" y1="10.16" x2="-35.56" y2="12.7" width="0.254" layer="94"/>
<wire x1="-111.76" y1="10.16" x2="-111.76" y2="12.7" width="0.254" layer="94"/>
<wire x1="-106.68" y1="10.16" x2="-106.68" y2="12.7" width="0.254" layer="94"/>
<wire x1="-101.6" y1="10.16" x2="-101.6" y2="12.7" width="0.254" layer="94"/>
<wire x1="-96.52" y1="10.16" x2="-96.52" y2="12.7" width="0.254" layer="94"/>
<wire x1="-114.3" y1="-15.24" x2="-114.3" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-109.22" y1="-15.24" x2="-109.22" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-104.14" y1="-15.24" x2="-104.14" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-99.06" y1="-15.24" x2="-99.06" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-15.24" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="-15.24" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="114.3" y1="10.16" x2="114.3" y2="12.7" width="0.254" layer="94"/>
<wire x1="109.22" y1="10.16" x2="109.22" y2="12.7" width="0.254" layer="94"/>
<wire x1="104.14" y1="10.16" x2="104.14" y2="12.7" width="0.254" layer="94"/>
<wire x1="99.06" y1="10.16" x2="99.06" y2="12.7" width="0.254" layer="94"/>
<text x="115.062" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="109.982" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="104.902" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="99.822" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="13.462" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="8.382" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-4.318" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-9.398" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-110.998" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-105.918" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-100.838" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-95.758" y="4.064" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-113.792" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-108.712" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-103.632" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-98.552" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-12.192" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-7.112" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="10.668" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="5.588" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="97.028" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="102.108" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="107.188" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="112.268" y="-10.16" size="1.4224" layer="97" rot="R90">+5V</text>
<text x="-34.798" y="4.064" size="1.4224" layer="97" rot="R90">GND</text>
<wire x1="96.52" y1="-15.24" x2="96.52" y2="-12.7" width="0.254" layer="94"/>
<wire x1="101.6" y1="-15.24" x2="101.6" y2="-12.7" width="0.254" layer="94"/>
<wire x1="106.68" y1="-15.24" x2="106.68" y2="-12.7" width="0.254" layer="94"/>
<wire x1="111.76" y1="-15.24" x2="111.76" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-91.44" y1="10.16" x2="-91.44" y2="12.7" width="0.254" layer="94"/>
<wire x1="-93.98" y1="-15.24" x2="-93.98" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-83.82" y1="-15.24" x2="-83.82" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-78.74" y1="-15.24" x2="-78.74" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-68.58" y1="-15.24" x2="-68.58" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-63.5" y1="-15.24" x2="-63.5" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-15.24" x2="-22.86" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="10.16" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="33.02" y1="10.16" x2="33.02" y2="12.7" width="0.254" layer="94"/>
<wire x1="30.48" y1="-15.24" x2="30.48" y2="-12.7" width="0.254" layer="94"/>
<wire x1="40.64" y1="-15.24" x2="40.64" y2="-12.7" width="0.254" layer="94"/>
<wire x1="60.96" y1="-15.24" x2="60.96" y2="-12.7" width="0.254" layer="94"/>
<wire x1="55.88" y1="-15.24" x2="55.88" y2="-12.7" width="0.254" layer="94"/>
<wire x1="71.12" y1="-15.24" x2="71.12" y2="-12.7" width="0.254" layer="94"/>
<wire x1="76.2" y1="-15.24" x2="76.2" y2="-12.7" width="0.254" layer="94"/>
<wire x1="86.36" y1="-15.24" x2="86.36" y2="-12.7" width="0.254" layer="94"/>
<wire x1="91.44" y1="-15.24" x2="91.44" y2="-12.7" width="0.254" layer="94"/>
<text x="91.948" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="86.868" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="76.708" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="71.628" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="61.468" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="56.388" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="41.148" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="30.988" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="33.528" y="5.08" size="1.4224" layer="97" rot="R90">NC</text>
<text x="15.748" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-14.732" y="5.08" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-22.352" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-62.992" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-68.072" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-78.232" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-83.312" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-93.472" y="-10.16" size="1.4224" layer="97" rot="R90">NC</text>
<text x="-90.932" y="5.08" size="1.4224" layer="97" rot="R90">NC</text>
</symbol>
<symbol name="1VCC-1GND">
<description>Power supply for the Chip</description>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<pin name="GND1" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC1" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<wire x1="-5.08" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AMIGA-32-BIT-LOCAL-SLOT" prefix="X">
<gates>
<gate name="G$1" symbol="AMIGA-32-BIT-LOCAL-SLOT" x="2.54" y="0"/>
<gate name="G$2" symbol="1VCC-1GND" x="-149.86" y="0" addlevel="always"/>
</gates>
<devices>
<device name="-MAINBOARD" package="KEL200-F">
<connects>
<connect gate="G$1" pin="!AS" pad="P$135"/>
<connect gate="G$1" pin="!AVEC" pad="P$10"/>
<connect gate="G$1" pin="!BERR" pad="P$31"/>
<connect gate="G$1" pin="!BG" pad="P$107"/>
<connect gate="G$1" pin="!BG30" pad="P$160"/>
<connect gate="G$1" pin="!BGACK" pad="P$6"/>
<connect gate="G$1" pin="!BOSS" pad="P$19"/>
<connect gate="G$1" pin="!BR" pad="P$27"/>
<connect gate="G$1" pin="!CBACK" pad="P$26"/>
<connect gate="G$1" pin="!CBR" pad="P$148"/>
<connect gate="G$1" pin="!CBREQ" pad="P$30"/>
<connect gate="G$1" pin="!CIIN" pad="P$130"/>
<connect gate="G$1" pin="!CIOUT" pad="P$103"/>
<connect gate="G$1" pin="!CPURST" pad="P$111"/>
<connect gate="G$1" pin="!DBEN" pad="P$102"/>
<connect gate="G$1" pin="!DMAEN" pad="P$68"/>
<connect gate="G$1" pin="!DS" pad="P$99"/>
<connect gate="G$1" pin="!DSACK0" pad="P$92"/>
<connect gate="G$1" pin="!DSACK1" pad="P$3"/>
<connect gate="G$1" pin="!EBCLR" pad="P$115"/>
<connect gate="G$1" pin="!ECS" pad="P$98"/>
<connect gate="G$1" pin="!EMUL" pad="P$32"/>
<connect gate="G$1" pin="!FPUCS" pad="P$133"/>
<connect gate="G$1" pin="!FPURST" pad="P$109"/>
<connect gate="G$1" pin="!HLT" pad="P$2"/>
<connect gate="G$1" pin="!INT2" pad="P$81"/>
<connect gate="G$1" pin="!INT6" pad="P$44"/>
<connect gate="G$1" pin="!IPL0" pad="P$118"/>
<connect gate="G$1" pin="!IPL1" pad="P$122"/>
<connect gate="G$1" pin="!IPL2" pad="P$126"/>
<connect gate="G$1" pin="!OCS" pad="P$134"/>
<connect gate="G$1" pin="!RAMSLOT" pad="P$14"/>
<connect gate="G$1" pin="!RESET" pad="P$119"/>
<connect gate="G$1" pin="!RMC" pad="P$106"/>
<connect gate="G$1" pin="!SBR" pad="P$11"/>
<connect gate="G$1" pin="!SCSI" pad="P$65"/>
<connect gate="G$1" pin="!STERM" pad="P$23"/>
<connect gate="G$1" pin="!WAIT" pad="P$56"/>
<connect gate="G$1" pin="A0" pad="P$34"/>
<connect gate="G$1" pin="A1" pad="P$38"/>
<connect gate="G$1" pin="A10" pad="P$43"/>
<connect gate="G$1" pin="A11" pad="P$47"/>
<connect gate="G$1" pin="A12" pad="P$51"/>
<connect gate="G$1" pin="A13" pad="P$55"/>
<connect gate="G$1" pin="A14" pad="P$59"/>
<connect gate="G$1" pin="A15" pad="P$63"/>
<connect gate="G$1" pin="A16" pad="P$67"/>
<connect gate="G$1" pin="A17" pad="P$71"/>
<connect gate="G$1" pin="A18" pad="P$75"/>
<connect gate="G$1" pin="A19" pad="P$79"/>
<connect gate="G$1" pin="A2" pad="P$42"/>
<connect gate="G$1" pin="A20" pad="P$83"/>
<connect gate="G$1" pin="A21" pad="P$87"/>
<connect gate="G$1" pin="A22" pad="P$91"/>
<connect gate="G$1" pin="A23" pad="P$95"/>
<connect gate="G$1" pin="A24" pad="P$66"/>
<connect gate="G$1" pin="A25" pad="P$70"/>
<connect gate="G$1" pin="A26" pad="P$74"/>
<connect gate="G$1" pin="A27" pad="P$78"/>
<connect gate="G$1" pin="A28" pad="P$82"/>
<connect gate="G$1" pin="A29" pad="P$86"/>
<connect gate="G$1" pin="A3" pad="P$46"/>
<connect gate="G$1" pin="A30" pad="P$90"/>
<connect gate="G$1" pin="A31" pad="P$94"/>
<connect gate="G$1" pin="A4" pad="P$50"/>
<connect gate="G$1" pin="A5" pad="P$54"/>
<connect gate="G$1" pin="A6" pad="P$58"/>
<connect gate="G$1" pin="A7" pad="P$62"/>
<connect gate="G$1" pin="A8" pad="P$35"/>
<connect gate="G$1" pin="A9" pad="P$39"/>
<connect gate="G$1" pin="CLK90EXP" pad="P$125"/>
<connect gate="G$1" pin="CPUCLKEXP" pad="P$136"/>
<connect gate="G$1" pin="D0" pad="P$170"/>
<connect gate="G$1" pin="D1" pad="P$174"/>
<connect gate="G$1" pin="D10" pad="P$158"/>
<connect gate="G$1" pin="D11" pad="P$154"/>
<connect gate="G$1" pin="D12" pad="P$150"/>
<connect gate="G$1" pin="D13" pad="P$146"/>
<connect gate="G$1" pin="D14" pad="P$142"/>
<connect gate="G$1" pin="D15" pad="P$138"/>
<connect gate="G$1" pin="D16" pad="P$171"/>
<connect gate="G$1" pin="D17" pad="P$175"/>
<connect gate="G$1" pin="D18" pad="P$179"/>
<connect gate="G$1" pin="D19" pad="P$183"/>
<connect gate="G$1" pin="D2" pad="P$178"/>
<connect gate="G$1" pin="D20" pad="P$187"/>
<connect gate="G$1" pin="D21" pad="P$191"/>
<connect gate="G$1" pin="D22" pad="P$195"/>
<connect gate="G$1" pin="D23" pad="P$199"/>
<connect gate="G$1" pin="D24" pad="P$167"/>
<connect gate="G$1" pin="D25" pad="P$163"/>
<connect gate="G$1" pin="D26" pad="P$159"/>
<connect gate="G$1" pin="D27" pad="P$155"/>
<connect gate="G$1" pin="D28" pad="P$151"/>
<connect gate="G$1" pin="D29" pad="P$147"/>
<connect gate="G$1" pin="D3" pad="P$182"/>
<connect gate="G$1" pin="D30" pad="P$143"/>
<connect gate="G$1" pin="D31" pad="P$139"/>
<connect gate="G$1" pin="D4" pad="P$186"/>
<connect gate="G$1" pin="D5" pad="P$190"/>
<connect gate="G$1" pin="D6" pad="P$194"/>
<connect gate="G$1" pin="D7" pad="P$198"/>
<connect gate="G$1" pin="D8" pad="P$166"/>
<connect gate="G$1" pin="D9" pad="P$162"/>
<connect gate="G$1" pin="EXT90" pad="P$15"/>
<connect gate="G$1" pin="EXTCPU" pad="P$110"/>
<connect gate="G$1" pin="FC0" pad="P$18"/>
<connect gate="G$1" pin="FC1" pad="P$22"/>
<connect gate="G$1" pin="FC2" pad="P$127"/>
<connect gate="G$1" pin="IPEND" pad="P$114"/>
<connect gate="G$1" pin="R_W" pad="P$7"/>
<connect gate="G$1" pin="SIZ0" pad="P$123"/>
<connect gate="G$1" pin="SIZ1" pad="P$131"/>
<connect gate="G$2" pin="GND1" pad="P$1 P$4 P$5 P$8 P$9 P$12 P$36 P$37 P$40 P$48 P$49 P$52 P$60 P$61 P$64 P$72 P$73 P$76 P$84 P$85 P$88 P$116 P$117 P$120 P$121 P$124 P$128 P$129 P$132 P$137 P$140 P$141 P$144 P$152 P$153 P$156 P$164 P$165 P$168 P$189 P$192 P$193 P$196 P$197 P$200"/>
<connect gate="G$2" pin="VCC1" pad="P$13 P$16 P$17 P$20 P$21 P$24 P$25 P$28 P$93 P$96 P$97 P$100 P$101 P$104 P$105 P$108 P$173 P$176 P$177 P$180 P$181 P$184 P$185 P$188"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CPUCARD" package="KEL200-M">
<connects>
<connect gate="G$1" pin="!AS" pad="P$135"/>
<connect gate="G$1" pin="!AVEC" pad="P$10"/>
<connect gate="G$1" pin="!BERR" pad="P$31"/>
<connect gate="G$1" pin="!BG" pad="P$107"/>
<connect gate="G$1" pin="!BG30" pad="P$160"/>
<connect gate="G$1" pin="!BGACK" pad="P$6"/>
<connect gate="G$1" pin="!BOSS" pad="P$19"/>
<connect gate="G$1" pin="!BR" pad="P$27"/>
<connect gate="G$1" pin="!CBACK" pad="P$26"/>
<connect gate="G$1" pin="!CBR" pad="P$148"/>
<connect gate="G$1" pin="!CBREQ" pad="P$30"/>
<connect gate="G$1" pin="!CIIN" pad="P$130"/>
<connect gate="G$1" pin="!CIOUT" pad="P$103"/>
<connect gate="G$1" pin="!CPURST" pad="P$111"/>
<connect gate="G$1" pin="!DBEN" pad="P$102"/>
<connect gate="G$1" pin="!DMAEN" pad="P$68"/>
<connect gate="G$1" pin="!DS" pad="P$99"/>
<connect gate="G$1" pin="!DSACK0" pad="P$92"/>
<connect gate="G$1" pin="!DSACK1" pad="P$3"/>
<connect gate="G$1" pin="!EBCLR" pad="P$115"/>
<connect gate="G$1" pin="!ECS" pad="P$98"/>
<connect gate="G$1" pin="!EMUL" pad="P$32"/>
<connect gate="G$1" pin="!FPUCS" pad="P$133"/>
<connect gate="G$1" pin="!FPURST" pad="P$109"/>
<connect gate="G$1" pin="!HLT" pad="P$2"/>
<connect gate="G$1" pin="!INT2" pad="P$81"/>
<connect gate="G$1" pin="!INT6" pad="P$44"/>
<connect gate="G$1" pin="!IPL0" pad="P$118"/>
<connect gate="G$1" pin="!IPL1" pad="P$122"/>
<connect gate="G$1" pin="!IPL2" pad="P$126"/>
<connect gate="G$1" pin="!OCS" pad="P$134"/>
<connect gate="G$1" pin="!RAMSLOT" pad="P$14"/>
<connect gate="G$1" pin="!RESET" pad="P$119"/>
<connect gate="G$1" pin="!RMC" pad="P$106"/>
<connect gate="G$1" pin="!SBR" pad="P$11"/>
<connect gate="G$1" pin="!SCSI" pad="P$65"/>
<connect gate="G$1" pin="!STERM" pad="P$23"/>
<connect gate="G$1" pin="!WAIT" pad="P$56"/>
<connect gate="G$1" pin="A0" pad="P$34"/>
<connect gate="G$1" pin="A1" pad="P$38"/>
<connect gate="G$1" pin="A10" pad="P$43"/>
<connect gate="G$1" pin="A11" pad="P$47"/>
<connect gate="G$1" pin="A12" pad="P$51"/>
<connect gate="G$1" pin="A13" pad="P$55"/>
<connect gate="G$1" pin="A14" pad="P$59"/>
<connect gate="G$1" pin="A15" pad="P$63"/>
<connect gate="G$1" pin="A16" pad="P$67"/>
<connect gate="G$1" pin="A17" pad="P$71"/>
<connect gate="G$1" pin="A18" pad="P$75"/>
<connect gate="G$1" pin="A19" pad="P$79"/>
<connect gate="G$1" pin="A2" pad="P$42"/>
<connect gate="G$1" pin="A20" pad="P$83"/>
<connect gate="G$1" pin="A21" pad="P$87"/>
<connect gate="G$1" pin="A22" pad="P$91"/>
<connect gate="G$1" pin="A23" pad="P$95"/>
<connect gate="G$1" pin="A24" pad="P$66"/>
<connect gate="G$1" pin="A25" pad="P$70"/>
<connect gate="G$1" pin="A26" pad="P$74"/>
<connect gate="G$1" pin="A27" pad="P$78"/>
<connect gate="G$1" pin="A28" pad="P$82"/>
<connect gate="G$1" pin="A29" pad="P$86"/>
<connect gate="G$1" pin="A3" pad="P$46"/>
<connect gate="G$1" pin="A30" pad="P$90"/>
<connect gate="G$1" pin="A31" pad="P$94"/>
<connect gate="G$1" pin="A4" pad="P$50"/>
<connect gate="G$1" pin="A5" pad="P$54"/>
<connect gate="G$1" pin="A6" pad="P$58"/>
<connect gate="G$1" pin="A7" pad="P$62"/>
<connect gate="G$1" pin="A8" pad="P$35"/>
<connect gate="G$1" pin="A9" pad="P$39"/>
<connect gate="G$1" pin="CLK90EXP" pad="P$125"/>
<connect gate="G$1" pin="CPUCLKEXP" pad="P$136"/>
<connect gate="G$1" pin="D0" pad="P$170"/>
<connect gate="G$1" pin="D1" pad="P$174"/>
<connect gate="G$1" pin="D10" pad="P$158"/>
<connect gate="G$1" pin="D11" pad="P$154"/>
<connect gate="G$1" pin="D12" pad="P$150"/>
<connect gate="G$1" pin="D13" pad="P$146"/>
<connect gate="G$1" pin="D14" pad="P$142"/>
<connect gate="G$1" pin="D15" pad="P$138"/>
<connect gate="G$1" pin="D16" pad="P$171"/>
<connect gate="G$1" pin="D17" pad="P$175"/>
<connect gate="G$1" pin="D18" pad="P$179"/>
<connect gate="G$1" pin="D19" pad="P$183"/>
<connect gate="G$1" pin="D2" pad="P$178"/>
<connect gate="G$1" pin="D20" pad="P$187"/>
<connect gate="G$1" pin="D21" pad="P$191"/>
<connect gate="G$1" pin="D22" pad="P$195"/>
<connect gate="G$1" pin="D23" pad="P$199"/>
<connect gate="G$1" pin="D24" pad="P$167"/>
<connect gate="G$1" pin="D25" pad="P$163"/>
<connect gate="G$1" pin="D26" pad="P$159"/>
<connect gate="G$1" pin="D27" pad="P$155"/>
<connect gate="G$1" pin="D28" pad="P$151"/>
<connect gate="G$1" pin="D29" pad="P$147"/>
<connect gate="G$1" pin="D3" pad="P$182"/>
<connect gate="G$1" pin="D30" pad="P$143"/>
<connect gate="G$1" pin="D31" pad="P$139"/>
<connect gate="G$1" pin="D4" pad="P$186"/>
<connect gate="G$1" pin="D5" pad="P$190"/>
<connect gate="G$1" pin="D6" pad="P$194"/>
<connect gate="G$1" pin="D7" pad="P$198"/>
<connect gate="G$1" pin="D8" pad="P$166"/>
<connect gate="G$1" pin="D9" pad="P$162"/>
<connect gate="G$1" pin="EXT90" pad="P$15"/>
<connect gate="G$1" pin="EXTCPU" pad="P$110"/>
<connect gate="G$1" pin="FC0" pad="P$18"/>
<connect gate="G$1" pin="FC1" pad="P$22"/>
<connect gate="G$1" pin="FC2" pad="P$127"/>
<connect gate="G$1" pin="IPEND" pad="P$114"/>
<connect gate="G$1" pin="R_W" pad="P$7"/>
<connect gate="G$1" pin="SIZ0" pad="P$123"/>
<connect gate="G$1" pin="SIZ1" pad="P$131"/>
<connect gate="G$2" pin="GND1" pad="P$1 P$4 P$5 P$8 P$9 P$12 P$36 P$37 P$40 P$48 P$49 P$52 P$60 P$61 P$64 P$72 P$73 P$76 P$84 P$85 P$88 P$116 P$117 P$120 P$121 P$124 P$128 P$129 P$132 P$137 P$140 P$141 P$144 P$152 P$153 P$156 P$164 P$165 P$168 P$189 P$192 P$193 P$196 P$197 P$200"/>
<connect gate="G$2" pin="VCC1" pad="P$13 P$16 P$17 P$20 P$21 P$24 P$25 P$28 P$93 P$96 P$97 P$100 P$101 P$104 P$105 P$108 P$173 P$176 P$177 P$180 P$181 P$184 P$185 P$188"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-ml">
<description>&lt;b&gt;Harting  Connectors&lt;/b&gt;&lt;p&gt;
Low profile connectors, straight&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="ML20">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<wire x1="-15.24" y1="3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="15.24" y2="3.175" width="0.127" layer="21"/>
<wire x1="-15.24" y1="3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="16.51" y1="-4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="-16.51" y1="4.445" x2="-16.51" y2="-4.445" width="0.127" layer="21"/>
<wire x1="15.24" y1="-3.175" x2="10.922" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-2.413" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-6.858" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-2.413" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.032" y1="-3.175" x2="2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.699" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="13.97" y1="4.445" x2="13.97" y2="4.699" width="0.127" layer="21"/>
<wire x1="15.24" y1="4.445" x2="16.51" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="-0.635" y2="4.699" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.699" x2="0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="4.445" x2="13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-0.635" y1="4.699" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-15.24" y2="4.699" width="0.127" layer="21"/>
<wire x1="-15.24" y1="4.699" x2="-15.24" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.699" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-0.635" y2="4.445" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="2.032" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="9.398" y2="-3.429" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.175" x2="2.032" y2="-3.175" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="10.922" y2="-3.429" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.175" x2="9.398" y2="-3.175" width="0.127" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="11.811" y2="-4.445" width="0.127" layer="21"/>
<wire x1="11.43" y1="-3.937" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="2.032" y1="-3.429" x2="2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="-3.429" x2="15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="15.494" y1="3.429" x2="-15.494" y2="3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="3.429" x2="-15.494" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-15.494" y1="-3.429" x2="-8.382" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-3.175" x2="-2.032" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-3.429" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.429" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="9.398" y1="-3.937" x2="8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.429" x2="10.922" y2="-3.937" width="0.127" layer="21"/>
<wire x1="10.922" y1="-3.937" x2="9.398" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-16.51" y1="-4.445" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-12.192" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-12.192" y1="-4.318" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-10.668" y2="-4.318" width="0.127" layer="21"/>
<wire x1="-10.668" y1="-4.445" x2="-9.271" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.429" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.429" x2="-2.032" y2="-3.429" width="0.0508" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-8.382" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.175" x2="-15.24" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-6.858" y2="-3.429" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.175" x2="-8.382" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-6.858" y1="-3.937" x2="-8.382" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-8.382" y1="-3.937" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-9.271" y1="-4.445" x2="-8.89" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-5.969" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-5.969" y1="-4.445" x2="-2.032" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-6.35" y1="-3.937" x2="-6.858" y2="-3.937" width="0.127" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="0.9144" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="0.9144" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="0.9144" shape="octagon"/>
<text x="-16.51" y="5.08" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-13.97" y="-1.905" size="1.27" layer="21" ratio="10">1</text>
<text x="-13.97" y="0.635" size="1.27" layer="21" ratio="10">2</text>
<text x="-1.016" y="-4.064" size="1.27" layer="21" ratio="10">20</text>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="20P">
<wire x1="3.81" y1="-15.24" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="-3.81" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="12.7" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="12.7" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="-1.27" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-10.16" x2="-1.27" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-12.7" x2="-1.27" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="7.62" x2="2.54" y2="7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="10.16" x2="2.54" y2="10.16" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="-1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="10.16" x2="-1.27" y2="10.16" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="13.462" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="13" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="15" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="17" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="19" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="-7.62" y="-12.7" visible="pad" length="middle" direction="pas"/>
<pin name="4" x="-7.62" y="-10.16" visible="pad" length="middle" direction="pas"/>
<pin name="6" x="-7.62" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="8" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas"/>
<pin name="10" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas"/>
<pin name="12" x="-7.62" y="0" visible="pad" length="middle" direction="pas"/>
<pin name="14" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas"/>
<pin name="16" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas"/>
<pin name="18" x="-7.62" y="7.62" visible="pad" length="middle" direction="pas"/>
<pin name="20" x="-7.62" y="10.16" visible="pad" length="middle" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ML20" prefix="SV" uservalue="yes">
<description>&lt;b&gt;HARTING&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="20P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ML20">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="10" pad="10"/>
<connect gate="1" pin="11" pad="11"/>
<connect gate="1" pin="12" pad="12"/>
<connect gate="1" pin="13" pad="13"/>
<connect gate="1" pin="14" pad="14"/>
<connect gate="1" pin="15" pad="15"/>
<connect gate="1" pin="16" pad="16"/>
<connect gate="1" pin="17" pad="17"/>
<connect gate="1" pin="18" pad="18"/>
<connect gate="1" pin="19" pad="19"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="20" pad="20"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
<connect gate="1" pin="7" pad="7"/>
<connect gate="1" pin="8" pad="8"/>
<connect gate="1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="EXPANSION" library="Amiga" deviceset="AMIGA-32-BIT-LOCAL-SLOT" device="-CPUCARD"/>
<part name="FRAME1" library="frames" deviceset="A3L-LOC" device=""/>
<part name="MAINBOARD" library="Amiga" deviceset="AMIGA-32-BIT-LOCAL-SLOT" device="-MAINBOARD" value="AMIGA-32-BIT-LOCAL-SLOT-MAINBOARD"/>
<part name="X1" library="con-ml" deviceset="ML20" device=""/>
<part name="GND20" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND21" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND22" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND23" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X2" library="con-ml" deviceset="ML20" device=""/>
<part name="GND1" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND2" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND3" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND4" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X3" library="con-ml" deviceset="ML20" device=""/>
<part name="GND5" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND6" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND7" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND8" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X4" library="con-ml" deviceset="ML20" device=""/>
<part name="GND9" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND10" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND11" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND12" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND14" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND15" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND16" library="SUPPLY1" deviceset="GND" device=""/>
<part name="X6" library="con-ml" deviceset="ML20" device=""/>
<part name="GND17" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND18" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND19" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND24" library="SUPPLY1" deviceset="GND" device=""/>
<part name="GND25" library="SUPPLY1" deviceset="GND" device=""/>
<part name="!BOSS" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!SBR" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!RAM" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!EMUL" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!DMAEN" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!SCSI" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!DBEN" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!RMC" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!IPEND" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!OCS" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!CBR" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!FPUCS" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!EBCLR" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!FPURST" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!CPURST" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!CIOUT" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="!BG" library="pinhead" deviceset="PINHD-1X1" device=""/>
<part name="X5" library="con-ml" deviceset="ML20" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="EXPANSION" gate="G$1" x="139.7" y="142.24"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="MAINBOARD" gate="G$1" x="142.24" y="215.9"/>
<instance part="X1" gate="1" x="321.31" y="223.52"/>
<instance part="GND20" gate="1" x="308.61" y="233.68" rot="R270"/>
<instance part="GND21" gate="1" x="308.61" y="210.82" rot="R270"/>
<instance part="GND22" gate="1" x="334.01" y="210.82" rot="R90"/>
<instance part="GND23" gate="1" x="334.01" y="233.68" rot="R90"/>
<instance part="X2" gate="1" x="321.31" y="190.5"/>
<instance part="GND1" gate="1" x="308.61" y="200.66" rot="R270"/>
<instance part="GND2" gate="1" x="308.61" y="177.8" rot="R270"/>
<instance part="GND3" gate="1" x="334.01" y="177.8" rot="R90"/>
<instance part="GND4" gate="1" x="334.01" y="200.66" rot="R90"/>
<instance part="X3" gate="1" x="321.31" y="157.48"/>
<instance part="GND5" gate="1" x="308.61" y="167.64" rot="R270"/>
<instance part="GND6" gate="1" x="308.61" y="144.78" rot="R270"/>
<instance part="GND7" gate="1" x="334.01" y="144.78" rot="R90"/>
<instance part="GND8" gate="1" x="334.01" y="167.64" rot="R90"/>
<instance part="X4" gate="1" x="321.31" y="121.92"/>
<instance part="GND9" gate="1" x="308.61" y="132.08" rot="R270"/>
<instance part="GND10" gate="1" x="308.61" y="109.22" rot="R270"/>
<instance part="GND11" gate="1" x="334.01" y="109.22" rot="R90"/>
<instance part="GND12" gate="1" x="334.01" y="132.08" rot="R90"/>
<instance part="GND14" gate="1" x="308.61" y="76.2" rot="R270"/>
<instance part="GND15" gate="1" x="334.01" y="76.2" rot="R90"/>
<instance part="GND16" gate="1" x="334.01" y="99.06" rot="R90"/>
<instance part="X6" gate="1" x="321.31" y="55.88"/>
<instance part="GND17" gate="1" x="308.61" y="66.04" rot="R270"/>
<instance part="GND18" gate="1" x="308.61" y="43.18" rot="R270"/>
<instance part="GND19" gate="1" x="334.01" y="43.18" rot="R90"/>
<instance part="GND24" gate="1" x="334.01" y="66.04" rot="R90"/>
<instance part="GND25" gate="1" x="308.61" y="99.06" rot="R270"/>
<instance part="!BOSS" gate="G$1" x="251.46" y="102.87"/>
<instance part="!SBR" gate="G$1" x="261.62" y="121.92"/>
<instance part="!RAM" gate="G$1" x="254" y="158.75"/>
<instance part="!EMUL" gate="G$1" x="236.22" y="161.29"/>
<instance part="!DMAEN" gate="G$1" x="190.5" y="160.02"/>
<instance part="!SCSI" gate="G$1" x="187.96" y="123.19"/>
<instance part="!DBEN" gate="G$1" x="134.62" y="158.75"/>
<instance part="!RMC" gate="G$1" x="129.54" y="166.37"/>
<instance part="!IPEND" gate="G$1" x="119.38" y="158.75"/>
<instance part="!OCS" gate="G$1" x="93.98" y="158.75"/>
<instance part="!CBR" gate="G$1" x="81.28" y="158.75"/>
<instance part="!FPUCS" gate="G$1" x="93.98" y="123.19"/>
<instance part="!EBCLR" gate="G$1" x="119.38" y="116.84" rot="R270"/>
<instance part="!FPURST" gate="G$1" x="121.92" y="116.84" rot="R270"/>
<instance part="!CPURST" gate="G$1" x="127" y="123.19"/>
<instance part="MAINBOARD" gate="G$2" x="66.04" y="76.2"/>
<instance part="EXPANSION" gate="G$2" x="86.36" y="76.2"/>
<instance part="!CIOUT" gate="G$1" x="137.16" y="118.11"/>
<instance part="!BG" gate="G$1" x="134.62" y="191.77"/>
<instance part="X5" gate="1" x="321.31" y="88.9"/>
</instances>
<busses>
<bus name="D30_[0..31],A30_[0..31],IPL30_[0..2],FC30_[0..2],STERM30,BR30,BG30,BGACK30,AS30,DS30,DSAC[0..1],RESET30,HALT30,CLK30,R/W30,BERR30,SIZ30_[0..1]">
<segment>
<wire x1="8.89" y1="113.03" x2="274.32" y2="113.03" width="0.762" layer="92"/>
<wire x1="274.32" y1="113.03" x2="276.86" y2="115.57" width="0.762" layer="92"/>
<wire x1="276.86" y1="115.57" x2="278.13" y2="115.57" width="0.762" layer="92"/>
<wire x1="278.13" y1="115.57" x2="278.13" y2="158.75" width="0.762" layer="92"/>
<wire x1="278.13" y1="158.75" x2="269.24" y2="167.64" width="0.762" layer="92"/>
<wire x1="269.24" y1="167.64" x2="8.89" y2="167.64" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="11.43" y1="186.69" x2="276.86" y2="186.69" width="0.762" layer="92"/>
<wire x1="276.86" y1="186.69" x2="279.4" y2="189.23" width="0.762" layer="92"/>
<wire x1="279.4" y1="189.23" x2="280.67" y2="189.23" width="0.762" layer="92"/>
<wire x1="280.67" y1="189.23" x2="280.67" y2="232.41" width="0.762" layer="92"/>
<wire x1="280.67" y1="232.41" x2="271.78" y2="241.3" width="0.762" layer="92"/>
<wire x1="271.78" y1="241.3" x2="11.43" y2="241.3" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="D30_7" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D7"/>
<wire x1="10.16" y1="157.48" x2="10.16" y2="166.37" width="0.1524" layer="91"/>
<wire x1="10.16" y1="166.37" x2="11.43" y2="167.64" width="0.1524" layer="91"/>
<label x="10.16" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D7"/>
<wire x1="12.7" y1="231.14" x2="12.7" y2="240.03" width="0.1524" layer="91"/>
<wire x1="12.7" y1="240.03" x2="13.97" y2="241.3" width="0.1524" layer="91"/>
<label x="12.7" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="213.36" x2="328.93" y2="213.36" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="3"/>
<label x="337.82" y="213.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_6" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D6"/>
<wire x1="15.24" y1="157.48" x2="15.24" y2="166.37" width="0.1524" layer="91"/>
<wire x1="15.24" y1="166.37" x2="16.51" y2="167.64" width="0.1524" layer="91"/>
<label x="15.24" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D6"/>
<wire x1="17.78" y1="231.14" x2="17.78" y2="240.03" width="0.1524" layer="91"/>
<wire x1="17.78" y1="240.03" x2="19.05" y2="241.3" width="0.1524" layer="91"/>
<label x="17.78" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="215.9" x2="328.93" y2="215.9" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="5"/>
<label x="337.82" y="215.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_5" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D5"/>
<wire x1="20.32" y1="157.48" x2="20.32" y2="166.37" width="0.1524" layer="91"/>
<wire x1="20.32" y1="166.37" x2="21.59" y2="167.64" width="0.1524" layer="91"/>
<label x="20.32" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D5"/>
<wire x1="22.86" y1="231.14" x2="22.86" y2="240.03" width="0.1524" layer="91"/>
<wire x1="22.86" y1="240.03" x2="24.13" y2="241.3" width="0.1524" layer="91"/>
<label x="22.86" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="218.44" x2="328.93" y2="218.44" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="7"/>
<label x="337.82" y="218.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_4" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D4"/>
<wire x1="25.4" y1="157.48" x2="25.4" y2="166.37" width="0.1524" layer="91"/>
<wire x1="25.4" y1="166.37" x2="26.67" y2="167.64" width="0.1524" layer="91"/>
<label x="25.4" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D4"/>
<wire x1="27.94" y1="231.14" x2="27.94" y2="240.03" width="0.1524" layer="91"/>
<wire x1="27.94" y1="240.03" x2="29.21" y2="241.3" width="0.1524" layer="91"/>
<label x="27.94" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="220.98" x2="328.93" y2="220.98" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="9"/>
<label x="337.82" y="220.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_3" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D3"/>
<wire x1="30.48" y1="157.48" x2="30.48" y2="166.37" width="0.1524" layer="91"/>
<wire x1="30.48" y1="166.37" x2="31.75" y2="167.64" width="0.1524" layer="91"/>
<label x="30.48" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D3"/>
<wire x1="33.02" y1="231.14" x2="33.02" y2="240.03" width="0.1524" layer="91"/>
<wire x1="33.02" y1="240.03" x2="34.29" y2="241.3" width="0.1524" layer="91"/>
<label x="33.02" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="223.52" x2="328.93" y2="223.52" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="11"/>
<label x="337.82" y="223.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_2" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D2"/>
<wire x1="35.56" y1="157.48" x2="35.56" y2="166.37" width="0.1524" layer="91"/>
<wire x1="35.56" y1="166.37" x2="36.83" y2="167.64" width="0.1524" layer="91"/>
<label x="35.56" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D2"/>
<wire x1="38.1" y1="231.14" x2="38.1" y2="240.03" width="0.1524" layer="91"/>
<wire x1="38.1" y1="240.03" x2="39.37" y2="241.3" width="0.1524" layer="91"/>
<label x="38.1" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="226.06" x2="328.93" y2="226.06" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="13"/>
<label x="337.82" y="226.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_1" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D1"/>
<wire x1="40.64" y1="157.48" x2="40.64" y2="166.37" width="0.1524" layer="91"/>
<wire x1="40.64" y1="166.37" x2="41.91" y2="167.64" width="0.1524" layer="91"/>
<label x="40.64" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D1"/>
<wire x1="43.18" y1="231.14" x2="43.18" y2="240.03" width="0.1524" layer="91"/>
<wire x1="43.18" y1="240.03" x2="44.45" y2="241.3" width="0.1524" layer="91"/>
<label x="43.18" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="228.6" x2="328.93" y2="228.6" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="15"/>
<label x="337.82" y="228.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_0" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D0"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="166.37" width="0.1524" layer="91"/>
<wire x1="45.72" y1="166.37" x2="46.99" y2="167.64" width="0.1524" layer="91"/>
<label x="45.72" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D0"/>
<wire x1="48.26" y1="231.14" x2="48.26" y2="240.03" width="0.1524" layer="91"/>
<wire x1="48.26" y1="240.03" x2="49.53" y2="241.3" width="0.1524" layer="91"/>
<label x="48.26" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="231.14" x2="328.93" y2="231.14" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="17"/>
<label x="337.82" y="231.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_8" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D8"/>
<wire x1="50.8" y1="157.48" x2="50.8" y2="166.37" width="0.1524" layer="91"/>
<wire x1="50.8" y1="166.37" x2="52.07" y2="167.64" width="0.1524" layer="91"/>
<label x="50.8" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D8"/>
<wire x1="53.34" y1="231.14" x2="53.34" y2="240.03" width="0.1524" layer="91"/>
<wire x1="53.34" y1="240.03" x2="54.61" y2="241.3" width="0.1524" layer="91"/>
<label x="53.34" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="198.12" x2="328.93" y2="198.12" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="17"/>
<label x="337.82" y="198.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_9" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D9"/>
<wire x1="55.88" y1="157.48" x2="55.88" y2="166.37" width="0.1524" layer="91"/>
<wire x1="55.88" y1="166.37" x2="57.15" y2="167.64" width="0.1524" layer="91"/>
<label x="55.88" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D9"/>
<wire x1="58.42" y1="231.14" x2="58.42" y2="240.03" width="0.1524" layer="91"/>
<wire x1="58.42" y1="240.03" x2="59.69" y2="241.3" width="0.1524" layer="91"/>
<label x="58.42" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="195.58" x2="328.93" y2="195.58" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="15"/>
<label x="337.82" y="195.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_10" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D10"/>
<wire x1="60.96" y1="157.48" x2="60.96" y2="166.37" width="0.1524" layer="91"/>
<wire x1="60.96" y1="166.37" x2="62.23" y2="167.64" width="0.1524" layer="91"/>
<label x="60.96" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D10"/>
<wire x1="63.5" y1="231.14" x2="63.5" y2="240.03" width="0.1524" layer="91"/>
<wire x1="63.5" y1="240.03" x2="64.77" y2="241.3" width="0.1524" layer="91"/>
<label x="63.5" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="193.04" x2="328.93" y2="193.04" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="13"/>
<label x="337.82" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="BG30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!BG"/>
<wire x1="129.54" y1="124.46" x2="129.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="129.54" y1="114.3" x2="130.81" y2="113.03" width="0.1524" layer="91"/>
<label x="129.54" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!BG"/>
<wire x1="132.08" y1="198.12" x2="132.08" y2="191.77" width="0.1524" layer="91"/>
<wire x1="132.08" y1="191.77" x2="132.08" y2="187.96" width="0.1524" layer="91"/>
<wire x1="132.08" y1="187.96" x2="133.35" y2="186.69" width="0.1524" layer="91"/>
<label x="132.08" y="195.58" size="1.27" layer="95" rot="R270"/>
<pinref part="!BG" gate="G$1" pin="1"/>
<junction x="132.08" y="191.77"/>
</segment>
</net>
<net name="D30_11" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D11"/>
<wire x1="66.04" y1="157.48" x2="66.04" y2="166.37" width="0.1524" layer="91"/>
<wire x1="66.04" y1="166.37" x2="67.31" y2="167.64" width="0.1524" layer="91"/>
<label x="66.04" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D11"/>
<wire x1="68.58" y1="231.14" x2="68.58" y2="240.03" width="0.1524" layer="91"/>
<wire x1="68.58" y1="240.03" x2="69.85" y2="241.3" width="0.1524" layer="91"/>
<label x="68.58" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="190.5" x2="328.93" y2="190.5" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="11"/>
<label x="337.82" y="190.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_12" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D12"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="166.37" width="0.1524" layer="91"/>
<wire x1="71.12" y1="166.37" x2="72.39" y2="167.64" width="0.1524" layer="91"/>
<label x="71.12" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D12"/>
<wire x1="73.66" y1="231.14" x2="73.66" y2="240.03" width="0.1524" layer="91"/>
<wire x1="73.66" y1="240.03" x2="74.93" y2="241.3" width="0.1524" layer="91"/>
<label x="73.66" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="187.96" x2="328.93" y2="187.96" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="9"/>
<label x="337.82" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_13" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D13"/>
<wire x1="76.2" y1="157.48" x2="76.2" y2="166.37" width="0.1524" layer="91"/>
<wire x1="76.2" y1="166.37" x2="77.47" y2="167.64" width="0.1524" layer="91"/>
<label x="76.2" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D13"/>
<wire x1="78.74" y1="231.14" x2="78.74" y2="240.03" width="0.1524" layer="91"/>
<wire x1="78.74" y1="240.03" x2="80.01" y2="241.3" width="0.1524" layer="91"/>
<label x="78.74" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="185.42" x2="328.93" y2="185.42" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="7"/>
<label x="337.82" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_14" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D14"/>
<wire x1="81.28" y1="157.48" x2="81.28" y2="166.37" width="0.1524" layer="91"/>
<wire x1="81.28" y1="166.37" x2="82.55" y2="167.64" width="0.1524" layer="91"/>
<label x="81.28" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D14"/>
<wire x1="83.82" y1="231.14" x2="83.82" y2="240.03" width="0.1524" layer="91"/>
<wire x1="83.82" y1="240.03" x2="85.09" y2="241.3" width="0.1524" layer="91"/>
<label x="83.82" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="182.88" x2="328.93" y2="182.88" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="5"/>
<label x="337.82" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="D30_15" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D15"/>
<wire x1="86.36" y1="157.48" x2="86.36" y2="166.37" width="0.1524" layer="91"/>
<wire x1="86.36" y1="166.37" x2="87.63" y2="167.64" width="0.1524" layer="91"/>
<label x="86.36" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D15"/>
<wire x1="88.9" y1="231.14" x2="88.9" y2="240.03" width="0.1524" layer="91"/>
<wire x1="88.9" y1="240.03" x2="90.17" y2="241.3" width="0.1524" layer="91"/>
<label x="88.9" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="180.34" x2="328.93" y2="180.34" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="3"/>
<label x="337.82" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="IPL30_2" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!IPL2"/>
<wire x1="101.6" y1="157.48" x2="101.6" y2="166.37" width="0.1524" layer="91"/>
<wire x1="101.6" y1="166.37" x2="102.87" y2="167.64" width="0.1524" layer="91"/>
<label x="101.6" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!IPL2"/>
<wire x1="104.14" y1="231.14" x2="104.14" y2="240.03" width="0.1524" layer="91"/>
<wire x1="104.14" y1="240.03" x2="105.41" y2="241.3" width="0.1524" layer="91"/>
<label x="104.14" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="152.4" x2="304.8" y2="152.4" width="0.1524" layer="91"/>
<label x="304.8" y="152.4" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="8"/>
</segment>
</net>
<net name="IPL30_1" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!IPL1"/>
<wire x1="106.68" y1="157.48" x2="106.68" y2="166.37" width="0.1524" layer="91"/>
<wire x1="106.68" y1="166.37" x2="107.95" y2="167.64" width="0.1524" layer="91"/>
<label x="106.68" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!IPL1"/>
<wire x1="109.22" y1="231.14" x2="109.22" y2="240.03" width="0.1524" layer="91"/>
<wire x1="109.22" y1="240.03" x2="110.49" y2="241.3" width="0.1524" layer="91"/>
<label x="109.22" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="154.94" x2="304.8" y2="154.94" width="0.1524" layer="91"/>
<label x="304.8" y="154.94" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="10"/>
</segment>
</net>
<net name="IPL30_0" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!IPL0"/>
<wire x1="111.76" y1="157.48" x2="111.76" y2="166.37" width="0.1524" layer="91"/>
<wire x1="111.76" y1="166.37" x2="113.03" y2="167.64" width="0.1524" layer="91"/>
<label x="111.76" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!IPL0"/>
<wire x1="114.3" y1="231.14" x2="114.3" y2="240.03" width="0.1524" layer="91"/>
<wire x1="114.3" y1="240.03" x2="115.57" y2="241.3" width="0.1524" layer="91"/>
<label x="114.3" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="157.48" x2="304.8" y2="157.48" width="0.1524" layer="91"/>
<label x="304.8" y="157.48" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="12"/>
</segment>
</net>
<net name="A30_31" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A31"/>
<wire x1="149.86" y1="157.48" x2="149.86" y2="166.37" width="0.1524" layer="91"/>
<wire x1="149.86" y1="166.37" x2="151.13" y2="167.64" width="0.1524" layer="91"/>
<label x="149.86" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A31"/>
<wire x1="152.4" y1="231.14" x2="152.4" y2="240.03" width="0.1524" layer="91"/>
<wire x1="152.4" y1="240.03" x2="153.67" y2="241.3" width="0.1524" layer="91"/>
<label x="152.4" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="165.1" x2="304.8" y2="165.1" width="0.1524" layer="91"/>
<label x="304.8" y="165.1" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="18"/>
</segment>
</net>
<net name="A30_30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A30"/>
<wire x1="154.94" y1="157.48" x2="154.94" y2="166.37" width="0.1524" layer="91"/>
<wire x1="154.94" y1="166.37" x2="156.21" y2="167.64" width="0.1524" layer="91"/>
<label x="154.94" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A30"/>
<wire x1="157.48" y1="231.14" x2="157.48" y2="240.03" width="0.1524" layer="91"/>
<wire x1="157.48" y1="240.03" x2="158.75" y2="241.3" width="0.1524" layer="91"/>
<label x="157.48" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="337.82" y1="165.1" x2="328.93" y2="165.1" width="0.1524" layer="91"/>
<label x="337.82" y="165.1" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="17"/>
</segment>
</net>
<net name="A30_29" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A29"/>
<wire x1="160.02" y1="157.48" x2="160.02" y2="166.37" width="0.1524" layer="91"/>
<wire x1="160.02" y1="166.37" x2="161.29" y2="167.64" width="0.1524" layer="91"/>
<label x="160.02" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A29"/>
<wire x1="162.56" y1="231.14" x2="162.56" y2="240.03" width="0.1524" layer="91"/>
<wire x1="162.56" y1="240.03" x2="163.83" y2="241.3" width="0.1524" layer="91"/>
<label x="162.56" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="127" x2="304.8" y2="127" width="0.1524" layer="91"/>
<label x="304.8" y="127" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="16"/>
</segment>
</net>
<net name="A30_28" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A28"/>
<wire x1="165.1" y1="157.48" x2="165.1" y2="166.37" width="0.1524" layer="91"/>
<wire x1="165.1" y1="166.37" x2="166.37" y2="167.64" width="0.1524" layer="91"/>
<label x="165.1" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A28"/>
<wire x1="167.64" y1="231.14" x2="167.64" y2="240.03" width="0.1524" layer="91"/>
<wire x1="167.64" y1="240.03" x2="168.91" y2="241.3" width="0.1524" layer="91"/>
<label x="167.64" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="124.46" x2="304.8" y2="124.46" width="0.1524" layer="91"/>
<label x="304.8" y="124.46" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="14"/>
</segment>
</net>
<net name="A30_27" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A27"/>
<wire x1="170.18" y1="157.48" x2="170.18" y2="166.37" width="0.1524" layer="91"/>
<wire x1="170.18" y1="166.37" x2="171.45" y2="167.64" width="0.1524" layer="91"/>
<label x="170.18" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A27"/>
<wire x1="172.72" y1="231.14" x2="172.72" y2="240.03" width="0.1524" layer="91"/>
<wire x1="172.72" y1="240.03" x2="173.99" y2="241.3" width="0.1524" layer="91"/>
<label x="172.72" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="121.92" x2="304.8" y2="121.92" width="0.1524" layer="91"/>
<label x="304.8" y="121.92" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="12"/>
</segment>
</net>
<net name="A30_26" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A26"/>
<wire x1="175.26" y1="157.48" x2="175.26" y2="166.37" width="0.1524" layer="91"/>
<wire x1="175.26" y1="166.37" x2="176.53" y2="167.64" width="0.1524" layer="91"/>
<label x="175.26" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A26"/>
<wire x1="177.8" y1="231.14" x2="177.8" y2="240.03" width="0.1524" layer="91"/>
<wire x1="177.8" y1="240.03" x2="179.07" y2="241.3" width="0.1524" layer="91"/>
<label x="177.8" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="119.38" x2="304.8" y2="119.38" width="0.1524" layer="91"/>
<label x="304.8" y="119.38" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="10"/>
</segment>
</net>
<net name="A30_25" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A25"/>
<wire x1="180.34" y1="157.48" x2="180.34" y2="166.37" width="0.1524" layer="91"/>
<wire x1="180.34" y1="166.37" x2="181.61" y2="167.64" width="0.1524" layer="91"/>
<label x="180.34" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A25"/>
<wire x1="182.88" y1="231.14" x2="182.88" y2="240.03" width="0.1524" layer="91"/>
<wire x1="182.88" y1="240.03" x2="184.15" y2="241.3" width="0.1524" layer="91"/>
<label x="182.88" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="116.84" x2="304.8" y2="116.84" width="0.1524" layer="91"/>
<label x="304.8" y="116.84" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="8"/>
</segment>
</net>
<net name="A30_24" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A24"/>
<wire x1="185.42" y1="157.48" x2="185.42" y2="166.37" width="0.1524" layer="91"/>
<wire x1="185.42" y1="166.37" x2="186.69" y2="167.64" width="0.1524" layer="91"/>
<label x="185.42" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A24"/>
<wire x1="187.96" y1="231.14" x2="187.96" y2="240.03" width="0.1524" layer="91"/>
<wire x1="187.96" y1="240.03" x2="189.23" y2="241.3" width="0.1524" layer="91"/>
<label x="187.96" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="114.3" x2="304.8" y2="114.3" width="0.1524" layer="91"/>
<label x="304.8" y="114.3" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="6"/>
</segment>
</net>
<net name="A30_7" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A7"/>
<wire x1="190.5" y1="157.48" x2="190.5" y2="166.37" width="0.1524" layer="91"/>
<wire x1="190.5" y1="166.37" x2="191.77" y2="167.64" width="0.1524" layer="91"/>
<label x="190.5" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A7"/>
<wire x1="193.04" y1="231.14" x2="193.04" y2="240.03" width="0.1524" layer="91"/>
<wire x1="193.04" y1="240.03" x2="194.31" y2="241.3" width="0.1524" layer="91"/>
<label x="193.04" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="111.76" x2="304.8" y2="111.76" width="0.1524" layer="91"/>
<label x="304.8" y="111.76" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="4"/>
</segment>
</net>
<net name="A30_6" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A6"/>
<wire x1="195.58" y1="157.48" x2="195.58" y2="166.37" width="0.1524" layer="91"/>
<wire x1="195.58" y1="166.37" x2="196.85" y2="167.64" width="0.1524" layer="91"/>
<label x="195.58" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A6"/>
<wire x1="198.12" y1="231.14" x2="198.12" y2="240.03" width="0.1524" layer="91"/>
<wire x1="198.12" y1="240.03" x2="199.39" y2="241.3" width="0.1524" layer="91"/>
<label x="198.12" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="78.74" x2="304.8" y2="78.74" width="0.1524" layer="91"/>
<label x="304.8" y="78.74" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="4"/>
</segment>
</net>
<net name="A30_5" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A5"/>
<wire x1="200.66" y1="157.48" x2="200.66" y2="166.37" width="0.1524" layer="91"/>
<wire x1="200.66" y1="166.37" x2="201.93" y2="167.64" width="0.1524" layer="91"/>
<label x="200.66" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A5"/>
<wire x1="203.2" y1="231.14" x2="203.2" y2="240.03" width="0.1524" layer="91"/>
<wire x1="203.2" y1="240.03" x2="204.47" y2="241.3" width="0.1524" layer="91"/>
<label x="203.2" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="83.82" x2="304.8" y2="83.82" width="0.1524" layer="91"/>
<label x="304.8" y="83.82" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="8"/>
</segment>
</net>
<net name="A30_4" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A4"/>
<wire x1="205.74" y1="157.48" x2="205.74" y2="166.37" width="0.1524" layer="91"/>
<wire x1="205.74" y1="166.37" x2="207.01" y2="167.64" width="0.1524" layer="91"/>
<label x="205.74" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A4"/>
<wire x1="208.28" y1="231.14" x2="208.28" y2="240.03" width="0.1524" layer="91"/>
<wire x1="208.28" y1="240.03" x2="209.55" y2="241.3" width="0.1524" layer="91"/>
<label x="208.28" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="86.36" x2="304.8" y2="86.36" width="0.1524" layer="91"/>
<label x="304.8" y="86.36" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="10"/>
</segment>
</net>
<net name="A30_3" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A3"/>
<wire x1="210.82" y1="157.48" x2="210.82" y2="166.37" width="0.1524" layer="91"/>
<wire x1="210.82" y1="166.37" x2="212.09" y2="167.64" width="0.1524" layer="91"/>
<label x="210.82" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A3"/>
<wire x1="213.36" y1="231.14" x2="213.36" y2="240.03" width="0.1524" layer="91"/>
<wire x1="213.36" y1="240.03" x2="214.63" y2="241.3" width="0.1524" layer="91"/>
<label x="213.36" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="88.9" x2="304.8" y2="88.9" width="0.1524" layer="91"/>
<label x="304.8" y="88.9" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="12"/>
</segment>
</net>
<net name="A30_2" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A2"/>
<wire x1="215.9" y1="157.48" x2="215.9" y2="166.37" width="0.1524" layer="91"/>
<wire x1="215.9" y1="166.37" x2="217.17" y2="167.64" width="0.1524" layer="91"/>
<label x="215.9" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A2"/>
<wire x1="218.44" y1="231.14" x2="218.44" y2="240.03" width="0.1524" layer="91"/>
<wire x1="218.44" y1="240.03" x2="219.71" y2="241.3" width="0.1524" layer="91"/>
<label x="218.44" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="93.98" x2="304.8" y2="93.98" width="0.1524" layer="91"/>
<label x="304.8" y="93.98" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="16"/>
</segment>
</net>
<net name="A30_1" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A1"/>
<wire x1="220.98" y1="157.48" x2="220.98" y2="166.37" width="0.1524" layer="91"/>
<wire x1="220.98" y1="166.37" x2="222.25" y2="167.64" width="0.1524" layer="91"/>
<label x="220.98" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A1"/>
<wire x1="223.52" y1="231.14" x2="223.52" y2="240.03" width="0.1524" layer="91"/>
<wire x1="223.52" y1="240.03" x2="224.79" y2="241.3" width="0.1524" layer="91"/>
<label x="223.52" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="96.52" x2="304.8" y2="96.52" width="0.1524" layer="91"/>
<label x="304.8" y="96.52" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="18"/>
</segment>
</net>
<net name="A30_0" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A0"/>
<wire x1="226.06" y1="157.48" x2="226.06" y2="166.37" width="0.1524" layer="91"/>
<wire x1="226.06" y1="166.37" x2="227.33" y2="167.64" width="0.1524" layer="91"/>
<label x="226.06" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A0"/>
<wire x1="228.6" y1="231.14" x2="228.6" y2="240.03" width="0.1524" layer="91"/>
<wire x1="228.6" y1="240.03" x2="229.87" y2="241.3" width="0.1524" layer="91"/>
<label x="228.6" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="63.5" x2="304.8" y2="63.5" width="0.1524" layer="91"/>
<label x="304.8" y="63.5" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="18"/>
</segment>
</net>
<net name="FC30_1" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="FC1"/>
<wire x1="241.3" y1="157.48" x2="241.3" y2="166.37" width="0.1524" layer="91"/>
<wire x1="241.3" y1="166.37" x2="242.57" y2="167.64" width="0.1524" layer="91"/>
<label x="241.3" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="FC1"/>
<wire x1="243.84" y1="231.14" x2="243.84" y2="240.03" width="0.1524" layer="91"/>
<wire x1="243.84" y1="240.03" x2="245.11" y2="241.3" width="0.1524" layer="91"/>
<label x="243.84" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="55.88" x2="304.8" y2="55.88" width="0.1524" layer="91"/>
<label x="304.8" y="55.88" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="12"/>
</segment>
</net>
<net name="FC30_0" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="FC0"/>
<wire x1="246.38" y1="157.48" x2="246.38" y2="166.37" width="0.1524" layer="91"/>
<wire x1="246.38" y1="166.37" x2="247.65" y2="167.64" width="0.1524" layer="91"/>
<label x="246.38" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="FC0"/>
<wire x1="248.92" y1="231.14" x2="248.92" y2="240.03" width="0.1524" layer="91"/>
<wire x1="248.92" y1="240.03" x2="250.19" y2="241.3" width="0.1524" layer="91"/>
<label x="248.92" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="53.34" x2="304.8" y2="53.34" width="0.1524" layer="91"/>
<label x="304.8" y="53.34" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="10"/>
</segment>
</net>
<net name="BGACK30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!BGACK"/>
<wire x1="261.62" y1="157.48" x2="261.62" y2="166.37" width="0.1524" layer="91"/>
<wire x1="261.62" y1="166.37" x2="262.89" y2="167.64" width="0.1524" layer="91"/>
<label x="261.62" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!BGACK"/>
<wire x1="264.16" y1="231.14" x2="264.16" y2="240.03" width="0.1524" layer="91"/>
<wire x1="264.16" y1="240.03" x2="265.43" y2="241.3" width="0.1524" layer="91"/>
<label x="264.16" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="48.26" x2="304.8" y2="48.26" width="0.1524" layer="91"/>
<label x="304.8" y="48.26" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="6"/>
</segment>
</net>
<net name="HALT30" class="0">
<segment>
<wire x1="266.7" y1="166.37" x2="267.97" y2="167.64" width="0.1524" layer="91"/>
<label x="266.7" y="158.75" size="1.27" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!HLT"/>
<wire x1="266.7" y1="157.48" x2="266.7" y2="166.37" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="269.24" y1="240.03" x2="270.51" y2="241.3" width="0.1524" layer="91"/>
<label x="269.24" y="232.41" size="1.27" layer="95" rot="R90"/>
<pinref part="MAINBOARD" gate="G$1" pin="!HLT"/>
<wire x1="269.24" y1="231.14" x2="269.24" y2="240.03" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="313.69" y1="45.72" x2="304.8" y2="45.72" width="0.1524" layer="91"/>
<pinref part="X6" gate="1" pin="4"/>
<label x="304.8" y="45.72" size="1.016" layer="95" rot="R180"/>
</segment>
</net>
<net name="R/W30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="R_W"/>
<wire x1="264.16" y1="124.46" x2="264.16" y2="114.3" width="0.1524" layer="91"/>
<wire x1="264.16" y1="114.3" x2="265.43" y2="113.03" width="0.1524" layer="91"/>
<label x="264.16" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="R_W"/>
<wire x1="266.7" y1="198.12" x2="266.7" y2="187.96" width="0.1524" layer="91"/>
<wire x1="266.7" y1="187.96" x2="267.97" y2="186.69" width="0.1524" layer="91"/>
<label x="266.7" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="48.26" x2="337.82" y2="48.26" width="0.1524" layer="91"/>
<label x="337.82" y="48.26" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="5"/>
</segment>
</net>
<net name="STERM30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!STERM"/>
<wire x1="243.84" y1="124.46" x2="243.84" y2="114.3" width="0.1524" layer="91"/>
<wire x1="243.84" y1="114.3" x2="245.11" y2="113.03" width="0.1524" layer="91"/>
<label x="243.84" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!STERM"/>
<wire x1="246.38" y1="198.12" x2="246.38" y2="187.96" width="0.1524" layer="91"/>
<wire x1="246.38" y1="187.96" x2="247.65" y2="186.69" width="0.1524" layer="91"/>
<label x="246.38" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="53.34" x2="337.82" y2="53.34" width="0.1524" layer="91"/>
<label x="337.82" y="53.34" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="9"/>
</segment>
</net>
<net name="BR30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!BR"/>
<wire x1="238.76" y1="124.46" x2="238.76" y2="114.3" width="0.1524" layer="91"/>
<wire x1="238.76" y1="114.3" x2="240.03" y2="113.03" width="0.1524" layer="91"/>
<label x="238.76" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!BR"/>
<wire x1="241.3" y1="198.12" x2="241.3" y2="187.96" width="0.1524" layer="91"/>
<wire x1="241.3" y1="187.96" x2="242.57" y2="186.69" width="0.1524" layer="91"/>
<label x="241.3" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="55.88" x2="337.82" y2="55.88" width="0.1524" layer="91"/>
<label x="337.82" y="55.88" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="11"/>
</segment>
</net>
<net name="BERR30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!BERR"/>
<wire x1="233.68" y1="124.46" x2="233.68" y2="114.3" width="0.1524" layer="91"/>
<wire x1="233.68" y1="114.3" x2="234.95" y2="113.03" width="0.1524" layer="91"/>
<label x="233.68" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!BERR"/>
<wire x1="236.22" y1="198.12" x2="236.22" y2="187.96" width="0.1524" layer="91"/>
<wire x1="236.22" y1="187.96" x2="237.49" y2="186.69" width="0.1524" layer="91"/>
<label x="236.22" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="58.42" x2="337.82" y2="58.42" width="0.1524" layer="91"/>
<label x="337.82" y="58.42" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="13"/>
</segment>
</net>
<net name="A30_8" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A8"/>
<wire x1="228.6" y1="124.46" x2="228.6" y2="114.3" width="0.1524" layer="91"/>
<wire x1="228.6" y1="114.3" x2="229.87" y2="113.03" width="0.1524" layer="91"/>
<label x="228.6" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A8"/>
<wire x1="231.14" y1="198.12" x2="231.14" y2="187.96" width="0.1524" layer="91"/>
<wire x1="231.14" y1="187.96" x2="232.41" y2="186.69" width="0.1524" layer="91"/>
<label x="231.14" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="60.96" x2="337.82" y2="60.96" width="0.1524" layer="91"/>
<label x="337.82" y="60.96" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="15"/>
</segment>
</net>
<net name="A30_9" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A9"/>
<wire x1="223.52" y1="124.46" x2="223.52" y2="114.3" width="0.1524" layer="91"/>
<wire x1="223.52" y1="114.3" x2="224.79" y2="113.03" width="0.1524" layer="91"/>
<label x="223.52" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A9"/>
<wire x1="226.06" y1="198.12" x2="226.06" y2="187.96" width="0.1524" layer="91"/>
<wire x1="226.06" y1="187.96" x2="227.33" y2="186.69" width="0.1524" layer="91"/>
<label x="226.06" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="63.5" x2="337.82" y2="63.5" width="0.1524" layer="91"/>
<label x="337.82" y="63.5" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="17"/>
</segment>
</net>
<net name="A30_10" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A10"/>
<wire x1="218.44" y1="124.46" x2="218.44" y2="114.3" width="0.1524" layer="91"/>
<wire x1="218.44" y1="114.3" x2="219.71" y2="113.03" width="0.1524" layer="91"/>
<label x="218.44" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A10"/>
<wire x1="220.98" y1="198.12" x2="220.98" y2="187.96" width="0.1524" layer="91"/>
<wire x1="220.98" y1="187.96" x2="222.25" y2="186.69" width="0.1524" layer="91"/>
<label x="220.98" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="96.52" x2="337.82" y2="96.52" width="0.1524" layer="91"/>
<label x="337.82" y="96.52" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="17"/>
</segment>
</net>
<net name="A30_11" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A11"/>
<wire x1="213.36" y1="124.46" x2="213.36" y2="114.3" width="0.1524" layer="91"/>
<wire x1="213.36" y1="114.3" x2="214.63" y2="113.03" width="0.1524" layer="91"/>
<label x="213.36" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A11"/>
<wire x1="215.9" y1="198.12" x2="215.9" y2="187.96" width="0.1524" layer="91"/>
<wire x1="215.9" y1="187.96" x2="217.17" y2="186.69" width="0.1524" layer="91"/>
<label x="215.9" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="93.98" x2="337.82" y2="93.98" width="0.1524" layer="91"/>
<label x="337.82" y="93.98" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="15"/>
</segment>
</net>
<net name="A30_12" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A12"/>
<wire x1="208.28" y1="124.46" x2="208.28" y2="114.3" width="0.1524" layer="91"/>
<wire x1="208.28" y1="114.3" x2="209.55" y2="113.03" width="0.1524" layer="91"/>
<label x="208.28" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A12"/>
<wire x1="210.82" y1="198.12" x2="210.82" y2="187.96" width="0.1524" layer="91"/>
<wire x1="210.82" y1="187.96" x2="212.09" y2="186.69" width="0.1524" layer="91"/>
<label x="210.82" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="91.44" x2="337.82" y2="91.44" width="0.1524" layer="91"/>
<label x="337.82" y="91.44" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="13"/>
</segment>
</net>
<net name="A30_13" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A13"/>
<wire x1="203.2" y1="124.46" x2="203.2" y2="114.3" width="0.1524" layer="91"/>
<wire x1="203.2" y1="114.3" x2="204.47" y2="113.03" width="0.1524" layer="91"/>
<label x="203.2" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A13"/>
<wire x1="205.74" y1="198.12" x2="205.74" y2="187.96" width="0.1524" layer="91"/>
<wire x1="205.74" y1="187.96" x2="207.01" y2="186.69" width="0.1524" layer="91"/>
<label x="205.74" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="88.9" x2="337.82" y2="88.9" width="0.1524" layer="91"/>
<label x="337.82" y="88.9" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="11"/>
</segment>
</net>
<net name="A30_14" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A14"/>
<wire x1="198.12" y1="124.46" x2="198.12" y2="114.3" width="0.1524" layer="91"/>
<wire x1="198.12" y1="114.3" x2="199.39" y2="113.03" width="0.1524" layer="91"/>
<label x="198.12" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A14"/>
<wire x1="200.66" y1="198.12" x2="200.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="200.66" y1="187.96" x2="201.93" y2="186.69" width="0.1524" layer="91"/>
<label x="200.66" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="86.36" x2="337.82" y2="86.36" width="0.1524" layer="91"/>
<label x="337.82" y="86.36" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="9"/>
</segment>
</net>
<net name="A30_15" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A15"/>
<wire x1="193.04" y1="124.46" x2="193.04" y2="114.3" width="0.1524" layer="91"/>
<wire x1="193.04" y1="114.3" x2="194.31" y2="113.03" width="0.1524" layer="91"/>
<label x="193.04" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A15"/>
<wire x1="195.58" y1="198.12" x2="195.58" y2="187.96" width="0.1524" layer="91"/>
<wire x1="195.58" y1="187.96" x2="196.85" y2="186.69" width="0.1524" layer="91"/>
<label x="195.58" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="83.82" x2="337.82" y2="83.82" width="0.1524" layer="91"/>
<label x="337.82" y="83.82" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="7"/>
</segment>
</net>
<net name="A30_16" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A16"/>
<wire x1="187.96" y1="124.46" x2="187.96" y2="114.3" width="0.1524" layer="91"/>
<wire x1="187.96" y1="114.3" x2="189.23" y2="113.03" width="0.1524" layer="91"/>
<label x="187.96" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A16"/>
<wire x1="190.5" y1="198.12" x2="190.5" y2="187.96" width="0.1524" layer="91"/>
<wire x1="190.5" y1="187.96" x2="191.77" y2="186.69" width="0.1524" layer="91"/>
<label x="190.5" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="81.28" x2="337.82" y2="81.28" width="0.1524" layer="91"/>
<label x="337.82" y="81.28" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="5"/>
</segment>
</net>
<net name="A30_17" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A17"/>
<wire x1="182.88" y1="124.46" x2="182.88" y2="114.3" width="0.1524" layer="91"/>
<wire x1="182.88" y1="114.3" x2="184.15" y2="113.03" width="0.1524" layer="91"/>
<label x="182.88" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A17"/>
<wire x1="185.42" y1="198.12" x2="185.42" y2="187.96" width="0.1524" layer="91"/>
<wire x1="185.42" y1="187.96" x2="186.69" y2="186.69" width="0.1524" layer="91"/>
<label x="185.42" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="78.74" x2="337.82" y2="78.74" width="0.1524" layer="91"/>
<label x="337.82" y="78.74" size="1.016" layer="95"/>
<pinref part="X5" gate="1" pin="3"/>
</segment>
</net>
<net name="A30_18" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A18"/>
<wire x1="177.8" y1="124.46" x2="177.8" y2="114.3" width="0.1524" layer="91"/>
<wire x1="177.8" y1="114.3" x2="179.07" y2="113.03" width="0.1524" layer="91"/>
<label x="177.8" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A18"/>
<wire x1="180.34" y1="198.12" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="180.34" y1="187.96" x2="181.61" y2="186.69" width="0.1524" layer="91"/>
<label x="180.34" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="111.76" x2="337.82" y2="111.76" width="0.1524" layer="91"/>
<label x="337.82" y="111.76" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="3"/>
</segment>
</net>
<net name="A30_19" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A19"/>
<wire x1="172.72" y1="124.46" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
<wire x1="172.72" y1="114.3" x2="173.99" y2="113.03" width="0.1524" layer="91"/>
<label x="172.72" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A19"/>
<wire x1="175.26" y1="198.12" x2="175.26" y2="187.96" width="0.1524" layer="91"/>
<wire x1="175.26" y1="187.96" x2="176.53" y2="186.69" width="0.1524" layer="91"/>
<label x="175.26" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="114.3" x2="337.82" y2="114.3" width="0.1524" layer="91"/>
<label x="337.82" y="114.3" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="5"/>
</segment>
</net>
<net name="A30_20" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A20"/>
<wire x1="167.64" y1="124.46" x2="167.64" y2="114.3" width="0.1524" layer="91"/>
<wire x1="167.64" y1="114.3" x2="168.91" y2="113.03" width="0.1524" layer="91"/>
<label x="167.64" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A20"/>
<wire x1="170.18" y1="198.12" x2="170.18" y2="187.96" width="0.1524" layer="91"/>
<wire x1="170.18" y1="187.96" x2="171.45" y2="186.69" width="0.1524" layer="91"/>
<label x="170.18" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="116.84" x2="337.82" y2="116.84" width="0.1524" layer="91"/>
<label x="337.82" y="116.84" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="7"/>
</segment>
</net>
<net name="A30_21" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A21"/>
<wire x1="162.56" y1="124.46" x2="162.56" y2="114.3" width="0.1524" layer="91"/>
<wire x1="162.56" y1="114.3" x2="163.83" y2="113.03" width="0.1524" layer="91"/>
<label x="162.56" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A21"/>
<wire x1="165.1" y1="198.12" x2="165.1" y2="187.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="187.96" x2="166.37" y2="186.69" width="0.1524" layer="91"/>
<label x="165.1" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="121.92" x2="337.82" y2="121.92" width="0.1524" layer="91"/>
<label x="337.82" y="121.92" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="11"/>
</segment>
</net>
<net name="A30_22" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A22"/>
<wire x1="157.48" y1="124.46" x2="157.48" y2="114.3" width="0.1524" layer="91"/>
<wire x1="157.48" y1="114.3" x2="158.75" y2="113.03" width="0.1524" layer="91"/>
<label x="157.48" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A22"/>
<wire x1="160.02" y1="198.12" x2="160.02" y2="187.96" width="0.1524" layer="91"/>
<wire x1="160.02" y1="187.96" x2="161.29" y2="186.69" width="0.1524" layer="91"/>
<label x="160.02" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="124.46" x2="337.82" y2="124.46" width="0.1524" layer="91"/>
<label x="337.82" y="124.46" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="13"/>
</segment>
</net>
<net name="A30_23" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="A23"/>
<wire x1="152.4" y1="124.46" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="153.67" y2="113.03" width="0.1524" layer="91"/>
<label x="152.4" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="A23"/>
<wire x1="154.94" y1="198.12" x2="154.94" y2="187.96" width="0.1524" layer="91"/>
<wire x1="154.94" y1="187.96" x2="156.21" y2="186.69" width="0.1524" layer="91"/>
<label x="154.94" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="127" x2="337.82" y2="127" width="0.1524" layer="91"/>
<label x="337.82" y="127" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="15"/>
</segment>
</net>
<net name="DS30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!DS"/>
<wire x1="147.32" y1="124.46" x2="147.32" y2="114.3" width="0.1524" layer="91"/>
<wire x1="147.32" y1="114.3" x2="148.59" y2="113.03" width="0.1524" layer="91"/>
<label x="147.32" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!DS"/>
<wire x1="149.86" y1="198.12" x2="149.86" y2="187.96" width="0.1524" layer="91"/>
<wire x1="149.86" y1="187.96" x2="151.13" y2="186.69" width="0.1524" layer="91"/>
<label x="149.86" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="129.54" x2="337.82" y2="129.54" width="0.1524" layer="91"/>
<label x="337.82" y="129.54" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="17"/>
</segment>
</net>
<net name="SIZ30_0" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="SIZ0"/>
<wire x1="109.22" y1="124.46" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="109.22" y1="114.3" x2="110.49" y2="113.03" width="0.1524" layer="91"/>
<label x="109.22" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="SIZ0"/>
<wire x1="111.76" y1="198.12" x2="111.76" y2="187.96" width="0.1524" layer="91"/>
<wire x1="111.76" y1="187.96" x2="113.03" y2="186.69" width="0.1524" layer="91"/>
<label x="111.76" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="160.02" x2="337.82" y2="160.02" width="0.1524" layer="91"/>
<label x="337.82" y="160.02" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="13"/>
</segment>
</net>
<net name="FC30_2" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="FC2"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<wire x1="104.14" y1="114.3" x2="105.41" y2="113.03" width="0.1524" layer="91"/>
<label x="104.14" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="FC2"/>
<wire x1="106.68" y1="198.12" x2="106.68" y2="187.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="187.96" x2="107.95" y2="186.69" width="0.1524" layer="91"/>
<label x="106.68" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="154.94" x2="337.82" y2="154.94" width="0.1524" layer="91"/>
<label x="337.82" y="154.94" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="9"/>
</segment>
</net>
<net name="SIZ30_1" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="SIZ1"/>
<wire x1="99.06" y1="124.46" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
<wire x1="99.06" y1="114.3" x2="100.33" y2="113.03" width="0.1524" layer="91"/>
<label x="99.06" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="SIZ1"/>
<wire x1="101.6" y1="198.12" x2="101.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="101.6" y1="187.96" x2="102.87" y2="186.69" width="0.1524" layer="91"/>
<label x="101.6" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="152.4" x2="337.82" y2="152.4" width="0.1524" layer="91"/>
<label x="337.82" y="152.4" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="7"/>
</segment>
</net>
<net name="AS30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!AS"/>
<wire x1="93.98" y1="124.46" x2="93.98" y2="114.3" width="0.1524" layer="91"/>
<wire x1="93.98" y1="114.3" x2="95.25" y2="113.03" width="0.1524" layer="91"/>
<label x="93.98" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!AS"/>
<wire x1="96.52" y1="198.12" x2="96.52" y2="187.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="187.96" x2="97.79" y2="186.69" width="0.1524" layer="91"/>
<label x="96.52" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="149.86" x2="337.82" y2="149.86" width="0.1524" layer="91"/>
<label x="337.82" y="149.86" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="5"/>
</segment>
</net>
<net name="D30_30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D30"/>
<wire x1="83.82" y1="124.46" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<wire x1="83.82" y1="114.3" x2="85.09" y2="113.03" width="0.1524" layer="91"/>
<label x="83.82" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D30"/>
<wire x1="86.36" y1="198.12" x2="86.36" y2="187.96" width="0.1524" layer="91"/>
<wire x1="86.36" y1="187.96" x2="87.63" y2="186.69" width="0.1524" layer="91"/>
<label x="86.36" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="182.88" x2="313.69" y2="182.88" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="6"/>
<label x="303.53" y="182.88" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_29" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D29"/>
<wire x1="78.74" y1="124.46" x2="78.74" y2="114.3" width="0.1524" layer="91"/>
<wire x1="78.74" y1="114.3" x2="80.01" y2="113.03" width="0.1524" layer="91"/>
<label x="78.74" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D29"/>
<wire x1="81.28" y1="198.12" x2="81.28" y2="187.96" width="0.1524" layer="91"/>
<wire x1="81.28" y1="187.96" x2="82.55" y2="186.69" width="0.1524" layer="91"/>
<label x="81.28" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="185.42" x2="313.69" y2="185.42" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="8"/>
<label x="303.53" y="185.42" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_28" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D28"/>
<wire x1="73.66" y1="124.46" x2="73.66" y2="114.3" width="0.1524" layer="91"/>
<wire x1="73.66" y1="114.3" x2="74.93" y2="113.03" width="0.1524" layer="91"/>
<label x="73.66" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D28"/>
<wire x1="76.2" y1="198.12" x2="76.2" y2="187.96" width="0.1524" layer="91"/>
<wire x1="76.2" y1="187.96" x2="77.47" y2="186.69" width="0.1524" layer="91"/>
<label x="76.2" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="187.96" x2="313.69" y2="187.96" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="10"/>
<label x="303.53" y="187.96" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_27" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D27"/>
<wire x1="68.58" y1="124.46" x2="68.58" y2="114.3" width="0.1524" layer="91"/>
<wire x1="68.58" y1="114.3" x2="69.85" y2="113.03" width="0.1524" layer="91"/>
<label x="68.58" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D27"/>
<wire x1="71.12" y1="198.12" x2="71.12" y2="187.96" width="0.1524" layer="91"/>
<wire x1="71.12" y1="187.96" x2="72.39" y2="186.69" width="0.1524" layer="91"/>
<label x="71.12" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="190.5" x2="313.69" y2="190.5" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="12"/>
<label x="303.53" y="190.5" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_25" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D25"/>
<wire x1="58.42" y1="124.46" x2="58.42" y2="114.3" width="0.1524" layer="91"/>
<wire x1="58.42" y1="114.3" x2="59.69" y2="113.03" width="0.1524" layer="91"/>
<label x="58.42" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D25"/>
<wire x1="60.96" y1="198.12" x2="60.96" y2="187.96" width="0.1524" layer="91"/>
<wire x1="60.96" y1="187.96" x2="62.23" y2="186.69" width="0.1524" layer="91"/>
<label x="60.96" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="195.58" x2="313.69" y2="195.58" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="16"/>
<label x="303.53" y="195.58" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_24" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D24"/>
<wire x1="53.34" y1="124.46" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="53.34" y1="114.3" x2="54.61" y2="113.03" width="0.1524" layer="91"/>
<label x="53.34" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D24"/>
<wire x1="55.88" y1="198.12" x2="55.88" y2="187.96" width="0.1524" layer="91"/>
<wire x1="55.88" y1="187.96" x2="57.15" y2="186.69" width="0.1524" layer="91"/>
<label x="55.88" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="198.12" x2="313.69" y2="198.12" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="18"/>
<label x="303.53" y="198.12" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_16" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D16"/>
<wire x1="48.26" y1="124.46" x2="48.26" y2="114.3" width="0.1524" layer="91"/>
<wire x1="48.26" y1="114.3" x2="49.53" y2="113.03" width="0.1524" layer="91"/>
<label x="48.26" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D16"/>
<wire x1="50.8" y1="198.12" x2="50.8" y2="187.96" width="0.1524" layer="91"/>
<wire x1="50.8" y1="187.96" x2="52.07" y2="186.69" width="0.1524" layer="91"/>
<label x="50.8" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="231.14" x2="313.69" y2="231.14" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="18"/>
<label x="303.53" y="231.14" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_17" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D17"/>
<wire x1="43.18" y1="124.46" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="43.18" y1="114.3" x2="44.45" y2="113.03" width="0.1524" layer="91"/>
<label x="43.18" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D17"/>
<wire x1="45.72" y1="198.12" x2="45.72" y2="187.96" width="0.1524" layer="91"/>
<wire x1="45.72" y1="187.96" x2="46.99" y2="186.69" width="0.1524" layer="91"/>
<label x="45.72" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="228.6" x2="313.69" y2="228.6" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="16"/>
<label x="303.53" y="228.6" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_18" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D18"/>
<wire x1="38.1" y1="124.46" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<wire x1="38.1" y1="114.3" x2="39.37" y2="113.03" width="0.1524" layer="91"/>
<label x="38.1" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D18"/>
<wire x1="40.64" y1="198.12" x2="40.64" y2="187.96" width="0.1524" layer="91"/>
<wire x1="40.64" y1="187.96" x2="41.91" y2="186.69" width="0.1524" layer="91"/>
<label x="40.64" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="226.06" x2="313.69" y2="226.06" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="14"/>
<label x="303.53" y="226.06" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_19" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D19"/>
<wire x1="33.02" y1="124.46" x2="33.02" y2="114.3" width="0.1524" layer="91"/>
<wire x1="33.02" y1="114.3" x2="34.29" y2="113.03" width="0.1524" layer="91"/>
<label x="33.02" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D19"/>
<wire x1="35.56" y1="198.12" x2="35.56" y2="187.96" width="0.1524" layer="91"/>
<wire x1="35.56" y1="187.96" x2="36.83" y2="186.69" width="0.1524" layer="91"/>
<label x="35.56" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="223.52" x2="313.69" y2="223.52" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="12"/>
<label x="303.53" y="223.52" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_20" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D20"/>
<wire x1="27.94" y1="124.46" x2="27.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="27.94" y1="114.3" x2="29.21" y2="113.03" width="0.1524" layer="91"/>
<label x="27.94" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D20"/>
<wire x1="30.48" y1="198.12" x2="30.48" y2="187.96" width="0.1524" layer="91"/>
<wire x1="30.48" y1="187.96" x2="31.75" y2="186.69" width="0.1524" layer="91"/>
<label x="30.48" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="220.98" x2="313.69" y2="220.98" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="10"/>
<label x="303.53" y="220.98" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_21" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D21"/>
<wire x1="22.86" y1="124.46" x2="22.86" y2="114.3" width="0.1524" layer="91"/>
<wire x1="22.86" y1="114.3" x2="24.13" y2="113.03" width="0.1524" layer="91"/>
<label x="22.86" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D21"/>
<wire x1="25.4" y1="198.12" x2="25.4" y2="187.96" width="0.1524" layer="91"/>
<wire x1="25.4" y1="187.96" x2="26.67" y2="186.69" width="0.1524" layer="91"/>
<label x="25.4" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="218.44" x2="313.69" y2="218.44" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="8"/>
<label x="303.53" y="218.44" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_22" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D22"/>
<wire x1="17.78" y1="124.46" x2="17.78" y2="114.3" width="0.1524" layer="91"/>
<wire x1="17.78" y1="114.3" x2="19.05" y2="113.03" width="0.1524" layer="91"/>
<label x="17.78" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D22"/>
<wire x1="20.32" y1="198.12" x2="20.32" y2="187.96" width="0.1524" layer="91"/>
<wire x1="20.32" y1="187.96" x2="21.59" y2="186.69" width="0.1524" layer="91"/>
<label x="20.32" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="215.9" x2="313.69" y2="215.9" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="6"/>
<label x="303.53" y="215.9" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="D30_23" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D23"/>
<wire x1="12.7" y1="124.46" x2="12.7" y2="114.3" width="0.1524" layer="91"/>
<wire x1="12.7" y1="114.3" x2="13.97" y2="113.03" width="0.1524" layer="91"/>
<label x="12.7" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D23"/>
<wire x1="15.24" y1="198.12" x2="15.24" y2="187.96" width="0.1524" layer="91"/>
<wire x1="15.24" y1="187.96" x2="16.51" y2="186.69" width="0.1524" layer="91"/>
<label x="15.24" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="213.36" x2="313.69" y2="213.36" width="0.1524" layer="91"/>
<pinref part="X1" gate="1" pin="4"/>
<label x="303.53" y="213.36" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="!BOSS" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!BOSS"/>
<wire x1="248.92" y1="124.46" x2="248.92" y2="102.87" width="0.1524" layer="91"/>
<label x="248.92" y="121.92" size="1.27" layer="95" rot="R270"/>
<pinref part="!BOSS" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!BOSS"/>
<wire x1="251.46" y1="198.12" x2="251.46" y2="176.53" width="0.1524" layer="91"/>
<label x="251.46" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
</net>
<net name="!WAIT" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!WAIT"/>
<wire x1="203.2" y1="157.48" x2="203.2" y2="172.72" width="0.1524" layer="91"/>
<label x="203.2" y="172.72" size="1.016" layer="95"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!WAIT"/>
<wire x1="205.74" y1="231.14" x2="205.74" y2="246.38" width="0.1524" layer="91"/>
<label x="205.74" y="246.38" size="1.016" layer="95"/>
</segment>
<segment>
<wire x1="313.69" y1="81.28" x2="304.8" y2="81.28" width="0.1524" layer="91"/>
<label x="304.8" y="81.28" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="6"/>
</segment>
</net>
<net name="D30_31" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D31"/>
<wire x1="88.9" y1="124.46" x2="88.9" y2="114.3" width="0.1524" layer="91"/>
<wire x1="88.9" y1="114.3" x2="90.17" y2="113.03" width="0.1524" layer="91"/>
<label x="88.9" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D31"/>
<wire x1="91.44" y1="198.12" x2="91.44" y2="187.96" width="0.1524" layer="91"/>
<wire x1="91.44" y1="187.96" x2="92.71" y2="186.69" width="0.1524" layer="91"/>
<label x="91.44" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="180.34" x2="313.69" y2="180.34" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="4"/>
<label x="303.53" y="180.34" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RESET30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!CPURST"/>
<wire x1="124.46" y1="124.46" x2="124.46" y2="123.19" width="0.1524" layer="91"/>
<wire x1="124.46" y1="123.19" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
<wire x1="124.46" y1="114.3" x2="125.73" y2="113.03" width="0.1524" layer="91"/>
<label x="124.46" y="115.57" size="1.27" layer="95" rot="R90"/>
<pinref part="!CPURST" gate="G$1" pin="1"/>
<junction x="124.46" y="123.19"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!CPURST"/>
<wire x1="127" y1="198.12" x2="127" y2="187.96" width="0.1524" layer="91"/>
<wire x1="127" y1="187.96" x2="128.27" y2="186.69" width="0.1524" layer="91"/>
<label x="127" y="189.23" size="1.27" layer="95" rot="R90"/>
</segment>
</net>
<net name="!FPUCS" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!FPUCS"/>
<wire x1="91.44" y1="124.46" x2="91.44" y2="123.19" width="0.1524" layer="91"/>
<pinref part="!FPUCS" gate="G$1" pin="1"/>
<wire x1="91.44" y1="123.19" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<junction x="91.44" y="123.19"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!FPUCS"/>
<wire x1="93.98" y1="198.12" x2="93.98" y2="182.88" width="0.1524" layer="91"/>
<label x="93.98" y="191.77" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="!EMUL" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!EMUL"/>
<wire x1="233.68" y1="157.48" x2="233.68" y2="161.29" width="0.1524" layer="91"/>
<label x="233.68" y="172.72" size="1.016" layer="95" rot="R90"/>
<pinref part="!EMUL" gate="G$1" pin="1"/>
<wire x1="233.68" y1="161.29" x2="233.68" y2="175.26" width="0.1524" layer="91"/>
<junction x="233.68" y="161.29"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!EMUL"/>
<wire x1="236.22" y1="231.14" x2="236.22" y2="248.92" width="0.1524" layer="91"/>
<label x="236.22" y="246.38" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="!CBREQ" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!CBREQ"/>
<wire x1="231.14" y1="157.48" x2="231.14" y2="175.26" width="0.1524" layer="91"/>
<label x="231.14" y="172.72" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="60.96" x2="304.8" y2="60.96" width="0.1524" layer="91"/>
<label x="304.8" y="60.96" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="16"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!CBREQ"/>
<wire x1="233.68" y1="231.14" x2="233.68" y2="248.92" width="0.1524" layer="91"/>
<label x="233.68" y="246.38" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="!CBACK" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!CBACK"/>
<wire x1="236.22" y1="157.48" x2="236.22" y2="175.26" width="0.1524" layer="91"/>
<label x="236.22" y="172.72" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="58.42" x2="304.8" y2="58.42" width="0.1524" layer="91"/>
<label x="304.8" y="58.42" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="14"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!CBACK"/>
<wire x1="238.76" y1="231.14" x2="238.76" y2="248.92" width="0.1524" layer="91"/>
<label x="238.76" y="246.38" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="!CIOUT30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!CIOUT"/>
<wire x1="134.62" y1="124.46" x2="134.62" y2="118.11" width="0.1524" layer="91"/>
<pinref part="!CIOUT" gate="G$1" pin="1"/>
<wire x1="134.62" y1="118.11" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<junction x="134.62" y="118.11"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!CIOUT"/>
<wire x1="137.16" y1="198.12" x2="137.16" y2="180.34" width="0.1524" layer="91"/>
<label x="137.16" y="191.77" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="DSAC30_0" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!DSACK0"/>
<wire x1="157.48" y1="157.48" x2="157.48" y2="166.37" width="0.1524" layer="91"/>
<wire x1="157.48" y1="166.37" x2="158.75" y2="167.64" width="0.1524" layer="91"/>
<label x="157.48" y="158.75" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!DSACK0"/>
<wire x1="160.02" y1="231.14" x2="160.02" y2="240.03" width="0.1524" layer="91"/>
<wire x1="160.02" y1="240.03" x2="161.29" y2="241.3" width="0.1524" layer="91"/>
<label x="160.02" y="232.41" size="1.27" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="129.54" x2="304.8" y2="129.54" width="0.1524" layer="91"/>
<label x="304.8" y="129.54" size="1.016" layer="95" rot="R180"/>
<pinref part="X4" gate="1" pin="18"/>
</segment>
</net>
<net name="DSAC30_1" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!DSACK1"/>
<wire x1="269.24" y1="124.46" x2="269.24" y2="114.3" width="0.1524" layer="91"/>
<wire x1="269.24" y1="114.3" x2="270.51" y2="113.03" width="0.1524" layer="91"/>
<label x="269.24" y="123.19" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!DSACK1"/>
<wire x1="271.78" y1="198.12" x2="271.78" y2="187.96" width="0.1524" layer="91"/>
<wire x1="271.78" y1="187.96" x2="273.05" y2="186.69" width="0.1524" layer="91"/>
<label x="271.78" y="196.85" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="328.93" y1="45.72" x2="337.82" y2="45.72" width="0.1524" layer="91"/>
<pinref part="X6" gate="1" pin="3"/>
<label x="337.82" y="45.72" size="1.016" layer="95"/>
</segment>
</net>
<net name="CLK30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="CPUCLKEXP"/>
<wire x1="93.98" y1="157.48" x2="93.98" y2="166.37" width="0.1524" layer="91"/>
<wire x1="93.98" y1="166.37" x2="95.25" y2="167.64" width="0.1524" layer="91"/>
<label x="93.98" y="160.02" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="CPUCLKEXP"/>
<wire x1="96.52" y1="231.14" x2="96.52" y2="240.03" width="0.1524" layer="91"/>
<wire x1="96.52" y1="240.03" x2="97.79" y2="241.3" width="0.1524" layer="91"/>
<label x="96.52" y="233.68" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="147.32" x2="304.8" y2="147.32" width="0.1524" layer="91"/>
<label x="304.8" y="147.32" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="4"/>
</segment>
</net>
<net name="D30_26" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="D26"/>
<wire x1="63.5" y1="124.46" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="64.77" y2="113.03" width="0.1524" layer="91"/>
<label x="63.5" y="121.92" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="D26"/>
<wire x1="66.04" y1="198.12" x2="66.04" y2="187.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="187.96" x2="67.31" y2="186.69" width="0.1524" layer="91"/>
<label x="66.04" y="195.58" size="1.27" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="303.53" y1="193.04" x2="313.69" y2="193.04" width="0.1524" layer="91"/>
<pinref part="X2" gate="1" pin="14"/>
<label x="303.53" y="193.04" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="!AVEC30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!AVEC"/>
<wire x1="256.54" y1="157.48" x2="256.54" y2="172.72" width="0.1524" layer="91"/>
<label x="256.54" y="171.45" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="313.69" y1="50.8" x2="304.8" y2="50.8" width="0.1524" layer="91"/>
<label x="304.8" y="50.8" size="1.016" layer="95" rot="R180"/>
<pinref part="X6" gate="1" pin="8"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!AVEC"/>
<wire x1="259.08" y1="231.14" x2="259.08" y2="246.38" width="0.1524" layer="91"/>
<label x="259.08" y="245.11" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="!RMC30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!RMC"/>
<wire x1="127" y1="157.48" x2="127" y2="166.37" width="0.1524" layer="91"/>
<pinref part="!RMC" gate="G$1" pin="1"/>
<wire x1="127" y1="166.37" x2="127" y2="172.72" width="0.1524" layer="91"/>
<junction x="127" y="166.37"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!RMC"/>
<wire x1="129.54" y1="231.14" x2="129.54" y2="246.38" width="0.1524" layer="91"/>
<label x="129.54" y="233.68" size="1.016" layer="95" rot="R90"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="X1" gate="1" pin="20"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="311.15" y1="233.68" x2="313.69" y2="233.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="2"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="311.15" y1="210.82" x2="313.69" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="331.47" y1="210.82" x2="328.93" y2="210.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="1" pin="19"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="331.47" y1="233.68" x2="328.93" y2="233.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="20"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="311.15" y1="200.66" x2="313.69" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="311.15" y1="177.8" x2="313.69" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="1"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="331.47" y1="177.8" x2="328.93" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="1" pin="19"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="331.47" y1="200.66" x2="328.93" y2="200.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="20"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="311.15" y1="167.64" x2="313.69" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="311.15" y1="144.78" x2="313.69" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="331.47" y1="144.78" x2="328.93" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X3" gate="1" pin="19"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="331.47" y1="167.64" x2="328.93" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="20"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="311.15" y1="132.08" x2="313.69" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="311.15" y1="109.22" x2="313.69" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="331.47" y1="109.22" x2="328.93" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X4" gate="1" pin="19"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="331.47" y1="132.08" x2="328.93" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="311.15" y1="76.2" x2="313.69" y2="76.2" width="0.1524" layer="91"/>
<pinref part="X5" gate="1" pin="2"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="331.47" y1="76.2" x2="328.93" y2="76.2" width="0.1524" layer="91"/>
<pinref part="X5" gate="1" pin="1"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="331.47" y1="99.06" x2="328.93" y2="99.06" width="0.1524" layer="91"/>
<pinref part="X5" gate="1" pin="19"/>
</segment>
<segment>
<pinref part="X6" gate="1" pin="20"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="311.15" y1="66.04" x2="313.69" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X6" gate="1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="311.15" y1="43.18" x2="313.69" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X6" gate="1" pin="1"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="331.47" y1="43.18" x2="328.93" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X6" gate="1" pin="19"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="331.47" y1="66.04" x2="328.93" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="311.15" y1="99.06" x2="313.69" y2="99.06" width="0.1524" layer="91"/>
<pinref part="X5" gate="1" pin="20"/>
</segment>
<segment>
<pinref part="MAINBOARD" gate="G$2" pin="GND1"/>
<pinref part="EXPANSION" gate="G$2" pin="GND1"/>
<wire x1="66.04" y1="68.58" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="68.58" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
<junction x="86.36" y="68.58"/>
<label x="99.06" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="!RAMSLOT" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!RAMSLOT"/>
<wire x1="254" y1="231.14" x2="254" y2="236.22" width="0.1524" layer="91"/>
<label x="254" y="236.22" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="251.46" y1="157.48" x2="251.46" y2="158.75" width="0.1524" layer="91"/>
<label x="251.46" y="162.56" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!RAMSLOT"/>
<pinref part="!RAM" gate="G$1" pin="1"/>
<wire x1="251.46" y1="158.75" x2="251.46" y2="162.56" width="0.1524" layer="91"/>
<junction x="251.46" y="158.75"/>
</segment>
</net>
<net name="!SBR" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!SBR"/>
<wire x1="259.08" y1="124.46" x2="259.08" y2="121.92" width="0.1524" layer="91"/>
<label x="259.08" y="120.65" size="1.016" layer="95" rot="R270"/>
<pinref part="!SBR" gate="G$1" pin="1"/>
<wire x1="259.08" y1="121.92" x2="259.08" y2="120.65" width="0.1524" layer="91"/>
<junction x="259.08" y="121.92"/>
</segment>
<segment>
<wire x1="261.62" y1="198.12" x2="261.62" y2="194.31" width="0.1524" layer="91"/>
<label x="261.62" y="194.31" size="1.016" layer="95" rot="R270"/>
<pinref part="MAINBOARD" gate="G$1" pin="!SBR"/>
</segment>
</net>
<net name="EXT90" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="EXT90"/>
<wire x1="254" y1="124.46" x2="254" y2="120.65" width="0.1524" layer="91"/>
<label x="254" y="120.65" size="1.016" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="256.54" y1="198.12" x2="256.54" y2="194.31" width="0.1524" layer="91"/>
<label x="256.54" y="194.31" size="1.016" layer="95" rot="R270"/>
<pinref part="MAINBOARD" gate="G$1" pin="EXT90"/>
</segment>
<segment>
<wire x1="328.93" y1="50.8" x2="337.82" y2="50.8" width="0.1524" layer="91"/>
<label x="337.82" y="50.8" size="1.016" layer="95"/>
<pinref part="X6" gate="1" pin="7"/>
</segment>
</net>
<net name="!SCSI" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!SCSI"/>
<wire x1="185.42" y1="124.46" x2="185.42" y2="123.19" width="0.1524" layer="91"/>
<label x="185.42" y="120.65" size="1.016" layer="95" rot="R270"/>
<pinref part="!SCSI" gate="G$1" pin="1"/>
<wire x1="185.42" y1="123.19" x2="185.42" y2="120.65" width="0.1524" layer="91"/>
<junction x="185.42" y="123.19"/>
</segment>
<segment>
<wire x1="187.96" y1="198.12" x2="187.96" y2="194.31" width="0.1524" layer="91"/>
<label x="187.96" y="194.31" size="1.016" layer="95" rot="R270"/>
<pinref part="MAINBOARD" gate="G$1" pin="!SCSI"/>
</segment>
</net>
<net name="!INT2" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!INT2"/>
<wire x1="167.64" y1="198.12" x2="167.64" y2="194.31" width="0.1524" layer="91"/>
<label x="167.64" y="194.31" size="1.016" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="165.1" y1="124.46" x2="165.1" y2="120.65" width="0.1524" layer="91"/>
<label x="165.1" y="120.65" size="1.016" layer="95" rot="R270"/>
<pinref part="EXPANSION" gate="G$1" pin="!INT2"/>
</segment>
<segment>
<wire x1="328.93" y1="119.38" x2="337.82" y2="119.38" width="0.1524" layer="91"/>
<label x="337.82" y="119.38" size="1.016" layer="95"/>
<pinref part="X4" gate="1" pin="9"/>
</segment>
</net>
<net name="!INT6" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!INT6"/>
<wire x1="220.98" y1="231.14" x2="220.98" y2="234.95" width="0.1524" layer="91"/>
<label x="220.98" y="234.95" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="218.44" y1="157.48" x2="218.44" y2="161.29" width="0.1524" layer="91"/>
<label x="218.44" y="161.29" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!INT6"/>
</segment>
<segment>
<wire x1="313.69" y1="91.44" x2="304.8" y2="91.44" width="0.1524" layer="91"/>
<label x="304.8" y="91.44" size="1.016" layer="95" rot="R180"/>
<pinref part="X5" gate="1" pin="14"/>
</segment>
</net>
<net name="!DMAEN" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!DMAEN"/>
<wire x1="187.96" y1="157.48" x2="187.96" y2="160.02" width="0.1524" layer="91"/>
<label x="187.96" y="161.29" size="1.016" layer="95" rot="R90"/>
<pinref part="!DMAEN" gate="G$1" pin="1"/>
<wire x1="187.96" y1="160.02" x2="187.96" y2="161.29" width="0.1524" layer="91"/>
<junction x="187.96" y="160.02"/>
</segment>
<segment>
<wire x1="190.5" y1="231.14" x2="190.5" y2="234.95" width="0.1524" layer="91"/>
<label x="190.5" y="234.95" size="1.016" layer="95" rot="R90"/>
<pinref part="MAINBOARD" gate="G$1" pin="!DMAEN"/>
</segment>
</net>
<net name="!ECS" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!ECS"/>
<wire x1="147.32" y1="231.14" x2="147.32" y2="234.95" width="0.1524" layer="91"/>
<label x="147.32" y="234.95" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="144.78" y1="157.48" x2="144.78" y2="161.29" width="0.1524" layer="91"/>
<label x="144.78" y="161.29" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!ECS"/>
</segment>
<segment>
<wire x1="313.69" y1="162.56" x2="304.8" y2="162.56" width="0.1524" layer="91"/>
<label x="304.8" y="162.56" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="16"/>
</segment>
</net>
<net name="!DBEN" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!DBEN"/>
<wire x1="132.08" y1="157.48" x2="132.08" y2="158.75" width="0.1524" layer="91"/>
<label x="132.08" y="162.56" size="1.016" layer="95" rot="R90"/>
<pinref part="!DBEN" gate="G$1" pin="1"/>
<wire x1="132.08" y1="158.75" x2="132.08" y2="162.56" width="0.1524" layer="91"/>
<junction x="132.08" y="158.75"/>
</segment>
<segment>
<wire x1="134.62" y1="231.14" x2="134.62" y2="236.22" width="0.1524" layer="91"/>
<label x="134.62" y="236.22" size="1.016" layer="95" rot="R90"/>
<pinref part="MAINBOARD" gate="G$1" pin="!DBEN"/>
</segment>
</net>
<net name="EXTCPU" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="EXTCPU"/>
<wire x1="124.46" y1="231.14" x2="124.46" y2="234.95" width="0.1524" layer="91"/>
<label x="124.46" y="234.95" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="121.92" y1="157.48" x2="121.92" y2="161.29" width="0.1524" layer="91"/>
<label x="121.92" y="161.29" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="EXTCPU"/>
</segment>
<segment>
<wire x1="313.69" y1="160.02" x2="304.8" y2="160.02" width="0.1524" layer="91"/>
<label x="304.8" y="160.02" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="14"/>
</segment>
</net>
<net name="IPEND" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="IPEND"/>
<wire x1="116.84" y1="157.48" x2="116.84" y2="158.75" width="0.1524" layer="91"/>
<label x="116.84" y="161.29" size="1.016" layer="95" rot="R90"/>
<pinref part="!IPEND" gate="G$1" pin="1"/>
<wire x1="116.84" y1="158.75" x2="116.84" y2="161.29" width="0.1524" layer="91"/>
<junction x="116.84" y="158.75"/>
</segment>
<segment>
<wire x1="119.38" y1="231.14" x2="119.38" y2="234.95" width="0.1524" layer="91"/>
<label x="119.38" y="234.95" size="1.016" layer="95" rot="R90"/>
<pinref part="MAINBOARD" gate="G$1" pin="IPEND"/>
</segment>
</net>
<net name="!OCS" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!OCS"/>
<wire x1="93.98" y1="231.14" x2="93.98" y2="236.22" width="0.1524" layer="91"/>
<label x="93.98" y="233.68" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="91.44" y1="157.48" x2="91.44" y2="158.75" width="0.1524" layer="91"/>
<label x="91.44" y="160.02" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!OCS"/>
<pinref part="!OCS" gate="G$1" pin="1"/>
<wire x1="91.44" y1="158.75" x2="91.44" y2="162.56" width="0.1524" layer="91"/>
<junction x="91.44" y="158.75"/>
</segment>
</net>
<net name="!CIIN" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!CIIN"/>
<wire x1="96.52" y1="157.48" x2="96.52" y2="162.56" width="0.1524" layer="91"/>
<label x="96.52" y="160.02" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="99.06" y1="231.14" x2="99.06" y2="236.22" width="0.1524" layer="91"/>
<label x="99.06" y="233.68" size="1.016" layer="95" rot="R90"/>
<pinref part="MAINBOARD" gate="G$1" pin="!CIIN"/>
</segment>
<segment>
<wire x1="313.69" y1="149.86" x2="304.8" y2="149.86" width="0.1524" layer="91"/>
<label x="304.8" y="149.86" size="1.016" layer="95" rot="R180"/>
<pinref part="X3" gate="1" pin="6"/>
</segment>
</net>
<net name="!BG30" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!BG30"/>
<wire x1="63.5" y1="157.48" x2="63.5" y2="162.56" width="0.1524" layer="91"/>
<label x="63.5" y="160.02" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="66.04" y1="231.14" x2="66.04" y2="236.22" width="0.1524" layer="91"/>
<label x="66.04" y="233.68" size="1.016" layer="95" rot="R90"/>
<pinref part="MAINBOARD" gate="G$1" pin="!BG30"/>
</segment>
<segment>
<wire x1="337.82" y1="147.32" x2="328.93" y2="147.32" width="0.1524" layer="91"/>
<label x="337.82" y="147.32" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="3"/>
</segment>
</net>
<net name="CLK90EXP" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="CLK90EXP"/>
<wire x1="104.14" y1="198.12" x2="104.14" y2="193.04" width="0.1524" layer="91"/>
<label x="104.14" y="193.04" size="1.016" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="101.6" y1="124.46" x2="101.6" y2="119.38" width="0.1524" layer="91"/>
<label x="101.6" y="119.38" size="1.016" layer="95" rot="R270"/>
<pinref part="EXPANSION" gate="G$1" pin="CLK90EXP"/>
</segment>
<segment>
<wire x1="328.93" y1="157.48" x2="337.82" y2="157.48" width="0.1524" layer="91"/>
<label x="337.82" y="157.48" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="11"/>
</segment>
</net>
<net name="!RESET" class="0">
<segment>
<pinref part="EXPANSION" gate="G$1" pin="!RESET"/>
<wire x1="114.3" y1="124.46" x2="114.3" y2="119.38" width="0.1524" layer="91"/>
<label x="114.3" y="121.92" size="1.016" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="116.84" y1="198.12" x2="116.84" y2="193.04" width="0.1524" layer="91"/>
<label x="116.84" y="194.31" size="1.016" layer="95" rot="R270"/>
<pinref part="MAINBOARD" gate="G$1" pin="!RESET"/>
</segment>
<segment>
<wire x1="328.93" y1="162.56" x2="337.82" y2="162.56" width="0.1524" layer="91"/>
<label x="337.82" y="162.56" size="1.016" layer="95"/>
<pinref part="X3" gate="1" pin="15"/>
</segment>
</net>
<net name="!EBCLR" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!EBCLR"/>
<wire x1="121.92" y1="198.12" x2="121.92" y2="193.04" width="0.1524" layer="91"/>
<label x="121.92" y="193.04" size="1.016" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="119.38" y1="124.46" x2="119.38" y2="119.38" width="0.1524" layer="91"/>
<label x="119.38" y="120.65" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!EBCLR"/>
<pinref part="!EBCLR" gate="G$1" pin="1"/>
</segment>
</net>
<net name="!FPURST" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!FPURST"/>
<wire x1="124.46" y1="198.12" x2="124.46" y2="193.04" width="0.1524" layer="91"/>
<label x="124.46" y="193.04" size="1.016" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="121.92" y1="124.46" x2="121.92" y2="119.38" width="0.1524" layer="91"/>
<label x="121.92" y="120.65" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!FPURST"/>
<pinref part="!FPURST" gate="G$1" pin="1"/>
</segment>
</net>
<net name="!CBR" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$1" pin="!CBR"/>
<wire x1="81.28" y1="231.14" x2="81.28" y2="234.95" width="0.1524" layer="91"/>
<label x="81.28" y="233.68" size="1.016" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="78.74" y1="157.48" x2="78.74" y2="158.75" width="0.1524" layer="91"/>
<label x="78.74" y="160.02" size="1.016" layer="95" rot="R90"/>
<pinref part="EXPANSION" gate="G$1" pin="!CBR"/>
<pinref part="!CBR" gate="G$1" pin="1"/>
<wire x1="78.74" y1="158.75" x2="78.74" y2="161.29" width="0.1524" layer="91"/>
<junction x="78.74" y="158.75"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="MAINBOARD" gate="G$2" pin="VCC1"/>
<pinref part="EXPANSION" gate="G$2" pin="VCC1"/>
<wire x1="66.04" y1="83.82" x2="86.36" y2="83.82" width="0.1524" layer="91"/>
<wire x1="86.36" y1="83.82" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<junction x="86.36" y="83.82"/>
<label x="99.06" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
