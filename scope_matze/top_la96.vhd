----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:12:50 10/15/2017 
-- Design Name: 
-- Module Name:    top_la96 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_la96 is
    Port ( x1 : in  STD_LOGIC_VECTOR (15 downto 0);
           x2 : in  STD_LOGIC_VECTOR (15 downto 0);
           x3 : in  STD_LOGIC_VECTOR (15 downto 0);
           x4 : in  STD_LOGIC_VECTOR (15 downto 0);
           x5 : in  STD_LOGIC_VECTOR (15 downto 0);
           x6 : in  STD_LOGIC_VECTOR (15 downto 0);
           --output : out  STD_LOGIC;
			  clk: in  STD_LOGIC
			  );
end top_la96;


architecture Behavioral of top_la96 is

COMPONENT clock_multi
PORT(
   CLKIN_IN : IN STD_LOGIC; 
	CLKFX_OUT : OUT STD_LOGIC;
	CLKIN_IBUFG_OUT : OUT STD_LOGIC;
	CLK0_OUT : OUT STD_LOGIC
	);
END COMPONENT;

signal x1_a: STD_LOGIC_VECTOR (15 downto 0);
signal x2_a: STD_LOGIC_VECTOR (15 downto 0);
signal x3_a: STD_LOGIC_VECTOR (15 downto 0);
signal x4_a: STD_LOGIC_VECTOR (15 downto 0);
signal x5_a: STD_LOGIC_VECTOR (15 downto 0);
signal x6_a: STD_LOGIC_VECTOR (15 downto 0);

signal x1_b: STD_LOGIC_VECTOR (15 downto 0);
signal x1_o: STD_LOGIC_VECTOR (15 downto 0);
signal x2_b: STD_LOGIC_VECTOR (15 downto 0);
signal x2_o: STD_LOGIC_VECTOR (15 downto 0);
signal x3_b: STD_LOGIC_VECTOR (15 downto 0);
signal x3_o: STD_LOGIC_VECTOR (15 downto 0);
signal x4_b: STD_LOGIC_VECTOR (15 downto 0);
signal x4_o: STD_LOGIC_VECTOR (15 downto 0);
signal x5_b: STD_LOGIC_VECTOR (15 downto 0);
signal x5_o: STD_LOGIC_VECTOR (15 downto 0);
signal x6_b: STD_LOGIC_VECTOR (15 downto 0);
signal x6_o: STD_LOGIC_VECTOR (15 downto 0);

signal clk_buff: STD_LOGIC;

signal clk_6: STD_LOGIC;
--signal clk_6_HALF: STD_LOGIC;

attribute keep : string;
attribute keep of x1_o : signal is "true";
attribute keep of x2_o : signal is "true";
attribute keep of x3_o : signal is "true";
attribute keep of x4_o : signal is "true";
attribute keep of x5_o : signal is "true";
attribute keep of x6_o : signal is "true";

begin

	Inst_clock_multi: clock_multi PORT MAP(
    CLKIN_IN => clk, 
    CLKFX_OUT => clk_6, 
    CLKIN_IBUFG_OUT =>clk_buff, 
    CLK0_OUT => open
    );

sample_data:process (clk_6) 
begin 
	if(rising_edge(clk_6))then

		
		x1_o <= x1;
		x2_o <= x2;
		x3_o <= x3;
		x4_o <= x4;
		x5_o <= x5;
		x6_o <= x6;
		

--		x1_a <= x1;
--		x1_b <= x1_a;
--		if(x1_a=x1_b)then
--			x1_o <= x1_b;
--			
--		end if;
--		
--		x2_a <= x2;
--		x2_b <= x2_a;
--		if(x2_a=x2_b)then
--			x2_o <= x2_b;
--			
--		end if;
--		
--		x3_a <= x3;
--		x3_b <= x3_a;
--		if(x3_a=x3_b)then
--			x3_o <= x3_b;
--			
--		end if;
--		
--		x4_a <= x4;
--		x4_b <= x4_a;
--		if(x4_a=x4_b)then
--			x4_o <= x4_b;
--			
--		end if;
--		
--		x5_a <= x5;
--		x5_b <= x5_a;
--		if(x5_a=x5_b)then
--			x5_o <= x5_b;
--			
--		end if;
--
--		x6_a <= x6;
--		x6_b <= x6_a;
--		if(x6_a=x6_b)then
--			x6_o <= x6_b;
--			
--		end if;
		

	end if;

end process;


end Behavioral;

