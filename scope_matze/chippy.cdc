#ChipScope Core Inserter Project File Version 3.0
#Sat Mar 19 21:49:50 CET 2022
Project.device.designInputFile=C\:\\Users\\Matze\\Amiga\\Hardwarehacks\\ChipScope\\scope_matze\\LA_NEU\\top_la96_cs.ngc
Project.device.designOutputFile=C\:\\Users\\Matze\\Amiga\\Hardwarehacks\\ChipScope\\scope_matze\\LA_NEU\\top_la96_cs.ngc
Project.device.deviceFamily=6
Project.device.enableRPMs=true
Project.device.outputDirectory=C\:\\Users\\Matze\\Amiga\\Hardwarehacks\\ChipScope\\scope_matze\\LA_NEU\\_ngo
Project.device.useSRL16=true
Project.filter.dimension=19
Project.filter<0>=
Project.filter<10>=ch?_in_t0*
Project.filter<11>=ch/([0-9])_in_t0*
Project.filter<12>=ch/([0-9])_in_t0
Project.filter<13>=ch/[?]_in_to
Project.filter<14>=ch[?]_in_to
Project.filter<15>=ch[.]_in_to
Project.filter<16>=ch._in_to
Project.filter<17>=ch.
Project.filter<18>=ch*_in_t0
Project.filter<1>=x?_o*
Project.filter<2>=x6_o*
Project.filter<3>=x6_o
Project.filter<4>=x?_o
Project.filter<5>=ch?_in_t
Project.filter<6>=ch*_in_t
Project.filter<7>=ch*_in_t*
Project.filter<8>=ch*_in_to
Project.filter<9>=ch?_in_to
Project.icon.boundaryScanChain=1
Project.icon.enableExtTriggerIn=false
Project.icon.enableExtTriggerOut=false
Project.icon.triggerInPinName=
Project.icon.triggerOutPinName=
Project.unit.dimension=1
Project.unit<0>.clockChannel=clk_6
Project.unit<0>.clockEdge=Rising
Project.unit<0>.dataChannel<0>=x1_o<0>
Project.unit<0>.dataChannel<10>=x1_o<10>
Project.unit<0>.dataChannel<11>=x1_o<11>
Project.unit<0>.dataChannel<12>=x1_o<12>
Project.unit<0>.dataChannel<13>=x1_o<13>
Project.unit<0>.dataChannel<14>=x1_o<14>
Project.unit<0>.dataChannel<15>=x1_o<15>
Project.unit<0>.dataChannel<16>=x2_o<0>
Project.unit<0>.dataChannel<17>=x2_o<1>
Project.unit<0>.dataChannel<18>=x2_o<2>
Project.unit<0>.dataChannel<19>=x2_o<3>
Project.unit<0>.dataChannel<1>=x1_o<1>
Project.unit<0>.dataChannel<20>=x2_o<4>
Project.unit<0>.dataChannel<21>=x2_o<5>
Project.unit<0>.dataChannel<22>=x2_o<6>
Project.unit<0>.dataChannel<23>=x2_o<7>
Project.unit<0>.dataChannel<24>=x2_o<8>
Project.unit<0>.dataChannel<25>=x2_o<9>
Project.unit<0>.dataChannel<26>=x2_o<10>
Project.unit<0>.dataChannel<27>=x2_o<11>
Project.unit<0>.dataChannel<28>=x2_o<12>
Project.unit<0>.dataChannel<29>=x2_o<13>
Project.unit<0>.dataChannel<2>=x1_o<2>
Project.unit<0>.dataChannel<30>=x2_o<14>
Project.unit<0>.dataChannel<31>=x2_o<15>
Project.unit<0>.dataChannel<32>=x3_o<0>
Project.unit<0>.dataChannel<33>=x3_o<1>
Project.unit<0>.dataChannel<34>=x3_o<2>
Project.unit<0>.dataChannel<35>=x3_o<3>
Project.unit<0>.dataChannel<36>=x3_o<4>
Project.unit<0>.dataChannel<37>=x3_o<5>
Project.unit<0>.dataChannel<38>=x3_o<6>
Project.unit<0>.dataChannel<39>=x3_o<7>
Project.unit<0>.dataChannel<3>=x1_o<3>
Project.unit<0>.dataChannel<40>=x3_o<8>
Project.unit<0>.dataChannel<41>=x3_o<9>
Project.unit<0>.dataChannel<42>=x3_o<10>
Project.unit<0>.dataChannel<43>=x3_o<11>
Project.unit<0>.dataChannel<44>=x3_o<12>
Project.unit<0>.dataChannel<45>=x3_o<13>
Project.unit<0>.dataChannel<46>=x3_o<14>
Project.unit<0>.dataChannel<47>=x3_o<15>
Project.unit<0>.dataChannel<48>=x4_o<0>
Project.unit<0>.dataChannel<49>=x4_o<1>
Project.unit<0>.dataChannel<4>=x1_o<4>
Project.unit<0>.dataChannel<50>=x4_o<2>
Project.unit<0>.dataChannel<51>=x4_o<3>
Project.unit<0>.dataChannel<52>=x4_o<4>
Project.unit<0>.dataChannel<53>=x4_o<5>
Project.unit<0>.dataChannel<54>=x4_o<6>
Project.unit<0>.dataChannel<55>=x4_o<7>
Project.unit<0>.dataChannel<56>=x4_o<8>
Project.unit<0>.dataChannel<57>=x4_o<9>
Project.unit<0>.dataChannel<58>=x4_o<10>
Project.unit<0>.dataChannel<59>=x4_o<11>
Project.unit<0>.dataChannel<5>=x1_o<5>
Project.unit<0>.dataChannel<60>=x4_o<12>
Project.unit<0>.dataChannel<61>=x4_o<13>
Project.unit<0>.dataChannel<62>=x4_o<14>
Project.unit<0>.dataChannel<63>=x4_o<15>
Project.unit<0>.dataChannel<64>=x5_o<0>
Project.unit<0>.dataChannel<65>=x5_o<1>
Project.unit<0>.dataChannel<66>=x5_o<2>
Project.unit<0>.dataChannel<67>=x5_o<3>
Project.unit<0>.dataChannel<68>=x5_o<4>
Project.unit<0>.dataChannel<69>=x5_o<5>
Project.unit<0>.dataChannel<6>=x1_o<6>
Project.unit<0>.dataChannel<70>=x5_o<6>
Project.unit<0>.dataChannel<71>=x5_o<7>
Project.unit<0>.dataChannel<72>=x5_o<8>
Project.unit<0>.dataChannel<73>=x5_o<9>
Project.unit<0>.dataChannel<74>=x5_o<10>
Project.unit<0>.dataChannel<75>=x5_o<11>
Project.unit<0>.dataChannel<76>=x5_o<12>
Project.unit<0>.dataChannel<77>=x5_o<13>
Project.unit<0>.dataChannel<78>=x5_o<14>
Project.unit<0>.dataChannel<79>=x5_o<15>
Project.unit<0>.dataChannel<7>=x1_o<7>
Project.unit<0>.dataChannel<80>=x6_o<0>
Project.unit<0>.dataChannel<81>=x6_o<1>
Project.unit<0>.dataChannel<82>=x6_o<2>
Project.unit<0>.dataChannel<83>=x6_o<3>
Project.unit<0>.dataChannel<84>=x6_o<4>
Project.unit<0>.dataChannel<85>=x6_o<5>
Project.unit<0>.dataChannel<86>=x6_o<6>
Project.unit<0>.dataChannel<87>=x6_o<7>
Project.unit<0>.dataChannel<88>=x6_o<8>
Project.unit<0>.dataChannel<89>=x6_o<9>
Project.unit<0>.dataChannel<8>=x1_o<8>
Project.unit<0>.dataChannel<90>=x6_o<10>
Project.unit<0>.dataChannel<91>=x6_o<11>
Project.unit<0>.dataChannel<92>=x6_o<12>
Project.unit<0>.dataChannel<93>=x6_o<13>
Project.unit<0>.dataChannel<94>=x6_o<14>
Project.unit<0>.dataChannel<95>=x6_o<15>
Project.unit<0>.dataChannel<9>=x1_o<9>
Project.unit<0>.dataDepth=2048
Project.unit<0>.dataEqualsTrigger=true
Project.unit<0>.dataPortWidth=96
Project.unit<0>.enableGaps=false
Project.unit<0>.enableStorageQualification=true
Project.unit<0>.enableTimestamps=false
Project.unit<0>.timestampDepth=0
Project.unit<0>.timestampWidth=0
Project.unit<0>.triggerChannel<0><0>=x1_o<0>
Project.unit<0>.triggerChannel<0><10>=x1_o<10>
Project.unit<0>.triggerChannel<0><11>=x1_o<11>
Project.unit<0>.triggerChannel<0><12>=x1_o<12>
Project.unit<0>.triggerChannel<0><13>=x1_o<13>
Project.unit<0>.triggerChannel<0><14>=x1_o<14>
Project.unit<0>.triggerChannel<0><15>=x1_o<15>
Project.unit<0>.triggerChannel<0><16>=x2_o<0>
Project.unit<0>.triggerChannel<0><17>=x2_o<1>
Project.unit<0>.triggerChannel<0><18>=x2_o<2>
Project.unit<0>.triggerChannel<0><19>=x2_o<3>
Project.unit<0>.triggerChannel<0><1>=x1_o<1>
Project.unit<0>.triggerChannel<0><20>=x2_o<4>
Project.unit<0>.triggerChannel<0><21>=x2_o<5>
Project.unit<0>.triggerChannel<0><22>=x2_o<6>
Project.unit<0>.triggerChannel<0><23>=x2_o<7>
Project.unit<0>.triggerChannel<0><24>=x2_o<8>
Project.unit<0>.triggerChannel<0><25>=x2_o<9>
Project.unit<0>.triggerChannel<0><26>=x2_o<10>
Project.unit<0>.triggerChannel<0><27>=x2_o<11>
Project.unit<0>.triggerChannel<0><28>=x2_o<12>
Project.unit<0>.triggerChannel<0><29>=x2_o<13>
Project.unit<0>.triggerChannel<0><2>=x1_o<2>
Project.unit<0>.triggerChannel<0><30>=x2_o<14>
Project.unit<0>.triggerChannel<0><31>=x2_o<15>
Project.unit<0>.triggerChannel<0><32>=x3_o<0>
Project.unit<0>.triggerChannel<0><33>=x3_o<1>
Project.unit<0>.triggerChannel<0><34>=x3_o<2>
Project.unit<0>.triggerChannel<0><35>=x3_o<3>
Project.unit<0>.triggerChannel<0><36>=x3_o<4>
Project.unit<0>.triggerChannel<0><37>=x3_o<5>
Project.unit<0>.triggerChannel<0><38>=x3_o<6>
Project.unit<0>.triggerChannel<0><39>=x3_o<7>
Project.unit<0>.triggerChannel<0><3>=x1_o<3>
Project.unit<0>.triggerChannel<0><40>=x3_o<8>
Project.unit<0>.triggerChannel<0><41>=x3_o<9>
Project.unit<0>.triggerChannel<0><42>=x3_o<10>
Project.unit<0>.triggerChannel<0><43>=x3_o<11>
Project.unit<0>.triggerChannel<0><44>=x3_o<12>
Project.unit<0>.triggerChannel<0><45>=x3_o<13>
Project.unit<0>.triggerChannel<0><46>=x3_o<14>
Project.unit<0>.triggerChannel<0><47>=x3_o<15>
Project.unit<0>.triggerChannel<0><48>=x4_o<0>
Project.unit<0>.triggerChannel<0><49>=x4_o<1>
Project.unit<0>.triggerChannel<0><4>=x1_o<4>
Project.unit<0>.triggerChannel<0><50>=x4_o<2>
Project.unit<0>.triggerChannel<0><51>=x4_o<3>
Project.unit<0>.triggerChannel<0><52>=x4_o<4>
Project.unit<0>.triggerChannel<0><53>=x4_o<5>
Project.unit<0>.triggerChannel<0><54>=x4_o<6>
Project.unit<0>.triggerChannel<0><55>=x4_o<7>
Project.unit<0>.triggerChannel<0><56>=x4_o<8>
Project.unit<0>.triggerChannel<0><57>=x4_o<9>
Project.unit<0>.triggerChannel<0><58>=x4_o<10>
Project.unit<0>.triggerChannel<0><59>=x4_o<11>
Project.unit<0>.triggerChannel<0><5>=x1_o<5>
Project.unit<0>.triggerChannel<0><60>=x4_o<12>
Project.unit<0>.triggerChannel<0><61>=x4_o<13>
Project.unit<0>.triggerChannel<0><62>=x4_o<14>
Project.unit<0>.triggerChannel<0><63>=x4_o<15>
Project.unit<0>.triggerChannel<0><64>=x5_o<0>
Project.unit<0>.triggerChannel<0><65>=x5_o<1>
Project.unit<0>.triggerChannel<0><66>=x5_o<2>
Project.unit<0>.triggerChannel<0><67>=x5_o<3>
Project.unit<0>.triggerChannel<0><68>=x5_o<4>
Project.unit<0>.triggerChannel<0><69>=x5_o<5>
Project.unit<0>.triggerChannel<0><6>=x1_o<6>
Project.unit<0>.triggerChannel<0><70>=x5_o<6>
Project.unit<0>.triggerChannel<0><71>=x5_o<7>
Project.unit<0>.triggerChannel<0><72>=x5_o<8>
Project.unit<0>.triggerChannel<0><73>=x5_o<9>
Project.unit<0>.triggerChannel<0><74>=x5_o<10>
Project.unit<0>.triggerChannel<0><75>=x5_o<11>
Project.unit<0>.triggerChannel<0><76>=x5_o<12>
Project.unit<0>.triggerChannel<0><77>=x5_o<13>
Project.unit<0>.triggerChannel<0><78>=x5_o<14>
Project.unit<0>.triggerChannel<0><79>=x5_o<15>
Project.unit<0>.triggerChannel<0><7>=x1_o<7>
Project.unit<0>.triggerChannel<0><80>=x6_o<0>
Project.unit<0>.triggerChannel<0><81>=x6_o<1>
Project.unit<0>.triggerChannel<0><82>=x6_o<2>
Project.unit<0>.triggerChannel<0><83>=x6_o<3>
Project.unit<0>.triggerChannel<0><84>=x6_o<4>
Project.unit<0>.triggerChannel<0><85>=x6_o<5>
Project.unit<0>.triggerChannel<0><86>=x6_o<6>
Project.unit<0>.triggerChannel<0><87>=x6_o<7>
Project.unit<0>.triggerChannel<0><88>=x6_o<8>
Project.unit<0>.triggerChannel<0><89>=x6_o<9>
Project.unit<0>.triggerChannel<0><8>=x1_o<8>
Project.unit<0>.triggerChannel<0><90>=x6_o<10>
Project.unit<0>.triggerChannel<0><91>=x6_o<11>
Project.unit<0>.triggerChannel<0><92>=x6_o<12>
Project.unit<0>.triggerChannel<0><93>=x6_o<13>
Project.unit<0>.triggerChannel<0><94>=x6_o<14>
Project.unit<0>.triggerChannel<0><95>=x6_o<15>
Project.unit<0>.triggerChannel<0><9>=x1_o<9>
Project.unit<0>.triggerConditionCountWidth=0
Project.unit<0>.triggerMatchCount<0>=1
Project.unit<0>.triggerMatchCountWidth<0><0>=0
Project.unit<0>.triggerMatchType<0><0>=0
Project.unit<0>.triggerPortCount=1
Project.unit<0>.triggerPortIsData<0>=true
Project.unit<0>.triggerPortWidth<0>=96
Project.unit<0>.triggerSequencerLevels=16
Project.unit<0>.triggerSequencerType=1
Project.unit<0>.type=ilapro
